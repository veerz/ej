$(document).ready(function(){
	var base_url = window.location.origin + "/EJBaru";

	$("#managerType ").change(function() {
			var data = {"jenismanage" : $("#managerType option:selected").attr("value"),
			"region" : $("#region option:selected").attr("value")
			};

		$("#managedBy").empty();
		$.ajax({
			url : base_url+"/StatMach1/getmanageby",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
	//							$("#menu").val ( data.msg);
					$("#managedBy").append(data.msg);
					$('.select2').select2();
					//alert(data.msg);
				}
				else{
					alert(data.msg);
				}
			}
		});
			//alert( "Handler for .change() called." );
		});

		$("#region ").change(function() {
				var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
					"region" : $("#region option:selected").attr("value")};
			$("#area").empty();
			$.ajax({
				url : base_url+"/StatMach1/getarea",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#area").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);
					}
					else{
						alert(data.msg);
					}
					}
				});

			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/StatMach1/getmanageby",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);

					}
					else{
						alert(data.msg);
					}
				}
				});
				//alert( "Handler for .change() called." );
			});

			$("#terminalType ").change(function() {
					var data = {"terminalType" :$("#terminalType option:selected").attr("value"),
				}
			$("#merk").empty();
			$.ajax({
				url : base_url+"/StatMach1/getmerk",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#merk").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);
					}
					else{
						alert(data.msg);
					}
					}
				});
});


			$(function(){
				$('.select2').select2();
			});

	if(temphasil !=0){
		js_array = document.getElementById("pi").getAttribute('dataval').split(',');

		var abc = document.getElementById("bar2").getAttribute('data-bar2');
		/* alert(var_dump(bardataMerk[])); */
		/* console.log(abc); */
		/* alert(bardataMerk.length); */
		var labelbardataMerk = [];
		var updatebardataMerk = [];
		var tidakupdatebardataMerk = [];
		var rmmtdkadabardataMerk = [];
		var atmtdkaktifbardataMerk = [];
		var probjaringanbardataMerk = [];

		for(i=0;i<bardataMerk.length;i++){
			labelbardataMerk.push(bardataMerk[i]['merk']);
			updatebardataMerk.push(bardataMerk[i]['RMMAgentAktif(EJUpdate)']);
			tidakupdatebardataMerk.push(bardataMerk[i]['RMMAgentAktif(EJNotUpdate)']);
			rmmtdkadabardataMerk.push(bardataMerk[i]['RMMTidakAda']);
			atmtdkaktifbardataMerk.push(bardataMerk[i]['ATMTidakAktif']);
			probjaringanbardataMerk.push(bardataMerk[i]['ProblemJaringan']);

		}

		var labelbardataregion = [];
		var tidakupdatebardataregion = [];
		var updatebardataregion = [];
		var rmmtdkadabardataregion = [];
		var atmtdkaktifbardataregion = [];
		var probjaringanbardataregion = [];
		for(i=0;i<bardataregion.length;i++){
			if(cekregion== 0){
					labelbardataregion.push(bardataregion[i]['regionAlias']);
			}
			else if(cekregion!= 0){
				if(cekarea == 0 && cekmanageby == 0){
					labelbardataregion.push(bardataregion[i]['area']);
				}else{
					labelbardataregion.push(bardataregion[i]['flmvendor']);
				}
			}

			updatebardataregion.push(bardataregion[i]['RMMAgentAktif(EJUpdate)']);
			tidakupdatebardataregion.push(bardataregion[i]['RMMAgentAktif(EJNotUpdate)']);
			rmmtdkadabardataregion.push(bardataregion[i]['RMMTidakAda']);
			atmtdkaktifbardataregion.push(bardataregion[i]['ATMTidakAktif']);
			probjaringanbardataregion.push(bardataregion[i]['ProblemJaringan']);
		}




let bar = document.getElementById("bar");
let bar_chart = new Chart(bar, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[{
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(252, 209, 22, 1)",
          borderWidth:1,
          borderColor:"rgba(252, 209, 22, 1)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
            label: 'Non Update',
            data:tidakupdatebardataregion,
            backgroundColor:'rgba(15, 43, 91, 2)',
            borderWidth:1,
            borderColor:'rgba(15, 43, 91, 2)',
            hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'RMM Agent Tidak Ada',
          data:rmmtdkadabardataregion,
          backgroundColor:"rgb(0, 140, 255)",
          borderWidth:1,
          borderColor:"rgb(0, 140, 255)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'ATM Tidak Aktif',
          data:atmtdkaktifbardataregion,
          backgroundColor:"rgb(249, 83, 0)",
          borderWidth:1,
          borderColor:"rgb(249, 83, 0)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'Problem jaringan',
          data:probjaringanbardataregion,
          backgroundColor:"rgb(175, 170, 163)",
          borderWidth:1,
          borderColor:"rgb(175, 170, 163)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
        ]
            },
            options: {
              scales:{
                xAxes: [{
					stacked:true,
                    gridLines:{
                      display:false,
                      drawBorder:false,
                    }
                }],
                yAxes: [{
                    ticks:{
						display:false,
						beginAtZero:true,
                    },
                    gridLines:{
                      display:true,
                      drawBorder:false,
                    }
                }]
            },
            legend:{
                display: true,
                position:'top',
                labels:{
                  fontColor: '#000',fontSize: 12,
                  fontStyle: 'bold'
                }
            },
            plugins: {
                labels:  {
                  render: function(label){
                    return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    },
                  fontSize: 0,
                  fontStyle: 'bold',
                  fontColor: '#000',
                  fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                  arc: true,
                  position: 'outside',
                  overlap: true,
                  outsidePadding: 24,
                }
        	}
        }
			});



let pi = document.getElementById("pi");
let pi_chart = new Chart(pi, {
    type: 'pie',
    data: {
			labels:  { render: 'value',
			fontSize: 12,
			fontStyle: 'bold',
			//fontColor: '#000',
			fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
		},
		labels:  ['Update','Tidak Update','RMM Agent Tidak Ada','ATM Tidak Aktif','Problem jaringan'],
        datasets: [{
            data: [js_array[1], js_array[2],js_array[3], js_array[4],js_array[5]],
            backgroundColor: [
							('rgba(252, 209, 22, 1)'),
							('rgba(15, 43, 91, 2)'),
							('rgb(0, 140, 255)'),
							('rgb(249, 83, 0)'),
							('rgb(175, 170, 163)')
            ],
						hoverBorderWidth: 2,
						hoverBorderColor:'#d1ccc0',
						fill: true,
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
								('#fff'),
                ('#fff')
            ],
            borderWidth: [2,2,2,2,2]
        }]
    },
    options: {
      yAxes: [{
          ticks: {
              beginAtZero:true
          }
      }]
  ,
  legend:{
      display: true,
      position:'top',
      labels:{
          fontColor: '#000',fontSize: 12,
          fontStyle: 'bold',
      }
  },
  plugins: {
      labels:  {
        /*render: function (args) {
              if(args.value/1000000>=1){
                return args.value/1000000 + ' M';
              }
              else {
                return args.value/1000 + ' K';
              }
          },*/
          render:'percentage',
        fontSize: 18,
        fontStyle: 'bold',
        fontColor: '#fff',
        fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
        arc:false,
        overlap: true,
        outsidePadding: 24,
      }
		}
	}
});

let bar2 = document.getElementById("bar2");
let bar2_chart = new Chart(bar2, {
    type: 'bar',
    data: {
        labels:labelbardataMerk,
        datasets:[{
          label: 'Update',
          data:updatebardataMerk,
          backgroundColor:"rgba(252, 209, 22, 1)",
          borderWidth:1,
          borderColor:"rgba(252, 209, 22, 1)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'Tidak Update',
          data:tidakupdatebardataMerk,
					backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'RMMAgent Tidak Ada',
          data:rmmtdkadabardataMerk,
          backgroundColor:"rgb(0, 140, 255)",
          borderWidth:1,
          borderColor:"rgb(0, 140, 255)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'ATM Tidak Aktif',
          data:atmtdkaktifbardataMerk,
          backgroundColor:"rgb(249, 83, 0)",
          borderWidth:1,
          borderColor:"rgb(249, 83, 0)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
				{
          label: 'Problem Jaringan',
          data:probjaringanbardataMerk,
          backgroundColor:"rgb(175, 170, 163)",
          borderWidth:1,
          borderColor:"rgb(175, 170, 163)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        }
        ]},
        options: {
          scales:{
            xAxes: [{
				stacked:true,
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{

                ticks:{
					beginAtZero:true,
					display:false,
					beginAtZero:true,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
        plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
              fontSize: 0,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
        }
			},
			});
		}
	});
