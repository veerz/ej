$(document).ready(function() {
  $('#mydata').DataTable( {
      "mydata_length": [[10, 25, 50, -1], [10, 25, 50, "All"]]
  } );
	});
	
	$("#transactionType").select2();
	$("#emodelType").select2();
	$("#etransactionType").select2();

  // $("#mydata").on("click", ".btn-warning", function(){
	// 				var data = {"key" : $(this).attr("data-key"),
	// 										"mt"  : $(this).attr("data-modelType"),
	// 										"trans_id" : $(this).attr("data-trans")};
  // 				// $("#menu").empty();
  // 				//alert( $(this).attr("data-key"));
  // 				$.ajax({
  // 					url : "ListOpCode/find_item",
  // 					type : "post",
  // 					dataType : "json",
  // 					data : data,
  // 					success : function(data){
  // 						if ( data.type === "done" ){
  // 						$("#editOPCode").val(data.msg['opcode']);
  // 						//$("#trans_id").val(data.msg['trans_id']);
	// 						$("#etransactionType").val(data.msg['trans_id']).trigger('change');
	// 						$("#emodelType").val(data.msg['mt']).trigger('change');
	// 						$("#emodelType ").prop('disabled', true);
	// 						$("#etransactionType ").prop('disabled', true);
	// 						$("#Modal_Edit").modal("show");
  // 						}
  // 						else{
  // 							alert(data.msg);
  // 						}
  // 			}
  // 		});
	// 	});
		$("#mydata").on("click", ".btn-warning", function(){
			var data = {"key" : $(this).attr("data-key"),
									"opcodes" : $(this).attr("data-opcode"),
									"moType"  : $(this).attr("data-model"),
									
								};
			// $("#menu").empty();
			$.ajax({
				url : "ListOpCode/find_item",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
					
					$("#editOPCode").val(data.msg['opcode']);
					$("#eftransactionType").prop("disabled", true);
					$("#efmodelType").prop("disabled", true);
					$("#eftransactionType").val(data.msg['trans_id']).trigger('change');
					$("#efmodelType").val(data.msg['mt']).trigger('change');

					$("#etransactionType").val(data.msg['trans_id']).trigger('change');
					$("#emodelType").val(data.msg['mt']).trigger('change');
					$("#Modal_Edit").modal("show");
					}
					else{
						alert(data.msg);
					}
		}
	});
});

$("#mydata").on("click", ".btn-danger", function(){
	var data = {"key" : $(this).attr("data-key"),
							"opcodes" : $(this).attr("data-opcode"),
							"moType"  : $(this).attr("data-model"),
							
						};
	// $("#menu").empty();
	$.ajax({
		url : "ListOpCode/delete",
		type : "post",
		dataType : "json",
		data : data,
		beforeSend : function(xhr, opts){
			var btn = confirm("Apakah anda ingin menghapus data "+(data.opcodes)+" ini?");
			if(btn === false){ 
				xhr.abort();
			}
		},
		success : function(data){
			if ( data.type === "done" ){
				alert("data OPCode telah dihapus");
				location.reload();
			}
			else{
				alert(data.msg);
			}
		}
	});
});

	function myFunction(tangkap,tangkap2){
		if(document.getElementById(tangkap2).checked == true)
		{
			//document.getElementById('submodulecol').style.display = "block!important";style
			$(tangkap).css("display","block");
		}else{
			$(tangkap).css("display","none");
		}
	}
