$(document).ready(function () {
	//Initialize Select2 Elements
	$('.select2').css('width', '100%');
	$(".select2").select2({
		theme: "bootstrap4"
	});
	var mydata = $('#mydata').DataTable({
		"ajax": 'showData',
		"order": [
			[0, "asc"]
		],
	});
	//Save previledgeGroup
	$('#btn_save').on('click', function () {
		var groupID = $('#groupID').val();
		var menuID = $('#menuID').val();
		var status = $('#status').val();
		$.ajax({
			type: "POST",
			url: 'save',
			dataType: "JSON",
			data: {
				groupID: groupID,
				menuID: menuID,
				status: status,
			},
			success: function (data) {
				$('#groupID').val("");
				$('#menuID').val("");
				$('#status').val("");
				swal(
					'Success!',
					'Input Data previledgeGroup berhasil',
					'success'
				);
				$('#Modal_Add').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for update record
	$('#show_data').on('click', '.item_edit', function () {
		var id = $(this).data('id');
		var url = 'getData/';
		var fixUrl = url += id;
		$.ajax({
			type: 'ajax',
			url: fixUrl,
			async: false,
			dataType: 'json',
			success: function (data) {
				$('#prvGroupID_edit').val(data["prvGroupID"]);
				$('#menuID_edit').val(data["menuID"]).trigger('change');
				$('#groupID_edit').val(data["groupID"]).trigger('change');
				$('#status_edit').val(data["status"]);
				$('#Modal_Edit').modal('show');
			}
		});
	});
	//update record to database
	$('#btn_update').on('click', function () {
		var menuID = $('#menuID_edit').val();
		var groupID = $('#groupID_edit').val();
		var status = $('#status_edit').val();
		var prvGroupID = $('#prvGroupID_edit').val();
		$.ajax({
			type: "POST",
			url: 'update',
			dataType: "JSON",
			data: {
				menuID: menuID,
				groupID: groupID,
				status: status,
				prvGroupID: prvGroupID
			},
			success: function (data) {
				$('#prvGroupID_edit').val("");
				$('#menuID_edit').val("");
				$('#menuName_edit').val("");
				$('#status_edit').val("");
				swal(
					'Success!',
					'Update Data previledgeGroup berhasil',
					'success'
				);
				$('#Modal_Edit').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for delete record
	$('#show_data').on('click', '.item_delete', function () {
		var prvGroupID = $(this).data('id');
		$('#Modal_Delete').modal('show');
		$('#prvgroupID_delete').val(prvGroupID);
	});
	//delete record to database
	$('#btn_delete').on('click', function () {
		var prvGroupID = $('#prvgroupID_delete').val();
		$.ajax({
			type: "POST",
			url: 'delete',
			dataType: "JSON",
			data: {
				prvGroupID: prvGroupID
			},
			success: function (data) {
				$('#prvgroupID_delete').val("");
				swal(
					'Success!',
					'Delete Data previledgeGroup berhasil',
					'success'
				);
				$('#Modal_Delete').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
});
