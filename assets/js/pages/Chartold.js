
$(document).ready(function(){

$(document).ready(function() {
    $('#mydata').DataTable( {
        "mydata_length": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    } );
} );

js_array = document.getElementById("pi").getAttribute('dataval').split(',');

 var abc = document.getElementById("bar2").getAttribute('data-bar2');
/* alert(var_dump(bardataMerk[])); */
/* console.log(abc); */
/* alert(bardataMerk.length); */
var labelbardataMerk = [];
var tidakupdatebardataMerk = [];
var updatebardataMerk = [];
for(i=0;i<bardataMerk.length;i++){
	labelbardataMerk.push(bardataMerk[i]['merk']);
	tidakupdatebardataMerk.push(bardataMerk[i]['notUpdate']);
	updatebardataMerk.push(bardataMerk[i]['Update']);
}

var labelbardataregion = [];
var tidakupdatebardataregion = [];
var updatebardataregion = [];
for(i=0;i<bardataregion.length;i++){
	labelbardataregion.push(bardataregion[i]['regionAlias']);
	tidakupdatebardataregion.push(bardataregion[i]['notUpdate']);
	updatebardataregion.push(bardataregion[i]['Update']);
}




let bar = document.getElementById("bar");

let bar_chart = new Chart(bar, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[{
            label: 'Tidak Update',
            data:tidakupdatebardataregion,
            backgroundColor:'rgba(252, 209, 22, 1)',
            borderWidth:1,
            borderColor:'rgba(252, 209, 22, 1)',
            hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0'
        },
        {
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        }
        ]
            },
            options: {
                  scales:{
                    xAxes: [{
                        gridLines:{
                          display:false,
                          drawBorder:false,
                        }
                    }],
                    yAxes: [{
                        ticks:{
                          display:false,
                          suggestedMax:2500,
                        },
                        gridLines:{
                          display:true,
                          drawBorder:false,
                        }
                    }]
                },
                legend:{
                    display: true,
                    position:'top',
                    labels:{
                      fontColor: '#000',fontSize: 12,
                      fontStyle: 'bold'
                    }
                },
                plugins: {
                    labels:  {
                      render: function(label){
                        return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        },

                      fontSize: 12,
                      fontStyle: 'bold',
                      fontColor: '#000',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: true,
                      position: 'outside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
            }
});


let pi = document.getElementById("pi");
let pi_chart = new Chart(pi, {
    type: 'pie',
    data: {
        labels:  { render: 'value',
        fontSize: 12,
        fontStyle: 'bold',
        //fontColor: '#000',
        fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
    },
    labels:  ['Tidak Update','Update'],

        datasets: [{
            data: [js_array[2], js_array[1]],
            backgroundColor: [
                ('rgba(252, 209, 22, 1)'),
                ('rgba(15, 43, 91, 2)'),
            ],hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0',
            fill: true,
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: [2,2]
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 12,
                fontStyle: 'bold',
            },
            onClick: (e) => e.stopPropagation()
        },
        plugins: {
            labels:  {
              /*render: function (args) {
                    if(args.value/1000000>=1){
                      return args.value/1000000 + ' M';
                    }
                    else {
                      return args.value/1000 + ' K';
                    }
                },*/
                render:'percentage',
              fontSize: 18,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});



let bar2 = document.getElementById("bar2");
let bar2_chart = new Chart(bar2, {
    type: 'bar',
    data: {
        labels: labelbardataMerk,
        datasets: [{
            label: "Tidak Update",
            data: tidakupdatebardataMerk,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Update",
            data: updatebardataMerk,
            backgroundColor:

                "rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:11000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },

              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    },
});



});
