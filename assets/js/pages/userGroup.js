$(document).ready(function () {
	var mydata = $('#mydata').DataTable({
		"ajax": 'showData',
		"order": [
			[0, "asc"]
		],
	});
	//Save userGroup
	$('#btn_save').on('click', function () {
		var groupName = $('#groupName').val();
		$.ajax({
			type: "POST",
			url: 'save',
			dataType: "JSON",
			data: {
				groupName: groupName
			},
			success: function (data) {
				$('#groupName').val("");
				swal(
					'Success!',
					'Input Data userGroup berhasil',
					'success'
				);
				$("#Modal_Add").modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for update record
	$('#show_data').on('click', '.item_edit', function () {
		var id = $(this).data('id');
		var url = 'getData/';
		var fixUrl = url += id;
		$.ajax({
			type: 'ajax',
			url: fixUrl,
			async: false,
			dataType: 'json',
			success: function (data) {
				$('#groupID_edit').val(data["groupID"]);
				$('#groupName_edit').val(data["groupName"]);
				$('#Modal_Edit').modal('show');
			}
		});
	});
	//update record to database
	$('#btn_update').on('click', function () {
		var groupID = $('#groupID_edit').val();
		var groupName = $('#groupName_edit').val();
		$.ajax({
			type: "POST",
			url: 'update',
			dataType: "JSON",
			data: {
				groupID: groupID,
				groupName: groupName
			},
			success: function (data) {
				$('#groupID_edit').val("");
				$('#groupName_edit').val("");
				swal(
					'Success!',
					'Update Data userGroup berhasil',
					'success'
				);
				$('#Modal_Edit').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for delete record
	$('#show_data').on('click', '.item_delete', function () {
		var groupID = $(this).data('id');
		$('#Modal_Delete').modal('show');
		$('#groupID_delete').val(groupID);
	});
	//delete record to database
	$('#btn_delete').on('click', function () {
		var groupID = $('#groupID_delete').val();
		$.ajax({
			type: "POST",
			url: 'delete',
			dataType: "JSON",
			data: {
				groupID: groupID
			},
			success: function (data) {
				$('#groupID_delete').val("");
				swal(
					'Success!',
					'Delete Data userGroup berhasil',
					'success'
				);
				$('#Modal_Delete').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
});
