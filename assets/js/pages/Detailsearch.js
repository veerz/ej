	function myFunction(tangkap,tangkap2){
		if(document.getElementById(tangkap2).checked == true)
		{
			$(tangkap).css("display","block");
		}else{
			$(tangkap).css("display","none");
		}
	}
	
	$("#submitreset").click(function() {
			$("#idmachine").val('').trigger('change');
			$('#idmachine').select2({
			minimumInputLength: 4});
			$("#startdate").val("");
			$("#enddate").val("");
			$("#namapengelola").val("");
			$("#deploymentyear").val("");
			$("#lokasiatm").val("");
			$("#region").val("");
			$("#area").val("");
			//$("#detailsearchtable").empty();
	});

	$(function(){
			var base_url = window.location.origin + "/EJBaru";
			$('#idmachine').select2({
				minimumInputLength: 4}
			);
			
	
});

$("#submitsearch ").click(function() {
			if($("#idmachine option:selected").val() === "" && $("#startdate").val()=== "" && $("#enddate").val()=== "" ){
				alert("Silahkan input Id Mesin, Tanggal Awal dan Tanggal Akhir");
				$("#idmachine").focus();
				$(".select2idmachine").select2("open");
			}else if($("#idmachine option:selected").val() === "" && ($("#startdate").val()!== "" && $("#enddate").val()!== "" )){
				alert("Please input ID MACHINE");
			$(".select2idmachine").select2("open");
			}else if($("#idmachine option:selected").val() === "" && ($("#startdate").val() === "" && $("#enddate").val()!== "" )){
				alert("Please input ID MACHINE and Start Date");
				$(".select2idmachine").select2("open");
			}else if($("#idmachine option:selected").val() === "" && ($("#startdate").val() !== "" && $("#enddate").val() === "" )){
				alert("Please input ID MACHINE and End Date");
			$(".select2idmachine").select2("open");
			}else if($("#idmachine option:selected").val() !== "" && ($("#startdate").val()=== ""&& $("#enddate").val()=== "" )){
				alert("Please input Start Date and End Date");
				$("#startdate").focus();
			}else if($("#idmachine option:selected").val() !== "" && $("#startdate").val()!== "" && $("#enddate").val()=== "" ){
				alert("Please input End Date");
				$("#enddate").focus();
			}else if($("#idmachine option:selected").val() !== "" && $("#startdate").val() === "" && $("#enddate").val()!== ""){
				alert("Please input Start Date");
				$("#startdate").focus();
			}else if($("#enddate").val()  < $("#startdate").val()){
				alert("Please input End Date greater than Start Date ");
				$("#enddate").focus();
			}
			else{
					var data = {"idmachine" : $("#idmachine option:selected").attr("value"),
								"startdate" : $("#startdate").val(),
								"enddate" : $("#enddate").val()
								};
					$.ajax({
							url :  window.location.origin + "/EJBaru/detailsearch/showData",
							type : "post",
							dataType : "json",
							data : data,
							beforeSend: function() {
							$("#detailsearchtable").empty();
							$("#detailsearchtable").append("<h3>Process Loading.....</h3>");
						   },
							success : function(data){
								if ( data.type === "done" ){
									$("#namapengelola").val(data.msg['managedBy']);
									$("#deploymentyear").val(data.msg['deploymentyear']);
									$("#lokasiatm").val(data.msg['address']);
									$("#region").val(data.msg['kanwil']);
									$("#area").val(data.msg['area']);
									$("#detailsearchtable").empty();
									$("#detailsearchtable").append(data.msg['result']);
									$('#detailsearchtable').DataTable( {
									  bDestroy:true,
									  dom: 'Bfrtip',
									buttons: [
										'copyHtml5',
											{	extend: 'excelHtml5',
												title: 'Detail Search '+ moment().format('YYYY-MM-DD'),
												//"className": "addNewRecord"
											},
											{	extend: 'csvHtml5',
												title: 'Detail Search '+ moment().format('YYYY-MM-DD'),
												//"className": "addNewRecord"
											},{
											extend: 'pdfHtml5',orientation: 'landscape',
											pageSize: 'LEGAL',title: 'Detail Search '+ moment().format('YYYY-MM-DD')},
									]
									} );
								}
								else{
									alert(data.msg);
								}
							}
						});
			}
		});

function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
