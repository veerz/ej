$(function(){
	$('.select2').select2();	
});

$(document).ready(function() {
    $('#mydata').DataTable( {
        "mydata_length": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    } );
	
	$("#passcek").change(function() {
			if($('#passcek').is( ":checked")){
				$('#epassword').prop('readonly', false);
				$("#showPass").css("display","block");
				$('#epassword').val("");
				$('#passcek').val("checked1");
			}else{
				$('#epassword').prop('readonly', true);
				$("#showPass").css("display","none");;
				// $('#epassword').val(data.passwords);
				$('#passcek').val("checked2");
			}
	});
	
	$("#mydata").on("click", ".btn-warning", function(){
		var data = {"key" : $(this).attr("data-key")};
		//alert( $(this).attr("data-key"));
		$.ajax({
			url : "UserManagement/find_item",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
					$("#eid_user").val(data.id_users);
					$("#eid_role").val(data.id_roles);
					$("#ename").val(data.names);
					$("#eusername").val(data.usernames);
					$("#estatus").val(data.statuss);
					$("#epassword").val(data.passwords);
					$("#Modal_Edit").modal("show");
				}
				else{
					alert(data.msg);
				}
			}
		});
	});
		

} );