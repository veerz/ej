$(document).ready(function () {
	//Initialize Select2 Elements
	$('.select2').css('width', '100%');
	$(".select2").select2({
		theme: "bootstrap4"
	});
	var mydata = $('#mydata').DataTable({
		"ajax": 'showData',
		"order": [
			[0, "asc"]
		],
	});
	//Save previledgeUser
	$('#btn_save').on('click', function () {
		var userID = $('#userID').val();
		var menuID = $('#menuID').val();
		var status = $('#status').val();
		$.ajax({
			type: "POST",
			url: 'save',
			dataType: "JSON",
			data: {
				userID: userID,
				menuID: menuID,
				status: status,
			},
			success: function (data) {
				$('#userID').val("");
				$('#menuID').val("");
				$('#status').val("");
				swal(
					'Success!',
					'Input Data previledgeUser berhasil',
					'success'
				);
				$('#Modal_Add').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for update record
	$('#show_data').on('click', '.item_edit', function () {
		var id = $(this).data('id');
		var url = 'getData/';
		var fixUrl = url += id;
		$.ajax({
			type: 'ajax',
			url: fixUrl,
			async: false,
			dataType: 'json',
			success: function (data) {
				$("#prvUserID_edit").val(data["prvUserID"]);
				$('#menuID_edit').val(data["menuID"]).trigger('change');;
				$('#userID_edit').val(data["userID"]).trigger('change');;
				$('#status_edit').val(data["status"]);
				$('#Modal_Edit').modal('show');
			}
		});
	});
	//update record to database
	$('#btn_update').on('click', function () {
		var menuID = $('#menuID_edit').val();
		var userID = $('#userID_edit').val();
		var status = $('#status_edit').val();
		var prvUserID = $('#prvUserID_edit').val();
		$.ajax({
			type: "POST",
			url: 'update',
			dataType: "JSON",
			data: {
				menuID: menuID,
				userID: userID,
				status: status,
				prvUserID: prvUserID
			},
			success: function (data) {
				$('#prvUserID_edit').val("");
				$('#menuID_edit').val("");
				$('#menuName_edit').val("");
				$('#status_edit').val("");
				swal(
					'Success!',
					'Update Data previledgeUser berhasil',
					'success'
				);
				$('#Modal_Edit').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for delete record
	$('#show_data').on('click', '.item_delete', function () {
		var prvUserID = $(this).data('id');
		$('#Modal_Delete').modal('show');
		$('#prvUserID_delete').val(prvUserID);
	});
	//delete record to database
	$('#btn_delete').on('click', function () {
		var prvUserID = $('#prvUserID_delete').val();
		$.ajax({
			type: "POST",
			url: 'delete',
			dataType: "JSON",
			data: {
				prvUserID: prvUserID
			},
			success: function (data) {
				$('#prvUserID_delete').val("");
				swal(
					'Success!',
					'Delete Data previledgeUser berhasil',
					'success'
				);
				$('#Modal_Delete').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
});
