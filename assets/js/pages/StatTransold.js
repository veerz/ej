$(document).ready(function(){
	var base_url = window.location.origin + "/ej";


	$("#filter").click(function(){

		var data = {"definisi" : $("#definisi option:selected").attr("value"),
								"region" : $("#region").val(),
								"area" : $("#area").val(),
								"managedBy" : $("#managedBy").val(),
								"terminalType" : $("#terminalType").val(),
								"merk" : $("#merk").val()
							};
			$.ajax({
					url : window.location.origin + "/ej/StatTrans/getData",
					type : "post",
					dataType : "json",
					data : data,
					success : function(data){
						if( data.type === "done"){

						}
						else{
								alert(data.msg);
					}
				}
			});
	});


	$("#managerType ").change(function() {
			var data = {"jenismanage" : $("#managerType option:selected").attr("value"),
			"region" : $("#region option:selected").attr("value")
			};

		$("#managedBy").empty();
		$.ajax({
			url : base_url+"/StatTrans/getmanageby",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
	//							$("#menu").val ( data.msg);
					$("#managedBy").append(data.msg);
					$('.select2').select2();
					//alert(data.msg);
				}
				else{
					alert(data.msg);
				}
			}
		});
			//alert( "Handler for .change() called." );
		});

		$("#region ").change(function() {
				var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
					"region" : $("#region option:selected").attr("value")};
			$("#area").empty();
			$.ajax({
				url : base_url+"/StatTrans/getarea",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#area").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);
					}
					else{
						alert(data.msg);
					}
					}
				});

			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/StatTrans/getmanageby",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);

					}
					else{
						alert(data.msg);
					}
				}
				});
				//alert( "Handler for .change() called." );
			});

			$("#terminalType ").change(function() {
					var data = {"terminalType" :$("#terminalType option:selected").attr("value"),
				}
			$("#merk").empty();
			$.ajax({
				url : base_url+"/StatTrans/getmerk",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#merk").append(data.msg);
						$('.select2').select2();
						//alert(data.msg);
					}
					else{
						alert(data.msg);
					}
					}
				});
			});

$(function(){
	$('.select2').select2();
});


var labelReplenishment = [];
var totalpengisian = [];
var totalpengosongan = [];
for(i=0;i<Replenishment.length;i++){
	if(i%2==0)
	labelReplenishment.push (Replenishment[i]['terminaltype']);
	if(Replenishment[i]['JenisTrx'] == "Pengisian")
        totalpengisian.push(Replenishment[i]['Total']);
    else
        totalpengosongan.push(Replenishment[i]['Total']);
}

//Mapping Status Transaksi
var labelStatusTrxMapping = [];
var jumlahStatusTrxMapping = [];
for(i=0;i<StatusTrxMapping.length;i++){
	labelStatusTrxMapping.push(StatusTrxMapping[i]['Status Transaksi']);
	jumlahStatusTrxMapping.push(StatusTrxMapping[i]['Jumlah']);
}

//Top 10 Machine Bad Status Transaksi
var labelStatusTrxBad = [];
var jumlahStatusTrxBad = [];
for(i=0;i<StatusTrxBad.length;i++){
	labelStatusTrxBad.push(StatusTrxBad[i]['TermID']);
	jumlahStatusTrxBad.push(StatusTrxBad[i]['Gagal']);
}

//Top 10 Machine Good Status Transaksi
var labelStatusTrxGood = [];
var jumlahStatusTrxGood = [];
for(i=0;i<StatusTrxGood.length;i++){
	labelStatusTrxGood.push(StatusTrxGood[i]['TermID']);
	jumlahStatusTrxGood.push(StatusTrxGood[i]['Sukses']);
}

//Status Transaksi berdasarkan Jenis Transaksi
var labelStatusTrxByJenis= [];
var jumlahStatusTrxByJenis = [];
for(i=0;i<StatusTrxByJenis.length;i++){
	labelStatusTrxByJenis.push(StatusTrxByJenis[i]['transactiongroup']);
	jumlahStatusTrxByJenis.push(StatusTrxByJenis[i]['Jumlah']);
}

//Status Transaction by Region
var labelregiontrx = [];
var jumlahtrxberhasil = [];
var jumlahtrxgagal = [];
for(i=0;i<regiontrx.length;i++){
	if(i%2==0)
	labelregiontrx.push (regiontrx[i]['Region']);

	if(regiontrx[i]['Status'] == "Gagal")
        jumlahtrxgagal.push(regiontrx[i]['Jumlah']);
    else
        jumlahtrxberhasil.push(regiontrx[i]['Jumlah']);
}


let bar3 = document.getElementById("bar3");
let bar3_chart = new Chart(bar3, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
				stepSize:100000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let horizontalBar = document.getElementById("horizontalBar");
let horizontalBar_chart = new Chart(horizontalBar, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxMapping,
        datasets: [{
            label: "Status Transaksi",
            data: jumlahStatusTrxMapping,
            backgroundColor:
            "rgba(117, 178, 221, 1)",
            borderColor: "rgba(117, 178, 221, 1)",
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:500000,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },

              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              marginleft:100,
            }
    }
    }
});


let bar7 = document.getElementById("bar7");
let bar7_chart = new Chart(bar7, {
    type: 'bar',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengisian,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        },{
          label: "Pengosongan",
          data: totalpengosongan,
          backgroundColor:
          "rgba(252, 209, 22, 1)",
          borderColor: [
              ('#fff'),
              ('#fff'),
              ('#fff'),
              ('#fff')
          ],
          borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:500,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});


let horbar4 = document.getElementById("horbar4");
let horbar4_chart = new Chart(horbar4, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxBad,
        datasets: [{
            label: "Top 10 Machine Bad Performance",
            data: jumlahStatusTrxBad,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:600,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },

              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});


let horbar5 = document.getElementById("horbar5");
let horbar5_chart = new Chart(horbar5, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxGood,
        datasets: [{
            label: "Top 10 Machine Good Performance",
            data: jumlahStatusTrxGood,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
				suggestedMax:1500,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function(label){
                return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                },

              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});


let pie5 = document.getElementById("pie5");
let pi5_chart = new Chart(pie5, {
    type: 'pie',
    data: {
        labels: labelStatusTrxByJenis,
        datasets: [{
            data: jumlahStatusTrxByJenis,
            backgroundColor: [
                ('yellow'),
                ('brown'),
                ('rgb(117, 178, 221)'),
				('rgb(44, 59, 65)'),
				('rgb(15, 43, 91, 2)'),
				('red'),
				('orange'),
				('green'),
				('purple'),
            ],
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'left',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
                    labels:  {
                      /*render: function (args) {
                            if(args.value/1000000>=1){
                              return args.value/1000000 + ' M';
                            }
                            else {
                              return args.value/1000 + ' K';
                            }
                        },*/
                        render:'percentage',
                      fontSize: 16,
                      fontStyle: 'bold',
                      fontColor: '#fff',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: false,
                      position: 'inside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
    }
});

});
