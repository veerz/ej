$(document).ready(function(){
	var base_url = window.location.origin + "/EJBaru";
	var StatusTrxMapping = "";
	var Replenishment = "";
	var StatusTrxBad = "";
	var StatusTrxGood = "";
	var StatusTrxByJenis = "";
	var regiontrx = "";
	var labelReplenishment = [];
	var totalpengisianATM = [];
	var totalpengosonganCDM = [];
	var totalpengisianCRM = [];
	var labelStatusTrxMapping = [];
	var jumlahStatusTrxMapping = [];
	var labelStatusTrxBad = [];
	var jumlahStatusTrxBad = [];
	var labelStatusTrxGood = [];
	var jumlahStatusTrxGood = [];
	// var labelStatusTrxByJenis= [];
	// var jumlahStatusTrxByJenis = [];
	var labelregiontrx = [];
	var jumlahtrxberhasil = [];
	var jumlahtrxgagal = [];

	var	cardRetainedRegion= "";//untuk total kartu tertelan

	var resetCanvas = function(){
  $('#bar3').remove(); // this is my <canvas> element
	$('#horizontalBar').remove();
	$('#bar7').remove();
	$('#horbar4').remove();
	$('#horbar5').remove();
	$('#bar71').remove();
	$('#bar72').remove();
	$('#bar73').remove();
	// $('#pie5').remove();

  $('#chartReport1').append('<canvas id="bar3"><canvas>');
	$('#chartReport2').append('<canvas id="horizontalBar"><canvas>');
	$('#chartReport3').append('<canvas id="bar7"><canvas>');
	$('#chartReport4').append('<canvas id="horbar4"><canvas>');
	$('#chartReport5').append('<canvas id="horbar5"><canvas>');
	$('#chartReport71').append('<canvas id="bar71"><canvas>');
	$('#chartReport72').append('<canvas id="bar72"><canvas>');
	$('#chartReport73').append('<canvas id="bar73"><canvas>');
	// $('#chartReport6').append('<canvas id="pie5"><canvas>');

  canvas = document.querySelector('#bar3');
	canvas = document.querySelector('#horizontalBar');
	canvas = document.querySelector('#bar7');
	canvas = document.querySelector('#horbar4');
	canvas = document.querySelector('#horbar5');
	canvas = document.querySelector('#bar71');
	canvas = document.querySelector('#bar72');
	canvas = document.querySelector('#bar73');
	// canvas = document.querySelector('#pie5');
  ctx = canvas.getContext('2d');
};
	$("#filter").click(function(){

		var data = {"definisi" : $("#definisi").val(),
								"region" : $("#region").val(),
								"area" : $("#area").val(),
								"managerType" : $("#managerType").val(),
								"managedBy" : $("#managedBy").val(),
								"terminalType" : $("#terminalType").val(),
								"merk" : $("#merk").val()
							};
			resetCanvas();
			$.ajax({
					url : window.location.origin + "/EJBaru/StatTrans/getData",
					type : "post",
					dataType : "json",
					data : data,
					beforeSend: function() {
						$("#tempsearch").empty();
						$("#adadata").css("display","none");
						$("#tempsearch").append("<h3>Process Loading.....</h3>");
						$("#nodata").empty();
						 },
					success : function(data){
						if( data.type === "done"){
							if(data.msg['isidata'] == "ada"){
								StatusTrxMapping	= data.msg['StatusTrxMapping'];
								Replenishment			= data.msg['Replenishment'];
								StatusTrxBad			= data.msg['StatusTrxBad'];
								StatusTrxGood			= data.msg['StatusTrxGood'];
								StatusTrxByJenis	= data.msg['StatusTrxByJenis'];
								regiontrx					= data.msg['regiontrx'];
								cardRetainedRegion =data.msg['cardRetainedRegion'];//untuk total kartu tertelan

								cekregion = data.msg['cekregion'];
								cekarea = data.msg['cekarea'];
								cekmanageby = data.msg['cekmanageby'];
								trxDef = data.msg['trxDef'];
								// cek
								$("#tempsearch").append("<h3>Process Loading.....</h3>");
								$("#adadata").css("display","block");
								$("#regiondata").empty();
								$("#regiondata").append(data.resultgridregion);
								$("#datajenistrans").empty();
								$("#datajenistrans").append(data.resultgridjenistrans);
								$("#gridgagalsukses").empty();
                $("#gridgagalsukses").append(data.gridgagalsukses);
								$("#bar3").empty();
								$("#horizontalBar").empty();
								$("#bar7").empty();
								$("#horbar4").empty();
								$("#horbar5").empty();
								$("#pie5").empty();
								$("#bar71").empty();
								$("#bar72").empty();
								$("#bar73").empty();
								$("#tempsearch").empty();

								labelReplenishment = [];
								totalpengisianATM = [];
								totalpengosonganCDM = [];
								totalpengisianCRM = [];
								for(i=0;i<Replenishment.length;i++){
									//if(i%2==0)
									labelReplenishment.push(Replenishment[i]['terminaltype']);

									if(Replenishment[i]['terminaltype'] == "ATM" ){
										totalpengisianATM.push(Replenishment[i]['Total']);
									/* if(Replenishment[i]['JenisTrx'] == "Pengosongan")
										totalpengisian.push(Replenishment[i]['Total']); */
										}else if(Replenishment[i]['terminaltype'] == "CDM" ){
										totalpengosonganCDM.push(Replenishment[i]['Total']);
										}else if(Replenishment[i]['terminaltype'] == "CRM"){
										totalpengisianCRM.push(Replenishment[i]['Total'])
									}
								}

								//Mapping Status Transaksi
								labelStatusTrxMapping = [];
								jumlahStatusTrxMapping = [];

								for(i=0;i<StatusTrxMapping.length;i++){
									labelStatusTrxMapping.push(StatusTrxMapping[i]['Status Transaksi']);
									jumlahStatusTrxMapping.push(StatusTrxMapping[i]['Jumlah']);
								}


								//Top 10 Machine Bad Status Transaksi
								labelStatusTrxBad = [];
								jumlahStatusTrxBad = [];
								for(i=0;i<StatusTrxBad.length;i++){
									labelStatusTrxBad.push(StatusTrxBad[i]['TermID']);
									jumlahStatusTrxBad.push(StatusTrxBad[i]['Gagal']);
								}

								//Top 10 Machine Good Status Transaksi
								labelStatusTrxGood = [];
								jumlahStatusTrxGood = [];
								for(i=0;i<StatusTrxGood.length;i++){
									labelStatusTrxGood.push(StatusTrxGood[i]['TermID']);
									jumlahStatusTrxGood.push(StatusTrxGood[i]['Sukses']);
								}

								//Status Transaction by Region
								labelregiontrx = [];
								jumlahtrxberhasil = [];
								jumlahtrxgagal = [];

								totaljumlahtrxgagal = 0;
								totaljumlahtrxberhasil = 0;
								for(i=0;i<regiontrx.length;i++){
									if(!trxDef){
											if(cekregion == 0    && cekarea == 0 && cekmanageby == 0){
												if(i%2==0)
												labelregiontrx.push(regiontrx[i]['Region']);
											}else if(cekregion != 0    && cekarea == 0 && cekmanageby == 0){
												if(i%2==0)
												labelregiontrx.push(regiontrx[i]['areaAlias']);
											}else if(cekregion == 0    && cekarea != 0 && cekmanageby == 0){
												if(i%2==0)
												labelregiontrx.push(regiontrx[i]['managedBy']);
											}else {
												if(i%2==0)
												labelregiontrx.push(regiontrx[i]['managedBy']);
											}
										}else{
											if(cekregion == 0    && cekarea == 0 && cekmanageby == 0){
												labelregiontrx.push(regiontrx[i]['Region']);
											}else if(cekregion != 0    && cekarea == 0 && cekmanageby == 0){
												labelregiontrx.push(regiontrx[i]['area']);
											}else if(cekregion == 0    && cekarea != 0 && cekmanageby == 0){
												labelregiontrx.push(regiontrx[i]['managedBy']);
											}else{
												labelregiontrx.push(regiontrx[i]['managedBy']);
										}
									}
											if(regiontrx[i]['Status'] == "Gagal"){
														jumlahtrxgagal.push(regiontrx[i]['Jumlah']);
														totaljumlahtrxgagal  = parseInt(totaljumlahtrxgagal) + parseInt(regiontrx[i]['Jumlah']);
												}else{
														jumlahtrxberhasil.push(regiontrx[i]['Jumlah']);
														totaljumlahtrxberhasil  = parseInt( totaljumlahtrxberhasil) + parseInt(regiontrx[i]['Jumlah']);
														}
								}

								function formatNumber (num) {
									return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
								}
								var tablegagalsukses="";
								tablegagalsukses += "<table id=\"jenistrans\" class=\"table table-bordered table-striped\" style=\"font-size:12px\"><tr><td><center>Total Transaksi Gagal</center></td><td><center>"+formatNumber(totaljumlahtrxgagal)+"</center></td></tr><tr><td><center>Total Transaksi Berhasil</center></td><td><center>"+formatNumber(totaljumlahtrxberhasil)+"</center></td></tr></table>";
								$("#tablegagalsukses").empty();
								$("#tablegagalsukses").append(tablegagalsukses);

								let bar3 = document.getElementById("bar3");
								let bar3_chart = new Chart(bar3, {
								    type: 'bar',
								    data: {
								        labels: labelregiontrx,
								        datasets: [{
								            label: "Gagal",
								            data: jumlahtrxgagal,
								            backgroundColor:
								            "rgba(252, 209, 22, 1)",
								            borderColor: [
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff')
								            ],
								            borderWidth: 1
								        },{
								        label: "Sukses",
								            data: jumlahtrxberhasil,
								            backgroundColor:"rgba(15, 43, 91, 2)",
								            borderColor: [
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff')
								            ],
								            borderWidth: 1
								        }]
								    },
								    options: {
								          scales:{
								            xAxes: [{
								                gridLines:{
								                  display:false,
								                  drawBorder:false,
								                }
								            }],
								            yAxes: [{
								                ticks:{
								                  display:false,
												// stepSize:100000,
								                },
								                gridLines:{
								                  display:true,
								                  drawBorder:false,
								                }
								            }]
								        },
								        legend:{
								            display: true,
								            position:'top',
								            labels:{
								              fontColor: '#000',fontSize: 12,
								              fontStyle: 'bold',
								            }
								        },
								        plugins: {
								            labels:  {
												render: function (args) {
												if(args.value/1000000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000000).toFixed(2) +' M';
												}
												if(args.value/1000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000).toFixed(2) +' K';
												}
												else {
												  return args.value;
												}
											},
											fontSize: 10,
											  fontStyle: 'bold',
											  fontColor: '#000',
											  fontFamily: '"Calibri"',
											  arc: true,
											  position: 'outside',
											  overlap: true,
											  outsidePadding: 24,
								            }
								    }
								    }
								});

								let horizontalBar = document.getElementById("horizontalBar");
								let horizontalBar_chart = new Chart(horizontalBar, {
								    type: 'horizontalBar',
								    data: {
								        labels: labelStatusTrxMapping,
								        datasets: [{
								            label: "Status Transaksi",
								            data: jumlahStatusTrxMapping,
								            backgroundColor:
								            "rgba(117, 178, 221, 1)",
								            borderColor: "rgba(117, 178, 221, 1)",
								            borderWidth: 1
								        }]
								    },
								    options: {
								          scales:{
								            xAxes: [{
								              ticks:{
								                display:false,
												beginAtZero:true,
												suggestedMax:500000,
								              },
								                gridLines:{
								                  display:true,
								                  drawBorder:false,
								                }
								            }],
								            yAxes: [{
								                gridLines:{
								                  display:false,

								                }
								            }]
								        },
								        legend:{
								            display: false,
								            position:'top',
								            labels:{
								              fontColor: '#000',fontSize: 12,
								              fontStyle: 'bold'
								            }
								        },
										plugins: {
								            labels:  {
								              render: function (args) {
												if(args.value/1000000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000000).toFixed(2) +' M';
												}
												if(args.value/1000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000).toFixed(2) +' K';
												}
												else {
												  return args.value;
												}
											},
											  fontSize: 12,
											  fontStyle: 'bold',
											  fontColor: '#000',
											  fontFamily: '"Calibri"',
								              marginleft:100,
								            }
								    }
								    }
								});


								let bar71 = document.getElementById("bar71");
								let bar71_chart = new Chart(bar71, {
										type: 'doughnut',
										data: {
												labels: labelReplenishment,
												datasets: [{
													label: "Pengisian",
															data: totalpengisianATM,
															backgroundColor:"rgba(15, 43, 91, 2)",
															borderColor: [
																	('#fff'),
																	('#fff'),
																	('#fff'),
																	('#fff')
															],
															borderWidth: 1
												}]
										},
										options: {

														yAxes: [{
																ticks: {
																		beginAtZero:true
																}
														}]
												,
												legend:{
														display: false,
														position:'top',
														labels:{
																fontColor: '#000',fontSize: 10,
																fontStyle: 'bold',
														}
												},
												plugins: {
														labels:  {
															render: function (args) {
																if(args.value/1000000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000000).toFixed(2) +' M';
																}
																if(args.value/1000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000).toFixed(2) +' K';
																}
																else {
																	return args.value;
																}
															},
																
												position: 'inside',
															fontSize: 10,
															fontStyle: 'bold',
															fontColor: '#fff',
															fontFamily: '"Calibri"',
															arc:false,
															overlap: true,
															outsidePadding: 24,
														}
										}
										}
								});

								let bar72 = document.getElementById("bar72");
								let bar72_chart = new Chart(bar72, {
										type: 'doughnut',
										data: {
												labels: labelReplenishment,
												datasets: [{
													label: "Pengisian",
															data: totalpengosonganCDM,
															backgroundColor:"rgba(15, 43, 91, 2)",
															borderColor: [
																	('#fff'),
																	('#fff'),
																	('#fff'),
																	('#fff')
															],
															borderWidth: 1
												}]
										},
										options: {

														yAxes: [{
																ticks: {
																		beginAtZero:true
																}
														}]
												,
												legend:{
														display: false,
														position:'top',
														labels:{
																fontColor: '#000',fontSize: 10,
																fontStyle: 'bold',
														}
												},
												plugins: {
														labels:  {
															render: function (args) {
																if(args.value/1000000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000000).toFixed(2) +' M';
																}
																if(args.value/1000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000).toFixed(2) +' K';
																}
																else {
																	return args.value;
																}
															},
																
												position: 'inside',
															fontSize: 10,
															fontStyle: 'bold',
															fontColor: '#fff',
															fontFamily: '"Calibri"',
															arc:false,
															overlap: true,
															outsidePadding: 24,
														}
										}
										}
								});

								let bar73 = document.getElementById("bar73");
								let bar73_chart = new Chart(bar73, {
										type: 'doughnut',
										data: {
												labels: labelReplenishment,
												datasets: [{
													label: "Pengisian",
															data: totalpengisianCRM,
															backgroundColor:"rgba(15, 43, 91, 2)",
															borderColor: [
																	('#fff'),
																	('#fff'),
																	('#fff'),
																	('#fff')
															],
															borderWidth: 1
												}]
										},
										options: {

														yAxes: [{
																ticks: {
																		beginAtZero:true
																}
														}]
												,
												legend:{
														display: false,
														position:'top',
														labels:{
																fontColor: '#000',fontSize: 10,
																fontStyle: 'bold',
														}
												},
												plugins: {
														labels:  {
															render: function (args) {
																if(args.value/1000000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000000).toFixed(2) +' M';
																}
																if(args.value/1000>=1){
																	// return Math.round((args.value/1000) + ' K';
																	return (args.value/1000).toFixed(2) +' K';
																}
																else {
																	return args.value;
																}
															},
															
												position: 'inside',
															fontSize: 10,
															fontStyle: 'bold',
															fontColor: '#fff',
															fontFamily: '"Calibri"',
															arc:false,
															overlap: true,
															outsidePadding: 24,
														}
										}
										}
								});


								let horbar4 = document.getElementById("horbar4");
								horbar4.height=200;
								let horbar4_chart = new Chart(horbar4, {
								    type: 'horizontalBar',
								    data: {
								        labels: labelStatusTrxBad,
								        datasets: [{
								            label: "Top 10 Machine Bad Performance",
								            data: jumlahStatusTrxBad,
								            backgroundColor: "rgba(117, 178, 221, 1)",
								            borderColor: [
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
																('#fff'),
																('#fff'),
																('#fff'),
																('#fff'),
								                ('#fff')
								            ],
								            borderWidth: 1
								        }]
								    },
								    options: {
								          scales:{
								            xAxes: [{
								              ticks:{
								                display:false,
																beginAtZero:true,
																stepSize:300,
																// suggestedMax:600,
								              },
								                gridLines:{
								                  display:true,
								                  drawBorder:false,
								                }
								            }],
								            yAxes: [{
								                gridLines:{
								                  display:false,

								                }
								            }]
								        },
								        legend:{
								            display: false,
								            position:'top',
								            labels:{
								              fontColor: '#000',fontSize: 12,
								              fontStyle: 'bold'
								            }
								        },
										plugins: {
								            labels:  {
								             render: function (args) {
												if(args.value/1000000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000000).toFixed(2) +' M';
												}
												if(args.value/1000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000).toFixed(2) +' K';
												}
												else {
												  return args.value;
												}
											},
											  fontSize: 12,
											  fontStyle: 'bold',
											  fontColor: '#000',
											  fontFamily: '"Calibri"',
								            }
								    }
								    }
								});


								let horbar5 = document.getElementById("horbar5");
								horbar5.height=200;
								let horbar5_chart = new Chart(horbar5, {
								    type: 'horizontalBar',
								    data: {
								        labels: labelStatusTrxGood,
								        datasets: [{
								            label: "Top 10 Machine Good Performance",
								            data: jumlahStatusTrxGood,
								            backgroundColor: "rgba(117, 178, 221, 1)",
								            borderColor: [
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
																('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff'),
								                ('#fff')
								            ],
								            borderWidth: 1
								        }]
								    },
								    options: {
								          scales:{
								            xAxes: [{
								              ticks:{
								                display:false,
																beginAtZero:true,
																stepSize:1000,
																//suggestedMax:2000,
								              },
								                gridLines:{
								                  display:true,
								                  drawBorder:false,
								                }
								            }],
								            yAxes: [{
								                gridLines:{
								                  display:false,

								                }
								            }]
								        },
								        legend:{
								            display: false,
								            position:'top',
								            labels:{
								              fontColor: '#000',fontSize: 12,
								              fontStyle: 'bold'
								            }
								        },
										plugins: {
								            labels:  {
								             render: function (args) {
															if(args.value/1000000>=1){
																// return Math.round((args.value/1000) + ' K';
																return (args.value/1000000).toFixed(2) +' M';
															}
															if(args.value/1000>=1){
																// return Math.round((args.value/1000) + ' K';
																return (args.value/1000).toFixed(2) +' K';
															}
															else {
																return args.value;
															}
														},
														fontSize: 12,
														fontStyle: 'bold',
														fontColor: '#000',
														fontFamily: '"Calibri"',
																}
								    			}
								    	}
									});
							}else{
								$("#nodata").append("<h3> Data tidak ditemukan</h3>");
								$("#adadata").css("display", "none");
								$("#tempsearch").empty();
								}
							}
							else{
									alert(data.msg);
						}
				}
			});
	});


	$(function(){
		$("#managerType ").change(function() {
		var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
		"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};
				$("#managedBy").empty();
				$.ajax({
					url : "AllFilter/getmanagebyJenisPengelola",
					type : "post",
					dataType : "json",
					data : data,
					success : function(data){
						if ( data.type === "done" ){
			//		$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
						$('#managedBy.select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});

						//alert(data.msg);
						}
						else{
							alert(data.msg);
						}
					}
				});
				//alert( "Handler for .change() called." );
		});

		$("#terminalType").change(function() {
			var data = {"terminalType" :$("#terminalType option:selected").attr("value"),
			}
			$("#merk").empty();
			$.ajax({
				url : base_url+"/AllFilter/getmerkbyJenisMesin",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
						//$("#menu").val ( data.msg);
						$("#merk").append(data.msg);
						$('#merk.select2').select2();$("#merk").val('Merk');
					}
					else{
						alert(data.msg);
					}
				}
			});
		});

	});

	$("#region").change(function() {
		var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
		"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};
		$("#area").empty();
		$.ajax({
			url : base_url+"/AllFilter/getareabyRegion",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
	//							$("#menu").val ( data.msg);
					$("#area").append(data.msg);
					$('#area.select2').select2();
					$('#area.select2').select2({placeholder: "Area",
	allowClear: true});
					//alert(data.msg);
				}
				else{
					alert(data.msg);
				}
				}
			});

		$("#managedBy").empty();
		$.ajax({
			url : base_url+"/AllFilter/getmanagebyJenisPengelola",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
	//							$("#menu").val ( data.msg);
					$("#managedBy").append(data.msg);
					$('#managedBy.select2').select2();
					$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});

				}
				else{
					alert(data.msg);
				}
			}
			});

			//alert( "Handler for .change() called." );
		});

		$("#area ").change(function() {
				var data = {"area" : $("#area option:selected").attr("value"),
				}

			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/AllFilter/getmanagebyJenisPengelola",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
						$("#managedBy").append(data.msg);
						$('#managedBy.select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});
					}
					else{
						alert(data.msg);
					}
					}
				});
			});

$(function(){
	setTimeout(function() {
		$("#filter").trigger('click');
	},2);
	$('.select2').select2();
});

$("#submitreset ").click(function() {
$(".select2").val('').trigger('change');
$("#definisi").val('').trigger('change');
$('#definisi.select2').select2({placeholder: "Definisi",
		allowClear: true});
$("#region").val("").trigger('change');
$('#region.select2').select2({placeholder: "Wilayah",
		allowClear: true});
$("#area").val("").trigger('change');
$('#area.select2').select2({placeholder: "Area",
		allowClear: true});
$("#managerType").val("").trigger('change');
$('#managerType.select2').select2({placeholder: "Jenis Pengelola",
		allowClear: true});
$("#managedBy").val("").trigger('change');
$('#managedBy.select2').select2({placeholder: "Nama Pengelola",
		allowClear: true});
$("#terminalType").val("").trigger('change');
$('#terminalType.select2').select2({placeholder: "Jenis Mesin",
		allowClear: true});
$("#merk").val("").trigger('change');
$('#merk.select2').select2({placeholder: "Merk",
		allowClear: true});
});


});
