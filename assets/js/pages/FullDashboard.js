
$(document).ready(function(){
  var base_url = window.location.origin + "/EJBaru";

var piedataupdate = "";
var bardataMerk = "";
var bardataregion = "";
var cardRetainedRegion = "";
var cardRetainedMerk = "";
var CardRetainedNamaBank = "";
var CardRetainedNamaBanktop15 = "";
var Replenishment = "";
var StatusTrxMapping = "";
var StatusTrxBad = "";
var StatusTrxGood = "";
var StatusTrxByJenis = "";
var regiontrx = "";


var labelbardataMerk = [];
var tidakupdatebardataMerk = [];
var updatebardataMerk = [];
var labelbardataregion = [];
var tidakupdatebardataregion = [];
var updatebardataregion = [];
var labelcardRetainedRegion = [];
var jumlahcardRetained = [];
var labelcardRetainedMerk = [];
var jumlahcardRetainedMerk = [];
var labelReplenishment = [];
var totalpengisianATM = [];
var totalpengosonganCDM = [];
var totalpengisianCRM = [];
var labelStatusTrxMapping = [];
var jumlahStatusTrxMapping = [];
var labelStatusTrxBad = [];
var jumlahStatusTrxBad = [];
var labelStatusTrxGood = [];
var jumlahStatusTrxGood = [];
var labelStatusTrxByJenis= [];
var jumlahStatusTrxByJenis = [];
var labelregiontrx = [];
var jumlahtrxberhasil = [];
var jumlahtrxgagal = [];
// js_array = document.getElementById("pi").getAttribute('dataval').split(',');
// var abc = document.getElementById("bar2").getAttribute('data-bar2');
/* alert(var_dump(bardataMerk[])); */
/* console.log(abc); */
/* alert(bardataMerk.length); */

$(function() {
  var data = {"status" : null,
              "region" : null,
              "area" : null,
              "managerType" :null,
              "managedBy" : null,
              "terminalType" : null,
              "merk" : null,
              "connectivityType" : null
            };

            $.ajax({
                url : window.location.origin + "/EJBaru/FullDashboard/showData",
                type : "post",
                dataType : "json",
                data : data,
                beforeSend: function() {
                  $("#tempsearch").empty();
                      $("#tempsearch").append("<h3>Process Loading.....</h3>");
                   },
                success : function(data){
                  if( data.type === "done"){
                    piedataupdate =data.msg['piedataupdate'];
                    bardataMerk = data.msg['bardataMerk'];
                    bardataregion = data.msg['bardataregion'];
                    cardRetainedRegion =data.msg['cardRetainedRegion'];
                    cardRetainedMerk = data.msg['cardRetainedMerk'];
                    CardRetainedNamaBank = data.msg['CardRetainedNamaBank'];
                    CardRetainedNamaBanktop15 =data.msg['CardRetainedNamaBanktop15'];
                    Replenishment = data.msg['Replenishment'];
                    StatusTrxMapping = data.msg['StatusTrxMapping'];
                    StatusTrxBad =data.msg['StatusTrxBad'];
                    StatusTrxGood = data.msg['StatusTrxGood'];
                    StatusTrxByJenis = data.msg['StatusTrxByJenis'];
                    regiontrx = data.msg['regiontrx'];
                    //alert(data.msg['cekregion']);
                    cekterm = data.msg['cekterm'];
                    cekregion = data.msg['cekregion'];
                    cekarea = data.msg['cekarea'];
                    cekmanageby = data.msg['cekmanageby'];
					$("#tempsearch").append("<h3>Process Loading.....</h3>");

                    $("#piedata").empty();
          					$("#piedata").append(data.resultgridpie);
                    $("#regiondata").empty();//DG
    								$("#regiondata").append(data.resultgridregion);//DG
                    $("#merkdata").empty();//DG
    								$("#merkdata").append(data.resultgridmerk);//DG
                    $("#cardRegion").empty();//DG
    								$("#cardRegion").append(data.resultgridCardRegion);//DG
    								// $("#datanamabank5").empty();
                    // $("#datanamabank5").append(data.resultgridnamabank5);
                    $("#datanamabank10first").empty();
                    $("#datanamabank10first").append(data.resultgridnamabank10first);
                    $("#datanamabank10next").empty();
                    $("#datanamabank10next").append(data.resultgridnamabank10next);
                    $("#datanamabank").empty();
                    $("#datanamabank").append(data.resultgridnamabank);
                    $("#resulttransregion").empty();
                    $("#resulttransregion").append(data.resulttransregion);
                    $("#datajenistrans5").empty();
                    $("#datajenistrans5").append(data.resultgridjenistrans5);
                    $("#datajenistrans").empty();
                    $("#datajenistrans").append(data.resultgridjenistrans);
                    $("#datajenistransfirst").empty();
                    $("#datajenistransfirst").append(data.resultgridjenistransfirst);
                    $("#datajenistransnext").empty();
                    $("#datajenistransnext").append(data.resultgridjenistransnext);
                    

                    $("#pi").empty();
                    $("#bar2").empty();
                    $("#bar2full").empty();
                    $("#bar").empty();
                    $("#barfull").empty();
                    $("#bar5full").empty();
                    $("#horbar2").empty();
                    $("#barfull").empty();
                    $("#bar3").empty();
                    $("#bar3full").empty();
                    $("#horizontalBar").empty();
                    $("#bar").empty();
                    $("#bar71").empty();
                    $("#bar72").empty();
                    $("#bar73").empty();
					$("#tempsearch").empty();

totalpieupdate= [];
totalpienotupdate = [];
for(i=0;i<piedataupdate.length;i++){
  totalpieupdate.push(piedataupdate[i]['Update']);
  totalpienotupdate.push(piedataupdate[i]['notUpdate']);
}

labelbardataMerk = [];
tidakupdatebardataMerk = [];
updatebardataMerk = [];
for(i=0;i<bardataMerk.length;i++){
	labelbardataMerk.push(bardataMerk[i]['merk']);
	tidakupdatebardataMerk.push(bardataMerk[i]['notUpdate']);
	updatebardataMerk.push(bardataMerk[i]['Update']);
}

labelbardataregion = [];
tidakupdatebardataregion = [];
updatebardataregion = [];
for(i=0;i<bardataregion.length;i++){
	labelbardataregion.push(bardataregion[i]['regionAlias']);
	tidakupdatebardataregion.push(bardataregion[i]['notUpdate']);
	updatebardataregion.push(bardataregion[i]['Update']);
}

//card retained by Region
labelcardRetainedRegion = [];
jumlahcardRetained = [];
totalcardRetainedAll= 0;
for(i=0;i<cardRetainedRegion.length;i++){
	labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
	jumlahcardRetained.push(cardRetainedRegion[i]['CardRetained']);
  totalcardRetainedAll = parseInt(totalcardRetainedAll) + parseInt(cardRetainedRegion[i]['CardRetained']);
}

//card retained by Merk
labelcardRetainedMerk = [];
jumlahcardRetainedMerk = [];
for(i=0;i<cardRetainedMerk.length;i++){
	labelcardRetainedMerk.push(cardRetainedMerk[i]['merk']);
	jumlahcardRetainedMerk.push(cardRetainedMerk[i]['Card Retained']);
}

labelReplenishment = [];
totalpengisianATM = [];
totalpengosonganCDM = [];
totalpengisianCRM = [];
for(i=0;i<Replenishment.length;i++){
  //if(i%2==0)
  labelReplenishment.push(Replenishment[i]['terminaltype']);

  if(Replenishment[i]['terminaltype'] == "ATM" ){
    totalpengisianATM.push(Replenishment[i]['Total']);
  /* if(Replenishment[i]['JenisTrx'] == "Pengosongan")
    totalpengisian.push(Replenishment[i]['Total']); */
    }else if(Replenishment[i]['terminaltype'] == "CDM" ){
    totalpengosonganCDM.push(Replenishment[i]['Total']);
    }else if(Replenishment[i]['terminaltype'] == "CRM"){
    totalpengisianCRM.push(Replenishment[i]['Total'])
  }
}


//Mapping Status Transaksi
labelStatusTrxMapping = [];
jumlahStatusTrxMapping = [];
for(i=0;i<StatusTrxMapping.length;i++){
	labelStatusTrxMapping.push(StatusTrxMapping[i]['Status Transaksi']);
	jumlahStatusTrxMapping.push(StatusTrxMapping[i]['Jumlah']);
}

//Top 10 Machine Bad Status Transaksi
labelStatusTrxBad = [];
jumlahStatusTrxBad = [];
for(i=0;i<StatusTrxBad.length;i++){
	labelStatusTrxBad.push(StatusTrxBad[i]['TermID']);
	jumlahStatusTrxBad.push(StatusTrxBad[i]['Gagal']);
}

//Top 10 Machine Good Status Transaksi
labelStatusTrxGood = [];
jumlahStatusTrxGood = [];
for(i=0;i<StatusTrxGood.length;i++){
	labelStatusTrxGood.push(StatusTrxGood[i]['TermID']);
	jumlahStatusTrxGood.push(StatusTrxGood[i]['Sukses']);
}

//Status Transaksi berdasarkan Jenis Transaksi
labelStatusTrxByJenis= [];
jumlahStatusTrxByJenis = [];
for(i=0;i<StatusTrxByJenis.length;i++){
	labelStatusTrxByJenis.push(StatusTrxByJenis[i]['transactiongroup']);
	jumlahStatusTrxByJenis.push(StatusTrxByJenis[i]['Jumlah']);
}

//Status Transaction by Region
labelregiontrx = [];
jumlahtrxberhasil = [];
jumlahtrxgagal = [];

totaljumlahtrxgagal = 0;
totaljumlahtrxberhasil = 0;
for(i=0;i<regiontrx.length;i++){
	if(i%2==0)
	labelregiontrx.push (regiontrx[i]['Region']);

	if(regiontrx[i]['Status'] == "Gagal"){
        jumlahtrxgagal.push(regiontrx[i]['Jumlah']);
        totaljumlahtrxgagal  = parseInt(totaljumlahtrxgagal) + parseInt(regiontrx[i]['Jumlah']);
    }else{
        jumlahtrxberhasil.push(regiontrx[i]['Jumlah']);
        totaljumlahtrxberhasil  = parseInt( totaljumlahtrxberhasil) + parseInt(regiontrx[i]['Jumlah']);
      }
    }

function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
}
var tablegagalsukses="";
tablegagalsukses += "<table id=\"jenistrans\" class=\"table table-bordered table-striped\" style=\"font-size:8px;margin-left:5%;padding:0.4rem!important\"><tr><td><center>Total Transaksi Gagal</center></td><td><center>"+formatNumber(totaljumlahtrxgagal)+"</center></td></tr><tr><td><center>Total Transaksi Berhasil</center></td><td><center>"+formatNumber(totaljumlahtrxberhasil)+"</center></td></tr></table>";
$("#tablegagalsukses").empty();
$("#tablegagalsukses").append(tablegagalsukses);

let bar = document.getElementById("bar");
bar.height = 2.5;
let bar_chart = new Chart(bar, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[
        {
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
        {
          label: 'Tidak Update',
          data:tidakupdatebardataregion,
          backgroundColor:'rgba(252, 209, 22, 1)',
          borderWidth:1,
          borderColor:'rgba(252, 209, 22, 1)',
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
      }
        ]
            },
            options: {
                  scales:{
                    xAxes: [{
                        gridLines:{
                          display:false,
                          drawBorder:false,
                        }
                    }],
                    yAxes: [{
                        ticks:{
                          display:false,
							stepSize: 500,
                          suggestedMax:3000,
                        },
                        gridLines:{
                          display:true,
                          drawBorder:false,
                        }
                    }]
                },
                legend:{
                    display: true,
                    position:'right',
                    labels:{
					  fontColor: '#000',fontSize: 10,
					  // usePointStyle:true,
					  boxWidth:15,
					  padding:4,
					  // align:'bottom',
					  fontStyle: 'bold'
					}
                },
                plugins: {
                    labels:  {
                      // render: function(label){
						// return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
						// },
						render: function (args) {
              if(args.value/1000000>=1){
                // return Math.round((args.value/1000) + ' K';
                return (args.value/1000000).toFixed(2) +' M';
              }
              if(args.value/1000>=1){
                // return Math.round((args.value/1000) + ' K';
                return (args.value/1000).toFixed(2) +' K';
              }
              else {
                return args.value;
              }
            },
                      fontSize: 10,
                      fontStyle: 'bold',
                      fontColor: '#000',
                      fontFamily: '"Calibri"',
                      arc: true,
                      position: 'outside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
            }
});

let barfull = document.getElementById("barfull");

let barfull_chart = new Chart(barfull, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[
        {
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        },
        {
          label: 'Tidak Update',
          data:tidakupdatebardataregion,
          backgroundColor:'rgba(252, 209, 22, 1)',
          borderWidth:1,
          borderColor:'rgba(252, 209, 22, 1)',
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
      }
        ]
            },
            options: {
                  scales:{
                    xAxes: [{
                        gridLines:{
                          display:false,
                          drawBorder:false,
                        }
                    }],
                    yAxes: [{
                        ticks:{
                          display:false,
                          suggestedMax:3000,
                        },
                        gridLines:{
                          display:true,
                          drawBorder:false,
                        }
                    }]
                },
                legend:{
                    display: true,
                    position:'top',
                    labels:{
					  fontColor: '#000',fontSize: 10,
					  // usePointStyle:true,
					  boxWidth:15,
					  padding:4,
					  // align:'bottom',
					  fontStyle: 'bold'
					}
                },
                plugins: {
                    labels:  {
                      render: function (args) {
                        if(args.value/1000000>=1){
                          // return Math.round((args.value/1000) + ' K';
                          return (args.value/1000000).toFixed(2) +' M';
                        }
                        if(args.value/1000>=1){
                          // return Math.round((args.value/1000) + ' K';
                          return (args.value/1000).toFixed(2) +' K';
                        }
                        else {
                          return args.value;
                        }
                      },
                      fontSize: 12,
                      fontStyle: 'bold',
                      fontColor: '#000',
                      fontFamily: '"Calibri"',
                      arc: true,
                      position: 'outside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
            }
});


let pi = document.getElementById("pi");
pi.height = 18;
let pi_chart = new Chart(pi, {
    type: 'pie',
    data: {
        labels:  { render: 'value',
        fontSize: 12,
        fontStyle: 'bold',
        //fontColor: '#000',
        fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
    },
    labels:  ['Update','Tidak Update'],

        datasets: [{
            data: [totalpieupdate,totalpienotupdate],
            backgroundColor: [
                ('rgba(15, 43, 91, 2)'),
                ('rgba(252, 209, 22, 1)'),
            ],hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0',
            fill: true,
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: [2,2]
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              /*render: function (args) {
                    if(args.value/1000000>=1){
                      return args.value/1000000 + ' M';
                    }
                    else {
                      return args.value/1000 + ' K';
                    }
                },*/
                render:'percentage',
              fontSize: 18,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar2 = document.getElementById("bar2");
let bar2_chart = new Chart(bar2, {
    type: 'bar',
    data: {
        labels: labelbardataMerk,
        datasets: [{
        label: "Update",
            data: updatebardataMerk,
            backgroundColor:

                "rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },
        {
          label: "Tidak Update",
          data: tidakupdatebardataMerk,
          backgroundColor:
          "rgba(252, 209, 22, 1)",
          borderColor: [
              ('#fff'),
              ('#fff'),
              ('#fff'),
              ('#fff')
          ],
          borderWidth: 1
      },]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
				  suggestedMax:14000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'right',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    },
});

let bar2full = document.getElementById("bar2full");
let bar2full_chart = new Chart(bar2full, {
    type: 'bar',
    data: {
        labels: labelbardataMerk,
        datasets: [{
        label: "Update",
            data: updatebardataMerk,
            backgroundColor:

                "rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },
        {
          label: "Tidak Update",
          data: tidakupdatebardataMerk,
          backgroundColor:
          "rgba(252, 209, 22, 1)",
          borderColor: [
              ('#fff'),
              ('#fff'),
              ('#fff'),
              ('#fff')
          ],
          borderWidth: 1
      },]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:14000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    },
});

let horizontalBar = document.getElementById("horizontalBar");
let horizontalBar_chart = new Chart(horizontalBar, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxMapping,
        datasets: [{

            data: jumlahStatusTrxMapping,
            backgroundColor:
            "rgba(117, 178, 221, 1)",
            borderColor: "rgba(117, 178, 221, 1)",
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				stepSize:2000000,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              marginleft:100,
            }
    }
    }
});

let horbar2 = document.getElementById("horbar2");
let horbar2_chart = new Chart(horbar2, {
    type: 'horizontalBar',
    data: {
        labels:labelcardRetainedMerk,
        datasets: [{
            label: "Kartu Tertelan",
            data:jumlahcardRetainedMerk,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
        				beginAtZero:true,
        				suggestedMax:1100,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 11,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
            }
    }
    }

});

let bar3 = document.getElementById("bar3");
let bar3_chart = new Chart(bar3, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  // stepSize:50000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar3full = document.getElementById("bar3full");
let bar3full_chart = new Chart(bar3full, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  // stepSize:50000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar71 = document.getElementById("bar71");
let bar71_chart = new Chart(bar71, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengisianATM,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
                
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri"',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});

let bar72 = document.getElementById("bar72");
let bar72_chart = new Chart(bar72, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengosonganCDM,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
                
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri"',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});

let bar73 = document.getElementById("bar73");
let bar73_chart = new Chart(bar73, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengisianCRM,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
               
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri"',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});



let bar5 = document.getElementById("bar5");
let bar5_chart = new Chart(bar5, {
    type: 'bar',
    data: {
        labels:labelcardRetainedRegion,
        datasets: [{
          label: "Kartu Tertelan",
            data: jumlahcardRetained,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:10,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
            fontSize: 12,
            fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
        },
    }
    }
});

let bar5full = document.getElementById("bar5full");
let bar5full_chart = new Chart(bar5full, {
    type: 'bar',
    data: {
        labels:labelcardRetainedRegion,
        datasets: [{
          label: "Kartu Tertelan",
            data: jumlahcardRetained,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:10,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
            fontSize: 10,
            fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
        },
    }
    }
});

let horbar4 = document.getElementById("horbar4");
horbar4.height=8;
let horbar4_chart = new Chart(horbar4, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxBad,
        datasets: [{
            label: "Top 10 Machine Bad Performance",
            data: jumlahStatusTrxBad,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:600,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
            }
    }
    }
});


let horbar5 = document.getElementById("horbar5");
horbar5.height=8;
let horbar5_chart = new Chart(horbar5, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxGood,
        datasets: [{
            label: "Top 10 Machine Good Performance",
            data: jumlahStatusTrxGood,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
				suggestedMax:1500,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
            }
    }
    }
});


let pie5 = document.getElementById("pie5");
let pie5_chart = new Chart(pie5, {
    type: 'pie',
    data: {
        labels: labelStatusTrxByJenis,
        datasets: [{
            data: jumlahStatusTrxByJenis,
            backgroundColor: [
                ('yellow'),
                ('brown'),
                ('rgb(117, 178, 221)'),
				('rgb(44, 59, 65)'),
				('rgb(15, 43, 91, 2)'),
				('red'),
				('orange'),
				('green'),
				('purple'),
            ],
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'left',

            labels:{
              fontColor: '#000',fontSize: 10,
              // usePointStyle:true,
              boxWidth:15,
              padding:4,
              // align:'bottom',
              fontStyle: 'bold'
            }
        },
		plugins: {
                    labels:  {
                      /*render: function (args) {
                            if(args.value/1000000>=1){
                              return args.value/1000000 + ' M';
                            }
                            else {
                              return args.value/1000 + ' K';
                            }
                        },*/
                        render:'percentage',
                      fontSize: 16,
                      fontStyle: 'bold',
                      fontColor: '#fff',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: false,
                      position: 'inside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
    }
});

let bar31 = document.getElementById("bar31");
let bar31_chart = new Chart(bar31, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:75000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar31full = document.getElementById("bar31full");
let bar31full_chart = new Chart(bar31full, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:500,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 8,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let horizontalBar1 = document.getElementById("horizontalBar1");
let horizontalBar1_chart = new Chart(horizontalBar1, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxMapping,
        datasets: [{

            data: jumlahStatusTrxMapping,
            backgroundColor:
            "rgba(117, 178, 221, 1)",
            borderColor: "rgba(117, 178, 221, 1)",
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
                // stepSize:100000,
                // suggestedMax:200000,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,
                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
                if(args.value/1000000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000000).toFixed(2) +' M';
                }
                if(args.value/1000>=1){
                  // return Math.round((args.value/1000) + ' K';
                  return (args.value/1000).toFixed(2) +' K';
                }
                else {
                  return args.value;
                }
              },
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
              marginleft:100,
            }
    }
    }
});

let pie51 = document.getElementById("pie51");
let pie51_chart = new Chart(pie51, {
    type: 'pie',
    data: {
        labels: labelStatusTrxByJenis,
        datasets: [{
            data: jumlahStatusTrxByJenis,
            backgroundColor: [
                ('yellow'),
                ('brown'),
                ('rgb(117, 178, 221)'),
				('rgb(44, 59, 65)'),
				('rgb(15, 43, 91, 2)'),
				('red'),
				('orange'),
				('green'),
				('purple'),
            ],
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'left',

            labels:{
              fontColor: '#000',fontSize: 10,
              // usePointStyle:true,
              boxWidth:15,
              padding:4,
              // align:'bottom',
              fontStyle: 'bold'
            }
        },
		plugins: {
                    labels:  {
                      /*render: function (args) {
                            if(args.value/1000000>=1){
                              return args.value/1000000 + ' M';
                            }
                            else {
                              return args.value/1000 + ' K';
                            }
                        },*/
                        render:'percentage',
                      fontSize: 16,
                      fontStyle: 'bold',
                      fontColor: '#fff',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: false,
                      position: 'inside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
    }
});


let horbar41 = document.getElementById("horbar41");
let horbar41_chart = new Chart(horbar41, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxBad,
        datasets: [{
            label: "Top 10 Machine Bad Performance",
            data: jumlahStatusTrxBad,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:600,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
            }
    }
    }
});


let horbar51 = document.getElementById("horbar51");
let horbar51_chart = new Chart(horbar51, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxGood,
        datasets: [{
            label: "Top 10 Machine Good Performance",
            data: jumlahStatusTrxGood,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
				suggestedMax:1500,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
             render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri"',
            }
          }
        }
      });
    }
  }
  });
});

});
