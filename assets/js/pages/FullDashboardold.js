
$(document).ready(function(){


js_array = document.getElementById("pi").getAttribute('dataval').split(',');

 var abc = document.getElementById("bar2").getAttribute('data-bar2');
/* alert(var_dump(bardataMerk[])); */
/* console.log(abc); */
/* alert(bardataMerk.length); */
var labelbardataMerk = [];
var tidakupdatebardataMerk = [];
var updatebardataMerk = [];
for(i=0;i<bardataMerk.length;i++){
	labelbardataMerk.push(bardataMerk[i]['merk']);
	tidakupdatebardataMerk.push(bardataMerk[i]['notUpdate']);
	updatebardataMerk.push(bardataMerk[i]['Update']);
}

var labelbardataregion = [];
var tidakupdatebardataregion = [];
var updatebardataregion = [];
for(i=0;i<bardataregion.length;i++){
	labelbardataregion.push(bardataregion[i]['regionAlias']);
	tidakupdatebardataregion.push(bardataregion[i]['notUpdate']);
	updatebardataregion.push(bardataregion[i]['Update']);
}

//card retained by Region
var labelcardRetainedRegion = [];
var jumlahcardRetained = [];
for(i=0;i<cardRetainedRegion.length;i++){
	labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
	jumlahcardRetained.push(cardRetainedRegion[i]['CardRetained']);
}

//card retained by Merk
 var labelcardRetainedMerk = [];
var jumlahcardRetainedMerk = [];
for(i=0;i<cardRetainedMerk.length;i++){
	labelcardRetainedMerk.push(cardRetainedMerk[i]['merk']);
	jumlahcardRetainedMerk.push(cardRetainedMerk[i]['Card Retained']);
}

var labelReplenishment = [];
var totalpengisian = [];
var totalpengosongan = [];
for(i=0;i<Replenishment.length;i++){
	//if(i%2==0)
	labelReplenishment.push(Replenishment[i]['terminaltype']);

	if(Replenishment[i]['JenisTrx'] == "Pengisian" )
        totalpengisian.push(Replenishment[i]['Total']);
	/* if(Replenishment[i]['JenisTrx'] == "Pengosongan")
		totalpengisian.push(Replenishment[i]['Total']); */
    else
        totalpengosongan.push(Replenishment[i]['Total']);
}

// var labelReplenishment = [];
// var totalpengisian = [];
// var totalpengosongan = [];
// for(i=0;i<Replenishment.length;i++){
// 	if(i%2==0)
// 	labelReplenishment.push (Replenishment[i]['terminaltype']);
//
// 	if(Replenishment[i]['JenisTrx'] == "Pengisian")
//         totalpengisian.push(Replenishment[i]['Total']);
//     else
//         totalpengosongan.push(Replenishment[i]['Total']);
// }

//Mapping Status Transaksi
var labelStatusTrxMapping = [];
var jumlahStatusTrxMapping = [];
for(i=0;i<StatusTrxMapping.length;i++){
	labelStatusTrxMapping.push(StatusTrxMapping[i]['Status Transaksi']);
	jumlahStatusTrxMapping.push(StatusTrxMapping[i]['Jumlah']);
}

//Top 10 Machine Bad Status Transaksi
var labelStatusTrxBad = [];
var jumlahStatusTrxBad = [];
for(i=0;i<StatusTrxBad.length;i++){
	labelStatusTrxBad.push(StatusTrxBad[i]['TermID']);
	jumlahStatusTrxBad.push(StatusTrxBad[i]['Gagal']);
}

//Top 10 Machine Good Status Transaksi
var labelStatusTrxGood = [];
var jumlahStatusTrxGood = [];
for(i=0;i<StatusTrxGood.length;i++){
	labelStatusTrxGood.push(StatusTrxGood[i]['TermID']);
	jumlahStatusTrxGood.push(StatusTrxGood[i]['Sukses']);
}

//Status Transaksi berdasarkan Jenis Transaksi
var labelStatusTrxByJenis= [];
var jumlahStatusTrxByJenis = [];
for(i=0;i<StatusTrxByJenis.length;i++){
	labelStatusTrxByJenis.push(StatusTrxByJenis[i]['transactiongroup']);
	jumlahStatusTrxByJenis.push(StatusTrxByJenis[i]['Jumlah']);
}

//Status Transaction by Region
var labelregiontrx = [];
var jumlahtrxberhasil = [];
var jumlahtrxgagal = [];
for(i=0;i<regiontrx.length;i++){
	if(i%2==0)
	labelregiontrx.push (regiontrx[i]['Region']);

	if(regiontrx[i]['Status'] == "Gagal")
        jumlahtrxgagal.push(regiontrx[i]['Jumlah']);
    else
        jumlahtrxberhasil.push(regiontrx[i]['Jumlah']);
}






let bar = document.getElementById("bar");

let bar_chart = new Chart(bar, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[{
            label: 'Tidak Update',
            data:tidakupdatebardataregion,
            backgroundColor:'rgba(252, 209, 22, 1)',
            borderWidth:1,
            borderColor:'rgba(252, 209, 22, 1)',
            hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0'
        },
        {
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        }
        ]
            },
            options: {
                  scales:{
                    xAxes: [{
                        gridLines:{
                          display:false,
                          drawBorder:false,
                        }
                    }],
                    yAxes: [{
                        ticks:{
                          display:false,
							stepSize: 500,
                          suggestedMax:3000,
                        },
                        gridLines:{
                          display:true,
                          drawBorder:false,
                        }
                    }]
                },
                legend:{
                    display: true,
                    position:'right',
                    labels:{
					  fontColor: '#000',fontSize: 10,
					  // usePointStyle:true,
					  boxWidth:15,
					  padding:4,
					  // align:'bottom',
					  fontStyle: 'bold'
					}
                },
                plugins: {
                    labels:  {
                      // render: function(label){
						// return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
						// },
						render: function (args) {
						if(args.value/1000>=1){
						  return Math.floor(args.value/1000) + ' K';
						}
						else {
						  return args.value;
						}
					},
                      fontSize: 10,
                      fontStyle: 'bold',
                      fontColor: '#000',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: true,
                      position: 'outside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
            }
});

let barfull = document.getElementById("barfull");

let barfull_chart = new Chart(barfull, {
    type: 'bar',
    data: {
        labels:labelbardataregion,
        datasets:[{
            label: 'Tidak Update',
            data:tidakupdatebardataregion,
            backgroundColor:'rgba(252, 209, 22, 1)',
            borderWidth:1,
            borderColor:'rgba(252, 209, 22, 1)',
            hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0'
        },
        {
          label: 'Update',
          data:updatebardataregion,
          backgroundColor:"rgba(15, 43, 91, 2)",
          borderWidth:1,
          borderColor:"rgba(15, 43, 91, 2)",
          hoverBorderWidth: 2,
          hoverBorderColor:'#d1ccc0'
        }
        ]
            },
            options: {
                  scales:{
                    xAxes: [{
                        gridLines:{
                          display:false,
                          drawBorder:false,
                        }
                    }],
                    yAxes: [{
                        ticks:{
                          display:false,
                          suggestedMax:3000,
                        },
                        gridLines:{
                          display:true,
                          drawBorder:false,
                        }
                    }]
                },
                legend:{
                    display: true,
                    position:'top',
                    labels:{
					  fontColor: '#000',fontSize: 10,
					  // usePointStyle:true,
					  boxWidth:15,
					  padding:4,
					  // align:'bottom',
					  fontStyle: 'bold'
					}
                },
                plugins: {
                    labels:  {
                      render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
                      fontSize: 12,
                      fontStyle: 'bold',
                      fontColor: '#000',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: true,
                      position: 'outside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
            }
});


let pi = document.getElementById("pi");
let pi_chart = new Chart(pi, {
    type: 'pie',
    data: {
        labels:  { render: 'value',
        fontSize: 12,
        fontStyle: 'bold',
        //fontColor: '#000',
        fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
    },
    labels:  ['Tidak Update','Update'],

        datasets: [{
            data: [js_array[2], js_array[1]],
            backgroundColor: [
                ('rgba(252, 209, 22, 1)'),
                ('rgba(15, 43, 91, 2)'),
            ],hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0',
            fill: true,
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: [2,2]
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              /*render: function (args) {
                    if(args.value/1000000>=1){
                      return args.value/1000000 + ' M';
                    }
                    else {
                      return args.value/1000 + ' K';
                    }
                },*/
                render:'percentage',
              fontSize: 18,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri"',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar2 = document.getElementById("bar2");
let bar2_chart = new Chart(bar2, {
    type: 'bar',
    data: {
        labels: labelbardataMerk,
        datasets: [{
            label: "Tidak Update",
            data: tidakupdatebardataMerk,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Update",
            data: updatebardataMerk,
            backgroundColor:

                "rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
				  suggestedMax:14000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'right',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    },
});

let bar2full = document.getElementById("bar2full");
let bar2full_chart = new Chart(bar2full, {
    type: 'bar',
    data: {
        labels: labelbardataMerk,
        datasets: [{
            label: "Tidak Update",
            data: tidakupdatebardataMerk,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Update",
            data: updatebardataMerk,
            backgroundColor:

                "rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:14000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    },
});

let horizontalBar = document.getElementById("horizontalBar");
let horizontalBar_chart = new Chart(horizontalBar, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxMapping,
        datasets: [{

            data: jumlahStatusTrxMapping,
            backgroundColor:
            "rgba(117, 178, 221, 1)",
            borderColor: "rgba(117, 178, 221, 1)",
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				stepSize:2000000,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              marginleft:100,
            }
    }
    }
});

let horbar2 = document.getElementById("horbar2");
let horbar2_chart = new Chart(horbar2, {
    type: 'horizontalBar',
    data: {
        labels:labelcardRetainedMerk,
        datasets: [{
            label: "Kartu Tertelan",
            data:jumlahcardRetainedMerk,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
        				beginAtZero:true,
        				suggestedMax:4,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 11,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }

});

let bar3 = document.getElementById("bar3");
let bar3_chart = new Chart(bar3, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:15000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar3full = document.getElementById("bar3full");
let bar3full_chart = new Chart(bar3full, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:500,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
			  fontColor: '#000',fontSize: 10,
			  // usePointStyle:true,
			  boxWidth:15,
			  padding:4,
			  // align:'bottom',
			  fontStyle: 'bold'
			}
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar71 = document.getElementById("bar71");
let bar71_chart = new Chart(bar71, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengisian,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
				render: function (args) {
					if(args.value/1000>=1){
					  return Math.floor(args.value/1000) + ' K';
					}
					else {
					  return args.value;
					}
				},
                render:'value',
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});

let bar72 = document.getElementById("bar72");
let bar72_chart = new Chart(bar72, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengosongan,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
					if(args.value/1000>=1){
					  return Math.floor(args.value/1000) + ' K';
					}
					else {
					  return args.value;
					}
				},
                render:'value',
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});

let bar73 = document.getElementById("bar73");
let bar73_chart = new Chart(bar73, {
    type: 'doughnut',
    data: {
        labels: labelReplenishment,
        datasets: [{
          label: "Pengisian",
              data: totalpengisian,
              backgroundColor:"rgba(15, 43, 91, 2)",
              borderColor: [
                  ('#fff'),
                  ('#fff'),
                  ('#fff'),
                  ('#fff')
              ],
              borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: false,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
					if(args.value/1000>=1){
					  return Math.floor(args.value/1000) + ' K';
					}
					else {
					  return args.value;
					}
				},
                render:'value',
				position: 'inside',
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
		}
    }
});

// let bar74 = document.getElementById("bar74");
// let bar74_chart = new Chart(bar74, {
//     type: 'doughnut',
//     data: {
//         labels: labelReplenishment,
//         datasets: [{
//           label: "Pengisian",
//               data: totalpengisian,
//               backgroundColor:"rgba(15, 43, 91, 2)",
//               borderColor: [
//                   ('#fff'),
//                   ('#fff'),
//                   ('#fff'),
//                   ('#fff')
//               ],
//               borderWidth: 1
//         }]
//     },
//     options: {
//
//             yAxes: [{
//                 ticks: {
//                     beginAtZero:true
//                 }
//             }]
//         ,
//         legend:{
//             display: false,
//             position:'top',
//             labels:{
//                 fontColor: '#000',fontSize: 10,
//                 fontStyle: 'bold',
//             }
//         },
//         plugins: {
//             labels:  {
//               render: function (args) {
//                     if(args.value/1000000>=1){
//                       return args.value/1000000 + ' M';
//                     }
//                     else {
//                       return args.value/1000 + ' K';
//                     }
//                 },
//                 render:'value',
// 				position: 'inside',
//               fontSize: 10,
//               fontStyle: 'bold',
//               fontColor: '#fff',
//               fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
//               arc:false,
//               overlap: true,
//               outsidePadding: 24,
//             }
// 		}
//     }
// });

let bar5 = document.getElementById("bar5");
let bar5_chart = new Chart(bar5, {
    type: 'bar',
    data: {
        labels:labelcardRetainedRegion,
        datasets: [{
          label: "Kartu Tertelan",
            data: jumlahcardRetained,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:10,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
            fontSize: 12,
            fontStyle: 'bold',
            fontColor: '#000',
            fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
        },
    }
    }
});

let bar5full = document.getElementById("bar5full");
let bar5full_chart = new Chart(bar5full, {
    type: 'bar',
    data: {
        labels:labelcardRetainedRegion,
        datasets: [{
          label: "Kartu Tertelan",
            data: jumlahcardRetained,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  suggestedMax:10,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
            fontSize: 10,
            fontStyle: 'bold',
            fontColor: '#000',
            fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
        },
    }
    }
});

let horbar4 = document.getElementById("horbar4");
let horbar4_chart = new Chart(horbar4, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxBad,
        datasets: [{
            label: "Top 10 Machine Bad Performance",
            data: jumlahStatusTrxBad,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:600,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});


let horbar5 = document.getElementById("horbar5");
let horbar5_chart = new Chart(horbar5, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxGood,
        datasets: [{
            label: "Top 10 Machine Good Performance",
            data: jumlahStatusTrxGood,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
				suggestedMax:1500,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
				ticks:{
				fontSize: 9,
              },
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});


let pie5 = document.getElementById("pie5");
let pie5_chart = new Chart(pie5, {
    type: 'pie',
    data: {
        labels: labelStatusTrxByJenis,
        datasets: [{
            data: jumlahStatusTrxByJenis,
            backgroundColor: [
                ('yellow'),
                ('brown'),
                ('rgb(117, 178, 221)'),
				('rgb(44, 59, 65)'),
				('rgb(15, 43, 91, 2)'),
				('red'),
				('orange'),
				('green'),
				('purple'),
            ],
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'left',

            labels:{
              fontColor: '#000',fontSize: 10,
              // usePointStyle:true,
              boxWidth:15,
              padding:4,
              // align:'bottom',
              fontStyle: 'bold'
            }
        },
		plugins: {
                    labels:  {
                      /*render: function (args) {
                            if(args.value/1000000>=1){
                              return args.value/1000000 + ' M';
                            }
                            else {
                              return args.value/1000 + ' K';
                            }
                        },*/
                        render:'percentage',
                      fontSize: 16,
                      fontStyle: 'bold',
                      fontColor: '#fff',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: false,
                      position: 'inside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
    }
});

let bar31 = document.getElementById("bar31");
let bar31_chart = new Chart(bar31, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:75000,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let bar31full = document.getElementById("bar31full");
let bar31full_chart = new Chart(bar31full, {
    type: 'bar',
    data: {
        labels: labelregiontrx,
        datasets: [{
            label: "Gagal",
            data: jumlahtrxgagal,
            backgroundColor:
            "rgba(252, 209, 22, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        },{
        label: "Sukses",
            data: jumlahtrxberhasil,
            backgroundColor:"rgba(15, 43, 91, 2)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
                gridLines:{
                  display:false,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                ticks:{
                  display:false,
                  stepSize:500,
                },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }]
        },
        legend:{
            display: true,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 10,
              fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 10,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc: true,
              position: 'outside',
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

let horizontalBar1 = document.getElementById("horizontalBar1");
let horizontalBar1_chart = new Chart(horizontalBar1, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxMapping,
        datasets: [{

            data: jumlahStatusTrxMapping,
            backgroundColor:
            "rgba(117, 178, 221, 1)",
            borderColor: "rgba(117, 178, 221, 1)",
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				stepSize:180000,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              marginleft:100,
            }
    }
    }
});

let pie51 = document.getElementById("pie51");
let pie51_chart = new Chart(pie51, {
    type: 'pie',
    data: {
        labels: labelStatusTrxByJenis,
        datasets: [{
            data: jumlahStatusTrxByJenis,
            backgroundColor: [
                ('yellow'),
                ('brown'),
                ('rgb(117, 178, 221)'),
				('rgb(44, 59, 65)'),
				('rgb(15, 43, 91, 2)'),
				('red'),
				('orange'),
				('green'),
				('purple'),
            ],
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'left',

            labels:{
              fontColor: '#000',fontSize: 10,
              // usePointStyle:true,
              boxWidth:15,
              padding:4,
              // align:'bottom',
              fontStyle: 'bold'
            }
        },
		plugins: {
                    labels:  {
                      /*render: function (args) {
                            if(args.value/1000000>=1){
                              return args.value/1000000 + ' M';
                            }
                            else {
                              return args.value/1000 + ' K';
                            }
                        },*/
                        render:'percentage',
                      fontSize: 16,
                      fontStyle: 'bold',
                      fontColor: '#fff',
                      fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
                      arc: false,
                      position: 'inside',
                      overlap: true,
                      outsidePadding: 24,
                    }
            }
    }
});

// let bar71 = document.getElementById("bar71");
// let bar71_chart = new Chart(bar71, {
    // type: 'bar',
    // data: {
        // labels: labelReplenishment,
        // datasets: [{
          // label: "Pengisian",
              // data: totalpengisian,
              // backgroundColor:"rgba(15, 43, 91, 2)",
              // borderColor: [
                  // ('#fff'),
                  // ('#fff'),
                  // ('#fff'),
                  // ('#fff')
              // ],
              // borderWidth: 1
        // },{
          // label: "Pengosongan",
          // data: totalpengosongan,
          // backgroundColor:
          // "rgba(252, 209, 22, 1)",
          // borderColor: [
              // ('#fff'),
              // ('#fff'),
              // ('#fff'),
              // ('#fff')
          // ],
          // borderWidth: 1
        // }]
    // },
    // options: {
          // scales:{
            // xAxes: [{
                // gridLines:{
                  // display:false,
                  // drawBorder:false,
                // }
            // }],
            // yAxes: [{
                // ticks:{
                  // display:false,
                  // stepSize:750,
                // },
                // gridLines:{
                  // display:true,
                  // drawBorder:false,
                // }
            // }]
        // },
        // legend:{
            // display: true,
            // position:'top',
            // labels:{
              // fontColor: '#000',fontSize: 12,
              // fontStyle: 'bold',
            // }
        // },
        // plugins: {
            // labels:  {
              // render: function(label){
                // return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                // },
              // fontSize: 12,
              // fontStyle: 'bold',
              // fontColor: '#000',
              // fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              // arc: true,
              // position: 'outside',
              // overlap: true,
              // outsidePadding: 24,
            // }
    // }
    // }
// });

let horbar41 = document.getElementById("horbar41");
let horbar41_chart = new Chart(horbar41, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxBad,
        datasets: [{
            label: "Top 10 Machine Bad Performance",
            data: jumlahStatusTrxBad,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
				('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
				beginAtZero:true,
				suggestedMax:600,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});


let horbar51 = document.getElementById("horbar51");
let horbar51_chart = new Chart(horbar51, {
    type: 'horizontalBar',
    data: {
        labels: labelStatusTrxGood,
        datasets: [{
            label: "Top 10 Machine Good Performance",
            data: jumlahStatusTrxGood,
            backgroundColor: "rgba(117, 178, 221, 1)",
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
				('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: 1
        }]
    },
    options: {
          scales:{
            xAxes: [{
              ticks:{
                display:false,
                beginAtZero:true,
				suggestedMax:1500,
              },
                gridLines:{
                  display:true,
                  drawBorder:false,
                }
            }],
            yAxes: [{
                gridLines:{
                  display:false,

                }
            }]
        },
        legend:{
            display: false,
            position:'top',
            labels:{
              fontColor: '#000',fontSize: 12,
              fontStyle: 'bold'
            }
        },
		plugins: {
            labels:  {
             render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
              fontSize: 12,
              fontStyle: 'bold',
              fontColor: '#000',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
            }
    }
    }
});

let doughnut1 = document.getElementById("doughnut1");
let doughnut1_chart = new Chart(doughnut1, {
    type: 'doughnut',
    data: {
        labels:  { render: 'value',
        fontSize: 12,
        fontStyle: 'bold',
        //fontColor: '#000',
        fontFamily: '"Calibri,Lucida Console", Monaco, monospace'
    },
    labels:  ['Tidak Update','Update'],

        datasets: [{
            data: [js_array[2], js_array[1]],
            backgroundColor: [
                ('rgba(252, 209, 22, 1)'),
                ('rgba(15, 43, 91, 2)'),
            ],hoverBorderWidth: 2,
            hoverBorderColor:'#d1ccc0',
            fill: true,
            borderColor: [
                ('#fff'),
                ('#fff'),
                ('#fff'),
                ('#fff')
            ],
            borderWidth: [2,2]
        }]
    },
    options: {

            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        ,
        legend:{
            display: true,
            position:'top',
            labels:{
                fontColor: '#000',fontSize: 10,
                fontStyle: 'bold',
            }
        },
        plugins: {
            labels:  {
              render: function (args) {
												if(args.value/1000>=1){
												  return Math.floor(args.value/1000) + ' K';
												}
												else {
												  return args.value;
												}
											},
                render:'percentage',
              fontSize: 18,
              fontStyle: 'bold',
              fontColor: '#fff',
              fontFamily: '"Calibri,Lucida Console", Monaco, monospace',
              arc:false,
              overlap: true,
              outsidePadding: 24,
            }
    }
    }
});

});
