 $(function () {
	$('#mydata').DataTable({
		"mydata_length": [[10, 25, 50, -1], [10, 25, 50, "All"]]
	});
        $("#menulist-7").change(function () {
            if ($(this).is(":checked")) {
               $("#menulist-10").prop("checked",true);
               $("#menulist-11").prop("checked",true);
               $("#menulist-13").prop("checked",true);
               $("#menulist-14").prop("checked",true);
               $("#menulist-15").prop("checked",true);
               $("#menulist-16").prop("checked",true);
               $("#menulist-17").prop("checked",true);
               $("#menulist-18").prop("checked",true);
			   $("#menulist-19").prop("checked",true);
			   //tambahan type definition
			   $("#menulist-25").prop("checked",true);
            } else {
					$("#menulist-10").prop("checked",false);
				   $("#menulist-11").prop("checked",false);
				   $("#menulist-13").prop("checked",false);
				   $("#menulist-14").prop("checked",false);
				   $("#menulist-15").prop("checked",false);
				   $("#menulist-16").prop("checked",false);
				   $("#menulist-17").prop("checked",false);
				   $("#menulist-18").prop("checked",false);
				   $("#menulist-19").prop("checked",false);
				   //tambahan type definition
				   $("#menulist-25").prop("checked",false);
            }
        });
		
		$("#menulist-10, #menulist-11,#menulist-13,#menulist-14,#menulist-15,#menulist-16,#menulist-17,#menulist-18,#menulist-19,#menulist-25").change(function () {
            if ($(this).is(":checked")) {
               $("#menulist-7").prop("checked",true);
            } 
        });
		
		$("#menulist-3").change(function () {
            if ($(this).is(":checked")) {
               $("#menulist-8").prop("checked",true);
               $("#menulist-9").prop("checked",true);
            } else {
                $("#menulist-8").prop("checked",false);
				$("#menulist-9").prop("checked",false);
             
            }
        });
		
		$("#menulist-8,#menulist-9").change(function () {
            if ($(this).is(":checked")) {
               $("#menulist-3").prop("checked",true);
            } 
        });
		
    });

$(function(){
	$('.select2').select2();

	$("#mydata").on("click", ".btn-info", function(){
		var data = {"key" : $(this).attr("data-key")};
		$(".menuall").prop("checked",false);
		//alert( $(this).attr("data-key"));
		$.ajax({
			url : "Role/find_item",
			type : "post",
			dataType : "json",
			data : data,
			success : function(data){
				if ( data.type === "done" ){
//							$("#menu").val ( data.msg);
					$.each(data.msg, function(index,value){
						//alert(value);
						$("#menulist-"+value["id_menu"]).prop("checked",true);
					});	
					$("#Modal_Access").modal("show");
					$("#menu").append('');
					$("#id_role").val(data.id_role);
					//$('.select2').select2();
					//alert(data.msg);
					
				}
				else{
					alert(data.msg);
				}
			}
		});
	});


	$("#mydata").on("click", ".btn-delete", function(){
		var data = {"key" : $(this).attr("data-key"),"name" : $(this).attr("data-name")};
		// $(".menuall").prop("checked",false);
		//alert( $(this).attr("data-key"));
		$.ajax({
			url : "Role/deleteRole",
			type : "post",
			dataType : "json",
			data : data,
			beforeSend : function(xhr, opts){
				var btn = confirm("Apakah anda ingin menghapus data "+(data.name)+" ini?");
				if(btn === false){ 
					xhr.abort();
				}
			},
			success : function(data){
				if ( data.type === "done" ){
					alert(data.hasil);
					if(data.hapus === 'ya'){
						location.reload();
					}
				}
				else{
					alert(data.msg);
				}
			}
		});
	});

});
