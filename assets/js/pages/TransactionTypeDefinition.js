
	$(document).ready(function() {
    $('#mydata').DataTable( {
        "mydata_length": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    } );
} );

$("#transNewGroupchecked ").change(function() {
    if($('#transNewGroupchecked').is( ":checked")){
        $('#transNewGroup').show();
        $("#transactionGroup ").prop('disabled', true);
    }else{
        $('#transNewGroup').hide();
        $("#transactionGroup ").prop('disabled', false);
    }
});

$("#etransNewGroupchecked ").change(function() {
    if($('#etransNewGroupchecked').is( ":checked")){
        $('#etransNewGroup').show();
        $("#etransactionGroup ").prop('disabled', true);
    }else{
        $('#etransNewGroup').hide();
        $("#etransactionGroup ").prop('disabled', false);
    }
});

$("#mydata").on("click", ".btn-edit", function(){
    var data = {"key" : $(this).attr("data-key")};
    $(".menuall").prop("checked",false);
    //alert( $(this).attr("data-key"));
    $.ajax({
        url : "TransactionTypeDefinition/find_item",
        type : "post",
        dataType : "json",
        data : data,
        success : function(data){
            if ( data.type === "done" ){
                $("#etransactionType").val(data.transtype);
                $("#etransactionGroup").val(data.transgroup);
                // $("#etranstypeID").val( $(this).attr("data-key"));
                $("#etranstypeID").val(data.transtypeID);
                $("#Modal_Edit").modal("show");
            }
            else{
                alert(data.msg);
            }
        }
    });
});

// $("#mydata").on("click", ".btn-delete", function(){
//     var data = {"key" : $(this).attr("data-key"),
//                 "info" : $(this).attr("data-info")};
//     //$(".menuall").prop("checked",false);
//     //alert( $(this).attr("data-key"));
//     $.ajax({
//         url : "TransactionTypeDefinition/find_item",
//         type : "post",
//         dataType : "json",
//         data : data,
//         beforeSend : function(){
//             var btn = confirm('Apakah Anda Ingin Menghapus Data '+ data.info + '?');
//         },
//         success : function(data){
//             if ( data.type === "done" ){
//                 if(btn === true){
//                     // if ( data.d === "done" ){
//                         var hapus = false;
//                         if(!hapus){
//                             hapus = true;
//                             $.post('TransactionTypeDefinition/delete', {transactionTypeID: $(this).attr("data-key")},
//                             function(data){
//                                 alert(data.msg);
//                             });
//                             hapus = false;
//                         }
//                 }else{
//                         return false;
//             }
//         }else{
//             alert(data.msg);
//             }
//         }
//     });
// });

// $("#mydata").on("click", ".btn-delete", function(){
//     var data = {"key" : $(this).attr("data-key"),
//                 "info" : $(this).attr("data-info")};
//     //$(".menuall").prop("checked",false);
//     //alert( $(this).attr("data-key"));
//     $.ajax({
//         url : "TransactionTypeDefinition/find_item",
//         type : "post",
//         dataType : "json",
//         data : data,
//         beforeSend : function(){
//             var btn = confirm('Apakah Anda Ingin Menghapus Data '+ data.info + '?');
//         },
//         success : function(data){
//             if(btn === true){
//             // if ( data.d === "done" ){
//                 var hapus = false;
//                 if(!hapus){
//                     hapus = true;
//                     $.post('TransactionTypeDefinition/delete', {transactionTypeID: $(this).attr("data-key")},
//                     function(data){
//                         alert(data.msg);
//                     });
//                     hapus = false;
//                 }
//             }else{
//                 return false;
//             }
//         }
//     });
// });
