$(document).ready(function(){
	var base_url = window.location.origin + "/EJBaru";
	$('#startdate').daterangepicker({
			timePicker: true,
			timePicker24Hour: true,
			 timePickerIncrement: 1,
			startDate:new Date(new Date().setHours(00, 00, 00)),
			endDate: new Date(new Date().setHours(23, 59, 59)),
			//endDate: moment().startOf('hour'),
			locale: {
            format: 'YYYY-MM-DD HH:mm:SS'
        }
	  });

	  $("#submitreset ").click(function() {
		$(".select2").val('').trigger('change');
		$("#terminalID").val('').trigger('change');
		$('#terminalID').select2({
			minimumInputLength: 4}
		);
		$("#resi").val("");
		$("#nocard").val("");
		// $("#region").val('Wilayah').trigger('change');
		// $('#region.select2').select2({placeholder: "Wilayah",
		// 	allowClear: true});
		// $("#area").val('Area').trigger('change');
		// $('#area.select2').select2({placeholder: "Area",
		// 	allowClear: true});
		// $("#managerType").val('Jenis Pengelola').trigger('change');
		// $('#managerType.select2').select2({placeholder: "Jenis Pengelola",
		// 	allowClear: true});
		// $("#managedBy").val('Nama Pengelola').trigger('change');
		// $('#managedBy.select2').select2({placeholder: "Nama Pengelola",
		// 	allowClear: true});
		// $("#transStat").val('tranStat').trigger('change');
		// $('#transStat.select2').select2({placeholder: "Transaction Status",
		// 	allowClear: true});
		
	});

	$("#managerType").change(function() {
		var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
		"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};
			// alert(data.area);

				$("#managedBy").empty();
				$.ajax({
					url : "AllFilter/getmanagebyJenisPengelola",
					type : "post",
					dataType : "json",
					data : data,
					success : function(data){
						if ( data.type === "done" ){
			//		$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
						$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
		allowClear: true});

						//alert(data.msg);
						}
						else{
							alert(data.msg);
						}
					}
				});
				//alert( "Handler for .change() called." );
		});

		$("#region").change(function() {
			var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
			"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};
			$("#area").empty();
			$.ajax({
				url : base_url+"/AllFilter/getareabyRegion",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#area").append(data.msg);
						$('#area.select2').select2();
						$('#area.select2').select2({placeholder: "Area",
		allowClear: true});
						//alert(data.msg);
					}
					else{
						alert(data.msg);
					}
					}
				});
	
			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/AllFilter/getmanagebyJenisPengelola",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
						$('#managedBy.select2').select2();
						$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
		allowClear: true});
	
					}
					else{
						alert(data.msg);
					}
				}
				});
	
				//alert( "Handler for .change() called." );
			});


		$("#tanggalcek ").change(function() {
				if($('#tanggalcek').is( ":checked")){
					$('#startdate').prop('disabled', false);
				}else{
					$('#startdate').prop('disabled', true);
				}
		});

		$("#submitsearch ").click(function() {
			//alert($("#startdate").val());
			var curdate = new Date();
			var nowdate = curdate.getDate();
			var tanggalcek = 0;
			if($('#tanggalcek').is( ":checked")){
				tanggalcek = 1;
				//alert("abc");
			}else{
				tanggalcek = 0;
				//alert("abcdef");
			}
			
			var data = {"idmachine" : $("#terminalID option:selected").attr("value"),
								"startdate" : $("#startdate").val(),
								"tanggalcek" : tanggalcek,
								"key" : $("#keyword").val(),
								"key2" : $("#keyword2").val(),
								"region" : $("#region").val(),
								"definisi" : $("#definisi").val(),
								"jenistransaksi" : $("#jenistransaksi").val(),
								"area" : $("#area").val(),
								"resi" : $("#resi").val(),
								"nocard" : $("#nocard").val(),
								"managedBy" : $("#managedBy").val(),
								"transStat" : $("#transStat").val(),
								};
			if( $("#terminalID option:selected").attr("value") === "" && $("#keyword option:selected").attr("value") === "" && $("#keyword2").val() === ""  && $("#transStat option:selected").val() === ""){
				alert("Silahkan input ID Mesin / Keyword List / Status Lainnya");
				//$("#terminalID").focus();
			}else{
					$.ajax({
							url :  window.location.origin + "/EJBaru/Browser/showData",
							type : "post",
							dataType : "json",
							data : data,
							beforeSend: function() {
							$("#detailsearchtable").empty();
									$("#loadingdiv").append("<h3>Process Loading.....</h3>");
						   },
							success : function(data){
								if ( data.type === "done" ){
									$("#loadingdiv").empty();
									$("#detailsearchtable").empty();
									$("#detailsearchtable").append(data.msg['result']);
									var events = $('#events');
									var table = $('#detailsearchtable').DataTable( {
										  bDestroy:true,
										  //dom: 'Bfrtip',
										  dom: '<"topbuttons"B>frt<"bottombuttons">ip',
										   "pageLength": 25,
										   select: true,
										  "columnDefs": [
											{
												"targets": [ 12 ],
												"visible": false
											},
											{
												"targets": [ 1 ],
												"visible": false
											}
											],
											buttons: [
											{
													text: 'Check  All',
													action: function () {
															//$(".resultchecks").prop("checked",true);
																var allPages = table.rows().nodes();
																var button = table.button(0);

																if(button.text() == "Check All"){
																	button.text(function ( dt, button, config ) {
																		return dt.i18n( 'buttons.input', 'Uncheck All' );
																	} );
																	$("input[type=checkbox]", allPages).each(function () {
																		$(this).prop('checked', true);
																	});
																	 table.rows().select();
																}
																else{
																	button.text(function ( dt, button, config ) {
																		return dt.i18n( 'buttons.input', 'Check All' );
																	} );
																		$("input[type=checkbox]", allPages).each(function () {
																			$(this).prop('checked', false);
																	});
																	 table.rows().deselect();
																}
													}
											},
											'copyHtml5',
											{	extend: 'excelHtml5',
												title: 'EJBrowser '+ moment().format('YYYY-MM-DD'),
												 exportOptions: {
													columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
												}
											},
											{	extend: 'csvHtml5',
												title: 'EJBrowser '+ moment().format('YYYY-MM-DD'),
												exportOptions: {
													columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
												}
											},{
											extend: 'pdfHtml5',orientation: 'landscape',
											pageSize: 'LEGAL',title: 'EJBrowser '+ moment().format('YYYY-MM-DD'),
											customize : function(doc) {
													doc.pageMargins = [2, 2,2,2 ]; doc.defaultStyle.fontSize = 7;
													doc['styles'] = {
														 tableHeader: {
																			fontSize:8,
																			color:'black',
																			fillColor:'#ddd',
																			alignment:'left'
																		}
													},
													 doc.styles['td:nth-child(2)'] = { 
														   width: '30px',
														   'max-width': '80px'
														 }
											}
											,exportOptions: {
													columns: [ 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
												}
											},
											 {
												text: 'Tampilkan Resi',
												action: function () {
																var listresi = [];
																var allPages = table.rows().nodes();
																$("input[type=checkbox]:checked", allPages).each(function () {
																			listresi.push($(this).attr("data-propid"));
																});

																var data = {"listresi" : listresi};
																//alert( listcheck.join(", "));
																if(listresi && listresi.length){
																	$.ajax({
																				url :  window.location.origin + "/EJBaru/Browser/showResi",
																				type : "post",
																				dataType : "json",
																				data : data,
																				beforeSend: function() {

																			   },
																				success : function(data){
																					if ( data.type === "done" ){
																						$("#content-resi").html("");
																						$("#content-resi").append(data.msg);
																						$("#modalresi").modal("show");
																					}
																					else{
																						alert(data.msg);
																					}
																				}
																			});
																}else{
																		alert("Please check minimum 1 Transaction");
																}
												}
											}
										],
										initComplete:function(){
											$(".bottombuttons").append( $(".topbuttons").clone(true));
										 }
										} );

										 //table.button(0).nodes().css('background':'lightorange' );
								}
								else{
									alert(data.msg);
								}
							}
						});
				}
		});

		/*
		$("#showresi").click(function(){
			var listresi = [];
		   $.each($("input[name='listcheck']:checked"), function(){
					listresi.push($(this).attr("data-propid"));
			});

			var data = {"listresi" : listresi};
			alert( listresi.join(", "));
			if(listresi && listresi.length){
				$.ajax({
							url :  window.location.origin + "/EJBaru/Browser/showResi",
							type : "post",
							dataType : "json",
							data : data,
							beforeSend: function() {

						   },
							success : function(data){
								if ( data.type === "done" ){
									$("#content-resi").html("");
									$("#content-resi").append(data.msg);
									$("#modalresi").modal("show");
								}
								else{
									alert(data.msg);
								}
							}
						});
			}else{
					alert("Please check minimum 1 Transaction");
			}
		}); */


$(function(){
	$('.select2').select2();
	$('#terminalID').select2({
		minimumInputLength: 4}
	);
});
});
