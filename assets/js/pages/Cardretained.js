$(document).ready(function(){
	var base_url = window.location.origin + "/EJBaru";
	var	cardRetainedRegion= "";
	var	cardRetainedMerk= "";
	var	CardRetainedNamaBank = "";
	var	CardRetainedNamaBanktop15 = "";
	var labelcardRetainedRegion = [];
	var jumlahcardRetained = [];
	var labelcardRetainedMerk = [];
	var jumlahcardRetainedMerk = [];

	var resetCanvas = function(){
  $('#bar5').remove(); // this is my <canvas> element
	$('#horbar2').remove();

  $('#chartReport1').append('<canvas id="bar5"><canvas>');
	$('#chartReport2').append('<canvas id="horbar2"><canvas>');
  canvas = document.querySelector('#bar5');
	canvas = document.querySelector('#horbar2');
  ctx = canvas.getContext('2d');
};
		// let d = new Date();
		// let oneDaysAgo = d.setDate(d.getDate() - 1);
		// $("#date").val(new Date(oneDaysAgo).toISOString().slice(0,10));

		$("#filter").click(function(){
		var data = {"termID" : $("#termID option:selected").attr("value"),
									"date" :$("#date").val(),
									"region" : $("#region").val(),
									"area" : $("#area").val(),
									"managedBy" : $("#managedBy").val(),
									"managerType" : $("#managerType").val(),
									"terminalType" : $("#terminalType").val(),
									"merk" : $("#merk").val(),
									"connectivityType" : $("#connectivityType").val()
								};
			resetCanvas();
			// if( $("#date").val()=== ""){
			// 	alert("Please input Date");
			// 	$("#date").focus();
			// }else{
				$.ajax({
						url : window.location.origin + "/EJBaru/Cardretained/getData",
						type : "post",
						dataType : "json",
						data : data,
						beforeSend: function() {
							$("#tempsearch").empty();
							$("#tempsearch").append("<h3>Process Loading.....</h3>");
							$("#adadata").css("display","none");
							$("#nodata").empty();
						},
						success : function(data){
							if( data.type === "done"){
								if(data.msg['isidata'] == "ada"){
								cardRetainedRegion =data.msg['cardRetainedRegion'];
								cardRetainedMerk = data.msg['cardRetainedMerk'];
								CardRetainedNamaBank = data.msg['CardRetainedNamaBank'];
								CardRetainedNamaBanktop15 = data.msg['CardRetainedNamaBanktop15'];
								//alert(data.msg['cekregion']);
								cekterm = data.msg['cekterm'];
								cekregion = data.msg['cekregion'];
								cekarea = data.msg['cekarea'];
								cekmanageby = data.msg['cekmanageby'];
								$("#tempsearch").append("<h3>Process Loading.....</h3>");
								$("#adadata").css("display","block");
								$("#resultgrid").empty();//DG
								$("#resultgrid").append(data.resultgrid);//DG
								$("#datanamabank15").empty();
								$("#datanamabank15").append(data.resultgridnamabank15);
								$("#datanamabank10first").empty();
								$("#datanamabank10first").append(data.resultgridnamabank10first);
								$("#datanamabank10next").empty();
								$("#datanamabank10next").append(data.resultgridnamabank10next);
								$("#datanamabank").empty();
								$("#datanamabank").append(data.resultgridnamabank);
								
								$("#bar5").empty();
								$("#horbar2").empty();
								$("#tempsearch").empty();
								//card retained by Region
								labelcardRetainedRegion = [];
								jumlahcardRetained = [];
								// alert(cekregion + "-" + cekarea + "-" + cekmanageby);

								for(i=0;i<cardRetainedRegion.length;i++){
									if(cekterm != 0){
										if(cekregion == 0  && cekarea == 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
										}else if(cekregion != 0    && cekarea == 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
										}else if(cekregion == 0    && cekarea != 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
										}else{
											labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
										}
									}else{
										if(cekregion == 0  && cekarea == 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['regionAlias']);
										}else if(cekregion != 0    && cekarea == 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['areaAlias']);
										}else if(cekregion == 0    && cekarea != 0 && cekmanageby == 0){
											labelcardRetainedRegion.push(cardRetainedRegion[i]['managedBy']);
										}else{
											labelcardRetainedRegion.push(cardRetainedRegion[i]['managedBy']);
										}
									}
									jumlahcardRetained.push(cardRetainedRegion[i]['CardRetained']);
								}

								//card retained by Merk
								labelcardRetainedMerk = [];
								jumlahcardRetainedMerk = [];
								for(i=0;i<cardRetainedMerk.length;i++){
									labelcardRetainedMerk.push(cardRetainedMerk[i]['merk']);
									jumlahcardRetainedMerk.push(cardRetainedMerk[i]['Card Retained']);
								}

								let bar5 = document.getElementById("bar5").getContext("2d");

								let bar5_chart = new Chart(bar5, {
									type: 'bar',
									data: {
										labels: labelcardRetainedRegion,
										datasets: [{
										label: "Kartu Tertelan",
											data: jumlahcardRetained,
											backgroundColor:"rgba(15, 43, 91, 2)",
											borderColor: "rgba(15, 43, 91, 2)",
											borderWidth: 1
										}]
									},
									options: {
										  scales:{
											xAxes: [{
												gridLines:{
												  display:false,
												  drawBorder:false,
												}
											}],
											yAxes: [{
												ticks:{
												  display:false,
													// stepSize:500,
													beginAtZero: true,
												  },
												gridLines:{
												  display:true,
												  drawBorder:false,
												}
											}]
										},
										legend:{
											display: false,
											position:'top',
											labels:{
											  fontColor: '#000',fontSize: 12,
											  fontStyle: 'bold',
											}
										},
										plugins: {
											labels:  {
											  // render: function(label){
												//return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
																// return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
												// },
												render: function (args) {
												if(args.value/1000000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000000).toFixed(2) +' M';
												}
												if(args.value/1000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000).toFixed(2) +' K';
												}
												else {
												  return args.value;
												}
											},
											fontSize: 12,
											  fontStyle: 'bold',
											  fontColor: '#000',
											  fontFamily: '"Calibri"',
											  arc: true,
											  position: 'outside',
											  overlap: true,
											  outsidePadding: 24,
										},
										}
									}
								});

								let horbar2 = document.getElementById("horbar2");
								let horbar2_chart = new Chart(horbar2, {
									type: 'horizontalBar',
									data: {
										labels: labelcardRetainedMerk,
										datasets: [{
											label: "Kartu Tertelan",
											data: jumlahcardRetainedMerk,
											backgroundColor:
											"rgba(117, 178, 221, 1)",
											borderColor: "rgba(117, 178, 221, 1)",
											borderWidth: 1
										}]
									},
									options: {
										  scales:{
											xAxes: [{
											  ticks:{
												display:false,
												beginAtZero:true,
												suggestedMax:2000,
											  },
												gridLines:{
												  display:true,
												  drawBorder:false,
												}
											}],
											yAxes: [{
												gridLines:{
												  display:false,

												}
											}]
										},
										legend:{
											display: false,
											position:'top',
											labels:{
											  fontColor: '#000',fontSize: 12,
											  fontStyle: 'bold'
											}
										},
										plugins: {
											labels:  {
											  // render: function(label){
												// return  '' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
												// },
												render: function (args) {
												if(args.value/1000000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000000).toFixed(2) +' M';
												}
												if(args.value/1000>=1){
												  // return Math.round((args.value/1000) + ' K';
												  return (args.value/1000).toFixed(2) +' K';
												}
												else {
												  return args.value;
												}
											},
											  fontSize: 12,
											  fontStyle: 'bold',
											  fontColor: '#000',
											  fontFamily: '"Calibri"',
											}
									}
									}
								});
							}else{
								$("#nodata").append("<h3> Data tidak ditemukan</h3>");
								$("#adadata").css("display", "none");
								$("#tempsearch").empty();
								}
							}
							else{
									alert(data.msg);
							}
						}
				});
			// }
		});


		$(function(){
			$("#managerType ").change(function() {
				var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
				"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};

					$("#managedBy").empty();
					$.ajax({
						url : "AllFilter/getmanagebyJenisPengelola",
						type : "post",
						dataType : "json",
						data : data,
						success : function(data){
							if ( data.type === "done" ){
				//		$("#menu").val ( data.msg);
							$("#managedBy").append(data.msg);
						$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});

							//alert(data.msg);
							}
							else{
								alert(data.msg);
							}
						}
					});
					//alert( "Handler for .change() called." );
			});

			$("#terminalType").change(function() {
				var data = {"terminalType" :$("#terminalType option:selected").attr("value"),
				}
				$("#merk").empty();
				$.ajax({
					url : base_url+"/AllFilter/getmerkbyJenisMesin",
					type : "post",
					dataType : "json",
					data : data,
					success : function(data){
						if ( data.type === "done" ){
							//$("#menu").val ( data.msg);
							$("#merk").append(data.msg);
							$('#merk.select2').select2();$("#merk").val('Merk');
						}
						else{
							alert(data.msg);
						}
					}
				});
			});

		});

		$("#region").change(function() {
		var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
		"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};
			$("#area").empty();
			$.ajax({
				url : base_url+"/AllFilter/getareabyRegion",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
							$("#area").append(data.msg);
							$('#area.select2').select2();
							$('#area.select2').select2({placeholder: "Area",
			allowClear: true});
					}
					else{
						alert(data.msg);
					}
					}
				});

			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/AllFilter/getmanagebyJenisPengelola",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
		//							$("#menu").val ( data.msg);
						$("#managedBy").append(data.msg);
					$('#managedBy.select2').select2();
					$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});

					}
					else{
						alert(data.msg);
					}
				}
				});

				//alert( "Handler for .change() called." );
			});

			$("#area ").change(function() {
				var data = {"jenismanage" :$("#managerType option:selected").attr("value"),
				"region" : $("#region option:selected").attr("value"),"area" : $("#area option:selected").attr("value")};

			$("#managedBy").empty();
			$.ajax({
				url : base_url+"/AllFilter/getmanagebyJenisPengelola",
				type : "post",
				dataType : "json",
				data : data,
				success : function(data){
					if ( data.type === "done" ){
						$("#managedBy").append(data.msg);
						$('#managedBy .select2').select2({placeholder: "Nama Pengelola",
	allowClear: true});
					}
					else{
						alert(data.msg);
					}
					}
				});
			});

$(function(){
	setTimeout(function() {
        $("#filter").trigger('click');
    },2); 
	$('.select2').select2();
	$('#termID').select2({
		minimumInputLength: 4
	});
});

$("#submitreset ").click(function() {
	$(".select2").val('').trigger('change');
	$("#termID").val('').trigger('change');
	$('#termID').select2({
		minimumInputLength: 4}
	);
	$('#termID.select2').select2({placeholder: "ID Mesin",
		allowClear: true});
	$("#region").val('Wilayah').trigger('change');
	$('#region.select2').select2({placeholder: "Wilayah",
		allowClear: true});
	$("#area").val('Area').trigger('change');
	$('#area.select2').select2({placeholder: "Area",
		allowClear: true});
	$("#managerType").val('Jenis Pengelola').trigger('change');
	$('#managerType.select2').select2({placeholder: "Jenis Pengelola",
		allowClear: true});
	$("#managedBy").val('Nama Pengelola').trigger('change');
	$('#managedBy.select2').select2({placeholder: "Nama Pengelola",
		allowClear: true});
	$("#terminalType").val('Jenis Mesin').trigger('change');
	$('#terminalType.select2').select2({placeholder: "Jenis Mesin",
		allowClear: true});
	$("#merk").val('Merk');
	$('#merk.select2').select2({placeholder: "Merk",
		allowClear: true});
	$("#connectivityType").val('Jarkom').trigger('change');
	$('#connectivityType.select2').select2({placeholder: "Jarkom",
		allowClear: true});
});

});
