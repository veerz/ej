$(document).ready(function () {
	var mydata = $('#mydata').DataTable({
		"ajax": 'showData',
		"order": [
			[0, "asc"]
		],
	});
	//Save userDetail
	$('#btn_save').on('click', function () {
		var groupID = $('#groupID').val();
		var userName = $('#userName').val();
		var Status = $('#Status').val();
		var password = $('#password').val();
		var firstname = $('#firstname').val();
		var phonenumber = $('#phonenumber').val();
		var lastname = $('#lastname').val();
		var userEmail = $('#userEmail').val();
		$.ajax({
			type: "POST",
			url: 'save',
			dataType: "JSON",
			data: {
				groupID: groupID,
				userName: userName,
				Status: Status,
				password: password,
				firstname: firstname,
				lastname: lastname,
				phonenumber: phonenumber,
				userEmail: userEmail
			},
			success: function (data) {
				$('#groupID').val("");
				$('#firstname').val("");
				$('#userName').val("");
				$('#password').val("");
				$('#Status').val("");
				$('#lastname').val("");
				$('#phonenumber').val("");
				swal(
					'Success!',
					'Input Data userDetail berhasil',
					'success'
				);
				$('#Modal_Add').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for update record
	$('#show_data').on('click', '.item_edit', function () {
		var id = $(this).data('id');
		var url = 'getData/';
		var fixUrl = url += id;
		$.ajax({
			type: 'ajax',
			url: fixUrl,
			async: false,
			dataType: 'json',
			success: function (data) {
				$('#userID_edit').val(data["userID"]);
				$('#groupID_edit').val(data["groupID"]);
				$('#firstname_edit').val(data["firstname"]);
				$('#userName_edit').val(data["userName"]);
				$('#password_edit').val("");
				$('#Status_edit').val(data["Status"]);
				$('#lastname_edit').val(data["lastname"]);
				$('#phonenumber_edit').val(data["phonenumber"]);
				$('#userEmail_edit').val(data["userEmail"]);
				$('#Modal_Edit').modal('show');
			}
		});
	});
	//update record to database
	$('#btn_update').on('click', function () {
		var userID = $('#userID_edit').val();
		var groupID = $('#groupID_edit').val();
		var userName = $('#userName_edit').val();
		var Status = $('#Status_edit').val();
		var password = $('#password_edit').val();
		var firstname = $('#firstname_edit').val();
		var phonenumber = $('#phonenumber_edit').val();
		var lastname = $('#lastname_edit').val();
		var userEmail = $('#userEmail_edit').val();
		$.ajax({
			type: "POST",
			url: 'update',
			dataType: "JSON",
			data: {
				userID: userID,
				groupID: groupID,
				userName: userName,
				Status: Status,
				password: password,
				firstname: firstname,
				lastname: lastname,
				phonenumber: phonenumber,
				userEmail: userEmail
			},
			success: function (data) {
				$('#userID_edit').val("");
				$('#groupID_edit').val("");
				$('#firstname_edit').val("");
				$('#userName_edit').val("");
				$('#password_edit').val("");
				$('#Status_edit').val("");
				$('#lastname_edit').val("");
				$('#phonenumber_edit').val("");
				$('#userEmail_edit').val("");
				swal(
					'Success!',
					'Update Data userDetail berhasil',
					'success'
				);
				$('#Modal_Edit').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
	//get data for delete record
	$('#show_data').on('click', '.item_delete', function () {
		var userID = $(this).data('id');
		$('#Modal_Delete').modal('show');
		$('#userID_delete').val(userID);
	});
	//delete record to database
	$('#btn_delete').on('click', function () {
		var userID = $('#userID_delete').val();
		$.ajax({
			type: "POST",
			url: 'delete',
			dataType: "JSON",
			data: {
				userID: userID
			},
			success: function (data) {
				$('#userID_delete').val("");
				swal(
					'Success!',
					'Delete Data userDetail berhasil',
					'success'
				);
				$('#Modal_Delete').modal('hide');
				mydata.ajax.reload();
			}
		});
		return false;
	});
});
