USE [master]
GO
/****** Object:  Database [EJBrowser]    Script Date: 04/10/2018 10:47:04 ******/
CREATE DATABASE [EJBrowser]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EJBrowser', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\EJBrowser.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EJBrowser_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\EJBrowser_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EJBrowser] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EJBrowser].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EJBrowser] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EJBrowser] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EJBrowser] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EJBrowser] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EJBrowser] SET ARITHABORT OFF 
GO
ALTER DATABASE [EJBrowser] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EJBrowser] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [EJBrowser] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EJBrowser] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EJBrowser] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EJBrowser] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EJBrowser] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EJBrowser] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EJBrowser] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EJBrowser] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EJBrowser] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EJBrowser] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EJBrowser] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EJBrowser] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EJBrowser] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EJBrowser] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EJBrowser] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EJBrowser] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EJBrowser] SET RECOVERY FULL 
GO
ALTER DATABASE [EJBrowser] SET  MULTI_USER 
GO
ALTER DATABASE [EJBrowser] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EJBrowser] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EJBrowser] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EJBrowser] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [EJBrowser]
GO
/****** Object:  Table [dbo].[MsCategoryMsg]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsCategoryMsg](
	[idCategory] [int] IDENTITY(1,1) NOT NULL,
	[nameCategory] [varchar](50) NULL,
	[statusCategory] [int] NULL,
	[updateBy] [int] NULL,
	[updateDate] [date] NULL,
 CONSTRAINT [PK_MsCategoryMsg] PRIMARY KEY CLUSTERED 
(
	[idCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsEntitiyType]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsEntitiyType](
	[idEntityType] [int] IDENTITY(1,1) NOT NULL,
	[nameEntityType] [varchar](50) NULL,
	[statusEntityType] [int] NULL,
	[updateBy] [int] NULL,
	[updateDate] [date] NULL,
 CONSTRAINT [PK_MsEntitiyType] PRIMARY KEY CLUSTERED 
(
	[idEntityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsEntity]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsEntity](
	[idEntitiy] [int] IDENTITY(1,1) NOT NULL,
	[nameEntity] [varchar](50) NULL,
	[idEntityType] [int] NULL,
	[detailEntity] [varchar](250) NULL,
	[statusEntity] [int] NULL,
	[updateBy] [int] NULL,
	[updateDate] [date] NULL,
 CONSTRAINT [PK_MsEntity] PRIMARY KEY CLUSTERED 
(
	[idEntitiy] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsMachine]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsMachine](
	[idMachine] [int] IDENTITY(1,1) NOT NULL,
	[nameMachine] [varchar](50) NULL,
	[statusMachine] [int] NULL,
	[updateBy] [int] NULL,
	[updateDate] [date] NULL,
 CONSTRAINT [PK_MsMachine] PRIMARY KEY CLUSTERED 
(
	[idMachine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsMenu]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsMenu](
	[menuID] [int] IDENTITY(1,1) NOT NULL,
	[menuName] [varchar](50) NULL,
	[status] [int] NULL,
	[controllerName] [varchar](50) NULL,
	[createby] [int] NULL,
	[updateby] [int] NULL,
	[updateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsMsg]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsMsg](
	[idMsg] [int] IDENTITY(1,1) NOT NULL,
	[idCategory] [int] NULL,
	[nameMsg] [varchar](50) NULL,
	[idMachine] [int] NULL,
	[statusMsg] [int] NULL,
	[updateBy] [int] NULL,
	[updateDate] [date] NULL,
 CONSTRAINT [PK_MsMsg] PRIMARY KEY CLUSTERED 
(
	[idMsg] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[previledgeGroup]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[previledgeGroup](
	[prvGroupID] [int] IDENTITY(1,1) NOT NULL,
	[groupID] [int] NULL,
	[menuID] [int] NULL,
	[status] [int] NULL,
	[createby] [int] NULL,
	[updateby] [int] NULL,
	[updateTime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[previledgeUser]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[previledgeUser](
	[prvUserID] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NULL,
	[menuID] [int] NULL,
	[status] [int] NULL,
	[createby] [int] NULL,
	[updateby] [int] NOT NULL,
	[updateTime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[userDetail]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userDetail](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[groupID] [varchar](50) NULL,
	[userName] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[Status] [int] NULL,
	[userEmail] [varchar](250) NULL,
	[lastlogin] [datetime] NULL,
	[createby] [int] NULL,
	[updateby] [int] NULL,
	[firstname] [varchar](100) NULL,
	[lastname] [varchar](100) NULL,
	[phonenumber] [varchar](100) NULL,
	[updateTime] [datetime] NULL,
 CONSTRAINT [PK__userDeta__CB9A1CDF6358005D] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userGroup]    Script Date: 04/10/2018 10:47:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userGroup](
	[groupID] [int] IDENTITY(1,1) NOT NULL,
	[groupName] [varchar](50) NULL,
	[updateby] [int] NULL,
	[createby] [int] NULL,
	[updateTime] [datetime] NULL,
 CONSTRAINT [PK__userGrou__88C102ADC0784571] PRIMARY KEY CLUSTERED 
(
	[groupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[MsMenu] ON 

INSERT [dbo].[MsMenu] ([menuID], [menuName], [status], [controllerName], [createby], [updateby], [updateTime]) VALUES (7, N'MsCategoryMsg', 1, N'MsCategoryMsg', 1, 1, NULL)
INSERT [dbo].[MsMenu] ([menuID], [menuName], [status], [controllerName], [createby], [updateby], [updateTime]) VALUES (8, N'MsEntity', 1, N'MsEntity', 1, 1, NULL)
INSERT [dbo].[MsMenu] ([menuID], [menuName], [status], [controllerName], [createby], [updateby], [updateTime]) VALUES (10, N'MsMachine', 1, N'MsMachine', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[MsMenu] OFF
SET IDENTITY_INSERT [dbo].[previledgeGroup] ON 

INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (1, 1, 10, 2, 1, 1, CAST(0x0000A96F003B4DA4 AS DateTime))
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (2, 2, 8, 2, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (3, 1, 10, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (4, 2, 8, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (5, 1, 8, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (6, 0, 10, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (7, 1, 7, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (13, 1, 7, 1, 1, 1, NULL)
INSERT [dbo].[previledgeGroup] ([prvGroupID], [groupID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (14, 2, 8, 1, 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[previledgeGroup] OFF
SET IDENTITY_INSERT [dbo].[previledgeUser] ON 

INSERT [dbo].[previledgeUser] ([prvUserID], [userID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (1, 1, 10, 2, 1, 1, CAST(0x0000A96E00A4A27C AS DateTime))
INSERT [dbo].[previledgeUser] ([prvUserID], [userID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (3, 1, 8, 1, 1, 1, NULL)
INSERT [dbo].[previledgeUser] ([prvUserID], [userID], [menuID], [status], [createby], [updateby], [updateTime]) VALUES (4, 3, 10, 2, 1, 1, CAST(0x0000A96F003AFB9C AS DateTime))
SET IDENTITY_INSERT [dbo].[previledgeUser] OFF
SET IDENTITY_INSERT [dbo].[userDetail] ON 

INSERT [dbo].[userDetail] ([userID], [groupID], [userName], [password], [Status], [userEmail], [lastlogin], [createby], [updateby], [firstname], [lastname], [phonenumber], [updateTime]) VALUES (1, N'1', N'test', N'', 1, N'test@test', CAST(0x0000000000000000 AS DateTime), 1, 1, N'test', N'tset', N'082222110099', CAST(0x0000A96F003B26F8 AS DateTime))
INSERT [dbo].[userDetail] ([userID], [groupID], [userName], [password], [Status], [userEmail], [lastlogin], [createby], [updateby], [firstname], [lastname], [phonenumber], [updateTime]) VALUES (2, N'1', N'test2', N'098f6bcd4621d373cade4e832627b4f6', 1, N'test@test', CAST(0x0000000000000000 AS DateTime), 1, 1, N'test', N'tset', N'082222110099', CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[userDetail] ([userID], [groupID], [userName], [password], [Status], [userEmail], [lastlogin], [createby], [updateby], [firstname], [lastname], [phonenumber], [updateTime]) VALUES (3, N'1', N'test3', N'098f6bcd4621d373cade4e832627b4f6', 1, N'test3', CAST(0x0000000000000000 AS DateTime), 1, 1, N'test', N'test', N'2131312', NULL)
SET IDENTITY_INSERT [dbo].[userDetail] OFF
SET IDENTITY_INSERT [dbo].[userGroup] ON 

INSERT [dbo].[userGroup] ([groupID], [groupName], [updateby], [createby], [updateTime]) VALUES (1, N'Admin', 1, 1, NULL)
INSERT [dbo].[userGroup] ([groupID], [groupName], [updateby], [createby], [updateTime]) VALUES (2, N'User', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[userGroup] OFF
USE [master]
GO
ALTER DATABASE [EJBrowser] SET  READ_WRITE 
GO
