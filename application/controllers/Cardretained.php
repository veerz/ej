<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Cardretained extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$listtermID = "SELECT DISTINCT terminalID FROM ATDS(nolock)";
		$termID = $this->db->query($listtermID);
		$data['termID'] = $termID->result_array();

    $listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

    $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    $listconnectivityVendor = "SELECT DISTINCT connectivityVendor FROM ATDS";
    $connectivityVendor = $this->db->query($listconnectivityVendor);
    $data['connectivityVendor'] = $connectivityVendor->result_array();

    $listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
    $connectivityType = $this->db->query($listconnectivityType);
    $data['connectivityType'] = $connectivityType->result_array();

    $data['params'] = "";
    $this->load->template('cardretained',$data);

	}

    public function getData()
    {
      $tanggal = ( $this->input->post("date")?:null);
      $termID = ( $this->input->post("termID")?:null);
      $region = ( $this->input->post("region")?:null);
      if($region != null){
      if($region == "REGION I / SUMATERA 1"){
        $region = "REG01";
      }if($region == "REGION II / SUMATERA 2"){
        $region = "REG02";
      }if($region == "REGION III / JAKARTA 1"){
        $region = "REG03";
      }if($region == "REGION IV / JAKARTA 2"){
        $region = "REG04";
      }if($region == "REGION V / JAKARTA 3"){
        $region = "REG05";
      }if($region == "REGION VI / JAWA 1"){
        $region = "REG06";
      }if($region == "REGION VII / JAWA 2"){
        $region = "REG07";
      }if($region == "REGION VIII / JAWA 3"){
        $region = "REG08";
      }if($region == "REGION IX / KALIMANTAN"){
        $region = "REG09";
      }if($region == "REGION X / SULAWESI DAN MALUKU"){
        $region = "REG10";
      }if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
        $region = "REG11";
      }if($region == "REGION XII / PAPUA"){
        $region = "REG12";
      }if($region == "LUAR NEGERI")
        $region = "REGLN";
    }

    $area = ( $this->input->post("area")?:null);
    $managerType =( $this->input->post("managerType")?: null);
    $managedBy = ( $this->input->post("managedBy")?:null);
    $terminalType = ( $this->input->post("terminalType")?:null);
    $merk = ( $this->input->post("merk")?:null);
    $connectivityType = ( $this->input->post("connectivityType")?:null);

    if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
    $param = "";
    $paramtampil="";
    if($tanggal!=null)	$param .=  " @tanggal='".$tanggal."',";
    else $param .=  " @tanggal=NULL,";
    if($termID!=null)	$param .=  " @termID='".$termID."',";
    else $param .=  " @termID=NULL,";
    if($region!=null)	$param .=  " @region='".$region."',";
    else $param .=  " @region=NULL,";
    if($area!=null)	$param .=  " @area='".$area."',";
    else $param .=  " @area=NULL,";
    if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
    else $param .=  " @managerType=NULL,";
    if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
    else $param .=  " @manager=NULL,";
    if($terminalType!=null)	$param .=  " @machineType='".$terminalType."',";
    else $param .=  " @machineType=NULL,";
    if($merk!=null)	$param .=  " @machineMake='".$merk."',";
    else $param .=  " @machineMake=NULL,";
    if($connectivityType!=null)	$param .=  " @commType='".$connectivityType."'";
    else $param .=  " @commType=NULL";

    $sqlcardRetainedRegion= "Exec getCardRetainedRegion".$param;
    $cardRetainedRegion = $this->db->query($sqlcardRetainedRegion);
    $data['cardRetainedRegion'] = $cardRetainedRegion->result_array();
    $cardRetainedRegions =   $data['cardRetainedRegion'];//DG
    //echo $param;die();
    
    $isidata ="";
	
    if(!$cardRetainedRegions)
    {
      $data['isidata'] = "tidak";
    }else{
      $data['isidata'] = "ada";
    }

    $cardRetainedMerk= "Exec getCardRetainedMerk".$param;
    $cardRetainedMerk = $this->db->query($cardRetainedMerk);
    $data['cardRetainedMerk'] = $cardRetainedMerk->result_array();

    $CardRetainedNamaBank= "Exec getCardRetainedNamaBank";
    $CardRetainedNamaBank = $this->db->query($CardRetainedNamaBank);
    $data['CardRetainedNamaBank'] = $CardRetainedNamaBank->result_array();
    $CardRetainedNamaBanks = $data['CardRetainedNamaBank'] ;//DG

    $CardRetainedNamaBanktop15= "Exec getCardRetainedNamaBanktop15".$param;
    $CardRetainedNamaBanktop15 = $this->db->query($CardRetainedNamaBanktop15);
    $data['CardRetainedNamaBanktop15'] = $CardRetainedNamaBanktop15->result_array();
    $CardRetainedNamaBanktop152 =   $data['CardRetainedNamaBanktop15'];//DG

    $CardRetainedNamaBanktop10= "Exec getCardRetainedNamaBanktop10".$param;
    $CardRetainedNamaBanktop10 = $this->db->query($CardRetainedNamaBanktop10);
    $data['CardRetainedNamaBanktop10'] = $CardRetainedNamaBanktop10->result_array();
    $CardRetainedNamaBanktop10s =   $data['CardRetainedNamaBanktop10'];//DG


    $data['cekterm'] = ($termID?:0);
    $data['cekregion'] = ( $region?:0);
    $data['cekarea'] = ( $area?:0);
    $data['cekmanageby'] = ( $managedBy?:0);
    $data['cekmerk'] = ( $merk?:0);

    /////buat datagrid cek
	  $cekmerk = ( $merk?:0);//DG
	  $cekterm= ( $termID?:0);//DG
    $cekregion = ( $region?:0);//DG
    $cekarea= ( $area?:0);//DG
    $cekmanageby= ( $managedBy?:0);//    

	// tempat generate data datagrid
	$resultgrid = "";
	$resultgrid .="<table id=\"mydata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td>";
    for($i=0;$i<count($cardRetainedRegions);$i++){
    $resultgrid .="<td bgcolor=#4A4A4B style=\"color:#fff\">";
    if($cekterm != "0"){
      if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
        $resultgrid .= $cardRetainedRegions[$i]['regionAlias'];
      }elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
        $resultgrid .=  $cardRetainedRegions[$i]['regionAlias'];
      }elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
        $resultgrid .=  $cardRetainedRegions[$i]['regionAlias'];
      }else{
        $resultgrid .=  $cardRetainedRegions[$i]['regionAlias'];
      }
        $resultgrid .="</td>";
    }else{
      if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
        $resultgrid .= $cardRetainedRegions[$i]['regionAlias'];
      }elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
        $resultgrid .=  $cardRetainedRegions[$i]['area'];
      }elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
        $resultgrid .=  $cardRetainedRegions[$i]['managedBy'];
      }else{
        $resultgrid .=  $cardRetainedRegions[$i]['managedBy'];
      }
        $resultgrid .="</td>";
      }
    }
	$resultgrid .="</tr><tr>
                            <td><label>Jumlah</label></td>";
	for($i=0;$i<count($cardRetainedRegions);$i++){
          	$resultgrid .="<td>" .number_format($cardRetainedRegions[$i]['CardRetained'],0,',','.'). "</td>";
			}
	$resultgrid .="</tr></table>";

	//ini tempat resultgridnamabank
	$resultgridnamabank15 = "";
	$resultgridnamabank15 .= "<table id=\"datanamabank15\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td></tr>";
  $totaltop15cardRetained = 0;
  $pembagi = 1;
  for($i=0;$i<count($CardRetainedNamaBanktop152);$i++){
      $totaltop15cardRetained += $CardRetainedNamaBanktop152[$i]['Card Retained'];
  }
  for($i=0;$i<count($CardRetainedNamaBanktop152);$i++){
    if($totaltop15cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop15cardRetained;
            $resultgridnamabank15 .="<tr><td>" .$CardRetainedNamaBanktop152[$i]['bankName']. "</td><td>" .number_format($CardRetainedNamaBanktop152[$i]['Card Retained'],0,',','.'). "</td><td>". round(($CardRetainedNamaBanktop152[$i]['Card Retained']/$pembagi)*100,2). "%</td>" ;
  }
  $resultgridnamabank15 .="</table>";

  //ini tempat resultgridnamabank 5 pertama
	$resultgridnamabank10first = "";
	$resultgridnamabank10first .= "<table id=\"datanamabank10first\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td></tr>";
  $totaltop10cardRetained = 0;
  $pembagi = 1;
  	for($i=0;$i<count($CardRetainedNamaBanktop10s);$i++){
			$totaltop10cardRetained += $CardRetainedNamaBanktop10s[$i]['Card Retained'];
		}
		
		//create table 1-5 nama bank
  if (count($CardRetainedNamaBanktop10s) > 5){
	  
		for($i=0;$i<5;$i++){
			if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
			$resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
		}
		
    }
	else{ 
		for($i=0; $i <count($CardRetainedNamaBanktop10s) ;$i++ ) {
			if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
			$resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
		}
    }
	/* 
  for($i=0;$i<count($CardRetainedNamaBanktop10s)/2;$i++){
      $totaltop10cardRetained += $CardRetainedNamaBanktop10s[$i]['Card Retained'];
  }
  for($i=0;$i<count($CardRetainedNamaBanktop10s)/2;$i++){
    if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
            $resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
  } */
  $resultgridnamabank10first .="</table>";

  //ini tempat resultgridnamabank 5 pertama
	$resultgridnamabank10next = "";
	
  // $totaltop10cardRetained = 0;
  	
	//create table 5-10 nama bank
  $pembagi = 1;

   if (count($CardRetainedNamaBanktop10s) > 5){
	   $resultgridnamabank10next .= "<table id=\"datanamabank10next\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td></tr>";
		for($i=5;$i<count($CardRetainedNamaBanktop10s);$i++){
			if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
			$resultgridnamabank10next .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
		}	
		$resultgridnamabank10next .="</table>";
    }
	else{ 
		
    }
  /* for($i=5;$i<count($CardRetainedNamaBanktop10s);$i++){
    if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
            $resultgridnamabank10next .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
  } */
  

  //Result All
  $resultgridnamabank = "";
	$resultgridnamabank .= "<table id=\"datanamabank\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Kartu Tertelan</label></center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Jumlah</label></center></td>
                            <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Persentase</label></center></td></tr>";
  $totaltopcardRetained = 0;
  $pembagi = 1;
  for($i=0;$i<count($CardRetainedNamaBanks);$i++){
      $totaltopcardRetained += $CardRetainedNamaBanks[$i]['Card Retained'];
  }
  for($i=0;$i<count($CardRetainedNamaBanks);$i++){
    if($totaltopcardRetained == 0)   $pembagi= 1;else $pembagi = $totaltopcardRetained;
            $resultgridnamabank .="<tr><td>" .$CardRetainedNamaBanks[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanks[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanks[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
  }
  $resultgridnamabank .="</table>";



    $msg['isidata'] = $isidata;
	  $msg['resultgrid'] = $resultgrid;
    $msg['resultgridnamabank15'] = $resultgridnamabank15;
    $msg['resultgridnamabank10first'] = $resultgridnamabank10first;
    $msg['resultgridnamabank10next'] = $resultgridnamabank10next;
    $msg['resultgridnamabank'] = $resultgridnamabank;
    $msg['msg']= $data;
    $msg['type'] = "done";
    //$msg['exec'] = $sqlcardRetainedRegion;
          //$msg['msg'] = $dataresult;
          //$msg['msg'] = $result;
    echo json_encode($msg);
    }

}
