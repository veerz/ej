<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->model("allmodel","am");
      $this->load->model("users_model","um");
	  $this->load->model('audit_model',"aum");
    }
    function index()
    {
      $this->session->sess_destroy();
	  $data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Logout",
	        'action'        => "Logout",
	        'detail'        => "User '".$this->session->userdata('username')."' has add data ".$data['Definisi']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
	    $result = $this->db->insert('AuditTrail', $data_audit);
        redirect('login');
		
    }

}
