<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminChange extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('AdminChange_Model');
        $this
            ->load
            ->model('Role_Model');
    	$this
                ->load
                ->model('menu_model');
        }
        /**
         * Index
         *
         * Fungsi ini berfungsi untuk menampilkan userGroup_View
         *
         * @return void
         */
    	 public function Cetak_priv_module(){
    		 $role = $this->session->userdata("role");
    		 $data = $this
    					  ->db
    					  ->select('settings_Menu.*')
    					  ->from('priviledgeRole')
    					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
    					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
    					  ->Where("roles.id_role", $role)
    					  ->Where("type", "MODULE")
    					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
						  ->order_by("priority","ASC")
    					  ->get()->result();
    		//$datalistmenu= $data->result();
    		return $data;
    	 }

    	 public function Cetak_priv_submodule(){
    		 $role = $this->session->userdata("role");
    		 $data = $this
    					  ->db
    					  ->select('settings_Menu.*')
    					  ->from('priviledgeRole')
    					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
    					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
    					  ->Where("roles.id_role", $role)
    					  ->Where("type", "SUBMODULE")
    					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
						  ->order_by("priority","ASC")
    					  ->get()->result();
    		//$datalistmenu= $data->result();
    		return $data;
    	 }

        function index()
        {
    		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
              redirect('login');
            }
    		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
    		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

        $data['all'] = $this->AdminChange_Model->getData()->result();

        $this
            ->load
            ->template('admin/AdminChange', $data);
	}

    
    public function update()
    {
        $this->form_validation->set_rules('id_user', 'id_user', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
		 $data_update = array(
			'password' =>  hash('sha384', $this->input->post('password')),
			'updated_by'  => $this->session->userdata("id_user")
  	    );
		$this->db->where('id_user', $this->input->post('id_user'));
		$result = $this->db->update('users', $data_update);
		
		/* $data = $this
            ->AdminChange_Model
            ->updateData(); */
        $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
		
        $data_audit = array(
  	        'id_user'       => $this->session->userdata('id_user'),
  	        'username'      => $this->session->userdata('username'),
  	        'page'          => "User Management",
  	        'action'        => "Edit",
  	        'detail'        => "User '".$this->session->userdata('username')."' has edit data password of ".$data['name']." in at ".date('Y-m-d H:i:s'),
  	        'created_date'  => date('Y-m-d H:i:s')
  	    );
  			$result = $this->db->insert('AuditTrail', $data_audit);
  			redirect('AdminChange', $data);
    }
}
