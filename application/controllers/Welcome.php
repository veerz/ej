<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Welcome Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/Welcome
 */
class Welcome extends CI_Controller
{

    /**
     * Index
     *
     * Fungsi ini bertugas menampilkan welcome_message View
     *
     * @return void
     */
    function index()
    {
        $this
            ->load
            ->template('welcome_message');
    }
}

