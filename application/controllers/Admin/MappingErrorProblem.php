<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MappingErrorProblem extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('Mappingerrorproblem_Model');
		$this
				->load
				->model('Role_Model');
$this
				->load
				->model('menu_model');
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
			{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			$data['all'] = $this->Mappingerrorproblem_Model->getData()->result();


		$this->load->template('admin\mappingErrorProblem',$data);
	}


	public function add()
	{
		$this->form_validation->set_rules('problem', 'problem', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
			redirect('Admin/MappingErrorProblem');
		}else{
			$data=array(
				"problem"=>$_POST['problem'],
        "error_code"=>$_POST['error_code'],
			);
			$this->db->insert('MappingErrorProblem',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "Mapping Error Problem",
					'action'        => "Add",
					'detail'        => "User '".$this->session->userdata('username')."' has add data ".$data['problem']." in at ".date('Y-m-d H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/MappingErrorProblem');
		}
	}

	public function edit()
	{
		$this->form_validation->set_rules('id_problem', 'id_problem', 'required');
		$this->form_validation->set_rules('problem', 'problem', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('Admin/MappingErrorProblem');
		}else{
			$data=array(
				"problem"=>$_POST['problem'],
        "error_code"=>$_POST['error_code'],
			);
			$this->db->where('id_problem', $_POST['id_problem']);
			$this->db->update('MappingErrorProblem',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Error Problem",
	        'action'        => "Edit",
	        'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['problem']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/MappingErrorProblem');
		}
	}

	public function hapus($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Admin/MappingErrorProblem');
		}else{
			$this->db->where('id_problem', $id);
			$this->db->delete('MappingErrorProblem');
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Error Problem",
	        'action'        => "Delete",
	        'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['problem']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			redirect('Admin/MappingErrorProblem');
		}
	}

}
