<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Audittrail extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this->load->model('audit_model');
    $this
            ->load
            ->model('users_model');
    $this
            ->load
            ->model('audit_model');
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

    $data['user'] = $this->users_model->getData()->result();
    $data['audittrail'] = $this->audit_model->getData()->result();

	 $data['params'] = "";
		$this->load->template('admin/Audittrail', $data);
	}

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {
		$username = $this->input->post('username');
		$start = $this->input->post('startdate');
		$end = $this->input->post('enddate');
		
		if(!empty($username) && empty($start) && $username != "All User"){
			$data = $this->db->select('*')->from('audittrail')->Where("id_user", $username)
					  ->order_by("id_trail","ASC")->get();
		}
		if(!empty($start) && empty($username) ){
			$tanggalsql = "Select * from audittrail where created_date >= '".date($start." 00:00:00.000")."' And  created_date <='".date($end." 23:59:00.000"). "'";
			$data = $this->db->query($tanggalsql);
		}
		if(!empty($start) && !empty($username) && $username != "All User")
		{
			$tanggalsql = "Select * from audittrail where created_date >= '".date($start." 00:00:00.000")."' And  created_date <='".date($end." 23:59:00.000"). "' and id_user =".$username;
			$data = $this->db->query($tanggalsql);
		}
		if($username == "All User" && empty($start))
		{
				$data = $this->db->select('*')->from('audittrail')
					  ->order_by("id_trail","ASC")->get();
		}
		if($username == "All User" && !empty($start))
		{
				$tanggalsql = "Select * from audittrail where created_date >= '".date($start." 00:00:00.000")."' And  created_date <='".date($end." 23:59:00.000"). "'";
			$data = $this->db->query($tanggalsql);
		}

		$dataresult= $data->result();
		
		if ( $dataresult == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "No data found";
		}else{
					$no =0 ;
					$result = "<thead><tr><th>No</th><th>User</th><th>Halaman</th><th>Aksi</th><th>Detail</th></tr></thead><tbody>";

					foreach($dataresult as $row)
					{
						$no++;
						 $result .= "
						 <tr class=\"odd gradeX\">
						<td>{$no}</td>
						<td>{$row->username}</td>
					<td>{$row->page}</td>
						<td>{$row->action}</td>
						<td>{$row->detail}</td>
						</tr>";
					}
					
					$result .= "</tbody>";
					$msg['type'] = "done";
					//$msg['msg'] = $dataresult;
					$msg['msg'] = $result;
					//$msg['id_role'] = $key;
				}
					echo json_encode($msg);
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return JSON data userDetail
     */
    public function getData()
    {
		$username = ($this->input->post("username")?:null);

    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return ARRAY data userDetail
     */
    public function getDataX()
    {

    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {
      if($this->form_validation->run()==FALSE){
          $this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
          redirect('admin/Audittrail');
      }else{
        $data = $this
            ->audit_model
            ->saveData();
      $this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
			redirect('Admin/Audittrail', $data);
				}
    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userDetail
     */
    public function update()
    {
      $data = $this
          ->audit_model
          ->updateData();
      $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
      redirect('admin/audittrail', $data);
    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete($id_trail)
    {
        $this->db->where('id_trail', $id_trail);
        $this->db->delete('AuditTrail');
        $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
        redirect('admin/audittrail');
    }
}
