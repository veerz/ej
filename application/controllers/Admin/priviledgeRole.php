<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * previledgeGroup Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/previledgeGroup
 */
class priviledgeRole extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model previledgeGroup
     */
    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('PriviledgeRole_Model');
        $this
            ->load
            ->model('menu_Model');
        $this
            ->load
            ->model('Role_Model');
        $this
            ->load
            ->model('users_Model');
    }

    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan previledgeGroupView
     *
     * @return void
     */
    function index()
    {
        $role = $this
            ->role_Model
            ->getData();
        $menu = $this
            ->menu_Model
            ->getData();
        $this
            ->load
            ->template('priviledgeRole', array(
            'roles' => $role,
            'settings_Menu' => $menu
        ));
    }

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data previledgeGroup dan menampilkannya.
     *
     *
     * @return JSON data previledgeGroup (Untuk DataTables)
     */
    /*function showData()
    {
        $getdata = $this
            ->priviledgeRole_Model
            ->getData();
        $data = array();
        foreach ($getdata as $value)
        {
            $row = array();
            $row[] = $value->id_prvRole;
            $row[] = $value->name;
            $row[] = $value->name;
            $row[] = ($value->status == 1 ? "Aktif" : "Non Aktif");
            $row[] = '<a href="javascript:void(0);" class="btn btn-warning btn-sm item_edit" data-id="' . $value->id_prvRole . '"><i class="fa fa-edit"></i> </a> <a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' . $value->id_prvRole . '"><i class="fa fa-trash"></i> </a>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data previledgeGroup dan menampilkannya.
     * @return JSON data previledgeGroup
     */
    function getData()
    {
        $id_role = $this
            ->uri
            ->segment(3);
        $data = $this
            ->previledgeGroup_Model
            ->getSingleData($id_role);
        echo json_encode($data);
    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data previledgeGroup
     */
    function save()
    {
        $data = $this
            ->priviledgeRole_Model
            ->saveData();
        echo json_encode($data);
    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data previledgeGroup
     */
    function update()
    {
        $id_role = $this
            ->input
            ->post('id_role');
        $id_menu = $this
            ->input
            ->post('id_menu');
        $value = $this
            ->input
            ->post('value');
        $data = $this
            ->priviledgeRole_Model
            ->updateData();
        $id_user = $this
            ->users_Model
            ->getDatabyGroupID($id_role);
        foreach ($id_user as $val)
        {
            $this
                ->priviledgeUser_Model
                ->UpdateFromRole($val['id_user'], $id_menu, $value, 1);
        }
        echo json_encode($data);

    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data previledgeGroup
     */
    function delete()
    {
        $data = $this
            ->priviledgeRole_Model
            ->deleteData();
        echo json_encode($data);
    }
}
