<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingcard extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('Mappingcardbin_Model');
		$this
				->load
				->model('Role_Model');
		$this
				->load
				->model('menu_model');
		//$db_1 = $this->load->database('db_1', TRUE); # load First DB
		$CRS = $this->load->database('CNEnterprise1', TRUE);  # load Second DB
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			$data['all'] = $this->Mappingcardbin_Model->getData()->result();
			// $data['all2'] = $this->Mappingcardbin_Model->getData2()->result();


		$this->load->template('admin\mappingcard',$data);
	}


	public function add()
	{
		$this->form_validation->set_rules('prefixNo', 'prefixNo', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
			redirect('Admin/Mappingcard');
		}else{
			$data=array(
				"prefixNo"=>$_POST['prefixNo'],
				"bankName"=>$_POST['bankName'],
				"INSERTEDBY"=>$this->session->userdata('username')
				//"cardType"=>$_POST['cardType'],
			);
			$this->db->insert('MsBIN',$data);
			
			$this->CRS_db = $this->load->database('CNEnterprise1', TRUE);
			$dataCRS=array(
				"PREFIXNO"=>$_POST['prefixNo'],
				"BANKNAME"=>$_POST['bankName'],
				"INSERTBY"=>$this->session->userdata('username')
			);
			$this->CRS_db->insert('MSI_PREFIXNO',$dataCRS);
			
			$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
			$data_audit = array(
  				'id_user'       => $this->session->userdata('id_user'),
  				'username'      => $this->session->userdata('username'),
  				'page'          => "Mappping Card Bin Number",
  				'action'        => "Add",
  				'detail'        => "Penambahan Mapping BIN Kartu '".$data['bankName']."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
  				'created_date'  => date('Y-m-d H:i:s')
  		);
  		$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingcard');
		}
	}

	public function edit()
	{
		$this->form_validation->set_rules('idBIN', 'idBIN', 'required');
		$this->form_validation->set_rules('prefixNo', 'prefixNo', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('Admin/Mappingcard');
		}else{
			$data=array(
				"prefixNo"=>$_POST['prefixNo'],
				"bankName"=>$_POST['bankName'],
				"INSERTEDBY"=>$this->session->userdata('username')
			);
			$this->db->where('idBIN', $_POST['idBIN']);
			$this->db->update('MsBIN',$data);
			
			$this->CRS_db = $this->load->database('CNEnterprise1', TRUE);
			$dataCRS=array(
				"PREFIXNO"=>$_POST['prefixNo'],
				"BANKNAME"=>$_POST['bankName'],
				"INSERTBY"=>$this->session->userdata('username')
			);
			$this->CRS_db->where('ID', $_POST['idBIN']);
			$this->CRS_db->update('MSI_PREFIXNO',$dataCRS);
			
			// auditrail edit
			
			// $sql = "Select * from AuditTrail where id =''".('idBIN')."'";
			// $data = $this->db->query($tanggalsql);
			// $dataresult= $data->result();
			
			// $prefixold ="";
			// $bankold ="";
			
			// foreach($dataresult as $row){
				// $prefixold = $row->PREFIXNO;
				// $bankold = $row->BANKNAME;
			// }
			
			// $updateinfo = "User '".$this->session->userdata('username');
			// if($prefix != $prefixold){
				// $updateinfo .= "prefixNo";
			// }
			// if($bank != $bankold){
				// $updateinfo .= "bankName";
			// }
			// $updateinfo .= date('Y-m-d H:i:s');
			
			//batas auditrail edit
			
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "Mapping Card Bin Number",
					'action'        => "Edit",
					'detail'        => "User '".$this->session->userdata('username')."' has edit data".$prefixold." menjadi ".$data['prefixNo']." in at ".date('Y-m-d H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingcard');
		}
	}

	public function hapus($idBIN)
	{
		if($idBIN==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Admin/Mappingcard');
		}else{
			$this->db->where('idBIN', $idBIN);
			$this->db->delete('MsBIN');
			
			$this->CRS_db = $this->load->database('CNEnterprise1', TRUE);
			$this->CRS_db->where('ID',$idBIN);
			$this->CRS_db->delete('MSI_PREFIXNO');
			
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "Mapping Card Bin Number",
					'action'        => "Delete",
					'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['prefixNo']." in at ".date('Y-m-d H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingcard');
		}
	}

}
