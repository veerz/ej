<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userGroup Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userGroup
 */
class Role extends CI_Controller
{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userGroup
     */
    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('Role_Model');
		$this
            ->load
						->model('menu_model');
						$this->load->model("allmodel","am");
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					 // ->Order_by("settings_Menu.id_menu", 'asc')
					   ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					 // ->Order_by("settings_Menu.id_menu", 'asc')
					   ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
 /* yang diatas ini copy kesemua Controller */

        $data['all'] = $this->Role_Model->getData()->result();
        $data['menus'] = $this->menu_model->getData();

        $this
            ->load
            ->template('admin/Role', $data);
    }
    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userGroup dan menampilkannya.
     *
     * @return JSON data userGroup
     */
    public function showData()
    {
        $getdata = $this
            ->Role_Model
            ->getData();
        $data = array();
        foreach ($getdata as $value)
        {
            $row = array();
            $row[] = $value->id_role;
            $row[] = $value->name;
            $row[] = '<a href="javascript:void(0);" class="btn btn-warning btn-sm item_edit" data-id="' . $value->id_role . '"><i class="fa fa-edit"></i> </a> <a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' . $value->id_role . '"><i class="fa fa-trash"></i> </a>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userGroup dan menampilkannya.
     * @return JSON data userGroup
     */
    function getData()
    {
        $id_role = $this
            ->uri
            ->segment(3);
        $data = $this
            ->Role_Model
            ->getSingleData($id_role);
        echo json_encode($data);
    }

    function find_item(){
			$msg = array();
			$key = $this->input->post("key");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "No data found";
			}
			else{
					$data = $this
					  ->db
					  ->select('settings_Menu.id_menu,settings_Menu.name as menuname, settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $key)
					  //->order_by("menuname","ASC")
					  ->order_by("priority","ASC")
					  ->get();
				$menus = $this->menu_model->getData();
				$dataresult= $data->result();
				$tempget = array();
				if ( $data == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "No data found";
				}else{
					$result = "";
					/* foreach($dataresult as $item)
					{
						$tempidmenu = $item->id_menu;
						array_push($tempget,$tempidmenu);
						//$result .= "<option selected=\"selected\" value=\"{$item->id_menu}\">{$item->menuname}</option>";
						 $result .= "<input type=\"checkbox\"  id=\"menulist-{$item->id_menu}\"  class=\"menuall\"  name=\"menu[]\" value=\"{$item->id_menu}\" checked=\"checked\"> {$item->menuname}<br/>";
					} */
					$msg['type'] = "done";
					//$msg['msg'] = $dataresult;
					$msg['msg'] = $dataresult;
					
					$msg['id_role'] = $key;
				}
			}
			echo json_encode($msg);
		}
    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userGroup
     */
    function save()
    {
			$this->form_validation->set_rules('name', 'name', 'required');
			if($this->form_validation->run()==FALSE){
					$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
					redirect('Admin/Role');
			}else{
				$aname = $this->input->post("name");
			$data = $this
				->Role_Model
				->saveData();
				$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
        $data_audit = array(
    				'id_user'       => $this->session->userdata('id_user'),
    				'username'      => $this->session->userdata('username'),
    				'page'          => "Role & Access",
    				'action'        => "Add",
    				'detail'        => "Penambahan Role '".$aname."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
    				'created_date'  => date('Y-m-d H:i:s')
    		);
        $result = $this->db->insert('AuditTrail', $data_audit);
				redirect('Admin/Role');
			}
    }
    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userGroup
     */
    function update()
    {
			$this->form_validation->set_rules('name', 'name', 'required');
      $data = $this
          ->Role_Model
          ->updateData();
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
      $data_audit = array(
          'id_user'       => $this->session->userdata('id_user'),
          'username'      => $this->session->userdata('username'),
          'page'          => "Role & Access",
          'action'        => "Edit",
          'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['name']." in at ".date('Y-m-d H:i:s'),
          'created_date'  => date('Y-m-d H:i:s')
      );
      $result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Role');
    }
    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     * =
     * @return JSON data userGroup
     */
    public function delete($id_role)
   	{
   		if($id_role==""){
   			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
   			redirect('Admin/Role');
   		}else{
				// $this->db->where('id_role', $id_role);
				// $this->db->delete('priviledgeRole');
				
				//ini yg baru
				// $cekuseraktif = "";
				// $listmatchrole = "SELECT id_user FROM users, roles WHERE id_role = id_role";
				// $matchrole = $this->db->query($listmatchrole);
				// $data['matchrole'] = $matchrole->result_array();
				
				// if($matchrole == )
				// $result = "SELECT id_user,id_role FROM users WHERE id_role = id_role";

				// $userexists = "";
				$listcekuser = $this->am->query_data("SELECT id_user, id_role FROM users WHERE id_role = id_role");
				if(count($listcekuser) <1){
					$this->db->where('id_role', $id_role);
					$this->db->delete('priviledgeRole');
					$this->db->where('id_role', $id_role);
					$this->db->delete('roles');
					
					$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
					$data_audit = array(
							'id_user'       => $this->session->userdata('id_user'),
							'username'      => $this->session->userdata('username'),
							'page'          => "Role & Access",
							'action'        => "Delete",
							'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['name']." in at ".date('Y-m-d H:i:s'),
							'created_date'  => date('Y-m-d H:i:s')
					);
					$result = $this->db->insert('AuditTrail', $data_audit);
					redirect('Admin/Role');
				}else{
					$this->response['msg'] = "Masih ada user aktif di dalam role tersebut";
					echo json_encode($this->response);
					echo "<script>
					alert('Masih ada user aktif di dalam role tersebut');
					</script>";
					//echo "<script> window.location.href='Admin/Role';</script>";
					redirect('Admin/Role');
				}
				
				// }else{
				// 	$this->session->set_flashdata('error',"Masih ada user aktif di role tersebut");
				// 	redirect('Admin/Role');
				// 	window.open "('Masih ada user aktif di role tersebut')";
				// }
   		}
   	}

	// public function deleteRole(){
	// 		$msg = array();
	// 		$key = $this->input->post("key");
	// 		if ( empty($key) ){
	// 			$msg['type'] = 'failed';
	// 			$msg['msg'] = "No data found";
	// 		}
	// 		else{
	// 			$listcekuser = $this->am->query_data("SELECT id_user, id_role FROM users WHERE id_role = id_role");
	// 			if(count($listcekuser) <1){
	// 				$this->db->where('id_role', $id_role);
	// 				$this->db->delete('priviledgeRole');
	// 				$this->db->where('id_role', $id_role);
	// 				$this->db->delete('roles');
					
	// 				$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
	// 				$data_audit = array(
	// 						'id_user'       => $this->session->userdata('id_user'),
	// 						'username'      => $this->session->userdata('username'),
	// 						'page'          => "Role & Access",
	// 						'action'        => "Delete",
	// 						'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['name']." in at ".date('Y-m-d H:i:s'),
	// 						'created_date'  => date('Y-m-d H:i:s')
	// 				);
	// 				$result = $this->db->insert('AuditTrail', $data_audit);
	// 				redirect('Admin/Role');
	// 				}
	// 				else{

	// 				}
	// 				$msg['id_role'] = $key;
	// 			}
	// 		}

		// public function deleteRole(){
			// $msg = array();
			// $key = $this->input->post("key");
			// if ( empty($key) ){
				// $msg['type'] = 'failed';
				// $msg['msg'] = "No data found";
			// }
			// else{
				// $listcekuser = $this->am->query_data("SELECT id_user, id_role FROM users WHERE id_role = id_role");
				// if(count($listcekuser) <1){
					// $this->db->where('id_role', $id_role);
					// $this->db->delete('priviledgeRole');
					// $this->db->where('id_role', $id_role);
					// $this->db->delete('roles');
					
					// $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
					// $data_audit = array(
							// 'id_user'       => $this->session->userdata('id_user'),
							// 'username'      => $this->session->userdata('username'),
							// 'page'          => "Role & Access",
							// 'action'        => "Delete",
							// 'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['name']." in at ".date('Y-m-d H:i:s'),
							// 'created_date'  => date('Y-m-d H:i:s')
					// );
					// $result = $this->db->insert('AuditTrail', $data_audit);
					// $hasil = 'Roles tersebut telah dihapus';
					// }
					// else{
						// $hasil = 'Roles tersebut masih memiliki user aktif';
					// }
				
					// $msg['id_role'] = $key;
					// $msg['hasil'] = $hasil;
					// $msg['type'] = "done";
					// echo json_encode($msg);
				// }
		// }
		
		public function deleteRole(){
			$msg = array();
			$key = $this->input->post("key");
			$id_role = $this->input->post("key");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "No data found";
			}
			else{
				$listcekuser = $this->am->query_data("SELECT id_user FROM users WHERE id_role = '".$key."'");
				if(!$listcekuser){
					$this->db->where('id_role', $id_role);
					$this->db->delete('priviledgeRole');
					$this->db->where('id_role', $id_role);
					$this->db->delete('roles');
					
					$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
					$data_audit = array(
							'id_user'       => $this->session->userdata('id_user'),
							'username'      => $this->session->userdata('username'),
							'page'          => "Role & Access",
							'action'        => "Delete",
							'detail'        => "User '".$this->session->userdata('username')."' has delete data at ".date('Y-m-d H:i:s'),
							'created_date'  => date('Y-m-d H:i:s')
					);
					$result = $this->db->insert('AuditTrail', $data_audit);
					$hasil = 'Roles tersebut telah dihapus';
					$hapus='ya';
				}
				else{
					$hasil = 'Roles tersebut masih memiliki user aktif';
					$hapus = 'tidak';
				}
				$msg['jumlah'] = count($listcekuser);
				$msg['id_role'] = $key;
				$msg['hasil'] = $hasil;
				$msg['type'] = "done";
				$msg['hapus'] = $hapus;
			}
			echo json_encode($msg);
		}
		
}
