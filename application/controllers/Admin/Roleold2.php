<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userGroup Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userGroup
 */
class Role extends CI_Controller
{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userGroup
     */

    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }

    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */

    function index()
    {
        $data['all'] = $this->Role_Model->getData()->result();
        $data['menus'] = $this->menu_model->getData();
        //$data['priviledgeRole'] = $this->PriviledgeRole_Model->getByRole();
        $this
            ->load
            ->template('admin/Role', $data);
    }

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userGroup dan menampilkannya.
     *
     * @return JSON data userGroup
     */
    public function showData()
    {
        $getdata = $this
            ->Role_Model
            ->getData();
        $data = array();
        foreach ($getdata as $value)
        {
            $row = array();
            $row[] = $value->id_role;
            $row[] = $value->name;
            $row[] = '<a href="javascript:void(0);" class="btn btn-warning btn-sm item_edit" data-id="' . $value->id_role . '"><i class="fa fa-edit"></i> </a> <a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' . $value->id_role . '"><i class="fa fa-trash"></i> </a>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userGroup dan menampilkannya.
     * @return JSON data userGroup
     */
    function getData()
    {
        $id_role = $this
            ->uri
            ->segment(3);
        $data = $this
            ->Role_Model
            ->getSingleData($id_role);
        echo json_encode($data);
    }

    function find_item(){
			$msg = array();
			// if ( $_POST ){
				$key = $this->input->post("key");
				if ( empty($key) ){
					$msg['type'] = 'failed';
					$msg['msg'] = "No data found";
				}
				else{
          $data = $this
              ->db
              ->select('priviledgeRole.id_role,priviledgeRole.name, roles.id_role,roles.name, settings_Menu.id_menu,settings_Menu.name as menuname')
              ->from('priviledgeRole')
              ->join('roles', 'roles.id_role = priviledgeRole.id_role')
              ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
              ->Where("roles.id_role", $key)
              ->get();

					if ( $data == '0' ){
						$msg['type'] = "failed";
						$msg['msg'] = "No data found";
					}
					else{
            $result = "";
						foreach($data as $item)
            {
              $result .= "<option value=\"{$item->id_menu}\">{$item->menuname}</option>";
            }
						$msg['type'] = "done";
						$msg['msg'] = $result;
					}
				}
			// }
			// else{
			// 	$msg['type'] = 'failed';
			// 	$msg['msg'] = "Invalid parameter";
			// }

			//echo json_encode($msg);
      var_dump($msg);
      die();

		}

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userGroup
     */
    function save()
    {
			$this->form_validation->set_rules('name', 'name', 'required');
			if($this->form_validation->run()==FALSE){
					$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
					redirect('Admin/Role');
			}else{
        $data = $this
            ->Role_Model
            ->saveData();
				$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
				redirect('Admin/Role');
					}
    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userGroup
     */
    function update()
    {
			$this->form_validation->set_rules('name', 'name', 'required');
      $data = $this
          ->Role_Model
          ->updateData();
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			redirect('Admin/Role');
    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     * =
     * @return JSON data userGroup
     */
     public function delete($id_role)
   	{
   		if($id_role==""){
   			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
   			redirect('Admin/Role');
   		}else{
   			$this->db->where('id_role', $id_role);
   			$this->db->delete('roles');
   			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
   			redirect('Admin/Role');
   		}
   	}

	public function createAccess(){
		$menulist = $this->input->post('menu');

		$this->db->where('id_role', 1);
   			//$this->db->delete('priviledgeRole');
		foreach($menulist as $key){
     //foreach($rolelist as $roles){
      $data = array(
				'id_role' => '1',
				'id_menu' => $key,
        'created_date'  => date('Y-m-d H:i:s'),
        'updated_date'  => date('Y-m-d H:i:s'),
        'created_by'    => 0, //development only
        'updated_by'    => 0
        //development only
			);
			$this->db->insert('priviledgeRole',$data);
//}
		}

		$this->session->set_flashdata('sukses',"Data Berhasil Diupdate");
   		redirect('Admin/Role');


	}


}
