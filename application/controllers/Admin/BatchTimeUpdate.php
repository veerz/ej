<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class BatchTimeUpdate extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this->load->model('batchtimeupdate_Model');
        $this
            ->load
            ->model('Role_Model');
		    $this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
    		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
              redirect('login');
            }
	      $data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
	      $data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
        $data['all'] = $this->batchtimeupdate_Model->getData()->result();

        $this->load->template('admin/batchtimeupdate', $data);
        // $this->load->view('layouts/footerView');
	}

    public function edit()
	{
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('timeupdate', 'timeupdate', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('Admin/BatchTimeUpdate');
		}else{
			$data=array(
				"timeupdate"=>$_POST['timeupdate'],
			);
			$this->db->where('id', $_POST['id']);
			$this->db->update('batchtimeUpdate',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
      $data_audit = array(
          'id_user'       => $this->session->userdata('id_user'),
          'username'      => $this->session->userdata('username'),
          'page'          => "Batch Time Update",
          'action'        => "Edit",
          'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['timeupdate']." in at ".date('Y-m-d H:i:s'),
          'created_date'  => date('Y-m-d H:i:s')
      );
			redirect('Admin/BatchTimeUpdate');
		}
	}
}
