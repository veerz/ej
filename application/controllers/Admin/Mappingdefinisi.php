<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingdefinisi extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('Mappingdefinisi_Model');
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
		$this->load->model('audit_model',"aum");
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
{
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
					redirect('login');
				}
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
		$data['all'] = $this->Mappingdefinisi_Model->getData()->result();

		$this->load->template('admin\mappingdefinisi',$data);
	}


	public function add()
	{
		$this->form_validation->set_rules('Definisi', 'Definisi', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
			redirect('Admin/Mappingdefinisi');
		}else{
			$data=array(
				"Definisi"=>$_POST['Definisi'],
			);
			$this->db->insert('MappingDefinisi',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Definisi",
	        'action'        => "Add",
	        'detail'        => "User '".$this->session->userdata('username')."' has logged out at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
	    $result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingdefinisi');
		}
	}

	public function edit()
	{
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('Definisi', 'Definisi', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('Admin/Mappingdefinisi');
		}else{
			$data=array(
				"Definisi"=>$_POST['Definisi'],
			);
			$this->db->where('status', $_POST['status']);
			$this->db->update('MappingDefinisi',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Definisi",
	        'action'        => "Edit",
	        'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['Definisi']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingdefinisi');
		}
	}

	public function hapus($status)
	{
		if($status==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Admin/Mappingdefinisi');
		}else{
			$this->db->where('status', $status);
			$this->db->delete('MappingDefinisi');
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Definisi",
	        'action'        => "Delete",
	        'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['Definisi']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingdefinisi');
		}
	}

}
