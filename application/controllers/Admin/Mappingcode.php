<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingcode extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('Mappingcode_Model');
		// $this->load->model('Mappingdefinisi_Model');
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
		$this->load->model('audit_model',"aum");
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			// $data['mapdef'] = $this->Mappingdefinisi_Model->getData()->result();
			$data['mapcode'] = $this->Mappingcode_Model->getData()->result();


		$this->load->template('admin/mappingcode', $data);
	}
	/**
	 * Save
	 *
	 * Fungsi ini bertugas melakukan save data.
	 *
	 * @return JSON data userDetail
	 */
	public function save()
	{
		$this->form_validation->set_rules('code', 'code', 'required');
		// $this->form_validation->set_rules('priority', 'priority', 'required');
		// $this->form_validation->set_rules('status', 'status', 'required');
		// $this->form_validation->set_rules('is_Active', 'is_Active', 'required');
		if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
				redirect('Admin/mappingcode');
		}else{
			$acode = $this->input->post("code");
			
			$data = $this
					->Mappingcode_Model
					->saveData();
		$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "Mapping Code",
				'action'        => "Add",
				'detail'        => "Penambahan Mapping Code '".$acode."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);
		redirect('Admin/mappingcode', $data);
			}
	}

	/**
	 * Update
	 *
	 * Fungsi ini bertugas melakukan update data
	 *
	 * @return JSON data userDetail
	 */
	public function update()
	{
			$this->form_validation->set_rules('status', 'status', 'required');
			// $this->form_validation->set_rules('name', 'name', 'required');
			// $this->form_validation->set_rules('username', 'username', 'required');
			// $this->form_validation->set_rules('password', 'password', 'required');
			$data = $this
					->Mappingcode_Model
					->updateData();
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Code",
	        'action'        => "Edit",
	        'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['code']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/mappingcode', $data);
	}

	/**
	 * Delete
	 *
	 * Fungsi ini bertugas melakukan delete data.
	 *
	 * @return JSON data userDetail
	 */
	public function delete($errorSuccessCodeIDefsD)
	{
			$this->db->where('errorSuccessCodeIDefsD', $errorSuccessCodeIDefsD);
			$this->db->delete('errorSuccessCodeDefs');
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Code",
	        'action'        => "Delete",
	        'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['code']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/mappingcode');
		}

		function find_item(){
			$msg = array();
			$key = $this->input->post("key");
			// echo $key;die();
			// $info = $this->input->post("info");
			if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "No data found";
			}
			else{
					$data = $this
						->db
						->select('*')
						->from('errorSuccessCodeDefs')
						->where("errorSuccessCodeIDefsD", $key)
						->get()->result_array();
				if ( $data == '0' ){
					$msg['type'] = "failed";
					$msg['msg'] = "No data found";
				}else{
					$result = "";
				
					foreach($data as $row){
						$msg['codes'] = $row['Definition'];
						$msg['definitions'] = $row['code'];
						$msg['statuss'] = $row['status'];
						$msg['isActives'] = $row['isActive'];
						$msg['prioritys'] = $row['priority'];
					}
					$msg['type'] = "done";
					//$msg['msg'] = $dataresult;
				
					$msg['errorSuccessCodeIDefsD'] = $key;
				}
			}
			echo json_encode($msg);
		}

}
