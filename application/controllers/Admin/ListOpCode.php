<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListOpCode extends CI_Controller {

	function __construct()
	{
			parent::__construct();
			// $this->load->model('Listopcode_Model');
			// $this->load->model('Transtypedef_Model');
			$this->load->model('Role_Model');
			$this->load->model('menu_model');
			}
			/**
			 * Index
			 *
			 * Fungsi ini berfungsi untuk menampilkan userGroup_View
			 *
			 * @return void
			 */
		public function Cetak_priv_module(){
			 $role = $this->session->userdata("role");
			 $data = $this
							->db
							->select('settings_Menu.*')
							->from('priviledgeRole')
							->join('roles', 'roles.id_role = priviledgeRole.id_role')
							->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
							->Where("roles.id_role", $role)
							->Where("type", "MODULE")
							/* ->Order_by("settings_Menu.id_menu", 'asc') */
							->order_by("priority","ASC")
							->get()->result();
			//$datalistmenu= $data->result();
			return $data;
		 }

		public function Cetak_priv_submodule(){
			 $role = $this->session->userdata("role");
			 $data = $this
							->db
							->select('settings_Menu.*')
							->from('priviledgeRole')
							->join('roles', 'roles.id_role = priviledgeRole.id_role')
							->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
							->Where("roles.id_role", $role)
							->Where("type", "SUBMODULE")
							/* ->Order_by("settings_Menu.id_menu", 'asc') */
							->order_by("priority","ASC")
							->get()->result();
			//$datalistmenu= $data->result();
			return $data;
		 }

		function index()
			{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
			}
					
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			$Listopcode = "SELECT DISTINCT t.transactionTypeID,o.OPCode,t.transactionType,t.transactionGroup,ob.modelType 
			FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodesTemp(nolock)o ON t.transactionTypeID=o.transactionTypeID 
			left join objects(nolock)ob on o.objectID=ob.objectID WHERE o.OPCode is NOT NULL
			ORDER BY t.transactionGroup ,t.transactionType , ob.modelType ASC"; 
			
			//ditambahkan orderby untuk pengecekan id  ORDER BY transactionTypeID ASC, OPCode ASC, modelType ASC
			$listopcode = $this->db->query($Listopcode);
			$data['listopcode'] = $listopcode->result_array();

			$listtransactionType = "SELECT transactiontypeID, transactionType, transactionGroup FROM transactionTypeDefinition(nolock)";
			$transactionType = $this->db->query($listtransactionType);
			$data['transactionType'] = $transactionType->result_array();

			$listmodelType = "SELECT DISTINCT modelType FROM objects(nolock)";
			$modelType = $this->db->query($listmodelType);
			$data['modelType'] = $modelType->result_array();

			$data['params'] = "";
			$this->load->template('admin/listopcode', $data);
		}


	public function saveData()
	{
		$OPCode = $this->input->post("OPCode");
		$transactionType = $this->input->post("transactionType");
		$modelType = $this->input->post("modelType");

		$param = "";
		if($OPCode!=null)	$param .=  " @OPCode='".$OPCode."',";
		else $param .=  " @OPCode=NULL,";
		if($transactionType!=null)	$param .=  " @transactionType='".$transactionType."',";
		else $param .=  " @transactionType=NULL,";
		// if($transactionGroup!=null)	$param .=  " @transactionGroup='".$transactionGroup."',";
		// else $param .=  " @transactionGroup=NULL,";
		if($modelType!=null)	$param .=  " @modelType='".$modelType."'";
		else $param .=  " @modelType=NULL";
		// echo $param;die();

		$listopcode = "Exec InsertOpcode".$param;
		$listopcode = $this->db->query($listopcode);
		$data['listopcode'] = $listopcode->result_array();
		//echo $param; die();
		
		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "List OP Code",
				'action'        => "Add",
				'detail'        => "Penambahan List Op Code '".$OPCode."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);
		redirect('Admin/ListOpCode');
	}

	/**
	 * Update
	 *
	 * Fungsi ini bertugas melakukan update data
	 *
	 * @return JSON data userDetail
	 */

	function find_item(){
		$msg = array();
		$key = $this->input->post("key");
		$opcode = $this->input->post("opcodes");
		$mt = $this->input->post("moType");
		// echo $mt;die();
		if ( empty($key) ){
				$msg['type'] = 'failed';
				$msg['msg'] = "No data found";
		}
		else{
			if($mt == ""){
			$mt = "NULL";
				$Listopcode = "SELECT DISTINCT t.transactionTypeID as trans_id,o.OPCode as opcode,t.transactionType as tt,t.transactionGroup as tg,ob.modelType as mt
				FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodesTemp(nolock)o ON t.transactionTypeID=o.transactionTypeID 
				left join objects(nolock)ob on o.objectID=ob.objectID WHERE o.OPCode is NOT NULL
				AND t.transactionTypeID = '".$key."' AND o.OPCode = '".$opcode."' AND ob.modelType is $mt
				ORDER BY t.transactionGroup, t.transactionType, ob.modelType ASC";
			
			}if($mt == "" && $opcode ==""){
				$mt = "NULL";
				$opcode ="NULL";
				$Listopcode = "SELECT DISTINCT t.transactionTypeID as trans_id,o.OPCode as opcode,t.transactionType as tt,t.transactionGroup as tg,ob.modelType as mt
				FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodesTemp(nolock)o ON t.transactionTypeID=o.transactionTypeID 
				left join objects(nolock)ob on o.objectID=ob.objectID WHERE o.OPCode is NOT NULL
				AND t.transactionTypeID = '".$key."' AND o.OPCode IS '".$opcode."' AND ob.modelType is $mt
				ORDER BY t.transactionGroup, t.transactionType, ob.modelType ASC";
			}
			else{
			$Listopcode = "SELECT DISTINCT t.transactionTypeID as trans_id,o.OPCode as opcode,t.transactionType as tt,t.transactionGroup as tg,ob.modelType as mt
			FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodesTemp(nolock)o ON t.transactionTypeID=o.transactionTypeID 
			left join objects(nolock)ob on o.objectID=ob.objectID WHERE o.OPCode is NOT NULL
			and t.transactionTypeID = '".$key."' AND o.OPCode = '".$opcode."' AND ob.modelType = '".$mt."'
			ORDER BY t.transactionGroup, t.transactionType, ob.modelType ASC";
			}
			//echo $Listopcode;die();
			$listopcode = $this->db->query($Listopcode);
			$dataresult = $listopcode->result();
			


			foreach($dataresult as $item)
			{
				$msg['opcode'] = $item->opcode;
				$msg['trans_id'] = $item->trans_id;
				$msg['tt'] = $item->tt;
				$msg['tg'] = $item->tg;
				$msg['mt'] = $item->mt; //ini tambahan
			}
			$tempget = array();
			if ( $dataresult == '0' ){
				$msg['type'] = "failed";
				$msg['msg'] = "No data found";
			}else{
				$msg['type'] = "done";
				$msg['msg'] = $msg;
				//$msg['msg'] = $result;
				$msg['trans_id'] = $key;
			}
		}
		echo json_encode($msg);
	}
   
	public function updateData()
	{
		$transTypeID = $this->input->post('etransactionType');
		$modelType = $this->input->post('emodelType');
		$updatedOPCode = $this->input->post('editOPCode');
		//echo $transTypeID;die();
		$param = "";
		if($transTypeID!=null)	$param .=  " @transTypeID='".$transTypeID."',";
		else $param .=  " @transTypeID=NULL,";
		if($modelType!=null)	$param .=  " @modelType='".$modelType."',";
		else $param .=  " @modelType=NULL,";
		if($updatedOPCode!=null)	$param .=  " @updatedOPCode='".$updatedOPCode."'";
		else $param .=  " @updatedOPCode=NULL";

		// echo $param;die();
		$listopcode = "Exec updateOPCodes".$param;
		$listopcode = $this->db->query($listopcode);
		$data['listopcode'] = $listopcode->result_array();
		
		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "List OP Code",
				'action'        => "Add",
				'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$updatedOPCode." in at ".date('Y-m-d H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);

		redirect('Admin/ListOpCode');
	}

	/**
	 * Delete
	 *
	 * Fungsi ini bertugas melakukan delete data.
	 *
	 * @return JSON data userDetail
	 */
	public function delete()
	{
		$msg = array();
		$transTypeID = $this->input->post("key");
		$modelType = $this->input->post("moType");
		$deletedOpcode = $this->input->post("opcodes");
		// echo $transTypeID;die();
		if ( empty($transTypeID) ){
			$msg['type'] = 'failed';
			$msg['msg'] = "No data found";
		}
		else{
			$param = "";
			if($transTypeID!=null)	$param .=  " @transTypeID='".$transTypeID."',";
			else $param .=  " @transTypeID=NULL,";
			if($modelType!=null)	$param .=  " @modelType='".$modelType."',";
			else $param .=  " @modelType=NULL,";
			if($deletedOpcode!=null)	$param .=  " @deletedOpcode='".$deletedOpcode."'";
			else $param .=  " @deletedOpcode=NULL";

			// echo $param;die();
			$listopcode = "Exec deleteOpcodes".$param;
			$listopcode = $this->db->query($listopcode);
			$data['listopcode'] = $listopcode->result();

			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "List OP Code",
					'action'        => "Delete",
					'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$deletedOpcode." in at ".date('Y-m-d H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			$msg['type'] = "done";
	
			echo json_encode($msg);
		}
	}
}
