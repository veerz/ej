<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListOpCode extends CI_Controller {

	function __construct()
	{
			parent::__construct();
			$this
					->load
					->model('Listopcode_Model');
			$this
					->load
					->model('transactionType_Model');
			// $this
			// 		->load
			// 		->model('objects_Model');
			$this
							->load
							->model('Role_Model');
			$this
							->load
							->model('menu_model');
			}
			/**
			 * Index
			 *
			 * Fungsi ini berfungsi untuk menampilkan userGroup_View
			 *
			 * @return void
			 */
		 public function Cetak_priv_module(){
			 $role = $this->session->userdata("role");
			 $data = $this
							->db
							->select('settings_Menu.*')
							->from('priviledgeRole')
							->join('roles', 'roles.id_role = priviledgeRole.id_role')
							->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
							->Where("roles.id_role", $role)
							->Where("type", "MODULE")
							/* ->Order_by("settings_Menu.id_menu", 'asc') */
							->order_by("priority","ASC")
							->get()->result();
			//$datalistmenu= $data->result();
			return $data;
		 }

		 public function Cetak_priv_submodule(){
			 $role = $this->session->userdata("role");
			 $data = $this
							->db
							->select('settings_Menu.*')
							->from('priviledgeRole')
							->join('roles', 'roles.id_role = priviledgeRole.id_role')
							->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
							->Where("roles.id_role", $role)
							->Where("type", "SUBMODULE")
							/* ->Order_by("settings_Menu.id_menu", 'asc') */
							->order_by("priority","ASC")
							->get()->result();
			//$datalistmenu= $data->result();
			return $data;
		 }

			function index()
			{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			// $listopcode = "Exec InsertOpcode".$param;
			// $opcode = $this->db->query($listopcode);
			// $data['opcode'] = $opcode->result_array();

			$listtransactionGroup = "SELECT DISTINCT transactionGroup FROM transactionTypeDefinition";
			$transactionGroup = $this->db->query($listtransactionGroup);
			$data['transactionGroup'] = $transactionGroup->result_array();

			//$listmodelType = "SELECT DISTINCT modelType FROM objects";
			//$modelType = $this->db->query($listmodelType);
			//$data['modelType'] = $modelType->result_array();

			$Listopcode = "SELECT DISTINCT o.OPCode,t.transactionType,t.transactionGroup FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodes(nolock)o ON t.transactionTypeID=o.transactionTypeID";
			$listopcode = $this->db->query($Listopcode);
			$data['listopcode'] = $listopcode->result_array();

			/*$data['listopcode'] = $this->Listopcode_Model->getData()->result(); */
			$data['trxtype'] = $this->transactionType_Model->getData()->result();

			$data['params'] = "";
			$this->load->template('admin/listopcode', $data);
}

	public function getData()
	{

		$param = "";
		if($transactionType!=null)	$param .=  " @transactionType='".$transactionType."',";
		else $param .=  " @transactionType=NULL,";
		if($transactionGroup!=null)	$param .=  " @transactionGroup='".$transactionGroup."',";
		else $param .=  " @transactionGroup=NULL,";
		if($OPCode!=null)	$param .=  " @OPCode='".$OPCode."',";
		else $param .=  " @OPCode=NULL,";
		if($modelType!=null)	$param .=  " @modelType='".$modelType."',";
		else $param .=  " @modelType=NULL,";

		$listopcode = "Exec InsertOpcode".$param;
		$opcode = $this->db->query($listopcode);
		$data['opcode'] = $opcode->result_array();

		$listtransactionGroup = "SELECT DISTINCT transactionGroup FROM transactionTypeDefinition";
		$transactionGroup = $this->db->query($listtransactionGroup);
		$data['transactionGroup'] = $transactionGroup->result_array();

		$listmodelType = "SELECT DISTINCT modelType FROM objects";
		$modelType = $this->db->query($listmodelType);
		$data['modelType'] = $modelType->result_array();

		$this->load->template('listopcode',$data);
	}

	/**
	 * Save
	 *
	 * Fungsi ini bertugas melakukan save data.
	 *
	 * @return JSON data userDetail
	 */
	// public function save()
	// {
	// 	$this->form_validation->set_rules('OPCode', 'OPCode', 'required');
	// 	if($this->form_validation->run()==FALSE){
	// 			$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
	// 			redirect('Admin/listopcode');
	// 	}else{
	// 		$data = $this
	// 				->Listopcode_Model
	// 				->saveData();
	// 	$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
	// 	redirect('Admin/listopcode', $data);
	// 		}
	// }

	public function saveData()
	{
		$transactionType = $this->input->post("transactionType");
		$transactionGroup = $this->input->post("transactionGroup");
		$OPCode = $this->input->post("OPCode");
		$modelType = $this->input->post("modelType");


		$param = "";
		if($transactionType!=null)	$param .=  " @transactionType='".$transactionType."',";
		else $param .=  " @transactionType=NULL,";
		if($transactionGroup!=null)	$param .=  " @transactionGroup='".$transactionGroup."',";
		else $param .=  " @transactionGroup=NULL,";
		if($OPCode!=null)	$param .=  " @OPCode='".$OPCode."',";
		else $param .=  " @OPCode=NULL,";
		if($modelType!=null)	$param .=  " @modelType='".$modelType."'";
		else $param .=  " @modelType=NULL";

		$listopcode = "Exec InsertOpcode".$param;
		$opcode = $this->db->query($listopcode);
		$data['opcode'] = $opcode->result_array();

		$listtransactionGroup = "SELECT DISTINCT transactionGroup FROM transactionTypeDefinition";
		$transactionGroup = $this->db->query($listtransactionGroup);
		$data['transactionGroup'] = $transactionGroup->result_array();

		$listmodelType = "SELECT DISTINCT modelType FROM objects";
		$modelType = $this->db->query($listmodelType);
		$data['modelType'] = $modelType->result_array();

		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "List OP Code",
				'action'        => "Add",
				'detail'        => "User '".$this->session->userdata('username')."' has add data ".$data['transactionTypeID']." in at ".date('Y-m-d H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);

		$this->load->template('listopcode',$data);
	}

	/**
	 * Update
	 *
	 * Fungsi ini bertugas melakukan update data
	 *
	 * @return JSON data userDetail
	 */

	 function find_item(){
		 $msg = array();
		 $key = $this->input->post("key");
		 if ( empty($key) ){
			 	$msg['type'] = 'failed';
			 	$msg['msg'] = "No data found";
		 }
		 else{
			 	$Listopcode = "SELECT DISTINCT o.OPCode,t.transactionType,t.transactionGroup FROM transactionTypeDefinition(nolock)t LEFT JOIN OPCodes(nolock)o ON t.transactionTypeID=o.transactionTypeID";
				$listopcode = $this->db->query($Listopcode);
				$data['listopcode'] = $listopcode->result_array();

			 $dataresult= $data->result();
			 $tempget = array();
			 if ( $data == '0' ){
				 $msg['type'] = "failed";
				 $msg['msg'] = "No data found";
			 }else{
				 $result = "";

				 $msg['type'] = "done";
				 //$msg['msg'] = $dataresult;
				 $msg['msg'] = $result;
				 $msg['OPCode'] = $key;
			 }
		 }
		 echo json_encode($msg);
	 }

	// public function update()
	// {
	// 		$this->form_validation->set_rules('OPCode', 'OPCode', 'required');
	// 		$menulist = $this->input->post('menu');
	// 		$OPCode = $this->input->post('OPCode');
	// 		//delete roles ulang
	//
	// 		$this->db->where('OPCode', $OPCode);
	// 		$this->db->delete('OPCOdes');
	//
	// 		foreach($menulist as $key){
	// 			$data = array(
	// 				'OPCode' => $OPCode,
	// 				'id_menu' => $key,
	// 				'created_date'  => date('Y-m-d H:i:s'),
	// 				'updated_date'  => date('Y-m-d H:i:s'),
	// 				'created_by'    => 0,
	// 				'updated_by'    => 0
	// 			);
	// 			$this->db->insert('priviledgeRole',$data);
	// 		}
	//
	// 		// $data = $this
	// 		// 		->Listopcode_Model
	// 		// 		->updateData();
	// 		$data_audit = array(
	// 				'id_user'       => $this->session->userdata('id_user'),
	// 				'username'      => $this->session->userdata('username'),
	// 				'page'          => "List OP Code",
	// 				'action'        => "Edit",
	// 				'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['transactionTypeID']." in at ".date('Y-m-d H:i:s'),
	// 				'created_date'  => date('Y-m-d H:i:s')
	// 		);
	// 		$result = $this->db->insert('AuditTrail', $data_audit);
	// 		$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
	// 		redirect('admin/listopcode', $data);
	// }

	public function updateData()
	{
		$transactionType = $this->input->post("transactionType");
		$transactionGroup = $this->input->post("transactionGroup");
		$OPCode = $this->input->post("OPCode");
		$modelType = $this->input->post("modelType");


		$param = "";
		if($transactionType!=null)	$param .=  " @transactionType='".$transactionType."',";
		else $param .=  " @transactionType=NULL,";
		if($transactionGroup!=null)	$param .=  " @transactionGroup='".$transactionGroup."',";
		else $param .=  " @transactionGroup=NULL,";
		if($OPCode!=null)	$param .=  " @OPCode='".$OPCode."',";
		else $param .=  " @OPCode=NULL,";
		if($modelType!=null)	$param .=  " @modelType='".$modelType."'";
		else $param .=  " @modelType=NULL";

		// $listopcode = "Exec InsertOpcode".$param;
		// $opcode = $this->db->query($listopcode);
		// $data['opcode'] = $opcode->result_array();

		$listtransactionGroup = "SELECT DISTINCT transactionGroup FROM transactionTypeDefinition";
		$transactionGroup = $this->db->query($listtransactionGroup);
		$data['transactionGroup'] = $transactionGroup->result_array();

		$listmodelType = "SELECT DISTINCT modelType FROM objects";
		$modelType = $this->db->query($listmodelType);
		$data['modelType'] = $modelType->result_array();

		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "List OP Code",
				'action'        => "Add",
				'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['OPCode']." in at ".date('Y-m-d H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);

		$this->load->template('listopcode',$data);
	}

	/**
	 * Delete
	 *
	 * Fungsi ini bertugas melakukan delete data.
	 *
	 * @return JSON data userDetail
	 */
	public function delete($id)
	{
			$this->db->where('id', $id);
			$this->db->delete('OPCodes');
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "List OP Code",
					'action'        => "Delete",
					'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['transactionTypeID']." in at ".date('Y-m-d H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/listopcode');
		}

}
