<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionTypeDefinition extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('transactionType_Model');
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
		$this->load->model('audit_model',"aum");
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			
			$data['transtypedef'] = $this->transactionType_Model->getData()->result();
			
			$listtransactionGroup = "SELECT DISTINCT transactionGroup FROM transactionTypeDefinition";
			$transactionGroup = $this->db->query($listtransactionGroup);
			$data['transactionGroup'] = $transactionGroup->result_array();
			$data['transactionGroups'] = $transactionGroup->result_array();
		$this->load->template('admin/TransactionTypeDefinition', $data);
	}

	public function save()
	{
		$newtransactionGroup = $this->input->post('newtransactionGroup');
		$transactionType     = $this->input->post('transactionType');
		$transactionGroup    = $this->input->post('transactionGroup');
		if($newtransactionGroup != NULL)
				$transactionGroup = $newtransactionGroup;
		
		$this->form_validation->set_rules('transactionType', 'transactionType', 'required');
	
		if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
				redirect('Admin/TransactionTypeDefinition');
		}else{	

			$data_transaction = array(
				'transactionType' => $transactionType,
				'transactionGroup' => $transactionGroup,
		);
		
		$result = $this->db->insert('transactionTypeDefinition', $data_transaction);
		$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
		$data_audit = array(
				'id_user'       => $this->session->userdata('id_user'),
				'username'      => $this->session->userdata('username'),
				'page'          => "Mapping Jenis Transaksi",
				'action'        => "Add",
				'detail'        => "Penambahan Mapping Jenis Transaksi '".$data_transaction['transactionType']."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
				'created_date'  => date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('AuditTrail', $data_audit);
		redirect('Admin/TransactionTypeDefinition', $data);
			}
	}

	public function update()
	{
		$newtransactionGroup = $this->input->post('enewtransactionGroup');
		$transactionType     = $this->input->post('etransactionType');
		$transactionGroup    = $this->input->post('etransactionGroup');
		$transtypeID    	 = $this->input->post('etranstypeID');
		if($newtransactionGroup != NULL)
				$transactionGroup = $newtransactionGroup;
		
		$this->form_validation->set_rules('etransactionType', 'etransactionType', 'required');
		
		if($this->form_validation->run()==FALSE){
				$this->session->set_flashdata('error',"Data Gagal Di Update");
				redirect('Admin/TransactionTypeDefinition');
		}else{		
			$data_transaction = array(
					'transactionType'  => $transactionType,
					'transactionGroup' => $transactionGroup,
			);
		// $result = $this->db->insert('transactionTypeDefinition', $data_transaction);
		
		
		$this->db->where('transactionTypeID', $transtypeID);
		// var_dump($transtypeID);die();
        $result = $this->db->update('transactionTypeDefinition',$data_transaction);
						
		$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
		$data_audit = array(
		'id_user'       => $this->session->userdata('id_user'),
		'username'      => $this->session->userdata('username'),
		'page'          => "Mapping Jenis Transaksi",
		'action'        => "Edit",
		'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['transactionType']." in at ".date('Y-m-d H:i:s'),
		'created_date'  => date('Y-m-d H:i:s')
	    );
		$result = $this->db->insert('AuditTrail', $data_audit);
		redirect('Admin/TransactionTypeDefinition', $data);
		}
	}

	public function delete($transactionTypeID)
	{
		if($transactionTypeID==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Admin/TransactionTypeDefinition');
		}else{
		$this->db->where('transactionTypeID', $transactionTypeID);
		$this->db->delete('transactionTypeDefinition');
		$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
		$data_audit = array(
		'id_user'       => $this->session->userdata('id_user'),
		'username'      => $this->session->userdata('username'),
		'page'          => "Mapping Jenis Transaksi",
		'action'        => "Delete",
		'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['transactionType']." in at ".date('Y-m-d H:i:s'),
		'created_date'  => date('Y-m-d H:i:s')
	);
		$result = $this->db->insert('AuditTrail', $data_audit);
		redirect('Admin/TransactionTypeDefinition');
		}
	}

	function find_item(){
		$msg = array();
		$key = $this->input->post("key");
		// $info = $this->input->post("info");
		if ( empty($key) ){
			$msg['type'] = 'failed';
			$msg['msg'] = "No data found";
		}
		else{
				$data = $this
					->db
					->select('*')
					->from('transactionTypeDefinition')
					->where("transactionTypeID", $key)
					->get()->result_array();
			if ( $data == '0' ){
				$msg['type'] = "failed";
				$msg['msg'] = "No data found";
			}else{
				$result = "";
			
				foreach($data as $row){
					$msg['transtypeID'] = $row['transactionTypeID'];
					$msg['transtype'] = $row['transactionType'];
					$msg['transgroup'] = $row['transactionGroup'];
				}
				$msg['type'] = "done";
				//$msg['msg'] = $dataresult;
			
				$msg['transtypeID'] = $key;
			}
		}
		echo json_encode($msg);
	}

}
