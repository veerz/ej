<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserManagement extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('users_model');
        $this
            ->load
            ->model('Role_Model');
    		$this
                ->load
                ->model('menu_model');
        }
        /**
         * Index
         *
         * Fungsi ini berfungsi untuk menampilkan userGroup_View
         *
         * @return void
         */
    	 public function Cetak_priv_module(){
    		 $role = $this->session->userdata("role");
    		 $data = $this
    					  ->db
    					  ->select('settings_Menu.*')
    					  ->from('priviledgeRole')
    					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
    					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
    					  ->Where("roles.id_role", $role)
    					  ->Where("type", "MODULE")
    					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
						  ->order_by("priority","ASC")
    					  ->get()->result();
    		//$datalistmenu= $data->result();
    		return $data;
    	 }

    	 public function Cetak_priv_submodule(){
    		 $role = $this->session->userdata("role");
    		 $data = $this
    					  ->db
    					  ->select('settings_Menu.*')
    					  ->from('priviledgeRole')
    					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
    					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
    					  ->Where("roles.id_role", $role)
    					  ->Where("type", "SUBMODULE")
    					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
						  ->order_by("priority","ASC")
    					  ->get()->result();
    		//$datalistmenu= $data->result();
    		return $data;
    	 }

        function index()
        {
    		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
              redirect('login');
            }
    		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
    		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

        $data['role'] = $this->Role_Model->getData()->result();
        $data['user'] = $this->users_model->getData()->result();

        $this
            ->load
            ->template('admin/usermanagement', $data);
	}

    // public function showData()
    // {
    //   $getdata = $this
    //     ->users_model
    //     ->getData();
    //   $data = array();
    //   foreach ($getdata as $value)
    //   {
    //       $row = array();
    //       $row[] = $value->id_user;
    //       $row[] = $value->name;
    //       $row[] = $value->username;
    //       $row[] = $value->id_role;
    //       $row[] = ($value->status == 1 ? "Aktif" : "Non Aktif");
    //       $row[] = '<a href="javascript:void(0);" class="btn btn-warning btn-sm item_edit" data-id="' . $value->id_user . '"><i class="fa fa-edit"></i> </a> <a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' . $value->id_user . '"><i class="fa fa-trash"></i> </a>';
    //       $data[] = $row;
    //   }

    //   $output = array(
    //       "data" => $data,
    //   );

    //   echo json_encode($output);
    // }

    // public function getData()
    // {
    //   $id_user = $this
    //       ->uri
    //       ->segment(3);
    //   $data = $this
    //       ->users_model
    //       ->getSingleData($id_user);
    //   echo json_encode($data);
    // }
    /*
    // public function getDataX()
    // {
    //     $id_role = $this
    //         ->uri
    //         ->segment(3);
    //     $data = $this
    //         ->users_model
    //         ->getDatabyRoleID($id_role);
    //     echo $data;
    // }
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {
      $this->form_validation->set_rules('name', 'name', 'required');
      $this->form_validation->set_rules('username', 'username', 'required');
      $this->form_validation->set_rules('password', 'password', 'required');
      if($this->form_validation->run()==FALSE){
          $this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
          redirect('Admin/usermanagement');
      }else{
		  $aname = $this->input->post("name");
        $data = $this
            ->users_model
            ->saveData();
      $this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
      $data_audit = array(
  				'id_user'       => $this->session->userdata('id_user'),
  				'username'      => $this->session->userdata('username'),
  				'page'          => "User Management",
  				'action'        => "Add",
  				'detail'        => "Penambahan User '".$aname."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
  				'created_date'  => date('Y-m-d H:i:s')
  		);
  		$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/usermanagement', $data);
				}
    }

    public function update()
    {
        $this->form_validation->set_rules('role', 'id_role', 'required');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $data = $this
            ->users_model
            ->updateData();
        $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
        $data_audit = array(
  	        'id_user'       => $this->session->userdata('id_user'),
  	        'username'      => $this->session->userdata('username'),
  	        'page'          => "User Management",
  	        'action'        => "Edit",
  	        'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['name']." in at ".date('Y-m-d H:i:s'),
  	        'created_date'  => date('Y-m-d H:i:s')
  	    );
  			$result = $this->db->insert('AuditTrail', $data_audit);
  			redirect('Admin/usermanagement', $data);
    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete($id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->delete('users');
        $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
        $data_audit = array(
  	        'id_user'       => $this->session->userdata('id_user'),
  	        'username'      => $this->session->userdata('username'),
  	        'page'          => "User Management",
  	        'action'        => "Delete",
  	        'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['name']." in at ".date('Y-m-d H:i:s'),
  	        'created_date'  => date('Y-m-d H:i:s')
  	    );
  			$result = $this->db->insert('AuditTrail', $data_audit);
        redirect('Admin/usermanagement');
      }

      function find_item(){
        $msg = array();
        $key = $this->input->post("key");
        // $info = $this->input->post("info");
        if ( empty($key) ){
          $msg['type'] = 'failed';
          $msg['msg'] = "No data found";
        }
        else{
            $data = $this
              ->db
              ->select('*')
              ->from('users')
              ->where("id_user", $key)
              ->get()->result_array();
          if ( $data == '0' ){
            $msg['type'] = "failed";
            $msg['msg'] = "No data found";
          }else{
            $result = "";
          
            foreach($data as $row){
              $msg['id_users'] = $row['id_user'];
              $msg['id_roles'] = $row['id_role'];
              $msg['names'] = $row['name'];
              $msg['usernames'] = $row['username'];
              $msg['passwords'] = 'ABCDEFG';
              $msg['statuss'] = $row['status'];
            }
            $msg['type'] = "done";
            //$msg['msg'] = $dataresult;
          
            $msg['id_users'] = $key;
          }
        }
        echo json_encode($msg);
      }


    }
