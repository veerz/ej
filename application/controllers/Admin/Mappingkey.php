<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingkey extends CI_Controller {

	function __construct(){
		parent::__Construct();
		$this->load->model('Mappingkeyword_Model');
		$this->load->model('Role_Model');
    $this->load->model('menu_model');
}
/**
 * Index
 *
 * Fungsi ini berfungsi untuk menampilkan userGroup_View
 *
 * @return void
 */
public function Cetak_priv_module(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "MODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

public function Cetak_priv_submodule(){
 $role = $this->session->userdata("role");
 $data = $this
				->db
				->select('settings_Menu.*')
				->from('priviledgeRole')
				->join('roles', 'roles.id_role = priviledgeRole.id_role')
				->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
				->Where("roles.id_role", $role)
				->Where("type", "SUBMODULE")
				/* ->Order_by("settings_Menu.id_menu", 'asc') */
				->order_by("priority","ASC")
				->get()->result();
//$datalistmenu= $data->result();
return $data;
}

function index()
{
			if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
						redirect('login');
					}
			$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
			$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

			$data['all'] = $this->Mappingkeyword_Model->getData()->result();


		$this->load->template('admin\mappingkey',$data);
	}


	public function add()
	{
		$this->form_validation->set_rules('keyword', 'keyword', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
			redirect('Admin/Mappingkey');
		}else{
			$data=array(
				"keyword"=>$_POST['keyword'],
				// "sqlWhereCondition" => $_POST['sqlWhereCondition']
			);
			$this->db->insert('keywordList',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
			$data_audit = array(
					'id_user'       => $this->session->userdata('id_user'),
					'username'      => $this->session->userdata('username'),
					'page'          => "Mapping Key Error Problem",
					'action'        => "Add",
					'detail'        => "Penambahan Mapping Keyword List EJ '".$data['keyword']."' Pada ".date('Y-m-d')." Pukul ".date('H:i:s'),
					'created_date'  => date('Y-m-d H:i:s')
			);
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingkey');
		}
	}

	public function edit()
	{
		$this->form_validation->set_rules('keywordID', 'keywordID', 'required');
		$this->form_validation->set_rules('keyword', 'keyword', 'required');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('Admin/Mappingkey');
		}else{
			$data=array(
				"keyword"=>$_POST['keyword'],
				// "sqlWhereCondition" => $_POST['sqlWhereCondition']
			);
			$this->db->where('keywordID', $_POST['keywordID']);
			$this->db->update('keywordList',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Diedit");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Key Error Problem",
	        'action'        => "Edit",
	        'detail'        => "User '".$this->session->userdata('username')."' has edit data ".$data['keyword']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingkey');
		}
	}

	public function hapus($id)
	{
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('Admin/Mappingkey');
		}else{
			$this->db->where('keywordID', $id);
			$this->db->delete('keywordList');
			$this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
			$data_audit = array(
	        'id_user'       => $this->session->userdata('id_user'),
	        'username'      => $this->session->userdata('username'),
	        'page'          => "Mapping Key Error Problem",
	        'action'        => "Delete",
	        'detail'        => "User '".$this->session->userdata('username')."' has delete data ".$data['keyword']." in at ".date('Y-m-d H:i:s'),
	        'created_date'  => date('Y-m-d H:i:s')
	    );
			$result = $this->db->insert('AuditTrail', $data_audit);
			redirect('Admin/Mappingkey');
		}
	}

}
