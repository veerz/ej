<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userGroup Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userGroup
 */
class adminTransaction extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userGroup
     */
    function __construct()
    {
        parent::__construct();
        $this
            ->load
            ->model('userGroup_Model');
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public

    function index()
    {
        $this
            ->load
            ->template('userGroup_View');
    }

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userGroup dan menampilkannya.
     *
     * @return JSON data userGroup
     */
    function showData()
    {
        $data = $this
            ->userGroup_Model
            ->getData();
        echo json_encode($data);
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userGroup dan menampilkannya.
     * @return JSON data userGroup
     */
    function getData()
    {
        $groupID = $this
            ->uri
            ->segment(3);
        $data = $this
            ->userGroup_Model
            ->getSingleData($groupID);
        echo json_encode($data);
    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userGroup
     */
    function save()
    {
        $data = $this
            ->userGroup_Model
            ->saveData();
        echo json_encode($data);
    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userGroup
     */
    function update()
    {
        $data = $this
            ->userGroup_Model
            ->updateData();
        echo json_encode($data);
    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userGroup
     */
    function delete()
    {
        $data = $this
            ->userGroup_Model
            ->deleteData();
        echo json_encode($data);
    }
}

