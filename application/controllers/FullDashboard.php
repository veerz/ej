<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class FullDashboard extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();

		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

        $this->load->view('layouts/headerViewFull');
        $this->load->view('fulldashboard', $data);
        $this->load->view('layouts/footerViewFull');
	}

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {
      if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
            redirect('login');
          }
          $sqlpieupdate = "Exec getEJUpdateStatusSummary";
      		$qpieupdate = $this->db->query($sqlpieupdate);
      		$data['piedataupdate'] = $qpieupdate->result_array();
          $bardataPie = $data['piedataupdate'];

      		$sqlbarMerk= "Exec getEJUpdateStatusByMerk";
      		$barMerk = $this->db->query($sqlbarMerk);
      		$data['bardataMerk'] = $barMerk->result_array();
          $bardataMerks = $data['bardataMerk'];

      		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel";
      		$barRegion = $this->db->query($sqlbarRegion);
      		$data['bardataregion'] = $barRegion->result_array();
          $bardataregions = $data['bardataregion'];

      		$cardRetainedRegion= "Exec getCardRetainedRegion";
      		$cardRetainedRegion = $this->db->query($cardRetainedRegion);
      		$data['cardRetainedRegion'] = $cardRetainedRegion->result_array();
          $cardRetainedRegions =   $data['cardRetainedRegion'];//DG

      		$cardRetainedMerk= "Exec getCardRetainedMerk";
      		$cardRetainedMerk = $this->db->query($cardRetainedMerk);
      		$data['cardRetainedMerk'] = $cardRetainedMerk->result_array();

      		$CardRetainedNamaBank= "Exec getCardRetainedNamaBank";
      		$CardRetainedNamaBank = $this->db->query($CardRetainedNamaBank);
      		$data['CardRetainedNamaBank'] = $CardRetainedNamaBank->result_array();
          $CardRetainedNamaBank = $data['CardRetainedNamaBank'];

          // $CardRetainedNamaBanktop15= "Exec getCardRetainedNamaBanktop15";
      		// $CardRetainedNamaBanktop15 = $this->db->query($CardRetainedNamaBanktop15);
      		// $data['CardRetainedNamaBanktop15'] = $CardRetainedNamaBanktop15->result_array();
          // $CardRetainedNamaBanktop152 =   $data['CardRetainedNamaBanktop15'];//DG

          // $CardRetainedNamaBanktop5= "Exec getCardRetainedNamaBanktop5";
      		// $CardRetainedNamaBanktop5 = $this->db->query($CardRetainedNamaBanktop5);
      		// $data['CardRetainedNamaBanktop5'] = $CardRetainedNamaBanktop5->result_array();
          // $CardRetainedNamaBanktop5 =   $data['CardRetainedNamaBanktop5'];//DG

          $CardRetainedNamaBanktop10= "Exec getCardRetainedNamaBanktop10";
          $CardRetainedNamaBanktop10 = $this->db->query($CardRetainedNamaBanktop10);
          $data['CardRetainedNamaBanktop10'] = $CardRetainedNamaBanktop10->result_array();
          $CardRetainedNamaBanktop10s =   $data['CardRetainedNamaBanktop10'];//DG

          $Replenishment = "Exec getReplenishment";
      		$Replenishment = $this->db->query($Replenishment);
      		$data['Replenishment'] = $Replenishment->result_array();

      		$StatusTrxMapping = "Exec getStatusTrxDetail";
      		$StatusTrxMapping = $this->db->query($StatusTrxMapping);
      		$data['StatusTrxMapping'] = $StatusTrxMapping->result_array();

      		$StatusTrxBad = "Exec getTop10BadPerformance";
      		$StatusTrxBad = $this->db->query($StatusTrxBad);
      		$data['StatusTrxBad'] = $StatusTrxBad->result_array();

      		$StatusTrxGood = "Exec getTop10GoodPerformance";
      		$StatusTrxGood = $this->db->query($StatusTrxGood);
      		$data['StatusTrxGood'] = $StatusTrxGood->result_array();

      		$StatusTrxByJenis = "Exec getJenisTrx";
      		$StatusTrxByJenis = $this->db->query($StatusTrxByJenis);
      		$data['StatusTrxByJenis'] = $StatusTrxByJenis->result_array();
          $StatusTrxJeniss =  $data['StatusTrxByJenis'];

          $StatusTrxByJenis5 = "Exec getJenisTrxtop5";
      		$StatusTrxByJenis5 = $this->db->query($StatusTrxByJenis5);
      		$data['StatusTrxByJenis5'] = $StatusTrxByJenis5->result_array();
          $StatusTrxJenis5 =  $data['StatusTrxByJenis5'];

      		$regiontrx = "Exec getStatusTrxRegion";
      		$regiontrx = $this->db->query($regiontrx);
      		$data['regiontrx'] = $regiontrx->result_array();
          $regiontrxs = $data['regiontrx'];//DG

          // $cekmerk = ( $merk?:0);//DG
        	// $cekterm= ( $termID?:0);//DG
          // $cekregion = ( $region?:0);//DG
          // $cekarea= ( $area?:0);//DG
          // $cekmanageby= ( $managedBy?:0);//DG
          // $cekmerk = ( $merk?:0);//DG

          $resultgridpie = "";
        	$resultgridpie .="<table id=\"piedata\" class=\"table table-bordered\" style=\"font-size:10px;width:280px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff;width:180px\"><label>Status EJ</label></td><td bgcolor=#4A4A4B style=\"color:#fff;width:50px\"><label>Jumlah</label></td><td bgcolor=#4A4A4B style=\"color:#fff;width:50px\"><label>Persentase</label></td>";
          // for($i=0;$i<count($bardataPie);$i++){
          //         	$resultgridpie .="<td>" .$bardataPie[$i]['merk']. "</td>";
          // }
          // "<td>" .($bardataPie[$i]['Update']/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100 . "</td>"
          $resultgridpie .="</tr><tr>
                    <td><label>Update</label></td>";
          for($i=0;$i<count($bardataPie);$i++){
                  	$resultgridpie .="<td><center>" .number_format($bardataPie[$i]['Update'],0,',','.'). "</center></td><td><center>" .round(($bardataPie[$i]['Update']/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100,2) . "% </center></td>";
      		}
          $resultgridpie .="</tr><tr>
                    <td><label>Tidak Update</label></td>";
          for($i=0;$i<count($bardataPie);$i++){
                  	$resultgridpie .="<td><center>" .number_format($bardataPie[$i]['notUpdate'],0,',','.'). "</center></td><td><center>" .round(($bardataPie[$i]['notUpdate']/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100,2) . "% </center></td>";
        	}
          $resultgridpie .="</tr><tr>
                    <td><label>Total</label></td>";
          for($i=0;$i<count($bardataPie);$i++){
                  	$resultgridpie .="<td><center>" .number_format(($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']),0,',','.'). "</center></td><td><center>" .(($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate'])/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100 . "% </center></td>";
        	}
          $resultgridpie .="</tr></table>";

          $resultgridregion = "";
        	$resultgridregion .="<table id=\"regiondata\" class=\"table table-bordered \" style=\"font-size:9px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Status EJ</label></center></td>";
          for($i=0;$i<count($bardataregions);$i++){
      		    $resultgridregion .="<td bgcolor=#4A4A4B style=\"color:#fff\"><center>" .$bardataregions[$i]['regionAlias']. "</center></td>";
      		}
          $resultgridregion .="</tr><tr>
                    <td><label>Update</label></td>";
          for($i=0;$i<count($bardataregions);$i++){
                  	$resultgridregion .="<td><center>" .number_format($bardataregions[$i]['Update'],0,',','.'). "</center></td>";
      		}
          $resultgridregion .="</tr><tr>
                    <td><label>Tidak Update</label></td>";
          for($i=0;$i<count($bardataregions);$i++){
                  	$resultgridregion .="<td><center>" .number_format($bardataregions[$i]['notUpdate'],0,',','.'). "</center></td>";
        	}
          $resultgridregion .="</tr></table>";


          //DG Merk
          $resultgridmerk = "";
        	$resultgridmerk .="<table id=\"merkdata\" class=\"table table-bordered \" style=\"font-size:10px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Status EJ</label></center></td>";
          for($i=0;$i<count($bardataMerks);$i++){
                  	$resultgridmerk .="<td bgcolor=#4A4A4B style=\"color:#fff\"><center>" .$bardataMerks[$i]['merk']. "</center></td>";
          }
          $resultgridmerk .="</tr><tr>
                    <td><label>Update</label></td>";
          for($i=0;$i<count($bardataMerks);$i++){
                  	$resultgridmerk .="<td><center>" .number_format($bardataMerks[$i]['Update'],0,',','.'). "</center></td>";
      		}
          $resultgridmerk .="</tr><tr>
                    <td><label>Tidak Update</label></td>";
          for($i=0;$i<count($bardataMerks);$i++){
                  	$resultgridmerk .="<td><center>" .number_format($bardataMerks[$i]['notUpdate'],0,',','.'). "</center></td>";
        	}
          $resultgridmerk .="</tr></table>";

          $resultgridCardRegion = "";
        	$resultgridCardRegion .="<table id=\"cardRegion\" class=\"table table-bordered \" style=\"font-size:8px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td>";
          for($i=0;$i<count($cardRetainedRegions);$i++){
              $resultgridCardRegion .="<td bgcolor=#4A4A4B style=\"color:#fff\"><center>" .$cardRetainedRegions[$i]['regionAlias']. "</center></td>";
          }
        	$resultgridCardRegion .="</tr><tr>
                                    <td><label>Jumlah</label></td>";
        	for($i=0;$i<count($cardRetainedRegions);$i++){
                  	$resultgridCardRegion .="<td><center>" .$cardRetainedRegions[$i]['CardRetained']. "</center></td>";
        			}
        	$resultgridCardRegion .="</tr></table>";

        	// //ini tempat resultgridnamabank top5
        	// $resultgridnamabank5 = "";
        	// $resultgridnamabank5 .= "<table id=\"datanamabank5\" class=\"table-bordered \" style=\"font-size:10px;width:400px;height:200px; \"><tr>
          //                           <td bgcolor=#4A4A4B style=\"color:#fff;width:200px\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff;width:50px\"><center><label>Jumlah</label></center></td>
          //                           <td bgcolor=#4A4A4B style=\"color:#fff;width:50px\"><center><label>Persentase</label></center></td></tr>";
          // $totaltop5cardRetained = 0;
          // for($i=0;$i<count($CardRetainedNamaBanktop5);$i++){
          //     $totaltop5cardRetained += $CardRetainedNamaBanktop5[$i]['Card Retained'];
          // }
          // for($i=0;$i<count($CardRetainedNamaBanktop5);$i++){
          //           $resultgridnamabank5 .="<tr><td>" .$CardRetainedNamaBanktop5[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop5[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop5[$i]['Card Retained'] / $totaltop5cardRetained)*100,2)." %</center></td></tr>";
          // }
          // $resultgridnamabank5 .="</table>";

          //ini tempat resultgridnamabank 5 pertama
          $resultgridnamabank10first = "";
          $resultgridnamabank10first .= "<table id=\"datanamabank10first\" class=\"table table-bordered \" style=\"font-size:8px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td></tr>";
          $totaltop10cardRetained = 0;
          $pembagi = 1;
            for($i=0;$i<count($CardRetainedNamaBanktop10s);$i++){
              $totaltop10cardRetained += $CardRetainedNamaBanktop10s[$i]['Card Retained'];
            }
            
            //create table 1-5 nama bank
          if (count($CardRetainedNamaBanktop10s) > 5){
            
            for($i=0;$i<5;$i++){
              if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
              $resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
            }
            
            }
          else{ 
            for($i=0; $i <count($CardRetainedNamaBanktop10s) ;$i++ ) {
              if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
              $resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
            }
            }
          /* 
          for($i=0;$i<count($CardRetainedNamaBanktop10s)/2;$i++){
              $totaltop10cardRetained += $CardRetainedNamaBanktop10s[$i]['Card Retained'];
          }
          for($i=0;$i<count($CardRetainedNamaBanktop10s)/2;$i++){
            if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
                    $resultgridnamabank10first .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
          } */
          $resultgridnamabank10first .="</table>";

          //ini tempat resultgridnamabank 5 pertama
          $resultgridnamabank10next = "";
          
          // $totaltop10cardRetained = 0;
            
          //create table 5-10 nama bank
          $pembagi = 1;

          if (count($CardRetainedNamaBanktop10s) > 5){
            $resultgridnamabank10next .= "<table id=\"datanamabank10next\" class=\"table table-bordered \" style=\"font-size:8px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td></tr>";
            for($i=5;$i<count($CardRetainedNamaBanktop10s);$i++){
              if($totaltop10cardRetained == 0)   $pembagi= 1;else $pembagi = $totaltop10cardRetained;
              $resultgridnamabank10next .="<tr><td>" .$CardRetainedNamaBanktop10s[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBanktop10s[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBanktop10s[$i]['Card Retained']/$pembagi)*100,2). "%</center></td>" ;
            }	
            $resultgridnamabank10next .="</table>";
            }
          else{ 
            
          }

          //ini tempat resultgridnamabank all
        	$resultgridnamabank = "";
        	$resultgridnamabank .= "<table id=\"datanamabank\" class=\"table table-bordered \" style=\"font-size:10px;\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Kartu Tertelan</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Jumlah</label></center></td>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Persentase</label><center></td></tr>";
          $totalcardRetained = 0;
          for($i=0;$i<count($CardRetainedNamaBank);$i++){
              $totalcardRetained += $CardRetainedNamaBank[$i]['Card Retained'];
          }
          for($i=0;$i<count($CardRetainedNamaBank);$i++){
                    $resultgridnamabank .="<tr><td>" .$CardRetainedNamaBank[$i]['bankName']. "</td><td><center>" .number_format($CardRetainedNamaBank[$i]['Card Retained'],0,',','.'). "</center></td><td><center>". round(($CardRetainedNamaBank[$i]['Card Retained'] / $totalcardRetained)*100,2)." %</center></td></tr>";
          }
          $resultgridnamabank .="</table>";

          //DG region transaksi
          $resulttransregion = "";
        	$resulttransregion .="<table id=\"regiontrans\" class=\"table table-bordered \" style=\"font-size:10px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td>";
          for($i=0;$i<count($regiontrxs);$i++){
            if($i%2 == 0)
      			$resulttransregion .= "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['Region']."</td>";
        }
          $resulttransregion .="</tr><tr>
                    <td><label>Gagal</label></td>";
          for($i=0;$i<count($regiontrxs);$i++){
              if($regiontrxs[$i]['Status'] == "Gagal")
                  	$resulttransregion .="<td>" .number_format($regiontrxs[$i]['Jumlah'],0,',','.'). "</td>";
      		}
          $resulttransregion .="</tr><tr>
                    <td><label>Sukses</label></td>";
          for($i=0;$i<count($regiontrxs);$i++){
              if($regiontrxs[$i]['Status'] == "Sukes")
                  	$resulttransregion .="<td>" .number_format($regiontrxs[$i]['Jumlah'],0,',','.'). "</td>";
        	}
          $resulttransregion .="</tr></table>";

          //ini top 5 jenis trans ga di pake
          $resultgridjenistrans5 = "";
          $resultgridjenistrans5 .= "<table id=\"jenistrans5\" class=\"table table-bordered \" style=\"font-size:8px;padding:0.4rem!important\"><tr>
                                    <td style=\"font-size:8px;padding:0.4rem!important\"><center>JENIS TRANSAKSI</center></td><td style=\"font-size:8px;padding:0.4rem!important\"><center>TOTAL</center></td></tr>";
          for($i=0;$i<count($StatusTrxJenis5);$i++){
            $resultgridjenistrans5 .= "<tr><td style=\"font-size:8px;padding:0.4rem!important\">". $StatusTrxJenis5[$i]['transactiongroup']. "</td><td style=\"font-size:8px;padding:0.4rem!important\">". $StatusTrxJenis5[$i]['Jumlah'] ."</td></tr>";
          }
          $resultgridjenistrans5 .= "</table>";

          $resultgridjenistrans = "";
          $resultgridjenistrans .= "<table id=\"jenistrans\" class=\"table table-bordered \" style=\"font-size:10px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center>JENIS TRANSAKSI</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>TOTAL</center></td></tr>";
          for($i=0;$i<count($StatusTrxJeniss);$i++){
            $resultgridjenistrans .= "<tr><td>". $StatusTrxJeniss[$i]['transactiongroup']. "</td><td>". number_format($StatusTrxJeniss[$i]['Jumlah'],0,',','.') ."</td></tr>";
          }
          $resultgridjenistrans .= "</table>";

          //ini untuk jenis transaksi dari array 0-4
          $resultgridjenistransfirst = "";
          $resultgridjenistransfirst .= "<table id=\"jenistransfirst\" class=\"table table-bordered \" style=\"font-size:8px;align=center\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center>JENIS TRANSAKSI</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>TOTAL</center></td></tr>";
          for($i=0;$i<5;$i++){
            $resultgridjenistransfirst .= "<tr><td>". $StatusTrxJeniss[$i]['transactiongroup']. "</td><td>". number_format($StatusTrxJeniss[$i]['Jumlah'],0,',','.') ."</td></tr>";
          }
          $resultgridjenistransfirst .= "</table>";
  
          //ini untuk jenis transaksi dari array 5-selesai
          $resultgridjenistransnext = "";
          $resultgridjenistransnext .= "<table id=\"jenistransnext\" class=\"table table-bordered \" style=\"font-size:8px;align=center;margin-right:30px\"><tr>
                                    <td bgcolor=#4A4A4B style=\"color:#fff\"><center>JENIS TRANSAKSI</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>TOTAL</center></td></tr>";
          for($i=5;$i<count($StatusTrxJeniss);$i++){
            $resultgridjenistransnext .= "<tr><td>". $StatusTrxJeniss[$i]['transactiongroup']. "</td><td>". number_format($StatusTrxJeniss[$i]['Jumlah'],0,',','.') ."</td></tr>";
          }
          $resultgridjenistransnext .= "</table>";

          $msg['resultgridpie'] = $resultgridpie;
          $msg['resultgridregion'] = $resultgridregion; //DG statmach 0 Reg
          $msg['resultgridmerk'] = $resultgridmerk; //DG statmach 0 Merk
        	$msg['resultgridCardRegion'] = $resultgridCardRegion;
          // $msg['resultgridnamabank5'] = $resultgridnamabank5;
          $msg['resultgridnamabank10first'] = $resultgridnamabank10first;
          $msg['resultgridnamabank10next'] = $resultgridnamabank10next;
          $msg['resultgridnamabank'] = $resultgridnamabank;
          $msg['resulttransregion'] = $resulttransregion;
          $msg['resultgridjenistrans5'] = $resultgridjenistrans5;
          $msg['resultgridjenistrans'] = $resultgridjenistrans;
          $msg['resultgridjenistransfirst'] = $resultgridjenistransfirst;
          $msg['resultgridjenistransnext'] = $resultgridjenistransnext;


          $msg['msg']= $data;
          $msg['type'] = "done";
          echo json_encode($msg);
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return JSON data userDetail
     */
    public function getData()
    {

    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return ARRAY data userDetail
     */
    public function getDataX()
    {

    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {

    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userDetail
     */
    public function update()
    {

    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete()
    {

    }
}
