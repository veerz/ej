<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Chart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

		$this->load->model('Role_Model');
		$this->load->model('menu_model');
    $this->load->model('audit_model',"aum");
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }

		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$sqlpieupdate = "Exec getEJUpdateStatusSummary";
		$qpieupdate = $this->db->query($sqlpieupdate);
		$data['piedataupdate'] = $qpieupdate->result_array();

		$sqlbarMerk= "Exec getEJUpdateStatusByMerk";
		$barMerk = $this->db->query($sqlbarMerk);
		$data['bardataMerk'] = $barMerk->result_array();

		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel";
		$barRegion = $this->db->query($sqlbarRegion);
		$data['bardataregion'] = $barRegion->result_array();

        //$this->load->view('chart',$data);
		$this->load->template('chart',$data);
	}

    public function showData(){

      if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
            redirect('login');
          }
      $piedataupdate = "Exec getEJUpdateStatusSummary";
  		$piedataupdate = $this->db->query($piedataupdate);
			$data['piedataupdate'] = $piedataupdate->result_array();
			$bardataPie = $data['piedataupdate'];
  		//var_dump( $data['piedataupdate'] );
  		//var_dump( $sqlpieupdate );
  		//die();

  	  //$data['params'] = $param;
  		$bardataMerk= "Exec getEJUpdateStatusByMerk";
  		$bardataMerk = $this->db->query($bardataMerk);
			$data['bardataMerk'] = $bardataMerk->result_array();
			$bardataMerks = $data['bardataMerk'];

  		$bardataregion= "Exec getEJUpdateStatusBottomPanel ";
  		$bardataregion = $this->db->query($bardataregion);
			$data['bardataregion'] = $bardataregion->result_array();
			$bardataregions = $data['bardataregion'];

			$data['cekregion'] = ( $region?:0);
			$data['cekarea'] = ( $area?:0);
			$data['cekmanageby'] = ( $managedBy?:0);
			$data['cekmerk'] = ( $merk?:0);

			$cekmerk = ( $merk?:0);
			$cekregion = ( $region?:0);
			$cekmanageby = ( $managedBy?:0);
			$cekarea = ( $area?:0);

      //Data Grid Region
    $resultgridregion = "";
  	$resultgridregion .="<table id=\"regiondata\" class=\"table table-bordered\" style=\"font-size:12px\"><tr>
                              <td><label>Status EJ</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
		$resultgridregion .="<td>";
		if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
			$resultgridregion .= $bardataregions[$i]['regionAlias'];
		}elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
			$resultgridregion .=  $bardataregions[$i]['area'];
		}elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
			$resultgridregion .=  $bardataregions[$i]['flmvendor'];
		}else{
			$resultgridregion .=  $bardataregions[$i]['flmvendor'];
		}
			$resultgridregion .="</td>";
    }
    $resultgridregion .="</tr><tr>
              <td><label>Update</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
            	$resultgridregion .="<td>" .number_format($bardataregions[$i]['Update'],0,',','.'). "</td>";
		}
    $resultgridregion .="</tr><tr>
              <td><label>Tidak Update</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
            	$resultgridregion .="<td>" .number_format($bardataregions[$i]['notUpdate'],0,',','.'). "</td>";
  	}
    $resultgridregion .="</tr></table>";


    //DG Merk
    $resultgridmerk = "";
  	$resultgridmerk .="<table id=\"merkdata\" class=\"table table-bordered table-striped\" style=\"font-size:12px\"><tr>
                              <td><label>Status EJ</label></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td>" .$bardataMerks[$i]['merk']. "</td>";
    }
    $resultgridmerk .="</tr><tr>
              <td><label>Update</label></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['Update'],0,',','.'). "</td>";
		}
    $resultgridmerk .="</tr><tr>
              <td><label>Tidak Update</label></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['notUpdate'],0,',','.'). "</td>";
  	}
    $resultgridmerk .="</tr></table>";


    //DG Merk
    $resultgridpie = "";
  	$resultgridpie .="<table id=\"piedata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                              <td><label>Status EJ</label></td><td><label>Jumlah</label></td><td><label>Persentase</label></td>";
    // for($i=0;$i<count($bardataPie);$i++){
    //         	$resultgridpie .="<td>" .$bardataPie[$i]['merk']. "</td>";
    // }
		// "<td>" .($bardataPie[$i]['Update']/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100 . "</td>"
		
		$totalPie = 0;
      $pembagi = 1;
      for($i=0;$i<count($bardataPie);$i++){
          $totalPie = $bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate'];
      }

    $resultgridpie .="</tr><tr>
							<td><label>Update</label></td>";
			for($i=0;$i<count($bardataPie);$i++){
				if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
								$resultgridpie .="<td>" .number_format($bardataPie[$i]['Update'],0,',','.'). "</td><td>".round(($bardataPie[$i]['Update']/$pembagi)*100,2). "%</td>" ;
		}
    $resultgridpie .="</tr><tr>
                <td><label>Tidak Update</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['notUpdate'],0,',','.'). "</td><td>". round(($bardataPie[$i]['notUpdate']/$pembagi)*100,2). "%</td>" ;
  	}
    $resultgridpie .="</tr><tr>
                  <td><label>Total</label></td>";
			$resultgridpie .="<td>".number_format($totalPie,0,',','.')."</td><td>". round(($totalPie/$totalPie)*100,2). "%</td>" ;
			
		$resultgridpie .="</tr></table>";

    $msg['resultgridpie'] = $resultgridpie;
    $msg['resultgridregion'] = $resultgridregion;
    $msg['resultgridmerk'] = $resultgridmerk;
    $msg['msg']= $data;
    $msg['type'] = "done";
      echo json_encode($msg);
    }
}
