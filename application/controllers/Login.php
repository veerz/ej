<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->model("allmodel","am");
      $this->load->model("users_model","um");
      $this->load->model('audit_model',"aum");
    }
    function index()
    {
		if($this->session->userdata('username' != NULL) or !empty($this->session->userdata('username'))){
          redirect('Chart');
        }
        $this->load->logintemplate('loginView');
    }
    function LoginProcess()
    {
      if(!$_POST){
        $this->response['msg'] = "Invalid Parametersssss";
        echo json_encode($this->response);exit;
      }
      $username= $this->input->post('username') ;
      $password= $this->input->post('$password') ;
      $hashpw=  hash('sha384', $this
                    ->input
                    ->post('password'));


      if(empty($username) and empty($password)){
        $this->response['msg'] = "Invalid Parameter";
          echo json_encode($this->response);exit;
      }else{
        $result = $this->am->query_data("SELECT * FROM  users WHERE username = '" .$username. "' and password= '".$hashpw."' and status = 'Aktif'",true);
        if($result){
			       $newdata = array(
    						'username'	=> $result['username'],
                'name'	=> $result['name'],
							  'role'	=> $result['id_role'],
                'id_user'	=> $result['id_user']
    				   );

					$this->session->set_userdata($newdata);
          $data_audit = array(
              'id_user'       => $newdata['id_user'],
              'username'      => $newdata['username'],
              'page'          => "Login",
              'action'        => "LoginProcess",
              'detail'        => "User '".$newdata['username']."' has logged in at ".date('Y-m-d H:i:s'),
              'created_date'  => date('Y-m-d H:i:s')
          );
          $result = $this->db->insert('AuditTrail', $data_audit);
					$data['type'] = 'done';
					$data['msg'] = 'Successfully login';
				}
				else{
					  $data['type'] = 'failed';
					  $data['msg'] = 'Failed Invalid username or password';
				}
      }
      echo json_encode($data);
      exit();
        //redirect('chart');
    }


	// function logout(){
		// $this->session->sess_destroy();
		// redirect(base_url('Login'));
	// }
}
