<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('fpdf/fpdf.php');

class StatMach0 extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
    }

	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

    $listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

    $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    $listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
    $connectivityType = $this->db->query($listconnectivityType);
    $data['connectivityType'] = $connectivityType->result_array();

	  $data['params'] = "";
    $this->load->template('StatMach0',$data);
	
	}

    public function ecsv2()
    {
		$status = ( $this->input->get("status")?:null);
		$region = ( $this->input->get("region")?:null);
		$area =( $this->input->get("area")?:null);
		$managerType =( $this->input->get("managerType")?: null);
		$managedBy =( $this->input->get("managedBy")?:null);
		$terminalType = ($this->input->get("terminalType")?:null);
		$merk = ($this->input->get("merk")?:null);
		$connectivityType = ($this->input->get("connectivityType")?:null);
		$param = "";
	
		if($status)
			$param .=  " @status='".$status."'";
		//else $param .=  " @status=NULL,";
		if($region && $param != "")$param .=  ",@region='".$region."'";
		if($region && $param == "")$param .=  "@region='".$region."'";
		if($area && $param != "") $param .=  ",@area='".$area."'";		
		if($area && $param == "") $param .=  " @area='".$area."'";		
		if($managerType && $param != "") $param .=  ", @managerType='".$managerType."'";
		if($managerType && $param == "") $param .=  " @managerType='".$managerType."'";
		if($managedBy&& $param != "")$param .=  ", @manager='".$managedBy."'";
		if($managedBy&& $param == "")$param .=  " @manager='".$managedBy."'";
		if($terminalType && $param != "")	$param .=  ", @machineType='".$terminalType."'";
		if($terminalType && $param == "")	$param .=  "@machineType='".$terminalType."'";
		// if($merk && $param != "")	$param .=  " @machineMake='".$merk."'";
		if($merk && $param != "")	$param .=  ", @machineMake='".$merk."'";
		if($merk && $param == "")	$param .=  "@machineMake='".$merk."'";
		if($connectivityType&& $param != "")	$param .=  ", @commType='".$connectivityType."'";
		if($connectivityType&& $param == "")	$param .=  " @commType='".$connectivityType."'";
		//else $param .=  " @commType=NULL";
		
		$getreporth0= "ExportRMMH0 ".$param;
		$qgetreporth0 = $this->db->query($getreporth0);
		$resultq= $qgetreporth0->result_array();
		/* 
		echo $getreporth0;die(); */
			
		
		if (isset($resultq)) {
			$filename = "Status Mesin H+0 (".date('dmY').").xls";
			 header("Content-Type: application/vnd-ms-excel"); 
			/* header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); */
			header("Content-Disposition: attachment; filename=\"$filename\"");
			header('Cache-Control: max-age=0');
			$isPrintHeader = false;
			if (! empty($resultq)) {
				foreach ($resultq as $row) {
					if (! $isPrintHeader) {
						echo implode("\t", array_keys($row)) . "\n";
						$isPrintHeader = true;
					}
					echo implode("\t", array_values($row)) . "\n";
				}
			}

			exit();
		}
		
		 //echo json_encode($msg);
    }
	
	public function epdf()
    {
		$status = ( $this->input->get("status")?:null);
		$region = ( $this->input->get("region")?:null);
		$area =( $this->input->get("area")?:null);
		$managerType =( $this->input->get("managerType")?: null);
		$managedBy =( $this->input->get("managedBy")?:null);
		$terminalType = ($this->input->get("terminalType")?:null);
		$merk = ($this->input->get("merk")?:null);
		$connectivityType = ($this->input->get("connectivityType")?:null);
		$param = "";
	
		if($status)
			$param .=  " @status='".$status."'";
		//else $param .=  " @status=NULL,";
		if($region && $param != "")$param .=  ",@region='".$region."'";
		if($region && $param == "")$param .=  "@region='".$region."'";
		if($area && $param != "") $param .=  ",@area='".$area."'";		
		if($area && $param == "") $param .=  " @area='".$area."'";		
		if($managerType && $param != "") $param .=  ", @managerType='".$managerType."'";
		if($managerType && $param == "") $param .=  " @managerType='".$managerType."'";
		if($managedBy&& $param != "")$param .=  ", @manager='".$managedBy."'";
		if($managedBy&& $param == "")$param .=  " @manager='".$managedBy."'";
		if($terminalType && $param != "")	$param .=  ", @machineType='".$terminalType."'";
		if($terminalType && $param == "")	$param .=  "@machineType='".$terminalType."'";
		// if($merk && $param != "")	$param .=  " @machineMake='".$merk."'";
		if($merk && $param != "")	$param .=  ", @machineMake='".$merk."'";
		if($merk && $param == "")	$param .=  "@machineMake='".$merk."'";
		if($connectivityType&& $param != "")	$param .=  ", @commType='".$connectivityType."'";
		if($connectivityType&& $param == "")	$param .=  " @commType='".$connectivityType."'";
		//else $param .=  " @commType=NULL";
		//var_dump( $param);die();
		$getreporth0= "ExportRMMH0 ".$param;
		//var_dump( $getreporth0);die();
		//echo $getreporth0;die();
		$qgetreporth0 = $this->db->query($getreporth0);
		$resultq= $qgetreporth0->result_array();
	
		if (isset($resultq)) {
			 $pdf = new FPDF('L','mm','A3');
			$pdf->AddPage("L","A3");
			 $pdf->SetFont('Arial','B',16);
			 $pdf->SetMargins(3.175, 3.175, 3.175);
        // mencetak string 
			$pdf->Cell(400,7,'Status Mesin H+0 ('.date('d/m/Y'). ') ',0,1,'C');
			$pdf->Ln();	$pdf->Ln();
			$pdf->SetFont('Arial','B',8);	
			$pdf->SetFillColor(169,169,169);	
			$pdf->Cell(20,6,"Terminal ID",1,0,"",true);
			$pdf->Cell(20,6,"SN",1,0,"",true);
			$pdf->Cell(16,6,"Dep. Year",1,0,"",true);
			$pdf->Cell(18,6,"Op. Status",1,0,"",true);
			$pdf->Cell(49,6,"Location",1,0,"",true);
			$pdf->Cell(17,6,"Merk",1,0,"",true);
			$pdf->Cell(28,6,"ModelType",1,0,"",true);
			$pdf->Cell(22,6,"Xpnet",1,0,"",true);
			$pdf->Cell(21,6,"Con Type",1,0,"",true);
			$pdf->Cell(21,6,"Vendor",1,0,"",true);
			$pdf->Cell(30,6,"ManagedBy",1,0,"",true);
			$pdf->Cell(21,6,"Kanwil",1,0,"",true);
			$pdf->Cell(38,6,"Area",1,0,"",true);
			$pdf->Cell(35,6,"Last RMMAgent Start ",1,0,"",true);
			//$pdf->Cell(25,6,"LastEJUpdate",1,0);
			$pdf->Cell(35,6,"LastTLFTransaction",1,0,"",true);
			$pdf->Cell(20,6,"Status",1,0,"",true);
			$pdf->Ln();
			$pdf->SetFont('Arial','',8);	
			foreach($resultq as $rowVal){
					$pdf->Cell(20,6,$rowVal['terminalID'],1,0);
					$pdf->Cell(20,6,$rowVal['SN'],1,0);
					$pdf->Cell(16,6,$rowVal['deploymentYear'],1,0);
					$pdf->Cell(18,6,$rowVal['operStatus'],1,0);
					$pdf->Cell(49,6,$rowVal['atmLocation'],1,0);
					$pdf->Cell(17,6,$rowVal['merk'],1,0);
					$pdf->Cell(28,6,$rowVal['modelType'],1,0);
					$pdf->Cell(22,6,$rowVal['xpnet_remoteAddress'],1,0);
					$pdf->Cell(21,6,$rowVal['connectivityType'],1,0);
					$pdf->Cell(21,6,$rowVal['connectivityVendor'],1,0);
					$pdf->Cell(30,6,$rowVal['managedBy'],1,0);
					$pdf->Cell(21,6,substr($rowVal['kanwil'],0,9),1,0);
					$pdf->Cell(38,6,substr($rowVal['area'],0,21),1,0);
					$pdf->Cell(35,6,$rowVal['lastRMMAgentStartTime'],1,0);
					//$pdf->Cell(25,6,$rowVal['lastEJUpdateTimestamp'],1,0);
					$pdf->Cell(35,6,$rowVal['lastTLFTransaction'],1,0);
					$pdf->Cell(20,6,$rowVal['Status'],1,0);
					$pdf->Ln();
			}
			$pdf->SetFont('Arial','',12);		
			$pdf->Ln();
			$pdf->Output();
			
			exit();
		}		 
    }

    public function getData()
    {
		$status = ( $this->input->post("status")?:null);
    //echo $status; die()
		$region = ( $this->input->post("region")?:null);
		if($region != null){
		if($region == "REGION I / SUMATERA 1"){
		  $region = "REG01";
		}if($region == "REGION II / SUMATERA 2"){
		  $region = "REG02";
		}if($region == "REGION III / JAKARTA 1"){
		  $region = "REG03";
		}if($region == "REGION IV / JAKARTA 2"){
		  $region = "REG04";
		}if($region == "REGION V / JAKARTA 3"){
		  $region = "REG05";
		}if($region == "REGION VI / JAWA 1"){
		  $region = "REG06";
		}if($region == "REGION VII / JAWA 2"){
		  $region = "REG07";
		}if($region == "REGION VIII / JAWA 3"){
		  $region = "REG08";
		}if($region == "REGION IX / KALIMANTAN"){
		  $region = "REG09";
		}if($region == "REGION X / SULAWESI DAN MALUKU"){
		  $region = "REG10";
		}if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
		  $region = "REG11";
		}if($region == "REGION XII / PAPUA"){
		  $region = "REG12";
		}if($region == "LUAR NEGERI")
		  $region = "REGLN";
	  }

		$area =( $this->input->post("area")?:null);
    //$managerType =( $this->input->post("managerType")?:null);
		$managerType =( $this->input->post("managerType")?: null);
		$managedBy =( $this->input->post("managedBy")?:null);
		// $connectivityVendor = ($this->input->post("connectivityVendor")?:null);
		$terminalType = ($this->input->post("terminalType")?:null);
		$merk = ($this->input->post("merk")?:null);
		$connectivityType = ($this->input->post("connectivityType")?:null);

		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }

  	$param = "";
    $paramregion = "";
  	$paramtampil="";
  	if($status!=null)	$param .=  " @status='".$status."',";
  	else $param .=  " @status=NULL,";
  	if($region!=null)	$param .=  " @region='".$region."',";
  	else $param .=  " @region=NULL,";
  	if($area!=null)	$param .=  " @area='".$area."',";
  	else $param .=  " @area=NULL,";
    if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
  	else $param .=  " @managerType=NULL,";
    if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
  	else $param .=  " @manager=NULL,";
  	// if($connectivityVendor!=null)	$param .=  " @managerType='".$connectivityVendor."',";
  	// else $param .=  " @managerType=NULL,";
  	if($terminalType!=null)	$param .=  " @machineType='".$terminalType."',";
  	else $param .=  " @machineType=NULL,";
  	if($merk!=null)	$param .=  " @machineMake='".$merk."',";
  	else $param .=  " @machineMake=NULL,";
  	if($connectivityType!=null)	$param .=  " @commType='".$connectivityType."'";
  	else $param .=  " @commType=NULL";
    // var_dump($param);die()
    if($status!=null)	$paramregion .=  " @status='".$status."',";
    else $paramregion .=  " @status=NULL,";
    if($region!=null)	$paramregion .=  " @region='".$region."',";
  	else $paramregion .=  " @region=NULL,";
  	if($area!=null)	$paramregion .=  " @area='".$area."',";
  	else $paramregion .=  " @area=NULL,";
    if($managedBy!=null)	$paramregion .=  " @manager='".$managedBy."'";
  	else $paramregion .=  " @manager=NULL";

  	/* echo $param;
  	die(); */
		$sqlpiedataupdate = "Exec getEJUpdateStatusSummary".$param;
		$piedataupdate = $this->db->query($sqlpiedataupdate);
		$data['piedataupdate'] = $piedataupdate->result_array();
		$bardataPie = $data['piedataupdate'];
		
		$isidata ="";
	
		if($bardataPie[0]["Update"]== 0 && $bardataPie[0]["notUpdate"]== 0)
		{
			$data['isidata'] = "tidak";
		}else{
			$data['isidata'] = "ada";
		}
		//var_dump( $data['piedataupdate'] );
		//var_dump( $sqlpieupdate );
		//die();
		
		//echo $sqlpiedataupdate;die();

	  $data['params'] = $param;
		$sqlbardataMerk= "Exec getEJUpdateStatusByMerk".$param;
		$bardataMerk = $this->db->query($sqlbardataMerk);
		$data['bardataMerk'] = $bardataMerk->result_array();
    $bardataMerks = $data['bardataMerk'];

		$sqlbardataregion= "Exec getEJUpdateStatusBottomPanel ".$param;
		$bardataregion = $this->db->query($sqlbardataregion);
		$data['bardataregion'] = $bardataregion->result_array();
		$bardataregions = $data['bardataregion'];
		

    $data['cekregion'] = ( $region?:0);
    $data['cekarea'] = ( $area?:0);
    $data['cekmanageby'] = ( $managedBy?:0);
    $data['cekmerk'] = ( $merk?:0);

    $cekmerk = ( $merk?:0);
    $cekregion = ( $region?:0);
    $cekmanageby = ( $managedBy?:0);
    $cekarea = ( $area?:0);


    //Data Grid Region
    $resultgridregion = "";
  	$resultgridregion .="<table id=\"regiondata\" class=\"table table-bordered\" style=\"font-size:12px\"><tr>
                              <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
		$resultgridregion .="<td bgcolor=#4A4A4B style=\"color:#fff\">";
		if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
			$resultgridregion .= $bardataregions[$i]['regionAlias'];
		}elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
			$resultgridregion .=  $bardataregions[$i]['area'];
		}elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
			$resultgridregion .=  $bardataregions[$i]['MANAGEDBY'];
		}else{
			$resultgridregion .=  $bardataregions[$i]['MANAGEDBY'];
		}
			$resultgridregion .="</td>";
    }
    $resultgridregion .="</tr><tr>
              <td><label>Update</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
            	$resultgridregion .="<td><center>" .number_format($bardataregions[$i]['Update'],0,',','.'). "</center></td>";
		}
    $resultgridregion .="</tr><tr>
              <td><label>Tidak Update</label></td>";
    for($i=0;$i<count($bardataregions);$i++){
            	$resultgridregion .="<td><center>" .number_format($bardataregions[$i]['notUpdate'],0,',','.'). "</center></td>";
  	}
    $resultgridregion .="</tr></table>";


    //DG Merk
    $resultgridmerk = "";
  	$resultgridmerk .="<table id=\"merkdata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                              <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Status EJ</label></center></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td bgcolor=#4A4A4B style=\"color:#fff\"><center>" .$bardataMerks[$i]['merk']. "</center></td>";
    }
    $resultgridmerk .="</tr><tr>
              <td><label>Update</label></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td><center>" .number_format($bardataMerks[$i]['Update'],0,',','.'). "</center></td>";
		}
    $resultgridmerk .="</tr><tr>
              <td><label>Tidak Update</label></td>";
    for($i=0;$i<count($bardataMerks);$i++){
            	$resultgridmerk .="<td><center>" .number_format($bardataMerks[$i]['notUpdate'],0,',','.'). "</center></td>";
  	}
    $resultgridmerk .="</tr></table>";


    //DG Merk
    $resultgridpie = "";
  	$resultgridpie .="<table id=\"piedata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                              <td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Status EJ</label></center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Jumlah</label></center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center><label>Persentase</label></center></td>";
    // for($i=0;$i<count($bardataPie);$i++){
    //         	$resultgridpie .="<td>" .$bardataPie[$i]['merk']. "</td>";
    // }
		// "<td>" .($bardataPie[$i]['Update']/($bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate']))*100 . "</td>"
		
		$totalPie = 0;
      $pembagi = 1;
      for($i=0;$i<count($bardataPie);$i++){
          $totalPie = $bardataPie[$i]['Update'] + $bardataPie[$i]['notUpdate'];
      }

    $resultgridpie .="</tr><tr>
							<td><label>Update</label></td>";
			for($i=0;$i<count($bardataPie);$i++){
				if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
								$resultgridpie .="<td><center>" .number_format($bardataPie[$i]['Update'],0,',','.'). "</center></td><td><center>".round(($bardataPie[$i]['Update']/$pembagi)*100,2). "%</center></td>" ;
		}
    $resultgridpie .="</tr><tr>
                <td><label>Tidak Update</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td><center>" .number_format($bardataPie[$i]['notUpdate'],0,',','.'). "</center></td><td><center>". round(($bardataPie[$i]['notUpdate']/$pembagi)*100,2). "%</center></td>" ;
  	}
    $resultgridpie .="</tr><tr>
									<td><label>Total</label></td>";
			if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
			$resultgridpie .="<td><center>".number_format($totalPie,0,',','.')."</center></td><td><center>". round(($totalPie/$pembagi)*100,2). "%</center></td>" ;
			
		$resultgridpie .="</tr></table>";

		$msg['isidata'] = $isidata;
    $msg['resultgridpie'] = $resultgridpie;
    $msg['resultgridregion'] = $resultgridregion;
		$msg['resultgridmerk'] = $resultgridmerk;
		
    $msg['msg']= $data;
    $msg['type'] = "done";
    echo json_encode($msg);
    // $storeArray();
    // while ($row = mysql_fetch_array($region)){
    //   $data['region'] = $row['kanwil'];
    // }

    //var_dump($data['region']);
    // var_dump($sqlbarRegion);
    // die();

    }

    public function getDataX()
    {

    }

    public function save()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
