<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('fpdf/fpdf.php');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class StatMach1 extends CI_Controller

{
    function __construct()
    {
        parent::__construct();
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
    }

	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
    
    $data['cekregion'] = 0;
    $data['cekarea'] = 0;
    $data['cekmanageby'] = 0;
    $data['cekmerk'] = 0;
    $listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

    $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    $listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
    $connectivityType = $this->db->query($listconnectivityType);
    $data['connectivityType'] = $connectivityType->result_array();

    $data['params'] = "";
		$this->load->template('StatMach1',$data);
  }
  
  public function ecsv2()
    {
		$summaryDay = ( $this->input->get("summaryDay")?:null);
		$status = ( $this->input->get("status")?:null);
		if($status != null){
			if($status == "RMMAgentAktif(EJUpdate)"){
				$status = "RMMAgent Aktif (EJ Update)";
			}if($status == "RMMAgentAktif(EJNotUpdate)"){
				$status = "RMMAgent Aktif (EJ Not Update)";
			}if($status == "RMMAgentTidakAda"){
				$status = "RMMAgent Tidak Ada";
			}if($status == "ATMTidakAktif"){
				$status = "ATM Tidak Aktif";
			}if($status == "ProblemJaringan"){
				$status = "Problem Jaringan";
			}
		}
		$region = ( $this->input->get("region")?:null);
		if($region != null){
			if($region == "REGION I / SUMATERA 1"){
				$region = "REG01";
			}if($region == "REGION II / SUMATERA 2"){
				$region = "REG02";
			}if($region == "REGION III / JAKARTA 1"){
				$region = "REG03";
			}if($region == "REGION IV / JAKARTA 2"){
				$region = "REG04";
			}if($region == "REGION V / JAKARTA 3"){
				$region = "REG05";
			}if($region == "REGION VI / JAWA 1"){
				$region = "REG06";
			}if($region == "REGION VII / JAWA 2"){
				$region = "REG07";
			}if($region == "REGION VIII / JAWA 3"){
				$region = "REG08";
			}if($region == "REGION IX / KALIMANTAN"){
				$region = "REG09";
			}if($region == "REGION X / SULAWESI DAN MALUKU"){
				$region = "REG10";
			}if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
				$region = "REG11";
			}if($region == "REGION XII / PAPUA"){
				$region = "REG12";
			}if($region == "LUAR NEGERI")
				$region = "REGLN";
			}
		$area =( $this->input->get("area")?:null);
		$managerType =( $this->input->get("managerType")?: null);
		$managedBy =( $this->input->get("managedBy")?:null);
		$terminalType = ($this->input->get("terminalType")?:null);
		$merk = ($this->input->get("merk")?:null);
		$connectivityType = ($this->input->get("connectivityType")?:null);
		$param = "";    
		if($summaryDay)$param .= " @summaryDay='".$summaryDay."'";
		if($status)$param .=  " ,@status='".$status."'";
		//if($status && $param != "")$param .=  ",@status='".$status."'";
		//if($status && $param == "")$param .=  "@status='".$status."'";
		if($region && $param != "")$param .=  ",@region='".$region."'";
		if($region && $param == "")$param .=  "@region='".$region."'";
		if($area && $param != "") $param .=  ",@area='".$area."'";		
		if($area && $param == "") $param .=  " @area='".$area."'";		
		if($managerType && $param != "") $param .=  ", @managerType='".$managerType."'";
		if($managerType && $param == "") $param .=  " @managerType='".$managerType."'";
		if($managedBy&& $param != "")$param .=  ", @manager='".$managedBy."'";
		if($managedBy&& $param == "")$param .=  " @manager='".$managedBy."'";
		if($terminalType && $param != "")	$param .=  ", @machineType='".$terminalType."'";
		if($terminalType && $param == "")	$param .=  "@machineType='".$terminalType."'";
		// if($merk && $param != "")	$param .=  " @machineMake='".$merk."'";
		if($merk && $param != "")	$param .=  ", @machineMake='".$merk."'";
		if($merk && $param == "")	$param .=  "@machineMake='".$merk."'";
		if($connectivityType&& $param != "")	$param .=  ", @commType='".$connectivityType."'";
		if($connectivityType&& $param == "")	$param .=  " @commType='".$connectivityType."'";
		$getreporth1= "ExportRMMH1 ".$param;
		$qgetreporth1 = $this->db->query($getreporth1);
		$resultq= $qgetreporth1->result_array();
		if (isset($resultq)) {
			// $filename = "Status Mesin H+1 (".date('dmY').").xls";
			$filename = "Status Mesin H+1 (".$summaryDay.").xls";
			header("Content-Type: application/vnd-ms-excel"); 
			header("Content-Disposition: attachment; filename=\"$filename\"");
			header('Cache-Control: max-age=0');
			$isPrintHeader = false;
			if (! empty($resultq)) {
				foreach ($resultq as $row) {
					if (! $isPrintHeader) {
						echo implode("\t", array_keys($row)) . "\n";
						$isPrintHeader = true;
					}
					echo implode("\t", array_values($row)) . "\n";
				}
			}
			exit();
		}
    }

	public function epdf()
    {
		$summaryDay = ( $this->input->get("summaryDay")?:null);
		$status = ( $this->input->get("status")?:null);
		if($status != null){
			if($status == "RMMAgentAktif(EJUpdate)"){
				$status = "RMMAgent Aktif (EJ Update)";
			}if($status == "RMMAgentAktif(EJNotUpdate)"){
				$status = "RMMAgent Aktif (EJ Not Update)";
			}if($status == "RMMAgentTidakAda"){
				$status = "RMMAgent Tidak Ada";
			}if($status == "ATMTidakAktif"){
				$status = "ATM Tidak Aktif";
			}if($status == "ProblemJaringan"){
				$status = "Problem Jaringan";
			}
		}
		$region = ( $this->input->get("region")?:null);
		if($region != null){
			if($region == "REGION I / SUMATERA 1"){
				$region = "REG01";
			}if($region == "REGION II / SUMATERA 2"){
				$region = "REG02";
			}if($region == "REGION III / JAKARTA 1"){
				$region = "REG03";
			}if($region == "REGION IV / JAKARTA 2"){
				$region = "REG04";
			}if($region == "REGION V / JAKARTA 3"){
				$region = "REG05";
			}if($region == "REGION VI / JAWA 1"){
				$region = "REG06";
			}if($region == "REGION VII / JAWA 2"){
				$region = "REG07";
			}if($region == "REGION VIII / JAWA 3"){
				$region = "REG08";
			}if($region == "REGION IX / KALIMANTAN"){
				$region = "REG09";
			}if($region == "REGION X / SULAWESI DAN MALUKU"){
				$region = "REG10";
			}if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
				$region = "REG11";
			}if($region == "REGION XII / PAPUA"){
				$region = "REG12";
			}if($region == "LUAR NEGERI")
				$region = "REGLN";
			}
		$area =( $this->input->get("area")?:null);
		$managerType =( $this->input->get("managerType")?: null);
		$managedBy =( $this->input->get("managedBy")?:null);
		$terminalType = ($this->input->get("terminalType")?:null);
		$merk = ($this->input->get("merk")?:null);
		$connectivityType = ($this->input->get("connectivityType")?:null);
		$param = "";    
		if($summaryDay)$param .= " @summaryDay='".$summaryDay."'";
		//if($status)$param .=  " ,@status='".$status."'";
		// if($summaryDay && $param != "")$param .=  ",@summaryDay='".$summaryDay."'";
		// if($summaryDay && $param == "")$param .=  "@summaryDay='".$summaryDay."'";
		if($status && $param != "")$param .=  ",@status='".$status."'";
		if($status && $param == "")$param .=  "@status='".$status."'";
		if($region && $param != "")$param .=  ",@region='".$region."'";
		if($region && $param == "")$param .=  "@region='".$region."'";
		if($area && $param != "") $param .=  ",@area='".$area."'";		
		if($area && $param == "") $param .=  " @area='".$area."'";		
		if($managerType && $param != "") $param .=  ", @managerType='".$managerType."'";
		if($managerType && $param == "") $param .=  " @managerType='".$managerType."'";
		if($managedBy&& $param != "")$param .=  ", @manager='".$managedBy."'";
		if($managedBy&& $param == "")$param .=  " @manager='".$managedBy."'";
		if($terminalType && $param != "")	$param .=  ", @machineType='".$terminalType."'";
		if($terminalType && $param == "")	$param .=  "@machineType='".$terminalType."'";
		// if($merk && $param != "")	$param .=  " @machineMake='".$merk."'";
		if($merk && $param != "")	$param .=  ", @machineMake='".$merk."'";
		if($merk && $param == "")	$param .=  "@machineMake='".$merk."'";
		if($connectivityType&& $param != "")	$param .=  ", @commType='".$connectivityType."'";
		if($connectivityType&& $param == "")	$param .=  " @commType='".$connectivityType."'";
		// var_dump( $param);die();
		$getreporth1= "ExportRMMH1".$param;
		//var_dump( $getreporth1);die();
		//echo $getreporth1 ;die();
		$qgetreporth1 = $this->db->query($getreporth1);
		$resultq= $qgetreporth1->result_array();
		// var_dump( $resultq);die();
		// echo $resultq;die();
		if (isset($resultq)) {
			 $pdf = new FPDF('L','mm','A2');
			$pdf->AddPage("L","A2");
			 $pdf->SetFont('Arial','B',16);
			 $pdf->SetMargins(3.175, 3.175, 3.175);
        // mencetak string 
			// $pdf->Cell(420,7,'Status Mesin H+1 ('.date('d/m/Y'). ') ',0,1,'C');
			$origDate = $summaryDay;
			$newDate = date("d/m/Y",strtotime($origDate));
			$pdf->Cell(420,7,'Status Mesin H+1 ('.$newDate. ') ',0,1,'C');
			$pdf->Ln();	$pdf->Ln();
			$pdf->SetFont('Arial','B',8);	
			$pdf->SetFillColor(169,169,169);
			$pdf->Cell(20,6,"Terminal ID",1,0,"",true);
			$pdf->Cell(20,6,"SN",1,0,"",true);
			$pdf->Cell(16,6,"Dep. Year",1,0,"",true);
			$pdf->Cell(18,6,"Op. Status",1,0,"",true);
			$pdf->Cell(49,6,"Location",1,0,"",true);
			$pdf->Cell(17,6,"Merk",1,0,"",true);
			$pdf->Cell(28,6,"ModelType",1,0,"",true);
			$pdf->Cell(22,6,"Xpnet",1,0,"",true);
			$pdf->Cell(21,6,"Con Type",1,0,"",true);
			$pdf->Cell(21,6,"Vendor",1,0,"",true);
			$pdf->Cell(30,6,"ManagedBy",1,0,"",true);
			$pdf->Cell(21,6,"Kanwil",1,0,"",true);
			$pdf->Cell(38,6,"Area",1,0,"",true);
			$pdf->Cell(35,6,"Last RMMAgent Start ",1,0,"",true);
			//$pdf->Cell(25,6,"LastEJUpdate",1,0);
			$pdf->Cell(35,6,"LastTLFTransaction",1,0,"",true);
			$pdf->Cell(44,6,"Status",1,0,"",true);
			$pdf->Ln();
			$pdf->SetFont('Arial','',8);	
			foreach($resultq as $rowVal){
					$pdf->Cell(20,6,$rowVal['terminalID'],1,0);
					$pdf->Cell(20,6,$rowVal['SN'],1,0);
					$pdf->Cell(16,6,$rowVal['deploymentYear'],1,0);
					$pdf->Cell(18,6,$rowVal['operStatus'],1,0);
					$pdf->Cell(49,6,$rowVal['atmLocation'],1,0);
					$pdf->Cell(17,6,$rowVal['merk'],1,0);
					$pdf->Cell(28,6,$rowVal['modelType'],1,0);
					$pdf->Cell(22,6,$rowVal['xpnet_remoteAddress'],1,0);
					$pdf->Cell(21,6,$rowVal['connectivityType'],1,0);
					$pdf->Cell(21,6,$rowVal['connectivityVendor'],1,0);
					$pdf->Cell(30,6,$rowVal['managedBy'],1,0);
					$pdf->Cell(21,6,substr($rowVal['kanwil'],0,9),1,0);
					$pdf->Cell(38,6,substr($rowVal['area'],0,21),1,0);
					$pdf->Cell(35,6,$rowVal['lastRMMAgentStartTime'],1,0);
					//$pdf->Cell(25,6,$rowVal['lastEJUpdateTimestamp'],1,0);
					$pdf->Cell(35,6,$rowVal['lastTLFTransaction'],1,0);
					$pdf->Cell(44,6,$rowVal['status'],1,0);
					$pdf->Ln();
			}
			$pdf->SetFont('Arial','',12);		
			$pdf->Ln();
			$pdf->Output();
			exit();
		}		
    }

    public function getData()
    {
      $summaryDay = ( $this->input->post("summaryDay")?:null);
      $status = ( $this->input->post("status")?:null);
  		$region = ( $this->input->post("region")?:null);
      if($region != null){
      if($region == "REGION I / SUMATERA 1"){
        $region = "REG01";
      }if($region == "REGION II / SUMATERA 2"){
        $region = "REG02";
      }if($region == "REGION III / JAKARTA 1"){
        $region = "REG03";
      }if($region == "REGION IV / JAKARTA 2"){
        $region = "REG04";
      }if($region == "REGION V / JAKARTA 3"){
        $region = "REG05";
      }if($region == "REGION VI / JAWA 1"){
        $region = "REG06";
      }if($region == "REGION VII / JAWA 2"){
        $region = "REG07";
      }if($region == "REGION VIII / JAWA 3"){
        $region = "REG08";
      }if($region == "REGION IX / KALIMANTAN"){
        $region = "REG09";
      }if($region == "REGION X / SULAWESI DAN MALUKU"){
        $region = "REG10";
      }if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
        $region = "REG11";
      }if($region == "REGION XII / PAPUA"){
        $region = "REG12";
      }if($region == "LUAR NEGERI")
        $region = "REGLN";
    }

			$area = ( $this->input->post("area")?:null);
			
			$managerType =( $this->input->post("managerType")?:null);
			// echo $managerType ;die();
			
      // $managerType = null;
      $managedBy = ( $this->input->post("managedBy")?:null);
      // $connectivityVendor = $this->input->post("connectivityVendor");
      $terminalType = ( $this->input->post("terminalType")?:null);
      $merk = ( $this->input->post("merk")?:null);
      $connectivityType = ( $this->input->post("connectivityType")?:null);

      if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
            redirect('login');
          }
      // $data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
      // $data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
      $param = "";
      // $paramregion = "";
    	$paramtampil="";

      if($summaryDay!=null)	$param .=  " @summaryDay='".$summaryDay."',";
    	else $param .=  " @summaryDay=NULL,";
    	if($status!=null)	$param .=  " @status='".$status."',";
    	else $param .=  " @status=NULL,";
    	if($region!=null)	$param .=  " @region='".$region."',";
    	else $param .=  " @region=NULL,";
    	if($area!=null)	$param .=  " @area='".$area."',";
    	else $param .=  " @area=NULL,";
    	if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
    	else $param .=  " @manager=NULL,";
      if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
    	else $param .=  " @managerType=NULL,";
    	// if($connectivityVendor!=null)	$param .=  " @managerType='".$connectivityVendor."',";
    	// else $param .=  " @managerType=NULL,";
    	if($terminalType!=null)	$param .=  " @machineType='".$terminalType."',";
    	else $param .=  " @machineType=NULL,";
    	if($merk!=null)	$param .=  " @machineMake='".$merk."',";
    	else $param .=  " @machineMake=NULL,";
    	if($connectivityType!=null)	$param .=  " @commType='".$connectivityType."'";
			else $param .=  " @commType=NULL";
			// echo $param;die();
			

      // if($status!=null)	$paramregion .=  " @status='".$status."',";
      // else $paramregion .=  " @status=NULL,";
      // if($region!=null)	$paramregion .=  " @region='".$region."',";
    	// else $paramregion .=  " @region=NULL,";
    	// if($area!=null)	$paramregion .=  " @area='".$area."',";
    	// else $paramregion .=  " @area=NULL,";
      // if($managedBy!=null)	$paramregion .=  " @manager='".$managedBy."'";
    	// else $paramregion .=  " @manager=NULL";

      //manggilSP
      $piedataupdate = "Exec getEJUpdateStatusSummaryH1".$param;
  		$piedataupdate = $this->db->query($piedataupdate);
  		$data['piedataupdate'] = $piedataupdate->result_array();
      $bardataPie = $data['piedataupdate'];

      $isidata ="";
	
      if($bardataPie[0]["RMMAgentAktif(EJUpdate)"]== 0 && $bardataPie[0]["RMMAgentAktif(EJNotUpdate)"]== 0 && $bardataPie[0]["RMMTidakAda"]== 0 && $bardataPie[0]["ATMTidakAktif"]== 0 && $bardataPie[0]["ProblemJaringan"]== 0)
      {
        $data['isidata'] = "tidak";
      }else{
        $data['isidata'] = "ada";
      }
  		//var_dump( $data['piedataupdate'] );
  		//var_dump( $sqlpieupdate );
  		//die();
  		//$data['params'] = $param;
  		$bardataMerk= "Exec getEJUpdateStatusByMerkH1".$param;
  		$bardataMerk = $this->db->query($bardataMerk);
  		$data['bardataMerk'] = $bardataMerk->result_array();
      $bardataMerks = $data['bardataMerk'];

  		// $sqlbarRegion= "Exec getEJUpdateStatusBottomPanelH1".$paramregion;
      // $data['params'] = $param;
      $bardataregion= "Exec getEJUpdateStatusBottomPanelH1".$param;
  		$bardataregion = $this->db->query($bardataregion);
  		$data['bardataregion'] = $bardataregion->result_array();
      $bardataregions = $data['bardataregion'];

      $data['cekregion'] = ( $region?:0);
      $data['cekarea'] = ( $area?:0);
      $data['cekmanageby'] = ( $managedBy?:0);
      $data['cekmerk'] = ( $merk?:0);

      $cekmerk = ( $merk?:0);
      $cekregion = ( $region?:0);
      $cekmanageby = ( $managedBy?:0);
      $cekarea = ( $area?:0);

      //Data Grid Region
      $resultgridregion = "";
    	$resultgridregion .="<table id=\"regiondata\" class=\"table table-bordered\" style=\"font-size:12px\"><tr>
                                <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
  		$resultgridregion .="<td bgcolor=#4A4A4B style=\"color:#fff\">";
  		if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
  			$resultgridregion .= $bardataregions[$i]['regionAlias'];
  		}elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
  			$resultgridregion .=  $bardataregions[$i]['area'];
  		}elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
  			$resultgridregion .=  $bardataregions[$i]['managedBy'];
  		}else{
  			$resultgridregion .=  $bardataregions[$i]['managedBy'];
  		}
  			$resultgridregion .="</td>";
      }
      $resultgridregion .="</tr><tr>
                <td><label>Update</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
              	$resultgridregion .="<td>".number_format($bardataregions[$i]['RMMAgentAktif(EJUpdate)'],0,',','.')."</td>";
  		}
      $resultgridregion .="</tr><tr>
                <td><label>Tidak Update</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
              	$resultgridregion .="<td>".number_format($bardataregions[$i]['RMMAgentAktif(EJNotUpdate)'],0,',','.')."</td>";
    	}
      $resultgridregion .="</tr><tr>
                <td><label>RMM Agent Tidak Ada</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
              	$resultgridregion .="<td>".number_format($bardataregions[$i]['RMMTidakAda'],0,',','.')."</td>";
    	}
      $resultgridregion .="</tr><tr>
                <td><label>ATM Tidak Aktif</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
              	$resultgridregion .="<td>".number_format($bardataregions[$i]['ATMTidakAktif'],0,',','.')."</td>";
    	}
      $resultgridregion .="</tr><tr>
                <td><label>Problem jaringan</label></td>";
      for($i=0;$i<count($bardataregions);$i++){
              	$resultgridregion .="<td>".number_format($bardataregions[$i]['ProblemJaringan'],0,',','.')."</td>";
    	}
      $resultgridregion .="</tr></table>";

      //DG Merk
      $resultgridmerk = "";
    	$resultgridmerk .="<table id=\"merkdata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                                <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td bgcolor=#4A4A4B style=\"color:#fff\">" .$bardataMerks[$i]['merk']. "</td>";
      }
      $resultgridmerk .="</tr><tr>
                <td><label>Update</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['RMMAgentAktif(EJUpdate)'],0,',','.'). "</td>";
  		}
      $resultgridmerk .="</tr><tr>
                <td><label>Tidak Update</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['RMMAgentAktif(EJNotUpdate)'],0,',','.'). "</td>";
    	}
      $resultgridmerk .="</tr><tr>
                <td><label>RMM Agent Tidak Ada</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['RMMTidakAda'],0,',','.'). "</td>";
    	}
      $resultgridmerk .="</tr><tr>
                <td><label>ATM Tidak Aktif</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['ATMTidakAktif'],0,',','.'). "</td>";
    	}
      $resultgridmerk .="</tr><tr>
                <td><label>Problem jaringan</label></td>";
      for($i=0;$i<count($bardataMerks);$i++){
              	$resultgridmerk .="<td>" .number_format($bardataMerks[$i]['ProblemJaringan'],0,',','.'). "</td>";
    	}
      $resultgridmerk .="</tr></table>";

      //DG Merk
      $resultgridpie = "";
      $resultgridpie .="<table id=\"piedata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                                <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Jumlah</label></td><td bgcolor=#4A4A4B style=\"color:#fff\"><label>Persentase</label></td>";
      // for($i=0;$i<count($bardataPie);$i++){
      //       	$resultgridpie .="<td>" .$bardataPie[$i]['merk']. "</td>";
      // }
      $totalPie = 0;
      $pembagi = 1;
      for($i=0;$i<count($bardataPie);$i++){
          $totalPie = $bardataPie[$i]['RMMAgentAktif(EJUpdate)'] + $bardataPie[$i]['RMMAgentAktif(EJNotUpdate)'] + $bardataPie[$i]['RMMTidakAda'] + $bardataPie[$i]['ATMTidakAktif'] + $bardataPie[$i]['ProblemJaringan'];
      }
      $resultgridpie .="</tr><tr>
                <td><label>Update</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['RMMAgentAktif(EJUpdate)'],0,',','.'). "</td><td>". round(($bardataPie[$i]['RMMAgentAktif(EJUpdate)']/$pembagi)*100,2). "%</td>" ;
  		}
      $resultgridpie .="</tr><tr>
                <td><label>Tidak Update</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['RMMAgentAktif(EJNotUpdate)'],0,',','.'). "</td><td>". round(($bardataPie[$i]['RMMAgentAktif(EJNotUpdate)']/$pembagi)*100,2). "%</td>" ;
    	}
      $resultgridpie .="</tr><tr>
                <td><label>RMM Tidak Ada</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['RMMTidakAda'],0,',','.'). "</td><td>". round(($bardataPie[$i]['RMMTidakAda']/$pembagi)*100,2). "%</td>" ;
    	}
      $resultgridpie .="</tr><tr>
                <td><label>ATM Tidak Aktif</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['ATMTidakAktif'],0,',','.'). "</td><td>". round(($bardataPie[$i]['ATMTidakAktif']/$pembagi)*100,2). "%</td>" ;
    	}
      $resultgridpie .="</tr><tr>
                <td><label>Problem Jaringan</label></td>";
      for($i=0;$i<count($bardataPie);$i++){
        if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
              	$resultgridpie .="<td>" .number_format($bardataPie[$i]['ProblemJaringan'],0,',','.'). "</td><td>". round(($bardataPie[$i]['ProblemJaringan']/$pembagi)*100,2). "%</td>" ;
    	}
      $resultgridpie .="</tr><tr>
                  <td><label>Total</label></td>";
                  if($totalPie == 0)   $pembagi= 1;else $pembagi = $totalPie;
      $resultgridpie .="<td>".number_format($totalPie,0,',','.')."</td><td>". round(($totalPie/$pembagi)*100,2). "%</td>" ;


      $resultgridpie .="</tr></table>";

      $msg['isidata'] = $isidata;
      $msg['resultgridpie'] = $resultgridpie;
      $msg['resultgridregion'] = $resultgridregion;
      $msg['resultgridmerk'] = $resultgridmerk;
 
      $msg['msg']= $data;
      $msg['type'] = "done";
            //$msg['msg'] = $dataresult;
            //$msg['msg'] = $result;
      echo json_encode($msg);
    }
   
}
