<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class FullDashboard extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();

		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  // ->Order_by("settings_Menu.id_menu", 'asc')
            ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$sqlpieupdate = "Exec getEJUpdateStatusSummary";
		$qpieupdate = $this->db->query($sqlpieupdate);
		$data['piedataupdate'] = $qpieupdate->result_array();

		$sqlbarMerk= "Exec getEJUpdateStatusByMerk";
		$barMerk = $this->db->query($sqlbarMerk);
		$data['bardataMerk'] = $barMerk->result_array();

		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel";
		$barRegion = $this->db->query($sqlbarRegion);
		$data['bardataregion'] = $barRegion->result_array();

		$cardRetainedRegion= "Exec getCardRetainedRegion";
		$cardRetainedRegion = $this->db->query($cardRetainedRegion);
		$data['cardRetainedRegion'] = $cardRetainedRegion->result_array();

		$cardRetainedMerk= "Exec getCardRetainedMerk";
		$cardRetainedMerk = $this->db->query($cardRetainedMerk);
		$data['cardRetainedMerk'] = $cardRetainedMerk->result_array();

		$CardRetainedNamaBank= "Exec getCardRetainedNamaBank";
		$CardRetainedNamaBank = $this->db->query($CardRetainedNamaBank);
		$data['CardRetainedNamaBank'] = $CardRetainedNamaBank->result_array();

    $CardRetainedNamaBanktop15= "Exec getCardRetainedNamaBanktop15";
		$CardRetainedNamaBanktop15 = $this->db->query($CardRetainedNamaBanktop15);
		$data['CardRetainedNamaBanktop15'] = $CardRetainedNamaBanktop15->result_array();

		/* $cardRetainedRegion= "Exec getCardRetainedRegion";
		$cardRetainedRegion = $this->db->query($cardRetainedRegion);
		$data['cardRetainedRegion'] = $cardRetainedRegion->result_array(); */

    $Replenishment = "Exec getReplenishment";
		$Replenishment = $this->db->query($Replenishment);
		$data['Replenishment'] = $Replenishment->result_array();

		$StatusTrxMapping = "Exec getStatusTrxDetail";
		$StatusTrxMapping = $this->db->query($StatusTrxMapping);
		$data['StatusTrxMapping'] = $StatusTrxMapping->result_array();

		$StatusTrxBad = "Exec getTop10BadPerformance";
		$StatusTrxBad = $this->db->query($StatusTrxBad);
		$data['StatusTrxBad'] = $StatusTrxBad->result_array();

		$StatusTrxGood = "Exec getTop10GoodPerformance";
		$StatusTrxGood = $this->db->query($StatusTrxGood);
		$data['StatusTrxGood'] = $StatusTrxGood->result_array();

		$StatusTrxByJenis = "Exec getJenisTrx";
		$StatusTrxByJenis = $this->db->query($StatusTrxByJenis);
		$data['StatusTrxByJenis'] = $StatusTrxByJenis->result_array();

		$regiontrx = "Exec getStatusTrxRegion";
		$regiontrx = $this->db->query($regiontrx);
		$data['regiontrx'] = $regiontrx->result_array();

        $this->load->view('layouts/headerViewFull');
        $this->load->view('fulldashboard', $data);
        $this->load->view('layouts/footerViewFull');
	}

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {

    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return JSON data userDetail
     */
    public function getData()
    {

    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return ARRAY data userDetail
     */
    public function getDataX()
    {

    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {

    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userDetail
     */
    public function update()
    {

    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete()
    {

    }
}
