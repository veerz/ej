<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Detailsearch extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$listterminalID = "SELECT DISTINCT terminalID FROM ATDS";
		$terminalID = $this->db->query($listterminalID);
		$data['terminalID'] = $terminalID->result_array();
    $param = "";
    $data['params'] = "";
		$this->load->template('Detailsearch',$data);

	}

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {
      $terminalID = $this->input->post('idmachine');
      $start = $this->input->post('startdate');
      $end = $this->input->post('enddate');
      $result ="";

      $param = " @termID = '".$terminalID."'";

			$sqlgetdata = "Exec getATMDetails".$param;
      		$dataid = $this->db->query($sqlgetdata);
      		$dataresult = $dataid->result();

          foreach($dataresult as $item)
          {
            $msg['msg']['kanwil'] = $item->kanwil;
            $msg['msg']['area'] = $item->area;
            $msg['msg']['JenisPengelola'] = $item->JenisPengelola;
            $msg['msg']['managedBy'] = $item->managedBy;
            $msg['msg']['address'] = $item->address;
            $msg['msg']['deploymentyear'] = $item->deploymentyear;
          }


		  $param2 = " @termID = '".$terminalID."', @startTime = '".$start." 00:00:00.000', @endTime ='".$end." 00:00:00.000' ";
		    $sqlgetdata2= "Exec getsearchDetail".$param2;
      		$datalist= $this->db->query($sqlgetdata2);
      		$dataresult2 = $datalist->result();
		$result2 = "<thead><th rowspan=\"2\" style=\"text-align:right; vertical-align: middle;\">#</th><th rowspan=\"2\" style=\"text-align:center; vertical-align: middle;\">Tanggal</th><th colspan=\"5\" style=\"text-align:center; vertical-align: middle;\">STATUS MESIN</th><th rowspan=\"2\" style=\"text-align:center; vertical-align: middle\">Kartu Tertelan</th><th colspan=\"2\" style=\"text-align:center; vertical-align: middle;\">Transaksi</th></tr><tr><th style=\"text-align:center\">ATM Tidak Aktif</th><th style=\"text-align:center\">Problem Jaringan</th><th style=\"text-align:center\">RMMAgent Aktif (EJ Tidak Update)</th><th style=\"text-align:center\">RMMAgent Aktif (EJ Update)</th><th style=\"text-align:center\">RMMAgent Tidak Aktif</th><th style=\"text-align:center\">Sukses</th><th style=\"text-align:center\">Gagal</th></thead><tbody>";
		$seq = 0;
		foreach($dataresult2 as $item)
        {
			$seq++;
			$tempstatus = $item->rmmState;
			$status1 = "";
			$status2 = "";
			$status3 = "";
			$status4 = "";
			$status5 = "";
			$tanggalget = substr($item->summaryDay,0,10);
			if($tempstatus == "ATM Tidak Aktif") $status1 = "<b>&#10004;</b>";
			if($tempstatus == "Problem Jaringan") $status2 = "<b>&#10004;</b>";
			if($tempstatus == "RMMAgent Aktif (EJ Not Update)") $status3 = "<b>&#10004;</b>";
			if($tempstatus == "RMMAgent Aktif (EJ Update)") $status4 = "<b>&#10004;</b>";
			if($tempstatus == "RMMAgent Tidak Ada") $status5= "<b>&#10004;</b>";
			$result2 .= "<tr><td>{$seq}</td><td>{$tanggalget}</td><td>{$status1}</td><td>{$status2}</td><td>{$status3}</td><td>{$status4}</td><td>{$status5}</td><td>{$item->kartuTertelan}</td><td>{$item->sukses}</td><td>{$item->gagal}</td></tr>";

          }

		  $result2 .= "</tbody>";
		$msg['msg']['result']= $result2;
          $msg['type'] = "done";
          //$msg['msg'] = $dataresult;
          //$msg['msg'] = $result;
          	echo json_encode($msg);

        /*  $listopcode = "Exec getDetailsearch".$param;
      		$opcode = $this->db->query($listopcode);
      		$data['detailsearch'] = $opcode->result();*/
    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return JSON data userDetail
     */
    public function getData()
    {

    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return ARRAY data userDetail
     */
    public function getDataX()
    {

    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {

    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userDetail
     */
    public function update()
    {

    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete()
    {

    }
}
