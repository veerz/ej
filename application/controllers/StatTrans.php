<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class StatTrans extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

	$listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

	$listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    $listconnectivityVendor = "SELECT DISTINCT connectivityVendor FROM ATDS";
    $connectivityVendor = $this->db->query($listconnectivityVendor);
    $data['connectivityVendor'] = $connectivityVendor->result_array();

	$listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $data['params'] = "";
    $this->load->template('StatTrans',$data);
	}

    public function getData()
    {
      // $status = ( $this->input->post("status")?:null);
      //echo $status; die()
      $trxDef = ( $this->input->post("definisi")?:null);
      if($trxDef == "2") $trxDef = '0';
      $region = ( $this->input->post("region")?:null);
      if($region != null){
      if($region == "REGION I / SUMATERA 1"){
        $region = "REG01";
      }if($region == "REGION II / SUMATERA 2"){
        $region = "REG02";
      }if($region == "REGION III / JAKARTA 1"){
        $region = "REG03";
      }if($region == "REGION IV / JAKARTA 2"){
        $region = "REG04";
      }if($region == "REGION V / JAKARTA 3"){
        $region = "REG05";
      }if($region == "REGION VI / JAWA 1"){
        $region = "REG06";
      }if($region == "REGION VII / JAWA 2"){
        $region = "REG07";
      }if($region == "REGION VIII / JAWA 3"){
        $region = "REG08";
      }if($region == "REGION IX / KALIMANTAN"){
        $region = "REG09";
      }if($region == "REGION X / SULAWESI DAN MALUKU"){
        $region = "REG10";
      }if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
        $region = "REG11";
      }if($region == "REGION XII / PAPUA"){
        $region = "REG12";
      }if($region == "LUAR NEGERI")
        $region = "REGLN";
    }

      $area =( $this->input->post("area")?:null);
      $managerType =( $this->input->post("managerType")?:null);
      // $managerType = null;
      $managedBy =( $this->input->post("managedBy")?:null);
      // $connectivityVendor = ($this->input->post("connectivityVendor")?:null);
      $terminalType = ($this->input->post("terminalType")?:null);
      $merk = ($this->input->post("merk")?:null);
      // $connectivityType = ($this->input->post("connectivityType")?:null);

      if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
            redirect('login');
          }
      // $data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
      // $data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

    $param = "";
    $paramtrans = "";
    $paramtampil="";
    if($trxDef!=null)	$param .=  " @trxDef='".$trxDef."',";
    else $param .=  " @trxDef=NULL,";
    if($region!=null)	$param .=  " @region='".$region."',";
    else $param .=  " @region=NULL,";
    if($area!=null)	$param .=  " @area='".$area."',";
    else $param .=  " @area=NULL,";
    if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
    else $param .=  " @managerType=NULL,";
    if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
    else $param .=  " @manager=NULL,";
    if($terminalType!=null)	$param .=  " @machineType='".$terminalType."',";
    else $param .=  " @machineType=NULL,";
    if($merk!=null)	$param .=  " @machineMake='".$merk."'";
    else $param .=  " @machineMake=NULL";
    //echo $param;die();

    if($region!=null)	$paramtrans .=  " @region='".$region."',";
    else $paramtrans .=  " @region=NULL,";
    if($area!=null)	$paramtrans .=  " @area='".$area."',";
    else $paramtrans .=  " @area=NULL,";
    if($managerType!=null)	$paramtrans .=  " @managerType='".$managerType."',";
    else $paramtrans .=  " @managerType=NULL,";
    if($managedBy!=null)	$paramtrans .=  " @manager='".$managedBy."',";
    else $paramtrans .=  " @manager=NULL,";
    if($terminalType!=null)	$paramtrans .=  " @machineType='".$terminalType."',";
    else $paramtrans .=  " @machineType=NULL,";
    if($merk!=null)	$paramtrans .=  " @machineMake='".$merk."'";
    else $paramtrans .=  " @machineMake=NULL";


    //ini masuk exec SP with params
    $regiontrx = "Exec getStatusTrxRegion".$param;
    $regiontrx = $this->db->query($regiontrx);
    $data['regiontrx'] = $regiontrx->result_array();
    $regiontrxs = $data['regiontrx'];//DG

    $isidata ="";
    if(!$regiontrxs)
      {
        $data['isidata'] = "tidak";
      }else{
        $data['isidata'] = "ada";
      }

    $Replenishment = "Exec getReplenishment".$param;
		$Replenishment = $this->db->query($Replenishment);
		$data['Replenishment'] = $Replenishment->result_array();

		$StatusTrxBad = "Exec getTop10BadPerformance".$paramtrans;
		$StatusTrxBad = $this->db->query($StatusTrxBad);
		$data['StatusTrxBad'] = $StatusTrxBad->result_array();

		$StatusTrxGood = "Exec getTop10GoodPerformance".$paramtrans;
		$StatusTrxGood = $this->db->query($StatusTrxGood);
		$data['StatusTrxGood'] = $StatusTrxGood->result_array();

		$StatusTrxByJenis = "Exec getJenisTrx".$param;
		$StatusTrxByJenis = $this->db->query($StatusTrxByJenis);
    $data['StatusTrxByJenis'] = $StatusTrxByJenis->result_array();
    $StatusTrxJeniss =  $data['StatusTrxByJenis'];

    $StatusTrxMapping = "Exec getStatusTrxDetail".$paramtrans;
		$StatusTrxMapping = $this->db->query($StatusTrxMapping);
    $data['StatusTrxMapping'] = $StatusTrxMapping->result_array();

    $data['cekregion'] = ( $region?:0);
    $data['cekarea'] = ( $area?:0);
    $data['cekmanageby'] = ( $managedBy?:0);
    $data['cekmerk'] = ( $merk?:0);
    $data['trxDef'] = $trxDef;

	$cekmerk = ( $merk?:0);
    $cekregion = ( $region?:0);
    $cekmanageby = ( $managedBy?:0);
    $cekarea = ( $area?:0);

	//Data Grid Region
    $resultgridregion = "";
  	$resultgridregion .="<table id=\"regiondata\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                              <td bgcolor=#4A4A4B style=\"color:#fff\"><label>Status EJ</label></td>";
    for($i=0;$i<count($regiontrxs);$i++){
    if($trxDef == NULL){
          if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
            if($i%2 == 0)
            $resultgridregion .= "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['Region']."</td>";
          }elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
            if($i%2 == 0)
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['areaAlias']."</td>";
          }elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
            if($i%2 == 0)
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['managedBy']."</td>";
          }else{
            if($i%2 == 0)
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['managedBy']."</td>";
          }
        }else{
          if($cekregion == "0"    && $cekarea == "0" && $cekmanageby == "0"){
            $resultgridregion .= "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['Region']."</td>";
          }elseif($cekregion != "0"    && $cekarea == "0" && $cekmanageby == "0"){
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['areaAlias']."</td>";
          }elseif($cekregion == "0"    && $cekarea != "0" && $cekmanageby == "0"){
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['managedBy']."</td>";
          }else{
            $resultgridregion .=  "<td bgcolor=#4A4A4B style=\"color:#fff\">".$regiontrxs[$i]['managedBy']."</td>";
        }
      }
    }
	
	if($trxDef=="0" || $trxDef == ""){
		$resultgridregion .="</tr><tr>
				  <td><label>Gagal</label></td>";
		for($i=0;$i<count($regiontrxs);$i++){
      if($regiontrxs[$i]['Status'] == "Gagal")
          // $totaltrxGagal = 0; 
          // $totaltrxGagal += $regiontrxs[$i]['Jumlah'];
					$resultgridregion .="<td>" . number_format($regiontrxs[$i]['Jumlah'],0,',','.'). "</td>";
			}
		}
	if($trxDef=="1" || $trxDef == ""){
		$resultgridregion .="</tr><tr>
				  <td><label>Sukses</label></td>";
		for($i=0;$i<count($regiontrxs);$i++){
      if($regiontrxs[$i]['Status'] == "Sukes")
          // $totaltrxSukses = 0 ;
          // $totaltrxSukses += $regiontrxs[$i]['Jumlah'];
					$resultgridregion .="<td>" . number_format($regiontrxs[$i]['Jumlah'],0,',','.'). "</td>";
		}
	}
    $resultgridregion .="</tr></table>";

    //DG untuk table transaksi gagal sukses
    // $resultgridtablegagalsukses = "";
    // $resultgridtablegagalsukses .= "<table id=\"gridgagalsukses\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
    // <td bgcolor=#4A4A4B style=\"color:#fff\"><center>Total Transaksi Gagal</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>". number_format($totaltrxSukses,0,',','.')."</center></td></tr><tr>
    // <td bgcolor=#4A4A4B style=\"color:#fff\"><center>Total Transaksi Gagal</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>". number_format($totaltrxGagal,0,',','.')."</center></td></tr>";
    // $resultgridtablegagalsukses .= "</table>";

    //DG untuk jenis transaksi
    $resultgridjenistrans = "";
    $resultgridjenistrans .= "<table id=\"jenistrans\" class=\"table table-bordered \" style=\"font-size:12px\"><tr>
                              <td bgcolor=#4A4A4B style=\"color:#fff\"><center>JENIS TRANSAKSI</center></td><td bgcolor=#4A4A4B style=\"color:#fff\"><center>TOTAL</center></td></tr>";
    for($i=0;$i<count($StatusTrxJeniss);$i++){
      $resultgridjenistrans .= "<tr><td>". $StatusTrxJeniss[$i]['transactiongroup']. "</td><td><center>". number_format($StatusTrxJeniss[$i]['Jumlah'],0,',','.') ."</center></td></tr>";
    }
    $resultgridjenistrans .= "</table>";

    $msg['isidata'] = $isidata;
    $msg['resultgridregion'] = $resultgridregion;
    $msg['resultgridjenistrans'] = $resultgridjenistrans;
    // $msg['gridgagalsukses'] = $resultgridtablegagalsukses;
    $msg['msg']= $data;
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    //$msg['msg'] = $result;
    echo json_encode($msg);

    }

    public function getDataX()
    {

    }

    public function save()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
