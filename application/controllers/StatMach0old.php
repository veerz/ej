<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class StatMach0old extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this->load->model('Role_Model');
		$this->load->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }

		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

    $sqlpieupdate = "Exec getEJUpdateStatusSummary";
		$qpieupdate = $this->db->query($sqlpieupdate);
		$data['piedataupdate'] = $qpieupdate->result_array();
    if(is_array($data['piedataupdate']) && count($data['piedataupdate'])>0)
      {
        $data['resultdiview'] = "ada";
      }else{
        $data['resultdiview'] = "ga ada";
      }
		$sqlbarMerk= "Exec getEJUpdateStatusByMerk";
		$barMerk = $this->db->query($sqlbarMerk);
		$data['bardataMerk'] = $barMerk->result_array();

		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel";
		$barRegion = $this->db->query($sqlbarRegion);
		$data['bardataregion'] = $barRegion->result_array();

    $data['cekregion'] = 0;
    $data['cekarea'] = 0;
    $data['cekmanageby'] = 0;
    $data['cekmerk'] = 0;


  	// $listterminalID = "SELECT DISTINCT terminalID FROM ATDS";
  	// $terminalID = $this->db->query($listterminalID);
  	// $data['terminalID'] = $terminalID->result_array();

    $listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

    $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    // $listconnectivityVendor = "SELECT DISTINCT connectivityVendor FROM ATDS";
    // $connectivityVendor = $this->db->query($listconnectivityVendor);
    // $data['connectivityVendor'] = $connectivityVendor->result_array();

    $listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
    $connectivityType = $this->db->query($listconnectivityType);
    $data['connectivityType'] = $connectivityType->result_array();


    // $storeArray();
    // while ($row = mysql_fetch_array($region)){
    //   $data['region'] = $row['kanwil'];
    // }

    //var_dump($data['region']);
	  $data['params'] = "";
    $this->load->template('StatMach0',$data);

	}

  public function getmanageby(){
    $jenismanage = $this->input->post("jenismanage");
    $region = $this->input->post("region");
	 $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
	if($region) {
		if($jenismanage){
			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%' and kanwil like '".$region."%'";
		}else
		{
			 $listmanagedBy .= " where kanwil like '".$region."'";
		}
	}else{
		 $listmanagedBy .= " where  managedBy like '".$jenismanage."%'";
	}

    $managedBy = $this->db->query($listmanagedBy);
    $dataresult = $managedBy->result();
    	$result = "";
		$result = "  <option selected disabled>Nama Pengelola</option>";
    foreach($dataresult as $item)
    {
      $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
    }
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    $msg['msg'] = $result;
    	echo json_encode($msg);
  }

  public function getmanagebyarea(){
    $jenismanage = $this->input->post("jenismanage");
    $area = $this->input->post("area");
	 $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
	if($area) {
		if($jenismanage){
			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%' and area like '".$area."%'";
		}else
		{
			 $listmanagedBy .= " where area like '".$area."'";
		}
	}else{
		 $listmanagedBy .= " where  managedBy like '".$jenismanage."%'";
	}

    $managedBy = $this->db->query($listmanagedBy);
    $dataresult = $managedBy->result();
    	$result = "";
		$result = "  <option selected disabled>Nama Pengelola</option>";
    foreach($dataresult as $item)
    {
      $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
    }
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    $msg['msg'] = $result;
    	echo json_encode($msg);
  }

  public function getmanagebymanagerType(){
    $jenismanage = $this->input->post("jenismanage");
    $managerType = $this->input->post("managerType");
	 $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
	if($managerType) {
		if($jenismanage){
			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%' and managedBy like '".$managerType."%'";
		}else
		{
			 $listmanagedBy .= " where managedBy like '".$managerType."'";
		}
	}else{
		 $listmanagedBy .= " where  managedBy like '".$jenismanage."%'";
	}

    $managedBy = $this->db->query($listmanagedBy);
    $dataresult = $managedBy->result();
    	$result = "";
		$result = "  <option selected disabled>Nama Pengelola</option>";
    foreach($dataresult as $item)
    {
      $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
    }
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    $msg['msg'] = $result;
    	echo json_encode($msg);
  }

   public function getarea(){
    $region = $this->input->post("region");
    $area = "SELECT DISTINCT area FROM ATDS where kanwil like '".$region."'";
    $area = $this->db->query($area);
    $dataresult = $area->result();
    	$result = "  <option selected disabled>Area</option>";
    foreach($dataresult as $item)
    {
      $result .= "<option value=\"{$item->area}\">{$item->area}</option>";
    }
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    $msg['msg'] = $result;
    	echo json_encode($msg);
  }

  public function getmerk(){
   $terminalType = $this->input->post("terminalType");
   $merk = "SELECT DISTINCT merk FROM ATDS where terminalType like '".$terminalType."'";
   $merk = $this->db->query($merk);
   $dataresult = $merk->result();
    $result = "  <option selected disabled>Merk</option>";
   foreach($dataresult as $item)
   {
     $result .= "<option value=\"{$item->merk}\">{$item->merk}</option>";
   }
   $msg['type'] = "done";
   //$msg['msg'] = $dataresult;
   $msg['msg'] = $result;
    echo json_encode($msg);
 }


    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {

    }

    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return JSON data userDetail
	 $region=null, $area=null,$managedBy=null,$connectivityVendor=null,$terminalType=null,$merk=null,$connectivityType=null
     */
    public function getData()
    {
		$status = ( $this->input->post("status")?:null);
    //echo $status; die()
		$region = ( $this->input->post("region")?:null);
		if($region != null){
		if($region == "REGION I / SUMATERA 1"){
		  $region = "REG01";
		}if($region == "REGION II / SUMATERA 2"){
		  $region = "REG02";
		}if($region == "REGION III / JAKARTA 1"){
		  $region = "REG03";
		}if($region == "REGION IV / JAKARTA 2"){
		  $region = "REG04";
		}if($region == "REGION V / JAKARTA 3"){
		  $region = "REG05";
		}if($region == "REGION VI / JAWA 1"){
		  $region = "REG06";
		}if($region == "REGION VII / JAWA 2"){
		  $region = "REG07";
		}if($region == "REGION VIII / JAWA 3"){
		  $region = "REG08";
		}if($region == "REGION IX / KALIMANTAN"){
		  $region = "REG09";
		}if($region == "REGION X / SULAWESI DAN MALUKU"){
		  $region = "REG10";
		}if($region == "REGION XI / BALI DAN NUSA TENGGARA"){
		  $region = "REG11";
		}if($region == "REGION XII / PAPUA"){
		  $region = "REG12";
		}if($region == "LUAR NEGERI")
		  $region = "REGLN";
	  }

		$area =( $this->input->post("area")?:null);
    //$managerType =( $this->input->post("managerType")?:null);
		$managerType =( $this->input->post("managerType")?: null);
		$managedBy =( $this->input->post("managedBy")?:null);
		// $connectivityVendor = ($this->input->post("connectivityVendor")?:null);
		$terminalType = ($this->input->post("terminalType")?:null);
		$merk = ($this->input->post("merk")?:null);
		$connectivityType = ($this->input->post("connectivityType")?:null);

		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);
	$param = "";
  $paramregion = "";
	$paramtampil="";
	if($status!=null)	$param .=  " @status='".$status."',";
	else $param .=  " @status=NULL,";
	if($region!=null)	$param .=  " @region='".$region."',";
	else $param .=  " @region=NULL,";
	if($area!=null)	$param .=  " @area='".$area."',";
	else $param .=  " @area=NULL,";
  if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
	else $param .=  " @managerType=NULL,";
  if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
	else $param .=  " @manager=NULL,";
	// if($connectivityVendor!=null)	$param .=  " @managerType='".$connectivityVendor."',";
	// else $param .=  " @managerType=NULL,";
	if($terminalType!=null)	$param .=  " @machineType='".$terminalType."',";
	else $param .=  " @machineType=NULL,";
	if($merk!=null)	$param .=  " @machineMake='".$merk."',";
	else $param .=  " @machineMake=NULL,";
	if($connectivityType!=null)	$param .=  " @commType='".$connectivityType."'";
	else $param .=  " @commType=NULL";

  if($status!=null)	$paramregion .=  " @status='".$status."',";
  else $paramregion .=  " @status=NULL,";
  if($region!=null)	$paramregion .=  " @region='".$region."',";
	else $paramregion .=  " @region=NULL,";
	if($area!=null)	$paramregion .=  " @area='".$area."',";
	else $paramregion .=  " @area=NULL,";
  if($managedBy!=null)	$paramregion .=  " @manager='".$managedBy."'";
	else $paramregion .=  " @manager=NULL";

	/* echo $param;
	die(); */
		$sqlpieupdate = "Exec getEJUpdateStatusSummary".$param;
		$qpieupdate = $this->db->query($sqlpieupdate);
		$data['piedataupdate'] = $qpieupdate->result_array();
		//var_dump( $data['piedataupdate'] );
		//var_dump( $sqlpieupdate );
		//die();

		$data['params'] = $param;
		$sqlbarMerk= "Exec getEJUpdateStatusByMerk".$param;
		$barMerk = $this->db->query($sqlbarMerk);
		$data['bardataMerk'] = $barMerk->result_array();


    if($data['piedataupdate'][0]['Update'] != 0 || $data['piedataupdate'][0]['notUpdate'] != 0)
      {
        $data['resultdiview'] = "ada";
      }else{
        $data['resultdiview'] = "ga ada";
      }


		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel ".$param;
		$barRegion = $this->db->query($sqlbarRegion);
		$data['bardataregion'] = $barRegion->result_array();



    $data['cekregion'] = ( $region?:0);
    $data['cekarea'] = ( $area?:0);
    $data['cekmanageby'] = ( $managedBy?:0);
    $data['cekmerk'] = ( $merk?:0);

    // var_dump(	$data['bardataregion']);
    //die();

  	// $listterminalID = "SELECT DISTINCT terminalID FROM ATDS";
  	// $terminalID = $this->db->query($listterminalID);
  	// $data['terminalID'] = $terminalID->result_array();

    $listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
    $kanwil = $this->db->query($listkanwil);
    $data['region'] = $kanwil->result_array();

    $listarea = "SELECT DISTINCT area FROM ATDS";
    $area = $this->db->query($listarea);
    $data['area'] = $area->result_array();

    $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
    $managedBy = $this->db->query($listmanagedBy);
    $data['managedBy'] = $managedBy->result_array();

    // $listconnectivityVendor = "SELECT DISTINCT connectivityVendor FROM ATDS";
    // $connectivityVendor = $this->db->query($listconnectivityVendor);
    // $data['connectivityVendor'] = $connectivityVendor->result_array();

    $listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
    $terminalType = $this->db->query($listterminalType);
    $data['terminalType'] = $terminalType->result_array();

    $listmerk = "SELECT DISTINCT merk FROM ATDS";
    $merk = $this->db->query($listmerk);
    $data['merk'] = $merk->result_array();

    $listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
    $connectivityType = $this->db->query($listconnectivityType);
    $data['connectivityType'] = $connectivityType->result_array();


    // $storeArray();
    // while ($row = mysql_fetch_array($region)){
    //   $data['region'] = $row['kanwil'];
    // }

    //var_dump($data['region']);
    // var_dump($sqlbarRegion);
    // die();
    $this->load->template('StatMach0',$data);
    }
    /**
     *  Get Data
     * Fungsi ini bertugas mengambil Single data userDetail dan menampilkannya.
     * @return ARRAY data userDetail
     */
    public function getDataX()
    {

    }

    /**
     * Save
     *
     * Fungsi ini bertugas melakukan save data.
     *
     * @return JSON data userDetail
     */
    public function save()
    {

    }

    /**
     * Update
     *
     * Fungsi ini bertugas melakukan update data
     *
     * @return JSON data userDetail
     */
    public function update()
    {

    }

    /**
     * Delete
     *
     * Fungsi ini bertugas melakukan delete data.
     *
     * @return JSON data userDetail
     */
    public function delete()
    {

    }
}
