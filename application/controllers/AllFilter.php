<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class AllFilter extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }



    public function getareabyRegion(){
     $region = $this->input->post("region");
  	 if($region){
  		$area = "SELECT DISTINCT area FROM ATDS where kanwil like '".$region."'";
  	 }else{
  			$area = "SELECT DISTINCT area FROM ATDS";
  	 }

     $area = $this->db->query($area);
     $dataresult = $area->result();

     $result = " <option value=\"\" selected disabled>Area</option>";
     foreach($dataresult as $item)
     {
       $result .= "<option value=\"{$item->area}\">{$item->area}</option>";
	   
     }
     $msg['type'] = "done";
     //$msg['msg'] = $dataresult;
     $msg['msg'] = $result;
     	echo json_encode($msg);
   }

	  public function getmanagebyJenisPengelola(){
        $jenismanage = $this->input->post("jenismanage");
        $region = $this->input->post("region");
        $area = $this->input->post("area");
        $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS ";
        if($region){
          if($jenismanage){
            if($area){
            $listmanagedBy .= "where managedBy like '".$jenismanage."%' and kanwil like '".$region."%' and area like '".$area."'";
            }else{
              $listmanagedBy .= "where managedBy like '".$jenismanage."%' and kanwil like '".$region."%'";
            }
          }else{
            if($area){
              $listmanagedBy .= "where kanwil like '".$region."%' and area like '".$area."'";
              }else{
                $listmanagedBy .= "where kanwil like '".$region."%'";
              }
          }
        }else{
			 if($jenismanage){
            if($area){
            $listmanagedBy .= "where managedBy like '".$jenismanage."%' and area like '".$area."'";
            }else{
              $listmanagedBy .= "where managedBy like '".$jenismanage."%'";
            }
          }else{
            if($area){
              $listmanagedBy .= "where area like '".$area."'";
              }else{
                $listmanagedBy .= "";
              }
          }
		}
        $managedBy = $this->db->query($listmanagedBy);
        $dataresult = $managedBy->result();
          	$result = "  <option value=\"\" selected disabled>Nama Pengelola</option>";
        foreach($dataresult as $item)
        {
          $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
        }
        $msg['type'] = "done";
        //$msg['msg'] = $dataresult;
        $msg['msg'] = $result;
          echo json_encode($msg);
    }


    public function getmanagebyRegion(){
      $jenismanage = $this->input->post("jenismanage");
      $region = $this->input->post("region");
  	 $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
  	if($region) {
  		if($jenismanage){
  			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%' and kanwil like '".$region."%'";
  		}else
  		{
  			 $listmanagedBy .= " where kanwil like '".$region."'";
  		}
  	}else{
  		 	if($jenismanage){
  			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%'";
  		}else
  		{
  			 $listmanagedBy .= " where kanwil like '".$region."'";
  		}
  	}

      $managedBy = $this->db->query($listmanagedBy);
      $dataresult = $managedBy->result();
      	$result = "";
  		$result = "  <option  value=\"\" selected disabled>Nama Pengelola</option>";
      foreach($dataresult as $item)
      {
        $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
      }
      $msg['type'] = "done";
      //$msg['msg'] = $dataresult;
      $msg['msg'] = $result;
      	echo json_encode($msg);
    }

   public function getmanagebyArea(){
     $jenismanage = $this->input->post("jenismanage");
     $area = $this->input->post("area");
 	 $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
 	if($area) {
 		if($jenismanage){
 			 $listmanagedBy .= " where  managedBy like '".$jenismanage."%' and area like '".$area."%'";
 		}else
 		{
 			 $listmanagedBy .= " where area like '".$area."'";
 		}
  }else{
      if($jenismanage){
       $listmanagedBy .= " where  managedBy like '".$jenismanage."%'";
    }else
    {
       $listmanagedBy .= " where area like '".$area."'";
    }
 	}

     $managedBy = $this->db->query($listmanagedBy);
     $dataresult = $managedBy->result();
     	$result = "";
 		$result = "  <option  value=\"\"  selected disabled>Nama Pengelola</option>";
     foreach($dataresult as $item)
     {
       $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
     }
     $msg['type'] = "done";
     //$msg['msg'] = $dataresult;
     $msg['msg'] = $result;
     	echo json_encode($msg);
   }

   public function getmerkbyJenisMesin(){
    $terminalType = $this->input->post("terminalType");
	if($terminalType){
		$merk = "SELECT DISTINCT merk FROM ATDS where terminalType like '".$terminalType."'";
	}else{
		$merk = "SELECT DISTINCT merk FROM ATDS";
	}
    $merk = $this->db->query($merk);
    $dataresult = $merk->result();
     $result = "  <option  value=\"\"  selected disabled>Merk</option>";
    foreach($dataresult as $item)
    {
      $result .= "<option value=\"{$item->merk}\">{$item->merk}</option>";
    }
    $msg['type'] = "done";
    //$msg['msg'] = $dataresult;
    $msg['msg'] = $result;
     echo json_encode($msg);
  }
}
