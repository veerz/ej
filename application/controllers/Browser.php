<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Browser extends CI_Controller

{
    /**
     * Constructor
     *
     * Fungsi ini berfungsi untuk meload model userDetail & userGroup
     */
    function __construct()
    {
        parent::__construct();
		$this
            ->load
            ->model('Role_Model');
		$this
            ->load
            ->model('menu_model');
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$listterminalID = "SELECT DISTINCT terminalID FROM ATDS";
		$terminalID = $this->db->query($listterminalID);
		$data['terminalID'] = $terminalID->result_array();

		$keyword = "SELECT DISTINCT keyword, sqlWhereCondition FROM keywordList";
		$keyword = $this->db->query($keyword);
		$data['keyword'] = $keyword->result_array();

		$listkanwil = "SELECT DISTINCT kanwil FROM ATDS";
		$kanwil = $this->db->query($listkanwil);
		$data['region'] = $kanwil->result_array();

		$listarea = "SELECT DISTINCT area FROM ATDS";
		$area = $this->db->query($listarea);
		$data['area'] = $area->result_array();
			
		//ini baru untuk kartu tertelan
		$listtransStat = "select keyword from keywordList(nolock) where sqlWhereCondition like 'transactionstatus2 in%'";
		$transStat = $this->db->query($listtransStat);
		$data['transStat'] = $transStat->result_array();
		
		$listtransaksi = "select distinct(transactionGroup) from transactionTypeDefinition";
		$jenistransaksi = $this->db->query($listtransaksi);
		$data['jenistransaksi'] = $jenistransaksi->result_array();

		$listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS";
		$managedBy = $this->db->query($listmanagedBy);
		$data['managedBy'] = $managedBy->result_array();

		$listconnectivityVendor = "SELECT DISTINCT connectivityVendor FROM ATDS";
		$connectivityVendor = $this->db->query($listconnectivityVendor);
		$data['connectivityVendor'] = $connectivityVendor->result_array();

		$listterminalType = "SELECT DISTINCT terminalType FROM ATDS";
		$terminalType = $this->db->query($listterminalType);
		$data['terminalType'] = $terminalType->result_array();

		$listmerk = "SELECT DISTINCT merk FROM ATDS";
		$merk = $this->db->query($listmerk);
		$data['merk'] = $merk->result_array();

		$listconnectivityType = "SELECT DISTINCT connectivityType FROM ATDS";
		$connectivityType = $this->db->query($listconnectivityType);
		$data['connectivityType'] = $connectivityType->result_array();
		// $storeArray();
		// while ($row = mysql_fetch_array($region)){
		//   $data['region'] = $row['kanwil'];
		// }
		//var_dump($data['region']);
		$this->load->template('browser',$data);
	}

    /**
     * Show Data
     *
     * Fungsi ini bertugas mengambil data userDetail dan menampilkannya.
     *
     * @return JSON data userDetail (DataTables)
     */
    public function showData()
    {
		$datetarik = explode(" - ",$this->input->post('startdate'));
		$region = ($this->input->post('region')?:null);
		$tanggalcek = ($this->input->post('tanggalcek')?:null);
		if($tanggalcek == '1'){
			$start = $datetarik[0] . ".000";
			$end = $datetarik[1] . ".000";	
		}else{
			$start = null;
			$end = null;	
		}
		$key = ($this->input->post('key')?:null);
		$key2 = ($this->input->post('key2')?:null);
		$region = ($this->input->post('region')?:null);
		$region =   str_replace("\/", "/ ", $region);
		$area = ($this->input->post('area')?:null);
		$area =   str_replace("\/", "/ ", $area);
		$terminalID = ($this->input->post('idmachine')?:null);
		$managedBy = ($this->input->post('managedBy')?:null);
		$definisi = ($this->input->post('definisi')?:null);
		$jenistransaksi = ($this->input->post('jenistransaksi')?:null);
		$managerType = ($this->input->post('managerType')?:null);
		$resi = ($this->input->post('resi')?:null);
		$nocard = ($this->input->post('nocard')?:null);
		//ini untuk kartu tertelan
		$transStat = ($this->input->post('transStat')?:null);
		$param = "";
		$paramtampil="";
		if($terminalID!=null)	$param .=  " @objectName='".$terminalID."',";
		else $param .=  " @objectName=NULL,";
		if($start!=null)	$param .=  " @startDate='".$start."',";
		else $param .=  " @startDate=NULL,";
		if($end!=null)	$param .=  " @endDate='".$end."',";
		else $param .=  " @endDate=NULL,";
		if($region!=null)	$param .=  " @region='".$region."',";
		else $param .=  " @region=NULL,";
		if($key!=null)	$param .=  ' @keyword="'.$key.'",';
		else $param .=  " @keyword=NULL,";
		if($key2!=null)	$param .=  ' @keyword2="'.$key2.'",';
		else $param .=  " @keyword2=NULL,";
		if($area!=null)	$param .=  " @area='".$area."',";
		else $param .=  " @area=NULL,";
		//ini yg baru untuk kartu tertelan
		if($transStat!=null)	$param .=  " @transactionStatus2='".$transStat."',";
		else $param .=  " @transactionStatus2=NULL,";
		if($definisi!=null)	$param .=  " @definisi='".$definisi."',";
		else $param .=  " @definisi=NULL,";
		if($jenistransaksi!=null)	$param .=  " @jenistransaksi='".$jenistransaksi."',";
		else $param .=  " @jenistransaksi=NULL,";
		if($managerType!=null)	$param .=  " @managerType='".$managerType."',";
		else $param .=  " @managerType=NULL,";
		if($managedBy!=null)	$param .=  " @manager='".$managedBy."',";
		else $param .=  " @manager=NULL,";
		if($resi!=null)	$param .=  " @recordNumber='".$resi."',";
		else $param .=  " @recordNumber=NULL,";
		if($nocard!=null)	$param .=  " @cardNumber='".$nocard."'";
		else $param .=  " @cardNumber=NULL";

		//echo($param);die();
		$sql = "Exec browseEJ".$param;
		$sqlq = $this->db->query($sql);
		$dataresult = $sqlq->result();
		$result = "<thead><tr><th>Select</th><th>#</th><th>ID Mesin</th><th>Jenis Mesin</th><th>Nomor Kartu</th><th>Jenis Transaksi</th><th>Tanggal Transaksi</th><th>Waktu Transaksi</th><th>Nominal</th><th>Nomor Resi</th><th>Definisi Transaksi</th><th>Status Transaksi</th><th>Tipe Transaksi</th><th>Keterangan</th><th>Aging</th><th>Nama Pengelola</th><th>Kanwil</,th><th style=\"width: 60px!important;\">Area</th><th>Jenis Pengelola</th><th>Jarkom</th><th>Tahun Deploy</th></tr></thead><tbody>";
		$seq = 0;
		foreach($dataresult as $item){
				$seq++;
				$kanwil = substr($item->kanwil,0,9);
				$result .= "<tr><td style=\" text-align: center;\"><input type=\"checkbox\" name=\"listcheck\" class=\"resultchecks\" data-propid=\"{$item->transactionid}\"></td><td>{$seq}</td><td>{$item->TermID}</td><td>{$item->JenisMesin}</td><td>{$item->nomorKartu}</td><td>{$item->JenisTransaksi}</td><td>{$item->tanggalTransaksi}</td><td>{$item->waktuTransaksi}</td><td>{$item->amount}</td><td>{$item->recordNumber}</td><td>{$item->definisiTransaksi}</td><td>{$item->statusTransaksi}</td>><td>{$item->tipeTransaksi}</td><td>{$item->keterangan}</td><td>{$item->age}</td><td>{$item->pengelola}</td><td>{$kanwil}</td><td style=\"width: 60px!important;\">{$item->area}</td><td>{$item->jenisPengelola}</td><td>{$item->connectivityType}</td><td>{$item->tahunDeploy}</td></tr>";
		}
		$result.= "</tbody>";
			$msg['msg']['result']= $result;
			$msg['msg']['exec']= $sql ;
			$msg['msg']['seq']= $seq ;
          $msg['type'] = "done";
          //$msg['msg'] = $dataresult;
          //$msg['msg'] = $result;
		  
		 
          	echo json_encode($msg);
									
    }

	// public function getmanageby(){
  //     $jenismanage = $this->input->post("jenismanage");
  //     $listmanagedBy = "SELECT DISTINCT managedBy FROM ATDS where managedBy like '".$jenismanage."%'";
  //     $managedBy = $this->db->query($listmanagedBy);
  //     $dataresult = $managedBy->result();
  //       $result = "";
  //     foreach($dataresult as $item)
  //     {
  //       $result .= "<option value=\"{$item->managedBy}\">{$item->managedBy}</option>";
  //     }
  //     $msg['type'] = "done";
  //     //$msg['msg'] = $dataresult;
  //     $msg['msg'] = $result;
  //       echo json_encode($msg);
  //   }

  //   public function getarea(){
  //    $region = $this->input->post("region");
	 
	//  if($region == "" || $region == Null){
	// 	 $area = "SELECT DISTINCT area FROM ATDS";
	//  }else{
	// 		$area = "SELECT DISTINCT area FROM ATDS where kanwil like '".$region."'";
	//  }
     
  //    $area = $this->db->query($area);
  //    $dataresult = $area->result();
  //    	$result = "  <option selected disabled>Area</option>";
  //    foreach($dataresult as $item)
  //    {
  //      $result .= "<option value=\"{$item->area}\">{$item->area}</option>";
  //    }
  //    $msg['type'] = "done";
  //    //$msg['msg'] = $dataresult;
  //    $msg['msg'] = $result;
  //    	echo json_encode($msg);
  //  }

	public function showResi()
    {
		$listresi = ($this->input->post('listresi')?:null);
		$result = "";
		foreach($listresi as $key){
			$hasil = $this->db->query("Select replace(EJText,char(13),'<br/>') as 'EJText', objectName, recordNumber from EJTransactions (nolock) where TransactionID = '". $key."' ")->result();
			foreach($hasil as $item)
			{
				$result .= "<p><b> Terminal ID :  ".$item->objectName. " ||  Resi Number : ".$item->recordNumber. "  </b> </p><pre> ".$item->EJText. " </pre><br /><hr style=\"border-top: dotted 5px;\" />";
			}
		}
		$msg['msg']= $result;
		$msg['type'] = "done";
		echo json_encode($msg);
	}
}
