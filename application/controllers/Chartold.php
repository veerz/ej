<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * userDetail Controller Class Doc Comment
 *
 * @category Controller
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test/userDetail
 */
class Chart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

		$this->load->model('Role_Model');
		$this->load->model('menu_model');
        $this->load->model('audit_model',"aum");
    }
    /**
     * Index
     *
     * Fungsi ini berfungsi untuk menampilkan userGroup_View
     *
     * @return void
     */
	 public function Cetak_priv_module(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "MODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

	 public function Cetak_priv_submodule(){
		 $role = $this->session->userdata("role");
		 $data = $this
					  ->db
					  ->select('settings_Menu.*')
					  ->from('priviledgeRole')
					  ->join('roles', 'roles.id_role = priviledgeRole.id_role')
					  ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
					  ->Where("roles.id_role", $role)
					  ->Where("type", "SUBMODULE")
					  /* ->Order_by("settings_Menu.id_menu", 'asc') */
					  ->order_by("priority","ASC")
					  ->get()->result();
		//$datalistmenu= $data->result();
		return $data;
	 }

    function index()
    {
		if($this->session->userdata('username' == NULL) or empty($this->session->userdata('username'))){
          redirect('login');
        }
    $data_audit = array(
        'id_user'       => $this->session->userdata('id_user'),
        'username'      => $this->session->userdata('username'),
        'page'          => "Home",
        'action'        => "Chart",
        'detail'        => "User '".$this->session->userdata('username')."' has view chart in at ".date('Y-m-d H:i:s'),
        'created_date'  => date('Y-m-d H:i:s')
    );
    $result = $this->db->insert('AuditTrail', $data_audit);
		$data['datalistmenu'] =json_decode(json_encode($this->Cetak_priv_module()), True);
		$data['datalistmenusub'] =json_decode(json_encode($this->Cetak_priv_submodule()), True);

		$sqlpieupdate = "Exec getEJUpdateStatusSummary";
		$qpieupdate = $this->db->query($sqlpieupdate);
		$data['piedataupdate'] = $qpieupdate->result_array();

		$sqlbarMerk= "Exec getEJUpdateStatusByMerk";
		$barMerk = $this->db->query($sqlbarMerk);
		$data['bardataMerk'] = $barMerk->result_array();

		$sqlbarRegion= "Exec getEJUpdateStatusBottomPanel";
		$barRegion = $this->db->query($sqlbarRegion);
		$data['bardataregion'] = $barRegion->result_array();

        //$this->load->view('chart',$data);
		$this->load->template('chart',$data);
	}
}
