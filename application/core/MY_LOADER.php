<?php
/**
 * Class untuk meload template header dan foooter
 */
class MY_Loader extends CI_Loader
{
    public function template($template_name, $vars = array() , $return = false)
    {
        if ($return):
            $content = $this->view('layouts/headerView', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('layouts/footerView', $vars, $return);

            return $content;
        else:
            $this->view('layouts/headerView', $vars);
            $this->view($template_name, $vars);
            $this->view('layouts/footerView', $vars);
        endif;
    }

    public function logintemplate($template_name, $vars = array() , $return = false)
    {
        if ($return):
            $content = $this->view('layouts/headerLoginView', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('layouts/footerLoginView', $vars, $return);

            return $content;
        else:
            $this->view('layouts/headerLoginView', $vars);
            $this->view($template_name, $vars);
            $this->view('layouts/footerLoginView', $vars);
        endif;
    }
}

