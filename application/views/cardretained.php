<!-- Content Wrapper. Contains page content -->
<style>
select:required:invalid {
  color: black;
}
option[value=""][disabled] {
  display: none;
  color: black;
}
option {
  color: black;
}
#adadata{
	display:none;
}
</style>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0 text-dark">Kartu Tertelan</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Kartu Tertelan</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

    <!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
                <!-- <form id="myform" name="myform"> -->
                <form method="post">
								<div class="card-header">
									<div>
									  <h6><b>Cari berdasarkan : </b></h6>
								  </div>

									<div class="d-flex justify-content-between">
                    <table class="card-body table-responsive">
											<td><input autocomplete="off" type="text" id="date" name="date" class="form-control" placeholder="Tanggal" style="height:29px;width:190px"></td>
                      <td>
                        <select id="termID" name="termID" class="form-control select2idmachine">
                          <option value="" selected disabled>ID Mesin</option>
                							<?php
                							foreach($termID as $termID){
                								?>
                							<option  value="<?php echo $termID['terminalID'] ?>">
                							<?php echo $termID['terminalID']; ?>
                							</option>
                							<?php
                							}
                							?>
            						</select>
                      </td>
                      <td>
                        <select id="region" name="region" class="form-control select2" style="width:190px">
                          <option selected disabled>Wilayah</option>
                          <?php
                          foreach($region as $region){
                            ?>
                          <option  value="<?php echo $region['kanwil'] ?>">
                          <?php echo $region['kanwil']; ?>
                          </option>
                          <?php
                          }
                          ?>
                        </select>
                        </td>

                      <td>
                        <select id="area" name="area" class="form-control select2" style="width:190px">
                          <option selected disabled>Area</option>
                          <?php
                          foreach($area as $area){
                            ?>
                          <option  value="<?php echo $area['area'] ?>">
                          <?php echo $area['area']; ?>
                          </option>
                          <?php
                          }
                          ?>
                        </select>
                      </td>
                      <td>
                        <select id="managerType" name="managerType" class="form-control select2" style="width:190px">
                          <option selected disabled>Jenis Pengelola</option>
            							<option value="Vendor">Vendor</option>
            							<option value="Branch">Branch</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>
                      <select id="managedBy" name="managedBy" class="form-control select2" style="width:190px">
                        <option selected disabled>Nama Pengelola</option>
                        <?php
                						foreach($managedBy as $managedBy){
                								?>
                							<option  value="<?php echo $managedBy['managedBy'] ?>">
                							<?php echo $managedBy['managedBy']; ?>
                							</option>
                							<?php
                            }
							           ?>
                      </select>
                      </td>
                      <td>
                        <select id="terminalType" name="terminalType" class="form-control select2" style="width:190px">
                          <option selected disabled>Jenis Mesin</option>
                          <?php
                          foreach($terminalType as $terminalType){
                            ?>
                          <option  value="<?php echo $terminalType['terminalType'] ?>">
                          <?php echo $terminalType['terminalType']; ?>
                          </option>
                          <?php
                          }
                          ?>
                        </select>
                      </td>
                      <td>
                        <select id="merk" name="merk" class="form-control select2" style="width:190px">
                          <option selected disabled>Merk</option>
                          <?php
                          foreach($merk as $merk){
                            ?>
                          <option  value="<?php echo $merk['merk'] ?>">
                          <?php echo $merk['merk']; ?>
                          </option>
                          <?php
                          }
                          ?>
                        </select>
                      </td>
                      <td>
                        <select id="connectivityType" name="connectivityType" class="form-control select2" style="width:190px">
                          <option selected disabled>Jarkom</option>
                          <?php
                          foreach($connectivityType as $connectivityType){
                            ?>
                          <option  value="<?php echo $connectivityType['connectivityType'] ?>">
                          <?php echo $connectivityType['connectivityType']; ?>
                          </option>
                          <?php
                          }
                          ?>
                        </select>
                      </td>
                    </table>
				      </div>
                  <div class="row">
                    <button type="button" class="btn btn-primary" id="filter" style="margin-left:2%">Submit</button>
                    <span style="font-size:12px;color:green"><?php echo $params; ?></span>
                    <button type="button" id="submitreset" class="btn btn-danger"style="margin-left:0.2%">Reset</button>
                    <div id="tempsearch" style="margin-left:1%"></div>
                  </div>
			</div>
      </form>
			<!-- /.card-header -->
    <!-- Main content -->
	  <div id="nodata" style="margin-left:1%">
										
    </div> 
  <div class="card-body table-responsive"  id="adadata">
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
		<div id="table">
            <div class="row">
              <div class="col-sm-12">
                <h5> <center><label>Kartu Tertelan Berdasarkan Wilayah</label></center> </h5>
                 <div id="chartReport1">
                   <canvas  id="bar5" width="90" height="40"></canvas>
                 </div>
                <div id="resultgrid" class="card-body table-responsive">

                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-5">
                  <h5> <center><label>Kartu Tertelan Berdasarkan Merek Mesin</label></center> </h5>
                  <div id="chartReport2">
                  <canvas  id="horbar2" width="90" height="55"></canvas>
                  </div>
              </div>
              <div class="col-sm-7">
              <h5 style="font-family:calibri;"> <center><label>Top 10 Kartu Tertelan Berdasarkan Bank </label></h5>
              <!-- <a data-toggle="modal" data-target="#Modal_kartu_tertelan_bank" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center>  -->
              <div class="row">
                  <div class="col-sm-6">
                    <div id="datanamabank10first" class="card-body table-responsive"></div>
                  </div>
                  <div class="col-sm-6">
                    <div id="datanamabank10next" class="card-body table-responsive"></div>
                  </div>
                </div>
                <div class="modal fade" id="Modal_kartu_tertelan_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
													<div class="modal-header bg-primary">
															<h9 style="font-family:calibri;" class="modal-title" id="exampleModalLabel">Total Kartu Tertelan Berdasarkan Bank</h9>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
															</button>
													</div>
													<div class="modal-body">
															<div class="form-group row">
																<div id="datanamabank" class="card-body table-responsive">

								                </div>

															</div>

													</div>
											</div>
									</div>
							</div>
              </div>
                
              </div>
            </div>
		</div>

				<!-- <div class="card-body">
                  <button type="button" class="btn btn-primary" data-toggle="tab" id="resi">Report Bulanan</button>
                  <button type="button" class="btn btn-primary" data-toggle="tab" value="Create PDF" id="btPrint" onclick="createPDF()">Print PDF</button>
                  <button type="button" class="btn btn-primary" data-toggle="tab" id="excel" >Print Excel</button>
              </div> -->
                </div>
				</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /.container-fluid -->
	</div>
	</div>

  <script src="<?php echo base_url(); ?>assets/plugins/foundation-datepicker-master/js/foundation-datepicker.js"></script>
	<script>
	$('#date').fdatepicker({
		format: 'yyyy-mm-dd',
	})
	</script>