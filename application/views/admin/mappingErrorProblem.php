<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    
                  </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Error Problem</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Machine Header - END - -->

    <!-- Master Machine Main - START - -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>Mapping Error Problem</label></h3>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus-circle"></i> <h9 style="font-family:calibri">Add<h9></button>
                            </div>
                        </div>
                         <!-- /.card-header -->
                        <div class="card-body table-responsive">
						  <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
							  <div class="row">
							  <div class="col-sm-12 col-md-6">
							  <div class="dataTables_length" id="mydata_length">
							  
							  </div>
							  </div>
							  <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">

							  
							  </div>
							  </div>
							  </div>

						  <div class="row">
							<div class="col-sm-12">
                           <table id="mydata" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Problem</th>
                                      <th>Error Code</th>
                                      <th>Aksi</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php $no =0 ;foreach($all as $row): $no++?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $row->problem; ?></td>
                                      <td><?php echo $row->error_code; ?></td>
                                      <td>
                                          <center>
                                            <div class="tooltip-demo">
                                              <a data-toggle="modal" data-target="#modal-edit<?=$row->id_problem;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a>
                                              <a href="<?php echo base_url('Admin/MappingErrorProblem/hapus/'.$row->id_problem); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Problem <?=$row->problem;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a>
                                            </div>
                                          </center>
                                      </td>
                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>
                          <!-- /.table-responsive -->
                      </div>
                      <!-- /.card-body -->
                    </div>
                </div>
      </div>

  </section>

    <!-- Master Machine Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/MappingErrorProblem/add') ?>" method="post">
        <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Error Problem</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Problem</label>
                            <div class="col-md-10">
                                <input type="text" name="problem" id="problem" class="form-control" placeholder="Problem"> </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Error Code</label>
                            <div class="col-md-10">
                                <input type="text" name="error_code" id="error_code" class="form-control" placeholder="Error Code"> </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Machine Modal - EDIT - START - -->

    <?php foreach($all as $row): ?>
      <div class="row">
        <div id="modal-edit<?=$row->id_problem;?>" class="modal fade">
          <div class="modal-dialog modal-lg">
            <form action="<?php echo base_url('Admin/MappingErrorProblem/edit'); ?>" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">

                <h4 class="modal-title">Edit Mapping Error Problem</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <div class="form-group" style="display:none;">
                <label>id problem</label>
                <input type="text" readonly value="<?=$row->id_problem;?>" name="id_problem" class="form-control" >
                  </div>

                <div class="form-group">
                  <label>Problem</label>
                  <div><input type="text" name="problem" autocomplete="off" value="<?=$row->problem;?>" required placeholder="Masukkan Problem" class="form-control" ></div>
                </div>

                <div class="form-group">
                  <label>Error Code</label>
                  <div><input type="text" name="error_code" autocomplete="off" value="<?=$row->error_code;?>" required placeholder="Masukkan Error Code" class="form-control" ></div>
                </div>

                <br>
              </div>
                <div class="modal-footer">
                  <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
              </form>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

    <!-- Master Machine Modal - EDIT - END - -->

</div>
