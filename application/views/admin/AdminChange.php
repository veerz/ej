


<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Change Password</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol>
                </div>

            </div>


        <!-- /.container-fluid -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->


						<div class="panel-body">
							<form class="form-group" method="POST" action="<?php echo base_url();?>detail/Show">
								<div class="row">
									<div class="col-md-12">
										<div class="card">
											
											<!-- /.card-header -->

											<div>
												<style>
													th, td {
													padding: 5px;
													}
												</style>

												<table class="card-body table-responsive">
													<tr>
														<td><label>Nama</label></td>
														<td><input value="<?php echo $this->session->userdata("name"); ?>" style="background-color:#E9ECEF;border-radius:5px;border-width:0.5px;" type="text" readonly id="name" name="name"></td>
													</tr>
													<tr>
														<td><label>Username</label></td>
														<td><input value="<?php echo $this->session->userdata("username"); ?>" style="background-color:#E9ECEF;border-radius:5px;border-width:0.5px;" type="text" readonly id="username" name="username"></td>
														
													</tr>
													<tr>
														<td><label>Password</label></td>
														<td><a href="javascript:void(0);" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit"> <i class="fa fa-edit" style="border-radius:5px;"> <h9 style="font-family:calibri">Edit<h9></i></a></td>
													</tr>
												</table>

											</div>
										</div>
										<!-- /.card -->
									</div>
								</div>
							<!-- /.row (main row) -->
							</form>
						</div>
		</div>
	</section>


   <div id="modal-edit" class="modal fade">
    <div class="modal-dialog modal-lg">
      <form action="<?php echo base_url('AdminChange/update') ?>" method="post">
        <div class="modal-content">
            <div class="modal-header bg-primary">

            <h4 class="modal-title">Settings Profil</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <!-- <div class="form-group" style="display:none;">
              <label>Id Users</label>
              <input display="none" type="text" readonly value="<?=$row->id_user;?>" name="id" class="form-control" >
            </div> -->
            <div class="form-group" style="display:none;">
              <label>Id User</label>
              <input display="none" type="text" readonly value="<?php echo $this->session->userdata("id_user"); ?>" name="id_user" id ="id_user"  class="form-control" >
            </div>
            <div class="form-group">
              <label>Nama</label>
              <div><input type="text" id="name" name="name" autocomplete="off" value="<?php echo $this->session->userdata("name"); ?>" readonly placeholder="Masukkan Nama" class="form-control"></div>
            </div>
            <div class="form-group">
              <label>Username</label>
              <div><input type="text" id="username" name="username" autocomplete="off" value="<?php echo $this->session->userdata("username"); ?>" readonly placeholder="Masukkan username" class="form-control"></div>
            </div>
            <div class="form-group">
              <label>Password</label>
              <div><input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
					<input type="checkbox" onclick="myFunction()">Show Password</div>
            </div>

            <br>
            </div>
            <div class="modal-footer">
              <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
              <button type="reset" value="Reload Page" onClick="window.location.reload()" id="reset" onclick="newFunction()" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </form>
    </div>
  </div>


	<!-- end of section -->
</div>
</div>
</div>

<script>
  function myFunction() {
    var x = document.getElementById("password");
    if (x.type === "password") {
    x.type = "text";
    } else {
      x.type = "password";
    }
  }


</script>
