<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <!-- <h1>Note:action for edit and delete on development</h1> -->
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">List Op Code</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Machine Header - END - -->

    <!-- Master Machine Main - START - -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                      <form action="<?php //echo base_url('ListOpCode/getData') ?>" method="post">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>List Op Code</label></h3>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus-circle"></i> Add</button>
                            </div>
                        </div>
                         <!-- /.card-header -->
                        <div class="card-body table-responsive">
                               <table id="mydata" class="table table-bordered table-striped">
                                  <thead>
                                      <tr>
                                          <th>No</th>
                                          <th>Opcode</th>
                                          <th>Deskripsi</th>
											                    <th>Tipe Transaksi</th>
                                          <th>Tipe Model</th>
                                          <th>Aksi</th>
                                      </tr>
                                  </thead>
                                  <tbody id="showData">
                                      <?php $no =0 ;foreach($listopcode as $row): $no++?>
                                      <tr class="odd gradeX">
                                        <td><pre><?php echo $no; ?></pre></td>
                                        <td style="white-space: pre-wrap;"><?php echo $row['OPCode']; ?></td>
                                        <td><?php echo $row['transactionType']; ?></td>
                                        <td><?php echo $row['transactionGroup']; ?></td>
                                        <td><?php echo $row['modelType']; ?></td>

                                          <td>
                                              <center>
                                                <div class="tooltip-demo">
                                                  <a href="javascript:void(0);" data-key="<?php echo $row['OPCode']?>" class="btn btn-sm btn-warning btn-circle" title="Edit OPCode"><i class="fa fa-edit"></i>
                                                  </a>
                                                  <!--<a href="<?php echo base_url('Admin/ListOpCode/delete/'.$row['OPCode']); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Opcode <?=$row['OPCode'];?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                                  </a>-->
                                                </div>
                                              </center>
                                          </td>
                                      </tr>
                                      <?php endforeach; ?>
                                  </tbody>
                              </table>
                              <!-- /.table-responsive -->
                          </div>
                      <!-- /.card-body -->
                    </div>
                </div>
              </div>
      </form>
  </section>

    <!-- Master Machine Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/ListOpCode/saveData')?>" method="post">
        <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title" id="exampleModalLabel">Add List Op Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Opcode</label>
                            <div class="col-md-10">
                                <input type="text"  name="OPCode" id="OPCode" class="form-control" placeholder="OPCode" maxlength="10"> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Deskripsi</label>
                            <div class="col-md-10">
                                <input type="text" name="transactionType" id="transactionType" class="form-control" placeholder="Deskripsi" maxlength="30"> </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-md-2 col-form-label">Model Mesin</label>
                          <div class="col-md-10">
                            <select name="modelType" id="modelType" class="form-control select2" style="width:100%;">
                            <option selected disabled> -- Model Mesin -- </option>
                              <?php
                                          foreach ($modelType as $modelType) {
                                              echo "<option value='".$modelType['modelType']."'>".$modelType['modelType']."</option>";
                              }
                              ?>
                            </select>
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                          <div class="col-md-10">
                            <select name="transactionGroup" id="transactionGroup" class="form-control select2" style="width:100%;">
                              <option selected disabled>Tipe Transaksi</option>
                              <?php
                              foreach($transactionGroup as $transactionGroup){
                                ?>
                              <option  value="<?php echo $transactionGroup['transactionGroup'] ?>">
                              <?php echo $transactionGroup['transactionGroup']; ?>
                              </option>
                              <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Machine Modal - EDIT - START - -->
        <div class="modal fade" id="Modal_Edit"  tabindex="-1" role="dialog"  aria-hidden="true">
            <form id="mydata" action="<?php echo base_url('Admin/ListOpCode/updateData'); ?>" method="post">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-primary">
                    <h4 class="modal-title">Edit List Op Code</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                      <label>Opcode</label>
                      <div><input type="text" readonly  name="editOPCode" id="editOPCode" value="" required placeholder="Masukkan Opcode" class="form-control" maxlength="10" ></div>
                    </div>

                    <div class="form-group">
                      <label>Deskripsi</label>
                      <div><input type="text" readonly  name="edittransactionType" id="edittransactionType" autocomplete="off" value="" required placeholder="Masukkan Deskripsi" class="form-control" maxlength="30" ></div>
                    </div>

                		<div class="form-group">
                      <label>Tipe Transaksi</label>
                    			<div>
                    			<select name="edittransactionGroup" id="edittransactionGroup" class="form-control select2" style="width:100%;">
                    			  <option selected disabled>Tipe Transaksi</option>
                        			  <?php
                              foreach($transactionGroup as $transactionGroup){
                                ?>
                              <option  value="<?php echo $transactionGroup['transactionGroup'] ?>">
                              <?php echo $transactionGroup['transactionGroup']; ?>
                              </option>
                              <?php
                              }
                              ?>
                    			</select>
                    		  </div>
                		</div>

                    <br>
                  </div>
                    <div class="modal-footer">
                      <button type="submit" id="btn_save" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
                      <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  </form>
              </div>
            </div>
      </div>
    <!-- Master Machine Modal - EDIT - END - -->
  </div>
</div>
</div>
</div>
