<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                  </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Definisi</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Machine Header - END - -->

    <!-- Master Machine Main - START - -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>Mapping Definisi</label></h3>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus-circle"></i> <h9 style="font-family:calibri">Add<h9></button>
                            </div>
                        </div>
                         <!-- /.card-header -->
                        <div class="card-body table-responsive">
      						  <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
      							  <div class="row">
      							  <div class="col-sm-12 col-md-6">
      							  <div class="dataTables_length" id="mydata_length">

      							  </div>
      							  </div>
      							  <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


      							  </div>
      							  </div>
      							  </div>

      						  <div class="row">
      							<div class="col-sm-12">
                                 <table id="mydata" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Definisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no =0 ;foreach($all as $row): $no++?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $row->Definisi; ?></td>

                                            <td>
                                                <center>
                                                  <div class="tooltip-demo">
                                                    <a data-toggle="modal" data-target="#modal-edit<?=$row->status;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="<?php echo base_url('Admin/Mappingdefinisi/hapus/'.$row->status); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Definisi <?=$row->Definisi;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                                    </a>
                                                  </div>
                                                </center>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.card-body -->
                          </div>


      </div>

  </section>

    <!-- Master Machine Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/Mappingdefinisi/add') ?>" method="post">
        <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Definisi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Definisi</label>
                            <div class="col-md-10">
                                <input type="text" name="Definisi" id="Definisi" class="form-control" placeholder="Definisi" maxlength="20" pattern="^[a-zA-Z]+$" title="Input harus berupa Alphabet"> </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                      <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Machine Modal - EDIT - START - -->

    <?php foreach($all as $row): ?>
      <div class="row">
        <div id="modal-edit<?=$row->status;?>" class="modal fade">
          <div class="modal-dialog modal-lg">
            <form action="<?php echo base_url('Admin/Mappingdefinisi/edit'); ?>" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">

                <h4 class="modal-title">Edit Mapping Definisi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group" style="display:none;">
                  <label>Id Mapping Definisi</label>
                <input display="none" type="text" readonly value="<?=$row->status;?>" name="status" class="form-control" >
                </div>
                <div class="form-group">
                  <label>Definisi</label>
                  <div><input type="text" id="Definisi" name="Definisi" autocomplete="off" value="<?=$row->Definisi;?>" required placeholder="Masukkan Definisi" class="form-control" maxlength="20" pattern="^[a-zA-Z]+$" title="Input harus berupa alphabet" ></div>
                </div>

                <br>
              </div>
                <div class="modal-footer">
                  <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  <button type="reset" id="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
              </form>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
    <!-- Master Machine Modal - EDIT - END - -->
</div>

<script>
function newFunction() {
  document.getElementById("#Definisi").reset();
}
</script>
