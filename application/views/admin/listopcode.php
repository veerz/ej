<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <!-- <h1>Note:action for edit and delete on development</h1> -->
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">List Op Code</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Machine Header - END - -->

    <!-- Master Machine Main - START - -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                      <form action="<?php //echo base_url('ListOpCode/getData') ?>" method="post">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>List Op Code</label></h3>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus-circle"></i> Add</button>
                            </div>
                        </div>
                         <!-- /.card-header -->
                        <div class="card-body table-responsive">
                               <table id="mydata" class="table table-bordered table-striped">
                                  <thead>
                                      <tr>
                                          <th>No</th>
                                          <th>Opcode</th>
                                          <th>Deskripsi</th>
											                    <th>Tipe Transaksi</th>
                                          <th>Tipe Model</th>
                                          <th>Aksi</th>
                                      </tr>
                                  </thead>
                                  <tbody id="showData">
                                      <?php $no =0 ;foreach($listopcode as $row): $no++?>
                                      <tr class="odd gradeX">
                                        <td><pre><?php echo $no; ?></pre></td>
                                        <td style="white-space: pre-wrap;"><?php echo $row['OPCode']; ?></td>
                                        <td><?php echo $row['transactionType']; ?></td>
                                        <td><?php echo $row['transactionGroup']; ?></td>
                                        <td><?php echo $row['modelType']; ?></td>

                                          <td>
                                              <center>
                                                <div class="tooltip-demo">
                                                  <a href="javascript:void(0);" data-key="<?php echo $row['transactionTypeID']?>" data-opcode="<?php echo $row['OPCode']?>" data-model="<?php echo $row['modelType']?>" class="btn btn-sm btn-warning btn-circle" title="Edit OPCode"><i class="fa fa-edit"></i>
                                                  </a>
                                                  <a data-key="<?php echo $row['transactionTypeID']?>" data-opcode="<?php echo $row['OPCode']?>" data-model="<?php echo $row['modelType']?>" class="btn btn-sm btn-danger btn-circle" title="Delete OPCode"><i class="fa fa-trash" style="color:white"></i></a>
                                                </div>
                                              </center>
                                          </td>
                                      </tr>
                                      <?php endforeach; ?>
                                  </tbody>
                              </table>
                              <!-- /.table-responsive -->
                          </div>
                      <!-- /.card-body -->
                    </div>
                </div>
              </div>
      </form>
  </section>

    <!-- Master Machine Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/ListOpCode/saveData')?>" method="post">
        <div class="modal fade" id="modal-tambah" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title" id="exampleModalLabel">Add List Op Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">OPCode</label>
                            <div class="col-md-10">
                                <input type="text"  name="OPCode" id="OPCode" class="form-control" placeholder="OPCode" maxlength="10" pattern="^[a-zA-Z- -]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" minlength="8" maxlength="8" required> </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-md-2 col-form-label">Model Mesin</label>
                          <div class="col-md-10">
                            <select name="modelType" id="modelType" class="form-control select2" style="width:100%;" required>
                            <option selected disabled value="">Model Mesin</option>
                            <?php
                              foreach($modelType as $mt){
                                ?>
                              <option  value="<?php echo $mt['modelType'] ?>">
                              <?php echo $mt['modelType']; ?>
                              </option>
                              <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                          <div class="col-md-10">
                            <select name="transactionType" id="transactionType" class="form-control select2" style="width:100%;" required>
                              <option selected disabled value="">Tipe Transaksi</option>
                              <?php
                              foreach($transactionType as $tt){
                                ?>
                              <option  value="<?php echo $tt['transactionType'] ?>">
                              <?php echo $tt['transactionType']; ?>
                              </option>
                              <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Machine Modal - EDIT - START - -->
        <div class="modal fade" id="Modal_Edit"  role="dialog"  aria-hidden="true">
            <form action="<?php echo base_url('Admin/ListOpCode/updateData'); ?>" method="post">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-primary">
                    <h4 class="modal-title">Edit List Op Code</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group row">
                      <label class="col-md-2 col-form-label">Opcode</label>
                      <div class="col-md-10">
											<input type="text"   name="editOPCode" id="editOPCode" value="" required placeholder="Masukkan Opcode" class="form-control" minlength="8" maxlength="8" >
											<input type="hidden"   name="trans_id" id="trans_id" value="" required placeholder="Masukkan Opcode" class="form-control" maxlength="10" >
										</div>
                    </div>

                  <div class="form-group row">
                    <label class="col-md-2 col-form-label">Model Mesin</label>
                    <div class="col-md-10">
                    <select name="efmodelType"  id="efmodelType" class="form-control select2" style="width:100%;">
                    <option selected disabled>Model Mesin</option>
                    <?php
                                foreach($modelType as $mt){
                                  ?>
                                <option  value="<?php echo $mt['modelType'] ?>">
                                <?php echo $mt['modelType']; ?>
                                </option>
                                <?php
                                }
                                ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                        <div class="col-md-10">
                        <select name="eftransactionType" id="eftransactionType" class="form-control select2" style="width:100%;">
                          <option selected disabled>Tipe Transaksi</option>
                              <?php
                              foreach($transactionType as $tt){
                              ?>
                            <option  value="<?php echo $tt['transactiontypeID'] ?>">
                            <?php echo $tt['transactionType']; ?>
                            </option>
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                  </div>

									<div class="form-group row" style="display:none;">
								  <label class="col-md-2 col-form-label">Model Mesin</label>
								  <div class="col-md-10">
									<select name="emodelType"  id="emodelType" class="form-control select2" style="width:100%;">
									<option selected disabled>Model Mesin</option>
                  <?php
                              foreach($modelType as $mt){
                                ?>
                              <option  value="<?php echo $mt['modelType'] ?>">
                              <?php echo $mt['modelType']; ?>
                              </option>
                              <?php
                              }
                              ?>
									</select>
								  </div>
								</div>

                		<div class="form-group row" style="display:none;">
                      <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                    			<div class="col-md-10">
                    			<select name="etransactionType" id="etransactionType" class="form-control select2" style="width:100%;">
                    			  <option selected disabled>Tipe Transaksi</option>
                        			  <?php
                                foreach($transactionType as $tt){
                                ?>
                              <option  value="<?php echo $tt['transactiontypeID'] ?>">
                              <?php echo $tt['transactionType']; ?>
                              </option>
                              <?php
                              }
                              ?>
                    			</select>
                    		  </div>
                		</div>

                    <br>
                  </div>
                    <div class="modal-footer">
                      <button type="submit" id="btn_save" class="btn btn-warning"><i class="icon-pencil5"></i> Edit</button>
                      <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  </form>
              </div>
            </div>
      </div>
    <!-- Master Machine Modal - EDIT - END - -->
  </div>
</div>
</div>
</div>
