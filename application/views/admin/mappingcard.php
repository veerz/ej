<script language="javascript">
    function hanyaAngka(e, decimal) {
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;

    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    } else
    if (decimal && (keychar == ".")) {
        return true;
    } else return false;
    }
</script>

<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Card Bin Number</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Machine Header - END - -->

    <!-- Master Machine Main - START - -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>Mapping Card Bin Number</label></h3>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus-circle"></i> <h9 style="font-family:calibri">Add<h9></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
						  <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
							  <div class="row">
							  <div class="col-sm-12 col-md-6">
							  <div class="dataTables_length" id="mydata_length">

							  </div>
							  </div>
							  <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


							  </div>
							  </div>
							  </div>

						  <div class="row">
							<div class="col-sm-12">
                           <table id="mydata" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>BIN</th>
                                      <th>Bank</th>
                                      <!-- <th>Tipe Card</th> -->
                                      <th>Aksi</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php $no =0 ;foreach($all as $row): $no++?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $row->prefixNo; ?></td>
                                      <td><?php echo $row->bankName; ?></td>
                                      <!-- <td><?php echo $row->cardType; ?></td> -->
                                      <td>
                                          <center>
                                            <div class="tooltip-demo">
                                              <a data-toggle="modal" data-target="#modal-edit<?=$row->idBIN;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a>
                                              <a href="<?php echo base_url('Admin/Mappingcard/hapus/'.$row->idBIN); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Bin <?=$row->bankName;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a>
                                            </div>
                                          </center>
                                      </td>
                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>
                          <!-- /.table-responsive -->
                      </div>
                      <!-- /.card-body -->
                    </div>



                 </div>

                  </div>
                </div>

                </div>
      </div>

  </section>

    <!-- Master Machine Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/Mappingcard/add') ?>" method="post">
        <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Card Bin Number</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">BIN</label>
                            <div class="col-md-10">

                                <input type="text" onkeypress="return hanyaAngka(event, false)" name="prefixNo" id="prefixNo" class="form-control" placeholder="BIN" maxlength="6" required> <!--pattern="^(0|[1-9][0-9]*)$" tittle="Input harus berupa angka"--> </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Bank</label>
                            <div class="col-md-10">
                                <input type="text" name="bankName" id="bankName" class="form-control" placeholder="Nama Bank" maxlength="50" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spesial Character" onblur="this.value=removeSpaces(this.value);" required> </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-md-2 col-form-label">Tipe Card</label>
                            <div class="col-md-10">
                                <input type="text" name="cardType" id="cardType" class="form-control" placeholder="Nama Tipe Card" maxlength="30"> </div>
                        </div> -->
                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                      <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Machine Modal - EDIT - START - -->

    <?php foreach($all as $row): ?>
      <div class="row">
        <div id="modal-edit<?=$row->idBIN;?>" class="modal fade">
          <div class="modal-dialog modal-lg">
            <form action="<?php echo base_url('Admin/Mappingcard/edit'); ?>" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">

                <h4 class="modal-title">Edit Mapping Card Bin Number</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group" style="display:none;">
                    <label>Id Bin</label>
                    <input type="text" readonly value="<?=$row->idBIN;?>" name="idBIN" class="form-control" >
                </div>

                <div class="form-group">
                  <label>BIN</label>
                  <div><input type="text" onkeypress="return hanyaAngka(event, false)" name="prefixNo" autocomplete="off" value="<?=$row->prefixNo;?>" required placeholder="Masukkan BIN" class="form-control" maxlength="6" required></div>
                </div>

                <div class="form-group">
                  <label>Bank</label>
                  <div><input type="text" name="bankName" autocomplete="off" value="<?=$row->bankName;?>" required placeholder="Masukkan Nama Bank" class="form-control" maxlength="50" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required></div>
                </div>

                <!-- <div class="form-group">
                  <label>Tipe Card</label>
                  <div><input type="text" name="cardType" autocomplete="off" value="<?=$row->cardType;?>" placeholder="Masukkan Tipe Card" class="form-control" maxlength="30" ></div>
                </div> -->

                <br>
              </div>
                <div class="modal-footer">
                  <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                  <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
              </form>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

    <!-- Master Machine Modal - EDIT - END - -->

</div>
