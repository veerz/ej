<!-- Content Wrapper. Contains page content -->
<style>

.select2-container--default .select2-selection--multiple .select2-selection__choice {
	color:#000!important;
}
</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"> <a href="#">Home</a> </li>
						<li class="breadcrumb-item active">Access Matrix</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="d-flex justify-content-between">
								<h3 class="card-title"><label>Access Matrix</label></h3>
								<a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
										<h9 style="font-family:calibri">Add<h9></i> </a>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive">
							<table id="mydata" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Role Name</th>
                    <th>Description</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody id="showData">

                  <?php $no =0 ;foreach($all as $row): $no++?>
                  <tr class="odd gradeX">
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->description; ?></td>

                      <td>
                          <center>
                            <div class="tooltip-demo">
                              <a data-toggle="modal" data-target="#Modal_Edit<?=$row->id_role;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                              </a>
							  <a class="btn btn-sm btn-danger btn-delete btn-circle" data-key="<?php echo $row->id_role?>" data-name="<?php echo $row->name?>" title="Delete Data"><i class="fa fa-trash" style="color:white"></i></a>
															<a href="javascript:void(0);" data-key="<?php echo $row->id_role?>" class="btn btn-sm btn-info btn-circle" data-popup="tooltip" data-placement="top" title="Edit Priviledge"><i class="fa fa-search"></i>
                              </a>
                            </div>
                          </center>
                      </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row (main row) -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- MODAL ADD -->
<form action="<?php echo base_url('Admin/Role/save') ?>" method="post">
	<div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- TODO : Ganti ke Dynamic Dropdown setelah tabel GroupDetail Terisi -->
					<div class="form-group row">
						<label class="col-md-2 col-form-label">Role Name</label>
						<div class="col-md-10">
							<input type="text" name="name" id="name" class="form-control" placeholder="Role Name" maxlength="30" pattern="^[a-zA-Z-0-9-]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required> </div>
					</div>
          <div class="form-group row">
						<label class="col-md-2 col-form-label">Description</label>
						<div class="col-md-10">
							<input type="text" name="description" id="description" class="form-control" placeholder="Description" maxlength="50" pattern="^[a-zA-Z- -]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required> </div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
          <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--END MODAL ADD-->
<!-- MODAL EDIT -->
<?php foreach($all as $row): ?>
	<div class="row">
		<div id="Modal_Edit<?=$row->id_role;?>" class="modal fade">
			<div class="modal-dialog modal-lg">
				<form action="<?php echo base_url('Admin/Role/update'); ?>" method="post">
				<div class="modal-content">
					<div class="modal-header bg-primary">

						<h4 class="modal-title">Edit Access Matrix </h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group"  style="display:none;">
							<label>Id Role</label>
						<input display="none" type="text" readonly value="<?=$row->id_role;?>" name="id_role" class="form-control" >
						</div>
						<div class="form-group">
							<label>Role</label>
							<div><input type="text" name="name" autocomplete="off" value="<?=$row->name;?>" required placeholder="Masukkan Role" class="form-control" maxlength="30" pattern="^[a-zA-Z-0-9-]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required></div>
						</div>
						<div class="form-group">
							<label>Description</label>
							<div><input type="text" name="description" autocomplete="off" value="<?=$row->description;?>" placeholder="Masukkan Description" class="form-control" maxlength="50" pattern="^[a-zA-Z- -]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required></div>
						</div>

						<br>
					</div>
						<div class="modal-footer">
							<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
							<button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
					</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<!--END MODAL EDIT-->

<!-- MODAL PRIVILEGE add-->

	<div class="modal fade" id="Modal_Access" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<form  action="<?php echo base_url('Admin/Role/createAccess') ?>" method="post">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Setting Access Matrix</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="form-group"  style="display:none;">
							<label>Id Role</label>
						<input display="none" type="text" readonly id="id_role" name="id_role" class="form-control">
						</div>
				<div class="form-group" style="margin:20px">
					<div class="col-md-12" style="margin:0 auto">
						<input type="hidden">
						<label class="col-md-2 col-form-label">Menu</label>
						<div id="menu">
							<?php
							foreach($menus as $item){
								if($item->type !="SUBMODULE")
								echo "<input type=\"checkbox\" id=\"menulist-{$item->id_menu}\"  class=\"menuall\"  name=\"menu[]\" value=\"{$item->id_menu}\"> {$item->name}<br/>";
							else	
								echo "<input style=\"margin-left:25px\" type=\"checkbox\" id=\"menulist-{$item->id_menu}\"  class=\"menuall\"  name=\"menu[]\" value=\"{$item->id_menu}\"> {$item->name}<br/>";
							}
							?>
						</div>
					</div>
				</div>
  				<div class="modal-footer">
						<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
						<button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
  				</div>
  			</div>
  		</div>
		</form>
  	</div>

