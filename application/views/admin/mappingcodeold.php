<div class="content-wrapper">
    <!-- Master Category Message Header - START - -->

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Code</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Category Message Header - END - -->

    <!-- Master Category Message Main - START - -->

    <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"><label>Mapping Code</label></h3>
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
                                Add</i> </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                      <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                          <div class="row">
                          <div class="col-sm-12 col-md-6">
                          <div class="dataTables_length" id="mydata_length">

                          </div>
                          </div>
                          <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


                          </div>
                          </div>
                          </div>

                      <div class="row">
                        <div class="col-sm-12">
                        <table id="mydata" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Definisi</th>
                                    <th>Status</th>
                                    <th>Aktif</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php $no =0 ;foreach($mapcode as $row): $no++?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $row->code; ?></td>
                                      <td><?php echo $row->Definition; ?></td>
                                      <td><?php echo $row->MappingDefinisi_Definisi; ?></td>
                                      <td>
                                        <?php if ($row->isActive == 1){
                                        echo "Yes";
                                        }else {
                                          echo "No";} ?></td>
                                      <td>
                                          <center>
                                            <div class="tooltip-demo">
                                              <a data-toggle="modal" data-target="#Modal_Edit<?=$row->errorSuccessCodeIDefsD;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a>
                                              <a href="<?php echo base_url('Admin/Mappingcode/delete/'.$row->errorSuccessCodeIDefsD); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Definisi <?=$row->code;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a>
                                            </div>
                                          </center>
                                      </td>
                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>
                    </div>
                </div>



            </div>
  </div>
</div>
</section>

    <!-- Master Category Message Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/Mappingcode/save') ?>" method="post">
        <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Code</label>
                            <div class="col-md-10">
                                <input type="text" name="code" id="code" class="form-control" placeholder="Code" maxlength="50" pattern="^[a-zA-Z]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);"> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Definisi</label>
                            <div class="col-md-10">
                                <input type="text" name="Definition" id="Definition" class="form-control" placeholder="Definisi" maxlength="30"> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Status</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control">
                                  <option selected disabled> -- Status -- </option>
                                  <?php
                                              foreach ($mapdef as $key) {
                                                  echo "<option value='".$key->status."'>".$key->Definisi."</option>";
                                  }
                                  ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Active</label>
                            <div class="col-md-10">
                                <select name="isActive" id="isActive" class="form-control">
                                  <option selected disabled> -- Active -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Category Message Modal - EDIT - START - -->

    <?php foreach($mapcode as $row): ?>
      <div class="row">
        <div id="Modal_Edit<?=$row->errorSuccessCodeIDefsD;?>" class="modal fade">
          <div class="modal-dialog modal-lg">
            <form action="<?php echo base_url('Admin/Mappingcode/update'); ?>" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">

                <h4 class="modal-title">Edit Mapping Code</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group"  style="display:none;">
                  <label class="col-md-2 col-form-label">Id Code</label>
                <input display="none" type="text" readonly value="<?=$row->errorSuccessCodeIDefsD;?>" name="errorSuccessCodeIDefsD" class="form-control" >
                </div>
                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Code</label>
                      <div class="col-md-10">
                          <input type="text" name="code" autocomplete="off" value="<?=$row->code;?>" id="code" class="form-control" placeholder="Code" maxlength="50" pattern="^[a-zA-Z]+$" title="Input harus berupa Alphabet dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);"> </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Definisi</label>
                      <div class="col-md-10">
                          <input type="text" name="Definition" autocomplete="off" value="<?=$row->Definition;?>" id="Definition" class="form-control" placeholder="Definition" maxlength="30"> </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Status</label>
                      <div class="col-md-10">
                          <select name="status" id="status" class="form-control">
                            <?php
                                        foreach ($mapdef as $key) {
                                            echo "<option value='".$key->status."'>".$key->Definisi."</option>";
                            }
                            ?>
                          </select>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Active</label>
                      <div class="col-md-10">
                          <select name="isActive" autocomplete="off" value="<?=$row->isActive;?>" id="isActive" class="form-control">
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                          </select>
                      </div>
                  </div>

              </div>
              <div class="modal-footer">
                  <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
                  <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
              </div>
              </form>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

    <!-- Master Category Message Modal - EDIT - END - -->

    <!-- Master Category Message Modal - DELETE - START - -->

    <form>
        <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Category Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> <strong>Apakah anda yakin ingin menghapus data ini?</strong> </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idCategory_delete" id="idCategory_delete" class="form-control">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - DELETE - END - -->

</div>
