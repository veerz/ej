

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Audit Trail</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

					<!-- Main content -->
					<section class="content">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<form id="myform" name="myform">
									<div class="card">
										<div class="card-header">
											<div class="d-flex justify-content-between">
												<h3 class="card-title"><label>Audit Trail</label></h3>

											</div>
										</div>
											<!-- /.card-header -->

											<div>
												<style>
													th, td {
													padding: 5px;
													}
												</style>

												<table class="card-body table-responsive">

													<tr>
														<td><label>Username</label></td>
														<td>
															<select id="username" name="username" class="form-control" style="width:210px;height:32px">
																<option value="username" selected disabled> -- Search Username -- </option>
																<option value="All User">All User</option>
																	<?php
																		foreach ($user as $key) {
																			echo "<option value='".$key->id_user."'>".$key->username."</option>";
																		}
																	?>
															</select>
														</td>
													</tr>


													<tr>
														<td><label>Input Tanggal</label></td>
														<td>
															 <input type="text" autocomplete="off" id="startdate" name="startdate" class="form-control" style="width:210px;height:29px"/>
														</td>
														<td><label style="width:50px"><center> s/d </center></label></td>
														<td>
														   <input type="text" autocomplete="off" id="enddate" name="enddate" class="form-control" style="width:210px;height:29px"/>
														</td>
													</tr>
												</table>
											</div>
											<!-- /.card-body -->

											<div class="card-body">
												<button type="button" name="Search" class="btn btn-primary search" id="submitsearch">Submit</button>
												<button onclick="resetform()" class="btn btn-danger">Reset</button>
											</div>


											<div class="card-body table-responsive">
												<div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
													<div class="row">
														<div class="col-sm-12 col-md-6">
															<div class="dataTables_length" id="mydata_length">

															</div>
														</div>
														<div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12">
															<table id="mydata" class="table table-bordered table-striped">
																
																	<?php
/*																	$no =0 ;foreach($audittrail as $row): $no++?>
																		<tr class="odd gradeX">
																			<td><?php echo $no; ?></td>
																			<td><?php echo $row->username; ?></td
																			<td><?php echo $row->page; ?></td>
																			<td><?php echo $row->action; ?></td>
																			<td><?php echo $row->detail; ?></td></tr>
																	<?php endforeach; 
																	*/?>
																			
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>

										</div>
										<!-- /.card -->
									</div>
								</div>
								<!-- /.row (main row) -->
							</form>
						</div>
					</div>
				</div>
							<!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>

<script src="<?php echo base_url(); ?>assets/js/select2.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/foundation-datepicker-master/js/foundation-datepicker.js"></script>
	<script>
	$('#startdate').fdatepicker({
		format: 'yyyy-mm-dd',
	}),
	$('#enddate').fdatepicker({
		format: 'yyyy-mm-dd',
	});
	
	$('#menu-id5').addClass("active1");
	$('#menu-id7').addClass("active1");
	//$('#menu-id7').addclass("active1");
	
	</script>
