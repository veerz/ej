<div class="content-wrapper">
    <!-- Master Category Message Header - START - -->

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Jenis Transaksi</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Category Message Header - END - -->

    <!-- Master Category Message Main - START - -->

    <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"><label>Mapping Jenis Transaksi</label></h3>
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
                                <h9 style="font-family:calibri">Add<h9></i> </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                     <div class="card-body table-responsive">
						  <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
							  <div class="row">
							  <div class="col-sm-12 col-md-6">
							  <div class="dataTables_length" id="mydata_length">

							  </div>
							  </div>
							  <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


							  </div>
							  </div>
							  </div>

                      <div class="row">
                        <div class="col-sm-12">
                        <table id="mydata" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe Transaksi</th>
                                    <th>Transaksi Group</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php $no =0 ;foreach($transtypedef as $row): $no++?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $row->transactionType; ?></td>
                                      <td><?php echo $row->transactionGroup; ?></td>
                                      <td>
                                          <center>
                                            <div class="tooltip-demo">
                                              <a class="btn btn-sm btn-warning btn-edit btn-circle" data-key="<?php echo $row->transactionTypeID?>" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a>
                                              <a href="<?php echo base_url('Admin/TransactionTypeDefinition/delete/'.$row->transactionTypeID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data <?=$row->transactionType;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a>
                                              <!-- <a class="btn btn-sm btn-danger btn-delete btn-circle" data-key="<?php echo $row->transactionTypeID?>" data-info="<?php echo $row->transactionType?>"  title="Hapus Data"><i class="fa fa-trash" style="color:white"></i>
                                              </a> -->
                                              <!-- <a href="<?php echo base_url('Admin/TransactionTypeDefinition/delete/'.$row->transactionTypeID); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Definisi <?=$row->transactionType;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a> -->
                                            </div>
                                          </center>
                                      </td>
                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>
                    </div>
                </div>



            </div>
  </div>
</div>
</section>

    <!-- Master Category Message Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/TransactionTypeDefinition/save') ?>" method="post">
        <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Jenis Transaksi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                            <div class="col-md-10">
                                <input type="text" name="transactionType" id="transactionType" class="form-control" placeholder="Tipe Transaksi" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spesial karakter" required> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Transaksi Group</label>
                            <div class="col-md-10">
                            <select name="transactionGroup" id="transactionGroup" class="form-control select2" style="width:100%;">
                              <option selected disabled>Transaksi Group</option>
                              <?php
                              foreach($transactionGroup as $transactionGroup){
                                ?>
                              <option  value="<?php echo $transactionGroup['transactionGroup'] ?>">
                              <?php echo $transactionGroup['transactionGroup']; ?>
                              </option>
                              <?php
                              }
                              ?>
                            </select>
                            <label><input type="checkbox" id="transNewGroupchecked">Add new Group</label>
                        </div>

                        <div class="form-group row" id="transNewGroup" style="display:none;">
                            <div class="col-md-12" style="margin-left:63%">
                                <input type="text" name="newtransactionGroup" id="newtransactionGroup" class="form-control" placeholder="Transaksi Group Baru" maxlength="30"> </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

   

    <!-- Master Category Message Modal - DELETE - START - -->

    <form method="post">
        <div class="modal fade" id="Modal_Delete" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Category Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> <strong>Apakah anda yakin ingin menghapus data ini?</strong> </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idCategory_delete" id="idCategory_delete" class="form-control">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - DELETE - END - -->

</div>

 <!-- Master Category Message Modal - EDIT - START - -->
    
 <div id="Modal_Edit" class="modal fade" aria-labelledby="exampleModalLabel" role="dialog"  aria-hidden="true">
        
 <form  action="<?php echo base_url('Admin/TransactionTypeDefinition/update') ?>" method="post">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">

            <h4 class="modal-title">Edit Mapping Jenis Transaksi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="form-group"  style="display:none;">
              <label class="col-md-2 col-form-label">Id TransTypeDef</label>
            <input readonly type="text" id="etranstypeID"  name="etranstypeID" class="form-control" >
            </div>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label">Tipe Transaksi</label>
                  <div class="col-md-10">
                      <input type="text" name="etransactionType" autocomplete="off" id="etransactionType" class="form-control" placeholder="Transaction Type" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spesial karakter" required> </div>
              </div>

              <div class="form-group row">
                    <label class="col-md-2 col-form-label">Transaksi Group</label>
                    <div class="col-md-10">
                    <select name="etransactionGroup" id="etransactionGroup" class="form-control select2" style="width:100%;">
                        <option selected disabled>Transaksi Group</option>
                        <?php
                        foreach($transactionGroups as $transactionGroups){
                        ?>
                        <option  value="<?php echo $transactionGroups['transactionGroup'] ?>">
                        <?php echo $transactionGroups['transactionGroup']; ?>
                        </option>
                        <?php
                        }
                        ?>
                    </select>
                    <label><input type="checkbox" id="etransNewGroupchecked">Add new Group</label>
                </div>

                <div class="form-group row" id="etransNewGroup" style="display:none;">
                    <div class="col-md-12" style="margin-left:63%">
                        <input type="text" name="enewtransactionGroup" id="enewtransactionGroup" class="form-control" placeholder="Transaksi Group Baru" maxlength="30"> </div>
                </div>       

          </div>
          <div class="modal-footer">
              <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
              <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </div>
          </div>
          </form>
    
    </div>
 

<!-- Master Category Message Modal - EDIT - END - -->
</div>

</div>