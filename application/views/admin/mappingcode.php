<div class="content-wrapper">
    <!-- Master Category Message Header - START - -->

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Mapping Code</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Master Category Message Header - END - -->

    <!-- Master Category Message Main - START - -->

    <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"><label>Mapping Code</label></h3>
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
                                <h9 style="font-family:calibri">Add<h9></i> </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                      <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                          <div class="row">
                          <div class="col-sm-12 col-md-6">
                          <div class="dataTables_length" id="mydata_length">

                          </div>
                          </div>
                          <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


                          </div>
                          </div>
                          </div>

                      <div class="row">
                        <div class="col-sm-12">
						<div class="card-body table-responsive">
                        <table id="mydata" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Definisi</th>
                                    <th>Status</th>
                                    <th>Priority</th>
                                    <th>Aktif</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php $no =0 ;foreach($mapcode as $row): $no++?>
                                  <tr class="odd gradeX">
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $row->Definition; ?></td>
                                      <td><?php echo $row->code; ?></td>
                                      <!-- <td><?php echo $row->MappingDefinisi_Definisi; ?></td> -->
                                      <td>
                                        <?php if ($row->status == 1){
                                        echo "Sukses";
                                        }else {
                                          echo "Gagal";} ?>
                                      </td>
                                      <td><?php echo $row->priority?></td>
                                      <!--ini Priority(new)-->
                                      <td>
                                        <?php if ($row->isActive == 1){
                                        echo "Yes";
                                        }else {
                                          echo "No";} ?>
                                      </td>

                                      <td>
                                          <center>
                                            <div class="tooltip-demo">
                                              <!-- <a data-toggle="modal" data-target="#Modal_Edit<?=$row->errorSuccessCodeIDefsD;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a> -->
                                              <a data-key="<?php echo $row->errorSuccessCodeIDefsD;?>" class="btn btn-sm btn-warning btn-circle" title="Edit Data"><i class="fa fa-edit"></i>
                                              </a>
                                              <a href="<?php echo base_url('Admin/Mappingcode/delete/'.$row->errorSuccessCodeIDefsD); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Definisi <?=$row->Definition;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                              </a>
                                            </div>
                                          </center>
                                      </td>
                                  </tr>
                                  <?php endforeach; ?>
                              </tbody>
                          </table>
						  </div>
                    </div>
                </div>



            </div>
  </div>
</div>
</section>

    <!-- Master Category Message Main - END - -->

    <!-- Master Category Message Modal - ADD - START - -->
    <form action="<?php echo base_url('Admin/Mappingcode/save') ?>" method="post">
        <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Mapping Code</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Code</label>
                            <div class="col-md-10">
                                <input type="text" autocomplete="off" name="code" id="code" class="form-control" placeholder="Code" maxlength="30" required> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Definisi</label>
                            <div class="col-md-10">
                                <input type="text" autocomplete="off" name="Definition" id="Definition" class="form-control" placeholder="Definisi" maxlength="50" pattern="^[a-zA-Z- -]+$" title="Input harus berupa Alphabet! Tidak boleh mengandung angka dan spesial karakter" onblur="this.value=removeSpaces(this.value);" required> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Status</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control" required>
                                  <option selected disabled  value=""> -- Status -- </option>
                                        <option value="1">Sukses</option>
                                        <option value="0">Gagal</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Priority</label>
                            <div class="col-md-10">
                                <input type="number" autocomplete="off" name="priority" id="priority" class="form-control" placeholder="Priority" maxlength="30" required> </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Active</label>
                            <div class="col-md-10">
                                <select name="isActive" id="isActive" class="form-control" required>
                                  <option selected disabled value=""> -- Active -- </option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Master Category Message Modal - ADD - END - -->

    <!-- Master Category Message Modal - EDIT - END - -->
    <div id="Modal_Edit" class="modal fade" aria-labelledby="exampleModalLabel" role="dialog"  aria-hidden="true">
        <form  action="<?php echo base_url('Admin/Mappingcode/update') ?>" method="post">
               <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
                 <div class="modal-header bg-primary">

                <h4 class="modal-title">Edit Mapping Code</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group"  style="display:none;">
                  <label class="col-md-2 col-form-label">errorSuccessCodeIDefsD</label>
                <input display="none" type="text" readonly id="eerrorSuccessCodeIDefsD" name="eerrorSuccessCodeIDefsD" class="form-control" >
                </div>
                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Code</label>
                      <div class="col-md-10">
                          <input type="text" name="ecode" autocomplete="off" id="ecode" class="form-control" placeholder="Code" maxlength="30" required> </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Definisi</label>
                      <div class="col-md-10">
                          <input type="text" name="eDefinition" autocomplete="off" id="eDefinition" class="form-control" placeholder="Definition" maxlength="50" pattern="^[a-zA-Z- -]+$" title="Input harus berupa Alphabet! Tidak boleh mengandung angka dan spesial karakter" onblur="this.value=removeSpaces(this.value);" required> </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Status</label>
                      <div class="col-md-10">
                      <select autocomplete="off" id="estatus" name="estatus" class="form-control" required>
                      <option selected disabled value=""> -- Status -- </option>
                              <option value="1">Sukses</option>
                              <option value="0">Gagal</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Priority</label>
                      <div class="col-md-10">
                          <input type="number" autocomplete="off" id="epriority" name="epriority" class="form-control" placeholder="priority" maxlength="30" required> </div>
                  </div>

                  <div class="form-group row">
                      <label class="col-md-2 col-form-label">Active</label>
                      <div class="col-md-10">
                          <select autocomplete="off" id="eisActive" name="eisActive" class="form-control" required>
                          <option selected disabled value=""> -- Active -- </option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                          </select>
                      </div>
                  </div>

              </div>
              <div class="modal-footer">
                  <button type="submit" value="Click" id="btn_save" class="btn btn-primary">Save</button>
                  <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
              </div>
              </form>
          </div>
        </div>
      </div>

    <!-- Master Category Message Modal - DELETE - START - -->

    <form>
        <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Category Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> <strong>Apakah anda yakin ingin menghapus data ini?</strong> </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idCategory_delete" id="idCategory_delete" class="form-control">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Master Category Message Modal - DELETE - END - -->
</div>
