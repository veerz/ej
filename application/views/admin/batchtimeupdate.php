<style>
td {
  -webkit-touch-callout: none;
   -webkit-user-select: none;
   -khtml-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}

  .table-wrapper {
      overflow-x:scroll;
      overflow-y:visible;
      width: 720px;
      margin-left: 325px;
  }

  thead > tr > th {background: #f0ffff;}
  thead > tr > td {background: #f0ffff;}

  td, th {
    padding: 10px 20px;
    width: 500px;
  }

  td {
    border: 1px solid #e6e6e6;
    cursor: move;
  }

  th {
    border: 1px solid #e6e6e6;
  }

  tbody tr {border: 1px solid #e6e6e6; font-weight: 300;}

  th:first-child {
    width: 336.5px;
    position: absolute;
    margin-top: -1px;
    left: 10px
  }
</style>

<script language="javascript">
    function hanyaAngka(e, decimal) {
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;
   
    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    } else
    if (decimal && (keychar == ".")) {
        return true;
    } else return false;
    }
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"> <a href="#">Home</a> </li>
						<li class="breadcrumb-item active">Batch Time Update</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title"><label>Batch Time Update</label></h3>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-edit"></i> <h9 style="font-family:calibri">Edit<h9></button>
                            </div>
                        </div>
						<!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="col-sm-12">
									<?php foreach($all as $row): ?>
										<label>Refresh Interval : &nbsp;&nbsp;</label>
										<input style="background-color:#E9ECEF;border-radius:5px;border-width:0.5px;width:8%" type="text" readonly id="timeupdate" name="timeupdate" value="<?=$row->timeupdate;?>">
										<label>Minutes&nbsp;&nbsp;</label>
									<?php endforeach; ?>
										<br>
										
									</div>
								</div>
								
								
								
								
							</div>
							<?php foreach($all as $row): ?>
							 <div id="modal-edit" class="modal fade">
								<div class="modal-dialog modal-lg">
									<form action="<?php echo base_url('Admin/BatchTimeUpdate/edit') ?>" method="post">
										<div class="modal-content">
											  <div class="modal-header bg-primary">

												<h4 class="modal-title">Batch Time Update</h4>
												<button type="button" class="close" data-dismiss="modal">&times;</button>
											  </div>
											  <div class="modal-body">
												<div class="form-group" style="display:none;">
													<label>Id Batch Time Update</label>
													<input display="none" type="text" readonly value="<?=$row->id;?>" name="id" class="form-control" >
												</div>
												<div class="form-group">
												  <label>Batch Time Update</label>
												  <div><input type="text" id="timeupdate" name="timeupdate" autocomplete="off" value="<?=$row->timeupdate;?>" placeholder="Masukkan timeupdate" class="form-control" onkeypress="return hanyaAngka(event, false)" maxlength="4"></div>
												</div>

												<br>
											  </div>
												<div class="modal-footer">
												  <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
												  <button type="reset" value="Reload Page" onClick="window.location.reload()" id="reset" onclick="newFunction()" class="btn btn-default" data-dismiss="modal">Cancel</button>
												</div>
										  </div>
									</form>
								</div>
							</div>
							<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="<?=base_url()?>assets/scriptings/jquery/scriptings.jquery.min.js"></script>
<script src="<?=base_url()?>assets/scriptings/jquery-ui/scriptings.jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>-->

<script type="text/javascript">
  $(function () {
    $('.table-wrapper tr').each(function () {
      var tr = $(this),
          h = 0;
      tr.children().each(function () {
        var td = $(this),
            tdh = td.height();
        if (tdh > h) h = tdh;
      });
      tr.css({height: h + 'px'});
    });
  });

</script>
