<div class="content-wrapper">
        <!-- Master Category Message Header - START - -->

        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                         </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                            <li class="breadcrumb-item active">User Management</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Master Category Message Header - END - -->

        <!-- Master Category Message Main - START - -->

        <section class="content">
      		<div class="container-fluid">
      			<!-- Main row -->
      			<div class="row">
      				<div class="col-md-12">
      					<div class="card">
      						<div class="card-header">
      							<div class="d-flex justify-content-between">
      								<h3 class="card-title"><label>User Management</label></h3>
      								<a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
      										<h9 style="font-family:calibri">Add<h9></i> </a>
      							</div>
      						</div>
      						 <!-- /.card-header -->
								<div class="card-body table-responsive">
								  <div id="mydata_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
									  <div class="row">
									  <div class="col-sm-12 col-md-6">
									  <div class="dataTables_length" id="mydata_length">

									  </div>
									  </div>
									  <div class="col-sm-12 col-md-6"><div id="mydata_filter" class="dataTables_filter">


									  </div>
									  </div>
									  </div>

								  <div class="row">
									<div class="col-sm-12">
								   <table id="mydata" class="table table-bordered table-striped">
									  <thead>
      									<tr>
      										<th>No</th>
													<th>Nama</th>
													<th>Username</th>
      										<th>Role</th>
                          <th>Status</th>
      										<th>Action</th>
      									</tr>
      								</thead>
      								<tbody id="showData">
                        <?php $no =0 ;foreach($user as $row): $no++?>
                        <tr class="odd gradeX">
                          <td><?php echo $no; ?></td>
													<td><?php echo $row->name; ?></td>
													<td><?php echo $row->username; ?></td>
                          <td><?php echo $row->role_name; ?></td>
                          <td><?php echo $row->status; ?></td>

                            <td>
                                <center>
                                  <div class="tooltip-demo">
                                    <!-- <a data-toggle="modal" data-target="#Modal_Edit<?=$row->id_user;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                                    </a> -->
																		<a data-key="<?php echo $row->id_user?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" title="Edit Data"><i class="fa fa-edit"></i>
                                    </a>
                                    <a href="<?php echo base_url('Admin/UserManagement/delete/'.$row->id_user); ?>"  onclick="return confirm('Apakah Anda Ingin Menghapus Data Role <?=$row->name;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                                    </a>
                                  </div>
                                </center>
                            </td>
                        </tr>
                        <?php endforeach; ?>
      								</tbody>
      							</table>
      						</div>
      						<!-- /.card-body -->
      					</div>
      					<!-- /.card -->
      				</div>
      			</div>
      			<!-- /.row (main row) -->
      		</div>
      		<!-- /.container-fluid -->
      	</section>

        <!-- Master Category Message Main - END - -->

        <!-- Master Category Message Modal - ADD - START - -->
        <form action="<?php echo base_url('Admin/UserManagement/save') ?>" method="post">
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add user Management</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Nama</label>
                                <div class="col-md-10">
                                    <input type="text"  maxlength="50" autocomplete="off" name="name" id="name" class="form-control" placeholder="Nama" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet/numerik" onblur="this.value=removeSpaces(this.value);" required> </div>
                            </div>

                            <div class="form-group row">
                  						<label class="col-md-2 col-form-label">Username</label>
                  						<div class="col-md-10">
                  							<input type="text" maxlength="20" autocomplete="off" minlength="8" name="username" id="username" class="form-control" placeholder="Username" pattern="^[a-zA-Z-0-9-]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required> </div>
                  					</div>

                            <div class="form-group row">
                              <label class="col-md-2 col-form-label">Password</label>
                              <div class="col-md-10">
																<input type="password" autocomplete="off" pattern="(?=^.{8,30}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$" title="Input harus berupa Alphabet/numerik, Minimum: 8 char, 1 huruf kapital, 1 angka, 1 simbol dan tidak boleh mengandung spasi" minlength="8" name="password" id="password" class="form-control" placeholder="Password" required>
																<input type="checkbox" onclick="myFunction()">Show Password
															</div>
														</div>
							

                            <div class="form-group row">
                  						<label class="col-md-2 col-form-label">Role</label>
                  						<div class="col-md-10">
                  							<select name="id_role" id="id_role" class="form-control select2" style="width:100%;" required>
                                <option selected disabled value=""> -- Role -- </option>
                                  <?php
                                          		foreach ($role as $key) {
                                              		echo "<option value='".$key->id_role."'>".$key->name."</option>";
                  								}
                  								?>
                  							</select>
                  						</div>
                  					</div>

                            <div class="form-group row">
                  						<label class="col-md-2 col-form-label">Status</label>
                  						<div class="col-md-10">
                  							<select name="status" id="status" class="form-control" required>
																<option selected disabled value=""> -- Status -- </option>
                  								<option value="Aktif">Aktif</option>
                  								<option value="Tidak Aktif">Tidak Aktif</option>
                  							</select>
                  						</div>
                  					</div>
                        </div>

                        <div class="modal-footer">
                					<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                          <button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Close</button>
                				</div>
                    </div>
                </div>
            </div>
        </form>

        <!-- Master Category Message Modal - ADD - END - -->

        <!-- Master Category Message Modal - EDIT - START - -->

        <div id="Modal_Edit" class="modal fade" aria-labelledby="exampleModalLabel" role="dialog"  aria-hidden="true">
        
 				<form  action="<?php echo base_url('Admin/UserManagement/update') ?>" method="post">
						<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header bg-primary">

        						<h4 class="modal-title">Edit User Management</h4>
        						<button type="button" class="close" data-dismiss="modal">&times;</button>
        					</div>
        					<div class="modal-body">
        						<div class="form-group"  style="display:none;">
        							<label class="col-md-2 col-form-label">Id User</label>
        						<input display="none" type="text" readonly name="eid_user" id="eid_user" class="form-control" >
        						</div>

        						<div class="form-group row">
        							<label class="col-md-2 col-form-label">Name</label>
        							<div class="col-md-10">
                        <input type="text" maxlength="50" id="ename" name="ename" autocomplete="off" required placeholder="Masukkan Name" class="form-control" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet/numerik" onblur="this.value=removeSpaces(this.value);" required></div>
        						</div>

        						<div class="form-group row">
        							<label class="col-md-2 col-form-label">Username</label>
        							<div class="col-md-10">
                        <input type="text" maxlength="20" id="eusername" name="eusername" autocomplete="off" required placeholder="Masukkan Username" class="form-control" pattern="^[a-zA-Z-0-9-]+$" title="Input harus berupa Alphabet/numerik dan tidak boleh mengandung spasi" onblur="this.value=removeSpaces(this.value);" required></div>
        						</div>

                    <div class="form-group row">
        							<label class="col-md-2 col-form-label">Password</label>
        							<div class="col-md-6">
												<input type="password" autocomplete="off"  minlength="8" name="epassword" id="epassword" required class="form-control" placeholder="Change Password" readonly>
											</div>
											<div class="col-md-4">
											<input type="checkbox" id="passcek" name="passcek" style="padding-left: 5px"> Change Password
											
											</div>
										</div>

										<div id="showPass" style="display:none;margin-left:17%">
											<input type="checkbox" onclick="myFunctionEdit()">Show Password
										</div>
										
										
										
                    <div class="form-group row">
                      <label class="col-md-2 col-form-label">Role</label>
                      <div class="col-md-10">
                        <select name="eid_role" id="eid_role" class="form-control" required>
												<option selected disabled value=""> -- Role -- </option>
													<?php
														foreach ($role as $key) {
															echo "<option value='".$key->id_role."'>".$key->name."</option>";
														}
													?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
												<label class="col-md-2 col-form-label">Status</label>
												<div class="col-md-10">
													<select name="estatus" id="estatus" class="form-control" required>
													<option selected disabled value=""> -- Status -- </option>
														<option value="Aktif">Aktif</option>
														<option value="Tidak Aktif">Tidak Aktif</option>
													</select>
												</div>
											</div>

        						<br>
        					</div>
        						<div class="modal-footer">
        							<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
        							<button type="reset" value="Reload Page" onClick="window.location.reload()" class="btn btn-default" data-dismiss="modal">Cancel</button>
        						</div>
        					</div>
        					</form>
        			</div>
        		</div>
        	</div>

        <!-- Master Category Message Modal - EDIT - END - -->

        <!-- Master Category Message Modal - DELETE - START - -->

        <form>
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Category Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body"> <strong>Apakah anda yakin ingin menghapus data ini?</strong> </div>
                        <div class="modal-footer">
                            <input type="hidden" name="idCategory_delete" id="idCategory_delete" class="form-control">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- Master Category Message Modal - DELETE - END - -->

    </div>
	
	<script>
		function myFunction() {
			var x = document.getElementById("password");
			if (x.type === "password") {
			x.type = "text";
			} else {
				x.type = "password";
			}
		}
		
		function myFunctionEdit() {
			var y = document.getElementById("epassword");
			if (y.type === "password") {
			y.type = "text";
			} else {
				y.type = "password";
			}
		}
	</script>
