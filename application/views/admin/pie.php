<!doctype html>
<html lang="en">
 <head>
  <meta charset="UTF-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <title>Pie Chart with Outside Labels</title>
  <!--<link href="https://playground.anychart.com/gallery/Pie_and_Donut_Charts/Pie_Chart_with_Outside_Labels/iframe" rel="canonical">-->
  <meta content="Circle Chart,Pie Chart" name="keywords">
  <meta content="AnyChart - JavaScript Charts designed to be embedded and integrated" name="description">
  <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/piechart/css/anychart-ui.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/piechart/css/anychart-font.min.css">
  <!--
  <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css?hcode=be5162d915534272a57d0bb781d27f2b" rel="stylesheet" type="text/css">
  <link href="https://cdn.anychart.com/releases/v8/fonts/css/anychart-font.min.css?hcode=be5162d915534272a57d0bb781d27f2b" rel="stylesheet" type="text/css">
  -->
  <style>html, body, #container {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}</style>
 </head>
 <body>
  <div id="container"></div>
  
  <script src="<?php echo base_url(); ?>assets/piechart/js/anychart-base.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/piechart/js/anychart-ui.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/piechart/js/anychart-exports.min.js"></script>
  
  <!--
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js?hcode=be5162d915534272a57d0bb781d27f2b"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js?hcode=be5162d915534272a57d0bb781d27f2b"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js?hcode=be5162d915534272a57d0bb781d27f2b"></script>
  -->
  <script type="text/javascript">anychart.onDocumentReady(function () {
	  
    // create pie chart with passed data
    var chart = anychart.pie([
        ['Apples', 1460000],
        ['Pears', 1470000],
        ['Bananas', 1480000],
        ['Grapes', 1486621],
        ['Oranges', 1480000]
    ]);

    // set chart title text settings
    chart.title('Fruits imported in 2015 (in kg)');
    // set chart labels position to outside
    chart.labels().position('outside');
    // set legend title settings
    chart.legend().title()
        .enabled(true)
        .text('Retail channels')
        .padding([0, 0, 10, 0]);

    // set legend position and items layout
    chart.legend()
        .position('center-bottom')
        .itemsLayout('horizontal')
        .align('center');

    // set container id for the chart
    chart.container('container');
    // initiate chart drawing
    chart.draw();
});</script>
 </body>
</html>