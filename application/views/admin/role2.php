<!-- Content Wrapper. Contains page content -->
<style>
.submodulecol,.submodulecol2,submodulcol3{
	display:none;
} 

</style>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Data Role</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"> <a href="#">Home</a> </li>
						<li class="breadcrumb-item active">Role</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="d-flex justify-content-between">
								<h3 class="card-title">Data Role</h3>
								<a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
										Add</i> </a>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive">
							<table id="mydata" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Role Name</th>
                    <th>Description</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody id="showData">

                  <?php $no =0 ;foreach($all as $row): $no++?>
                  <tr class="odd gradeX">
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->description; ?></td>

                      <td>
                          <center>
                            <div class="tooltip-demo">
                              <a data-toggle="modal" data-target="#Modal_Edit<?=$row->id_role;?>" class="btn btn-sm btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data"><i class="fa fa-edit"></i>
                              </a>
                              <a href="<?php echo base_url('Admin/Role/delete/'.$row->id_role); ?>"  onclick="return confirm('Apakah Anda Ingin Menghapus Data Role <?=$row->name;?> ?');" class="btn btn-sm btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i>
                              </a>
															<a data-toggle="modal" data-target="#Modal_Access" class="btn btn-sm btn-info btn-circle" data-popup="tooltip" data-placement="top" title="Edit Priviledge"><i class="fa fa-search"></i>
                              </a>
                            </div>
                          </center>
                      </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row (main row) -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- MODAL ADD -->
<form action="<?php echo base_url('Admin/Role/save') ?>" method="post">
	<div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<!-- TODO : Ganti ke Dynamic Dropdown setelah tabel GroupDetail Terisi -->
					<div class="form-group row">
						<label class="col-md-2 col-form-label">Role Name</label>
						<div class="col-md-10">
							<input type="text" name="name" id="name" class="form-control" placeholder="Role Name"> </div>
					</div>
          <div class="form-group row">
						<label class="col-md-2 col-form-label">Description</label>
						<div class="col-md-10">
							<input type="text" name="description" id="description" class="form-control" placeholder="Description"> </div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--END MODAL ADD-->
<!-- MODAL EDIT -->
<?php foreach($all as $row): ?>
	<div class="row">
		<div id="Modal_Edit<?=$row->id_role;?>" class="modal fade">
			<div class="modal-dialog modal-lg">
				<form action="<?php echo base_url('Admin/Role/update'); ?>" method="post">
				<div class="modal-content">
					<div class="modal-header bg-primary">

						<h4 class="modal-title">Edit Role</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group"  style="display:none;">
							<label>Id Role</label>
						<input display="none" type="text" readonly value="<?=$row->id_role;?>" name="id_role" class="form-control" >
						</div>
						<div class="form-group">
							<label>Role</label>
							<div><input type="text" name="name" autocomplete="off" value="<?=$row->name;?>" required placeholder="Masukkan Role" class="form-control" ></div>
						</div>
						<div class="form-group">
							<label>Description</label>
							<div><input type="text" name="description" autocomplete="off" value="<?=$row->description;?>" required placeholder="Masukkan Description" class="form-control" ></div>
						</div>

						<br>
					</div>
						<div class="modal-footer">
							<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
					</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<!--END MODAL EDIT-->

<!--MODAL DELETE-->
<form>
	<div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Hapus DataRole</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <strong>Apakah anda yakin ingin menghapus data ini?</strong> </div>
				<div class="modal-footer">
					<input type="hidden" name="groupID_delete" id="groupID_delete" class="form-control">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
					<button type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--END MODAL DELETE-->

<!-- MODAL PRIVILEGE-->
<form  action="<?php echo base_url('Admin/PriviledgeRole/save') ?>" method="post">>
	<div class="modal fade" id="Modal_Access" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New priviledgeRole</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
              <table class="table table-bordered table-striped table-responsive" data-table>
                  <thead>
                      <tr>
                          <th>Module</th>
						  <th class="submodulecol" style="display:none;">SubModule</th>
                          <th>Access</th>
                      </tr>
                  </thead>

                <tbody>
      							<tr> <!--  id="admin" class="expands" onclick="toggle('mp','mc','mep,'mkl','um','raa','btu')" -->
      								<td>Admin</td>
      								<td class="submodulecol" ></td>
											<td><input type="checkbox" id="moduleadmin" name="moduleadmin" onclick="myFunction('.submodulecol','moduleadmin')" data-toggle="collapse" data-target="#openadmin"></td>
      							</tr>


    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Mapping Definisi</td>
    									<td><input type="checkbox"  class="checkbox-pv" id="mapdef" name="mapdef" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>


    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Mapping Code</td>
    									<td><input type="checkbox" class="checkbox-pv" id="mapcode" name="mapcode" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Mapping Error Problem</td>
    									<td><input type="checkbox" class="checkbox-pv" id="maperror" name="maperror" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Mapping Keyword List</td>
    									<td><input type="checkbox" class="checkbox-pv" id="mapkey" name="mapkey" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >User Management</td>
    									<td><input type="checkbox" class="checkbox-pv" id="user" name="user" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Access Matrix</td>
    									<td><input type="checkbox" class="checkbox-pv" id="access" name="access" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

    								<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Batch Time Update</td>
    									<td><input type="checkbox" class="checkbox-pv" id="batchtimeup" name="batchtimeup" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

										<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Mapping Card Bin Number</td>
    									<td><input type="checkbox" class="checkbox-pv" id="mapcard" name="mapcard" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

										<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >List OP Code</td>
    									<td><input type="checkbox" class="checkbox-pv" id="opcode" name="opcode" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

										<tr id="openadmin" class="collapse">
											<td></td>
    									<td class="submodulecol" >Audit Trail</td>
    									<td><input type="checkbox" class="checkbox-pv" id="trail" name="trail" data-menu="{{ $menu->id }}" onclick="clicksave({{ $menu->id }})" value="{{ $menu->id }}"></td>
    								</tr>

										<tr> <!--  id="admin" class="expands" onclick="toggle('mp','mc','mep,'mkl','um','raa','btu')" -->
      								<td>Status Mesin</td>
      								<td></td>
											<td><input type="checkbox" id="machinestatus" name="machinestatus" onclick="myFunction('.submodulecol2','machinestatus')" data-toggle="collapse" data-target="#openmachine"></td>
      							</tr>


    								<tr id="openmachine" class="collapse">
											<td class="submodulecol2"></td>
    									<td class="submodulecol2">Status Mesin H+0</td>
    									<td><input type="checkbox"  class="checkbox-pv" id="stat0" name="stat0" data-menu="{{ $menu->id }}" ></td>
    								</tr>


    								<tr id="openmachine" class="collapse">
											<td class="submodulecol2"></td>
    									<td class="submodulecol2">Status Mesin H+1</td>
    									<td><input type="checkbox" class="checkbox-pv" id="stat1" name="stat1" data-menu="{{ $menu->id }}"></td>
    								</tr>

										<tr>
                        <td>Dashboard</td>
												<td class="submodulcol3"></td>
                        <td><input type="checkbox" class="checkbox-pv" id="menu" name="menu" data-menu="{{ $menu->id }}"  ></td>
                    </tr>

										<tr>
                        <td>Status Transaksi</td>
												<td class="submodulcol3"></td>
                        <td><input type="checkbox" class="checkbox-pv" id="transaction" name="transaction" data-menu="{{ $menu->id }}"></td>
                    </tr>

										<tr>
                        <td>EJ Browser</td>
												<td class="submodulcol3"></td>
                        <td><input type="checkbox" class="checkbox-pv" id="browser" name="browser" data-menu="{{ $menu->id }}"  ></td>
                    </tr>

										<tr>
                        <td>Kartu Tertelan</td>
												<td class="submodulcol3"></td>
                        <td><input type="checkbox" class="checkbox-pv" id="menu" name="menu" data-menu="{{ $menu->id }}"></td>
                    </tr>

										<tr>
                        <td>Detail Search</td>
												<td class="submodulcol3"></td>
                        <td><input type="checkbox" class="checkbox-pv" id="menu" name="menu" data-menu="{{ $menu->id }}"></td>
                    </tr>
                </tbody>
              </table>
				    </div>
  				<div class="modal-footer">
						<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  				</div>
  			</div>
  		</div>
  	</div>
</form>

