<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
				<h1 class="m-0 text-dark">DETAIL SEARCH</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Detail Search</li>
                    </ol>
                </div>
            </div>
        <!-- /.container-fluid -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
						<div class="panel-body">
							<form id="myform" name="myform">
							<!--<form class="form-group" method="POST" action="<?php echo base_url();?>detail/Show">-->
								<div class="row">
									<div class="col-md-12">
										<div class="card">
											<div class="card-header">
												<div class="d-flex justify-content-between">
													<h3 class="card-title"><label>Detail Search</label></h3>
												</div>
											</div>
											<!-- /.card-header -->
											<div class="row">
												<style>
													th, td {
													padding: 5px;
													}
													.dt-button{
														color: #ffffff!important;
														background-color: #0062cc!important;
														border-color: #005cbf!important;
														background-image:none!important;
													}
												</style>
												<div class="col-sm-7">
													<table class="card-body table-responsive">
														<tr>
															<td><label>ID Mesin</label></td>
															<td>
																<select id="idmachine" name="idmachine" required class="form-control select2idmachine" style="width:185px;height:29px">
																	<option value="" selected disabled>ID Mesin</option>
																	<?php
																	foreach($terminalID as $terminalID){
																		?>
																	<option  value="<?php echo $terminalID['terminalID'] ?>">
																	<?php echo $terminalID['terminalID']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
															</td>
														</tr>
														<tr>
															<td><label>Input Tanggal</label></td>
															<td>
															<input autocomplete="off"  required type="text" id="startdate" name="startdate" class="form-control" style="width:185px;height:29px">
															</td>
															<td><label> s/d </label></td>
															<td><input autocomplete="off" type="text" id="enddate" name="enddate" class="form-control" style="width:185px;height:29px"></td>
														</tr>
														<tr>
															<td><label>Nama Pengelola</label></td>
																<td><input autocomplete="off"  required type="text" id="namapengelola" name="namapengelola" class="form-control" style="width:185px;height:29px" readonly></td>
														</tr>
													</table>
												</div>
												<div class="col-sm-5">
													<table class="card-body table-responsive">
														<tr>
															<td><label>Deployment Year</label></td>
															<td><input type="text" id="deploymentyear" name="deploymentyear" class="form-control" style="width:185px;height:29px" readonly></td>
														</tr>
														<tr>
															<td><label>Lokasi ATM</label></td>
															<td><input type="text" id="lokasiatm" name="lokasiatm" class="form-control" style="width:185px;height:29px" readonly></td>
														</tr>
														<tr>
															<td><label>Region</label></td>
															<td><input type="text" id="region" name="region" class="form-control" style="width:185px;height:29px" readonly></td>
														</tr>
														<tr>
															<td><label>Area</label></td>
															<td><input type="text" id="area" name="area" class="form-control" style="width:185px;height:29px" readonly></td>
														</tr>
													</table>
												</div>
											</div>
												<div class="card-body">
													<button type="button" class="btn btn-primary" id="submitsearch">Submit</button>
													<button  id="submitreset" class="btn btn-danger">Reset</button>
												</div>
										</div>
										<!-- /.card -->
									</div>
								</div>
							<!-- /.row (main row) -->
							</form>
						</div>
			<section class="content">
				<div class="container-fluid">
					<div class="card">
						<div class="row">
							<div class="card-body table-responsive">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div id="mytable">
										<table id="detailsearchtable"class="table table-bordered table-striped" style="font-size:12px">

										</table>
									</div>
								</div>
							</div>
								<!-- /.container-fluid -->
						</div>
					</div>
						<!--div class="card-body">
							<button  class="btn btn-primary" data-toggle="tab" value="Create PDF" id="btPrint" onclick="createPDF()">Print PDF</button>
							<button type="button" class="btn btn-primary" data-toggle="tab" id="excel" >Print Excel</button>							
						</div-->
					</div>
				</div>
			</section>
		</div>
	</section>
	<!-- end of section -->
</div>
</div>
</div>


<!-- script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script -->
<script src="<?php echo base_url(); ?>assets/plugins/foundation-datepicker-master/js/foundation-datepicker.js"></script>
	<script>
	$('#startdate').fdatepicker({
		format: 'yyyy-mm-dd'
	}),
	$('#enddate').fdatepicker({
		format: 'yyyy-mm-dd'
	})
	</script>
	