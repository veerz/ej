<!-- Content Wrapper. Contains page content -->
<?php
/*========================================================*/
$StatusTrxMappings = $StatusTrxMapping;
$StatusTrxMapping =json_encode($StatusTrxMapping);
/*========================================================*/
$StatusTrxBads = $StatusTrxBad;
$StatusTrxBad =json_encode($StatusTrxBad);
/*========================================================*/
$StatusTrxGoods = $StatusTrxGood;
$StatusTrxGood =json_encode($StatusTrxGood);

/*========================================================*/
$StatusTrxByJeniss = $StatusTrxByJenis;
$StatusTrxByJenis =json_encode($StatusTrxByJenis);

$Replenishments = $Replenishment;
$Replenishment =json_encode($Replenishment);
for($i=0;$i<count($Replenishments);$i++){
	if($Replenishments[$i]['JenisTrx'] == "Pengisian")
	$totalpengisian  = $Replenishments[0]['Total'];
	else
	$totalpengosongan = $Replenishments[0]['Total'];
}


$regiontrxs = $regiontrx;
$regiontrx =json_encode($regiontrx);

for($i=0;$i<count($regiontrxs);$i++){
	if($regiontrxs[$i]['Status'] == "Gagal")
	$totalTrxGagal  = $regiontrxs[$i]['Jumlah'];
	else
	$totalTrxSukses = $regiontrxs[$i]['Jumlah'];
}
?>

<script type="text/javascript">
var StatusTrxMapping = <?php echo $StatusTrxMapping; ?>;
var StatusTrxBad = <?php echo $StatusTrxBad; ?>;
var StatusTrxGood = <?php echo $StatusTrxGood; ?>;
var StatusTrxByJenis = <?php echo $StatusTrxByJenis; ?>;
var regiontrx = <?php echo $regiontrx; ?>;
var Replenishment = <?php echo $Replenishment; ?>;
var cekregion =  '<?php echo $cekregion; ?>';
var cekarea =  '<?php echo $cekarea; ?>';
var cekmanageby =  '<?php echo $cekmanageby; ?>';
</script>

<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
								<h1 class="m-0 text-dark">Status Transaksi</h1>
        				</div>
								<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Status Transaksi</li>
                    </ol>
                </div>
        <!-- /.container-fluid -->
						</div>
				</div>
		</div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
							<form method="post">
							<!-- <form action="<?php echo base_url('StatTrans/getData') ?>" method="post"> -->
                <div class="card-header">
									<div>
                      <h6><b>Cari berdasarkan : </b></h6>
                  </div>
                    <div class="d-flex justify-content-between">
                      <table class="card-body table-responsive">
												<tr>
                        <td>
                          <select id="definisi" name="definisi" class="form-control select2" style="width:240px">
                            <option selected disabled>Definisi Transaksi</option>
                            <?php
														foreach($definisi as $definisi){
															?>
														<option  value="<?php echo $definisi['status'] ?>">
														<?php echo $definisi['Definisi']; ?>
														</option>
														<?php
														}
														?>
													</select>
                        </td>
                        <td>
                          <select id="region" name="region" class="form-control select2" style="width:240px">
                           <option selected disabled>Wilayah</option>
																<?php
								    							foreach($region as $region){
								    								?>
								    							<option  value="<?php echo $region['kanwil'] ?>">
								    							<?php echo $region['kanwil']; ?>
								    							</option>
								    							<?php
								    							}
								    							?>
								    						</select>
												</td>
                        <td>
                          <select id="area" name="area" class="form-control select2" style="width:240px">
															<option selected disabled>Area</option>
																<?php
								    							foreach($area as $area){
								    								?>
								    							<option  value="<?php echo $area['area'] ?>">
								    							<?php echo $area['area']; ?>
								    							</option>
								    							<?php
								    							}
								    							?>
								    						</select>
															</td>
													<td>
													  <select id="managerType" name="managerType" class="form-control select2" style="width:240px">
														  <option selected disabled>Jenis Pengelola</option>
															<option value="Vendor-">Vendor</option>
															<option value="Branch-">Branch</option>
														</select>
													</td>
												</tr>
											<tr>
                        <td>
                          <select id="managedBy" name="managedBy" class="form-control select2" style="width:240px">
	                        <option selected disabled>Nama Pengelola</option>
														<?php
															foreach($managedBy as $managedBy){
																?>
															<option  value="<?php echo $managedBy['managedBy'] ?>">
															<?php echo $managedBy['managedBy']; ?>
															</option>
															<?php
															}
															?>
													  </select>
												</td>
                        <td>
                          <select id="terminalType" name="terminalType" class="form-control select2" style="width:240px">
                            <option selected disabled>Jenis Mesin</option>
														<?php
						    							foreach($terminalType as $terminalType){
						    								?>
						    							<option  value="<?php echo $terminalType['terminalType'] ?>">
						    							<?php echo $terminalType['terminalType']; ?>
						    							</option>
						    							<?php
						    							}
						    							?>
						    						</select>
													</td>
                        <td>
                          <select id="merk" name="merk" class="form-control select2" style="width:240px">
													<option selected disabled>Merk</option>
													<?php
					    							foreach($merk as $merk){
					    								?>
					    							<option  value="<?php echo $merk['merk'] ?>">
					    							<?php echo $merk['merk']; ?>
					    							</option>
					    							<?php
					    							}
					    							?>
					    						</select>
												</td>
											</tr>
											<tr>
											</tr>
                      </table>
                    </div>
										<button type="button" class="btn btn-primary btn-submit" id="filter" style="margin-left:2%">Submit</button>
										<span style="font-size:12px;color:green"><?php echo $params; ?></span>
                	</div>
								</form>


            <div class="card-body table-responsive">
            <!-- Main row -->
            <div class="row">
    					<div class="col-sm-12">
                <h5> <center><label>Transaksi Sukses/Gagal Berdasarkan Wilayah</label></center> </h5>
    						<canvas  id="bar3" width="60" height="30"></canvas>
    						<div class="card-body table-responsive">
    															<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
    																<tr>
    																	<td><label>Status EJ</label></td>
    																	<?php
    																	for($i=0;$i<count($regiontrxs);$i++){
    																		if($i%2==0){
    																	?>
    																	<td><?php echo $regiontrxs[$i]['Region']; }}?></td>
    																</tr>
    																<tr>
    																	<td>Gagal</td>
    																	<?php
    																	for($i=0;$i<count($regiontrxs);$i++){
    																		if($regiontrxs[$i]['Status'] == "Gagal"){
    																	?>
    																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
    																</tr>
    																<tr>
    																	<td>Sukses</td>
    																	<?php
    																	for($i=0;$i<count($regiontrxs);$i++){
    																		if($regiontrxs[$i]['Status'] == "Sukes"){
    																	?>
    																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
    																</tr>
    															</table>
    													</div>
    					</div>
            </div>
              <div class="row">
      					   <div class="col-sm-4">
  					         <div class="card-body table-responsive">
    		                <table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
            								<tr>
            									<td><label>Total RTX Gagal</label></td>
            									<td><?php echo number_format($totalTrxGagal,0,',','.') ?></td>
            								</tr>
            								<tr>
            									<td><label>Total TRX Sukses</label></td>
            									<td><?php echo number_format($totalTrxSukses,0,',','.') ?></td>
            								</tr>
            								<tr>
            									<td><label>Total EJ Salah</label></td>
            									<td>-</td>
            								</tr>
            								<tr>
            									<td><label>Total Pengisian ATM</label></td>
            									<td><?php echo number_format($totalpengisian,0,',','.') ?></td>
            								</tr>
            								<tr>
            									<td><label>Total Pengosongan CDM</label></td>
															<td>540</td>
            									<!-- <td><?php echo number_format($totalpengosongan,0,',','.') ?></td> -->
            								</tr>
            								<tr>
            									<td><label>Total Card Ratained</label></td>
            									<td>2.500</td>
            								</tr>
    			              </table>
  						       </div>
  					      </div>
                  <div class="col-sm-4">
                    <h5> <center><label>Status Transaksi Gagal</label></center> </h5>
        						<canvas  id="horizontalBar" width="60" height="50"></canvas>
                  </div>
				  <div class="col-sm-4">
					<h5> <center><label>Pengisian / Pengosongan Mesin</label></center> </h5>
					<canvas  id="bar7" width="60" height="50"></canvas>
				  </div>
					</div>
						<div class="row">
							<div class="col-sm-4">
								<canvas  id="horbar4" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
								<canvas  id="horbar5" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
								<h5><center><label>Jenis Transaksi</label></center> </h5>
								<canvas  id="pie5" width="20" height="15"></canvas>
							</div>
						</div>
					</div>

    			</div>
    		</div>
    </section>
</div>
