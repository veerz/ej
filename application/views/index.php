<style>
td {
  -webkit-touch-callout: none;
   -webkit-user-select: none;
   -khtml-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
}

  .table-wrapper {
      overflow-x:scroll;
      overflow-y:visible;
  }

  thead > tr > th {background: #f0ffff;}
  thead > tr > td {background: #f0ffff;}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">EJ Browser</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"> <a href="#">Home</a> </li>
						<li class="breadcrumb-item active">EJBrowser</li>
					</ol>
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
		<div class="panel panel-default" style="border-radius: 0; background: rgba(200, 200, 200, .10); border: 2px dashed rgba(225, 225, 225, 1.0); margin-bottom: 30px;">
      <div class="panel-body">
			<form class="form-group" method="POST" action="<?php echo base_url();?>cBukuBesar/BukuBesarPerMonth">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<div class="d-flex justify-content-between">
								<h3 class="card-title">Data EJ Browser</h3>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive">
							<label for="terminal">Terminals: &nbsp;&nbsp;</label>
							<select>
								<option selected disabled>Select Terminals</option>
								<option value='1'>All Terminals</option>
								<option value='2'>Spesific Terminal</option>
							</select>
						</div>
						
						<div class="card-body" style="margin-top: -30px;">
							<label>Keyword EJ: &nbsp;&nbsp;</label>
							<input type="text" required/>
						</div>
						
						<div class="card-body" style="margin-top: -30px;">
							<label>Range Time &nbsp;&nbsp;</label>
							<label>Start Date: &nbsp;&nbsp;</label>
							<input type="date" id="start">
							<label>&nbsp;&nbsp;End Date: &nbsp;&nbsp;</label>
							<input type="date" id="end">
						</div>
						
						<div class="card-body" style="margin-top: -30px;">
							<label for="sequence">Sequence: &nbsp;&nbsp;</label>
								<select id="sequence" name="sequence" >
									<option selected disabled>Select Sequence</option>
									<option value="1">Single</option>
									<option value="2">Multiple</option>
								</select>
						</div>
						
						<div class="card-body" style="margin-top: -30px;">
							<label>Number of Records: &nbsp;&nbsp;</label>
							<input type="text">
						</div>
						<!-- /.card-body -->
						
						<div class="card-body">
						<button type="submit" class="btn btn-primary" id="search">Search</button>
						</div>
						
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row (main row) -->
			</form>
			</div>
			</div>
		</div>
		<!-- /.container-fluid -->
		
		<?PHP Print $this->session->flashdata('ReturnCode');?>

    <div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="d-flex justify-content-between">
						<h3 class="card-title">Data EJBrowser</h3>
							<!--a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"> <i class="fa fa-plus">
							Tambah userDetail</i> </a-->
					</div>
				</div>
				<!-- /.card-header -->
			<div class="card-body table-responsive">
			<table id="mydata" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">Id ATM</th>
						<th class="text-center">No.Sequence</th>
						<th class="text-center">Time Stamp</th>
						<th class="text-center">Tanggal</th>
						<th class="text-center">No.Kartu</th>
						<th class="text-center">Lokasi</th>
						<th class="text-center">Jenis Transaksi</th>
						<th class="text-center">Nominal</th>
						<th class="text-center">Status</th>
						<th class="text-center">Remark</th>
					</tr>
				</thead>
				<tbody id="showdata"> </tbody>
			</table>
			</div>
						<!-- /.card-body -->
			</div>
					<!-- /.card -->
		</div>
	</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="<?=base_url()?>assets/scriptings/jquery/scriptings.jquery.min.js"></script>
<script src="<?=base_url()?>assets/scriptings/jquery-ui/scriptings.jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>

<script type="text/javascript">
  $(function () {
    $('.table-wrapper tr').each(function () {
      var tr = $(this),
          h = 0;
      tr.children().each(function () {
        var td = $(this),
            tdh = td.height();
        if (tdh > h) h = tdh;
      });
      tr.css({height: h + 'px'});
    });
  });
</script>