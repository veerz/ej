<!-- Content Wrapper. Contains page content -->
<!-- ini yg lagi di coba -->
<?php
if($resultdiview == "ada"){
	$totalupdatePie = $piedataupdate[0]['RMMAgentAktif(EJUpdate)']+ $piedataupdate[0]['RMMAgentAktif(EJNotUpdate)'] + $piedataupdate[0]['RMMTidakAda'] + $piedataupdate[0]['ATMTidakAktif'] + $piedataupdate[0]['ProblemJaringan'];
//$totalupdatePie = $totalupdatePie ? 0 : 1;
	$temphasil = 0;
	if($totalupdatePie == 0) {
		$temphasil = 0;
	}else{
		$temphasil = 1;
		$updatePie = $piedataupdate[0]['RMMAgentAktif(EJUpdate)'];
		$nonupdatePie = $piedataupdate[0]['RMMAgentAktif(EJNotUpdate)'];
		$rmmtdkadaPie = $piedataupdate[0]['RMMTidakAda'];
		$atmtdkaktifPie = $piedataupdate[0]['ATMTidakAktif'];
		$probjaringanPie = $piedataupdate[0]['ProblemJaringan'];

		$updatePiepercentage = $updatePie / $totalupdatePie * 100;
		$nonupdatePiepercentage = $nonupdatePie / $totalupdatePie * 100;
		$rmmtdkadaPiepercentage = $rmmtdkadaPie / $totalupdatePie * 100;
		$atmtdkaktifPiepercentage = $atmtdkaktifPie / $totalupdatePie * 100;
		$probjaringanPiepercentage = $probjaringanPie / $totalupdatePie * 100;
		$nilai = $totalupdatePie.",".$updatePie.",".$nonupdatePie.",".$rmmtdkadaPie.",".$atmtdkaktifPie.",".$probjaringanPie;

		/*========================================================*/
		$bardataMerks = $bardataMerk;
		$bardataMerk=json_encode($bardataMerk);
		/*========================================================*/
		$bardataregions = $bardataregion;
		$bardataregion =json_encode($bardataregion);
		/*========================================================*/
		}
	}
else{
	$temphasil = 0;
}
?>

<script type="text/javascript">
var resultdiview = '<?php echo $resultdiview?>';
if(resultdiview =='ga ada'){
	//alert("Data tidak ditemukan!");
	temphasil = 0;
}else{
	var bardataMerk = <?php echo $bardataMerk; ?>;
	var bardataregion = <?php echo $bardataregion; ?>;
	var cekregion =  '<?php echo $cekregion; ?>';
	var cekarea =  '<?php echo $cekarea; ?>';
	var cekmanageby =  '<?php echo $cekmanageby; ?>';
	var cekmerk =  '<?php echo $cekmerk; ?>';
	temphasil = 1;
}
</script>

<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Status Mesin H+1</h1>
        				</div>
								<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Status Mesin H+1</li>
                    </ol>
                </div>
        <!-- /.container-fluid -->
						</div>
				</div>
		</div>
    <section class="content">
        <div class="container-fluid">
            <div class="card">

              <form action="<?php echo base_url('StatMach1/getData') ?>" method="post">
                <div class="card-header">
                  <div>
                      <h6><b>Cari berdasarkan : </b></h6>
                  </div>
                    <div class="d-flex justify-content-between">
                      <table class="card-body table-responsive">
					  						<tr>
                        <td><input type="text" id="summaryDay" name="summaryDay" class="form-control" style="height:29px;width:190px" placeholder="Tanggal" required/></td>
                        <td>
          							  <select id="status" name="status" class="form-control select2" style="width:190px">
          								<option selected disabled>Status</option>
          								<option value="RMMAgentAktif(EJUpdate)">RMMAgent Aktif (EJ Update)</option>
          								<option value="RMMAgentAktif(EJNotUpdate)">RMMAgent Aktif (EJ Not Update)</option>
													<option value="RMMAgentTidakAda">RMMAgent tidak ada</option>
													<option value="ATMTidakAktif">RMMAgent tidak aktif</option>
													<option value="ProblemJaringan">Problem Jaringan</option>
          							  </select>
          							</td>
          							<td>
                          <select id="region" name="region" class="form-control select2" style="width:190px;">
                            <option selected disabled>Wilayah</option>
                            <?php
		              							foreach($region as $region){
		              								?>
		              							<option  value="<?php echo $region['kanwil'] ?>">
		              							<?php echo $region['kanwil']; ?>
		              							</option>
		              							<?php
		              							}
		              							?>
              						</select>
          							  </td>

          							<td>
                          <select id="area" name="area" class="form-control select2" style="width:190px">
                            <option selected disabled>Area</option>
		                            <?php
				              							foreach($area as $area){
				              								?>
				              							<option  value="<?php echo $area['area'] ?>">
				              							<?php echo $area['area']; ?>
				              							</option>
				              							<?php
																	}
		              							?>
              						</select>
          							</td>
          							<td>
                          <select id="managerType" name="managerType" class="form-control select2" style="width:190px">
  														<option selected disabled>Jenis Pengelola</option>
  														<option value="Vendor-">Vendor</option>
  														<option value="Branch-">Branch</option>
  												</select>
          							</td>
          						</tr>
          						<tr>
                        <td>
                          <select id="managedBy" name="managedBy" class="form-control select2" style="width:190px">
                            <option selected disabled>Nama Pengelola</option>
                            <?php
			                           foreach($managedBy as $managedBy){
			                              ?>
			                            <option  value="<?php echo $managedBy['managedBy'] ?>">
			                            <?php echo $managedBy['managedBy']; ?>
			                            </option>
			                            <?php
																}
                            ?>
                          </select>
          							</td>
          							<td>
                          <select id="terminalType" name="terminalType" class="form-control select2" style="width:190px">
                            <option selected disabled>Jenis Mesin</option>
                            <?php
			              							foreach($terminalType as $terminalType){
			              								?>
			              							<option  value="<?php echo $terminalType['terminalType'] ?>">
			              							<?php echo $terminalType['terminalType']; ?>
			              							</option>
			              							<?php
			              							}
			              							?>
              						</select>
          							</td>
          							<td>
                          <select id="merk" name="merk" class="form-control select2" style="width:190px">
                            <option selected disabled>Merk</option>
                            <?php
              							foreach($merk as $merk){
              								?>
              							<option  value="<?php echo $merk['merk'] ?>">
              							<?php echo $merk['merk']; ?>
              							</option>
              							<?php
              							}
              							?>
              						</select>
          							</td>
          							<td>
                          <select id="connectivityType" name="connectivityType" class="form-control select2" style="width:190px">
                            <option selected disabled>Jarkom</option>
                            <?php
              							foreach($connectivityType as $connectivityType){
              								?>
              							<option  value="<?php echo $connectivityType['connectivityType'] ?>">
              							<?php echo $connectivityType['connectivityType']; ?>
              							</option>
              							<?php
              							}
              							?>
              						</select>
          							</td>
									</tr>
									<tr>

								</tr>
						  </table>
						</div>
								<button type="submit" class="btn btn-primary btn-submit " id="filter" style="margin-left:2%">Submit</button>
							 <span style="font-size:12px;color:green"><?php echo $params; ?></span>
            </div>
            </form>

						<?php if($resultdiview == "ada"){
							?>

            <div class="card-body table-responsive">
            <div class="row">
                <div class="col-sm-4">
										<h5> <center><label>Status RRM Agent</label></center> </h5>
                    <canvas id="pi" dataval="<?php echo $nilai ?>" width="60" height="50"></canvas>
										  <br>
                    <div class="card-body table-responsive">
                      <table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
						<tr>
                            <td><label>Status EJ</label></td>
                            <td><label>Jumlah</label></td>
                            <td><label>Persentase</label></td>
                        </tr>
                          <tr>
                              <td>Update</td>
                              <td><?php echo number_format($updatePie,0,',','.') ?></td>
							  <td><?php echo round($updatePiepercentage,2)?> %</td>
                          </tr>
                          <tr>
                              <td>Tidak Update</td>
                              <td><?php echo number_format($nonupdatePie,0,',','.')?></td>
							  <td><?php echo round($nonupdatePiepercentage,2)?> %</td>
                          </tr>
                          <tr>
                              <td>RMM Agent Tidak Ada</td>
                              <td><?php echo number_format($rmmtdkadaPie,0,',','.')?></td>
							  <td><?php echo round($rmmtdkadaPiepercentage,2)?> %</td>
                          </tr>
                          <tr>
                              <td>ATM Tidak Aktif</td>
                              <td><?php echo number_format($atmtdkaktifPie,0,',','.')?></td>
							  <td><?php echo round($atmtdkaktifPiepercentage,2)?> %</td>
                          </tr>
                          <tr>
                              <td>Problem jaringan</td>
                              <td><?php echo number_format($probjaringanPie,0,',','.')?></td>
							  <td><?php echo round($probjaringanPiepercentage,2)?> %</td>
                          </tr>
						  <tr>
                              <td>Total Mesin</td>
                              <td><?php echo number_format($totalupdatePie,0,',','.') ?></td>
							   <td>100%</td>
                          </tr>
                      </table>
                      </div>

</div>
<div class="col-sm-8" >
						<h5> <center><label>Status Mesin Berdasarkan Merek</label></center> </h5>
							<canvas  id="bar2" width="59" height="26"></canvas>
							<div class="card-body table-responsive">
							<input type="hidden"  id="tarikdatabarmerk"/>
								<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
								<tr>
										<td><label>Status EJ</label></td>
											<?php
											for($i=0;$i<count($bardataMerks);$i++){
											?>
											<td><?php echo $bardataMerks[$i]['merk']; }?></td>
									</tr>
									<tr>
										<td>Update</td>
											<?php for($i=0;$i<count($bardataMerks);$i++){?>
											<td><?php echo $bardataMerks[$i]['RMMAgentAktif(EJUpdate)']; } ?></td>
									</tr>
									<tr>
										<td>Tidak Update</td>
											<?php for($i=0;$i<count($bardataMerks);$i++){?>
											<td><?php echo $bardataMerks[$i]['RMMAgentAktif(EJNotUpdate)']; }?></td>
									</tr>
									<tr>
										<td>RMM Agent Tidak Ada</td>
											<?php for($i=0;$i<count($bardataMerks);$i++){?>
											<td><?php echo $bardataMerks[$i]['RMMTidakAda'];}?></td>
									</tr>
									<tr>
										<td>ATM Tidak Aktif</td>
											<?php for($i=0;$i<count($bardataMerks);$i++){?>
											<td><?php echo $bardataMerks[$i]['ATMTidakAktif'];}?></td>
									</tr>
									<tr>
										<td>Problem jaringan</td>
											<?php for($i=0;$i<count($bardataMerks);$i++){?>
											<td><?php echo $bardataMerks[$i]['ProblemJaringan'];}?></td>
									</tr>
								</table>

							</div>
						</div>
						<div class="col-sm-12" style="margin-top:30px;">
						<h5> <center><label>Status Mesin Berdasarkan Wilayah</label></center> </h5>
							<canvas  id="bar" width="60" height="30" ></canvas>

							<div class="card-body table-responsive">
										<table id="mydata" class="table table-bordered table-striped"  style="font-size:12px">
											<tr>
										<td><label>Status EJ</label></td>
											<?php
											for($i=0;$i<count($bardataregions);$i++){
												?>
												<td><?php
												if($cekregion == "0"){
														echo $bardataregions[$i]['regionAlias'];
												}if($cekregion!= "0"){
													if($cekarea== "0" && $cekmanageby == "0" ){
															echo $bardataregions[$i]['area'];
													}else{
															echo $bardataregions[$i]['flmvendor'];
													}
												}
											 }?></td>
									</tr>
									<tr>
										<td>Update</td>
											<?php for($i=0;$i<count($bardataregions);$i++){?>
											<td><?php echo $bardataregions[$i]['RMMAgentAktif(EJUpdate)']; } ?></td>
									</tr>
									<tr>
										<td>Tidak Update</td>
											<?php for($i=0;$i<count($bardataregions);$i++){?>
											<td><?php echo $bardataregions[$i]['RMMAgentAktif(EJNotUpdate)'];}?></td>
									</tr>
									<tr>
										<td>RMM Agent Tidak Ada</td>
											<?php for($i=0;$i<count($bardataregions);$i++){?>
											<td><?php echo $bardataregions[$i]['RMMTidakAda'];}?></td>
									</tr>
									<tr>
										<td>ATM Tidak Aktif</td>
											<?php for($i=0;$i<count($bardataregions);$i++){?>
											<td><?php echo $bardataregions[$i]['ATMTidakAktif'];}?></td>
									</tr>
									<tr>
										<td>Problem jaringan</td>
											<?php for($i=0;$i<count($bardataregions);$i++){?>
											<td><?php echo $bardataregions[$i]['ProblemJaringan'];}?></td>
									</tr>
										</table>
								</div>
						</div>
					</div>
				</div>
					<?php
						}else{
					?>
					<div class="card-body table-responsive">
			                <div class="row">
							<h3> Data tidak ditemukan </h3>
							</div>
					</div>

					<?php
						}
					?>
				</div>
			</section>
    </div>

	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script>
	$('#summaryDay').datepicker({
		format: 'yyyy-mm-dd',
	})
	</script>
