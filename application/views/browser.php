<script language="javascript">
    function hanyaAngka(e, decimal) {
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;

    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    } else
    if (decimal && (keychar == ".")) {
        return true;
    } else return false;
    }
</script>

<style>
.daterangepicker{
	border-radius: 10px;
}
.daterangepicker .drp-calendar.left,.daterangepicker .drp-calendar.right {
padding: 4px 0 4px 4px;
}
.daterangepicker .calendar-table th, .daterangepicker .calendar-table td{
	line-height: 14px;font-size: 11px;    min-width: 15px;
    width: 30px;height: 10px!important;
}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
								<h1 class="m-0 text-dark">EJ BROWSER</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">EJ Browser</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
					<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Main row -->
          <div class="card">
							<form id="myform" name="myform">
							<!--<form action="<?php echo base_url('browse/Show') ?>" method="post">-->
										<div class="card-header">
											<div>
												<h3 class="card-title"><label>Data EJ Browser</label></h3>
											</div>
										</div>
											<!-- /.card-header -->

											<div class="row">
												<style>
													th, td {
													padding: 5px;
													}
													#events {
														margin-bottom: 1em;
														padding: 1em;
														background-color: #f6f6f6;
														border: 1px solid #999;
														border-radius: 3px;
														height: 100px;
														overflow: auto;
													}
													.addNewRecord {
															background-color: #0062cc !important;
															color: #ffffff !important;
														}
													.dt-button{
														color: #ffffff!important;
														background-color: #0062cc!important;
														border-color: #005cbf!important;
														background-image:none!important;
													}
													.sorting_asc,.sorting,.sorting_desc{
														vertical-align: top!important;
													}
													.select-info{
														margin-left:20px;
													}
													.bottombuttons{
														margin-top:5px;
													}
												</style>

												<div class="col-sm-7">
													<table class="card-body table-responsive">
														<tr>
															<td><label>ID Mesin</label></td>
															<td>
																<select id="terminalID" name="terminalID" class="form-control select2idmachine" style="width:185px;height:29px">
																	<option value="" selected disabled>ID Mesin</option>
																	<?php
																	foreach($terminalID as $terminalID){
																		?>
																	<option  value="<?php echo $terminalID['terminalID'] ?>">
																	<?php echo $terminalID['terminalID']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
															</td>
														</tr>
														<tr>
															<td><label>Input Tanggal</label></td>
															<td>
															     <input type="text" id="startdate"  autocomplete="off"  name="startdate" class="form-control" style="width:350px;height:29px;float:left;margin-right:15px" disabled/>
																	<input type="checkbox" id="tanggalcek">
															</td>
														</tr>
														<tr>
															<td><label>Cari</label></td>
															<td>
																<select id="keyword" name="keyword" class="form-control select2" style="width:185px;height:29px;float:left!important;margin-right:15px">
																	<option value="" selected disabled> Keyword List</option>
																	<?php
																	foreach($keyword as $keyword){
																		?>
																	<option  value="<?php echo $keyword['keyword'] ?>">
																	<?php echo $keyword['keyword']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
																<input type="text"  id="keyword2" name="keyword2" style="color: #495057; margin-left:5px; background-color: #ffffff;background-clip: padding-box;border: 1px solid #ced4da; margin-top :2px;border-radius: 0.25rem;width:185px;height:29px">
															</td>
														</tr>
														<tr>
															<td><label>Wilayah</label></td>
															<td>
																<select id="region" name="region" class="form-control select2" style="width:185px;height:29px">
																	<option value="" selected disabled>Wilayah</option>
																	<?php
																	foreach($region as $region){
																		?>
																	<option  value="<?php echo $region['kanwil'] ?>">
																	<?php echo $region['kanwil']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
															</td>
														</tr>
														<tr>
															<td><label>Area</label></td>
															<td>
																<select id="area" name="area" class="form-control select2" style="width:185px;height:29px">
																	<option value="" selected disabled>Area</option>

																  <?php
																	foreach($area as $area){
																		?>
																	<option  value="<?php echo $area['area'] ?>">
																	<?php echo $area['area']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
															</td>
														</tr>
														<tr>
															<td><label>Status Lainnya</label></td>
															<td>
																<select id="transStat" name="transStat" class="form-control select2" style="width:185px;height:29px">
																	<option value="" selected disabled> Status Lainnya </option>

																  <?php
																	foreach($transStat as $transStat){
																		?>
																	<option  value="<?php echo $transStat['keyword'] ?>">
																	<?php echo $transStat['keyword']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
															</td>
														</tr>
													</table>
												</div>
												<div class="col-sm-5">
													<table class="card-body table-responsive">
														<tr>
															<td><label>Definisi Transaksi</label></td>
															<td>
                                <select id="definisi" name="definisi" class="form-control select2" style="width:185px;height:29px">
        														<option value="" selected disabled>Definisi Transaksi</option>
        														<option value="sukses">Sukses</option>
        														<option value="gagal">Gagal</option>
        														<option value="unknown">Unknown</option>
        												</select>
															</td>
														</tr>
														<tr>
															<td><label>Jenis Transaksi</label></td>
															<td>
                                <select id="jenistransaksi" name="jenistransaksi" class="form-control select2" style="width:185px;height:29px">
        														<option value="" selected disabled>Jenis Transaksi</option>
        														 <?php
																	foreach($jenistransaksi as $jenistransaksi){
																		?>
																	<option  value="<?php echo $jenistransaksi['transactionGroup'] ?>">
																	<?php echo $jenistransaksi['transactionGroup']; ?>
																	</option>
																	<?php
																	}
																	?>
        												</select>
															</td>
														</tr>
														<tr>
															<td><label>Jenis Pengelola</label></td>
															<td>
                                <select id="managerType" name="managerType" class="form-control select2" style="width:185px;height:29px">
        														<option value="" selected disabled>Jenis Pengelola</option>
        														<option value="Vendor-">Vendor</option>
        														<option value="Branch-">Branch</option>
        												</select>
															</td>
														</tr>
														<tr>
															<td><label>Nama Pengelola</label></td>
                              <td>
                                <select id="managedBy" name="managedBy" class="form-control select2" style="width:185px;height:29px">
                                  <option value="" selected disabled>Nama Pengelola</option>
                                  <?php
                                        foreach($managedBy as $managedBy){
                                          ?>
                                        <option  value="<?php echo $managedBy['managedBy'] ?>">
                                        <?php echo $managedBy['managedBy']; ?>
                                        </option>
                                        <?php
                                      }
                                        ?>
                              </select>
                              </td>
														</tr>
														<tr>
															<td><label>Nomor Resi &nbsp;&nbsp;</label></td>
															<td>
																<input type="text" onkeypress="return hanyaAngka(event, false)" maxlength="4" id="resi" name="resi" class="form-control" style="width:185px;height:29px" maxlength="4" pattern="^(0|[1-9][0-9]*)$" title="Input harus berupa angka">
															</td>
														</tr>
														<tr>
															<td><label>Nomor Kartu &nbsp;&nbsp;</label></td>
															<td>
																<input type="text" onkeypress="return hanyaAngka(event, false)" maxlength="16" id="nocard" name="nocard" class="form-control" style="width:185px;height:29px">
															</td>
														</tr>
													</table>
											</div>
											<!-- /.card-body -->
											<div class="card-body">
												<div style="float:left;margin-right:15px;">
													<button type="button" class="btn btn-primary" id="submitsearch">Submit</button>
													<button type="button" id="submitreset" class="btn btn-danger">Reset</button>
												</div>
												<div id="loadingdiv"></div>
											</div>
									</div>
										<!-- /.card -->
								</div>
							</div>
							<!-- /.row (main row) -->
							</form>
						</div>

				</div>
				<!-- /.container-fluid -->
			</section>
			<!-- /.content -->

			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex justify-content-between">
										<h3 class="card-title"><label>List EJ Browser</label></h3>
									</div>
								</div>
								<!-- /.card-header -->
								<div id="mytable">
								<div id="tabledynamic" class="card-body table-responsive">
									<table id="detailsearchtable" class="table table-bordered table-striped table-responsive" style="font-size:11px">
									</table>
								</div>
								</div>
							<!--div class="card-body">
								<button type="button" class="btn btn-primary" id="showresi">Tampilkan Resi</button>
								<button type="button" class="btn btn-primary"  value="Create PDF" id="btPrint" onclick="createPDF()">Print PDF</button>
								<button type="button" class="btn btn-primary"  id="excel" >Print Excel</button>
							</div-->
							</div>
						</div>
					</div>
				</div>
			</section>
        <!-- /.container-fluid -->
    	</div>
		
		 <div class="modal fade" id="modalresi" tabindex="-1" role="dialog" >
		 <div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Resi Detail</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div id="content-resi" style="padding:20px">
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
