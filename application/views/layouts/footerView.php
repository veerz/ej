<footer class="main-footer">
	<strong>Copyright &copy; 2019 <a href="http://hbm.co.id">HBM</a></strong>
	All rights reserved.
	<div class="float-right d-none d-sm-inline-block">
		<b>Version</b> 1.0.0-alpha
	</div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)

</script>
<!-- Bootstrap 4 -->
<!--script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script-->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
<!--script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap-datetimepicker.min.js"></script-->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/adminlte.js"></script>
<!--script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script-->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<!-- date-range-picker -->
<!-- <script src="<?php echo base_url(); ?>assets/scriptings/datetimepicker-master/jquery.datetimepicker.js"></script> -->
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<!-- <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script> -->
<script src="<?php echo base_url(); ?>assets/plugins/foundation-datepicker-master/js/foundation-datepicker.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/Chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chartjs-plugin-labels.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chartjs-plugin-labelshor.js"></script>
<script src="<?php echo base_url(); ?>assets/piechart/PieceLabel.min.js"></script>
<!-- Sweetalert2 -->
<script src="<?php echo base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
<!--script src="<?php echo base_url(); ?>assets/js/promise-polyfill.js"></script-->
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- print pdf -->
<!--script src="<?php echo base_url(); ?>assets/js/pdf/jspdf.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pdf/jQuery-2.1.3.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pdf/pdfFromHTML.js"></script-->

<!-- Load JS berdasarkan Page -->
<script src="<?php echo base_url(); ?>assets/js/pages/<?php
if ($this->uri->segment(1) == "Admin" || $this->uri->segment(1) == "admin"){
	 echo $this->uri->segment(2);
}else
{ echo $this->uri->segment(1); }
?>.js"></script>
<script>
var uripage = "<?php
if ($this->uri->segment(1) == "Admin" || $this->uri->segment(1) == "admin"){
	 echo $this->uri->segment(2);
}else
{ echo $this->uri->segment(1); }
?>";
//alert(uripage);
$( ".nav-item" ).each(function() {
	if($(this).attr("data-uri") == uripage || $(this).attr("data-uri") == "Admin/" +uripage){
		 $(this).children('a').css('color','#ffffff!important');
		 $(this).children('a').css('background-color','rgba(255, 255, 255, 0.2)!important');
		$(this).closest( '.nav-treeview').toggle(200);
		$(this).closest( '.nav-parent-uri').children('a').css('color','#ffffff!important');
		$(this).closest( '.nav-parent-uri').children('a').css('background-color','rgba(255, 255, 255, 0.2)!important');
	}else{
		 $(this).css('active1');
	}
});

 
//alert(uripage);
</script>
</body>

</html>
