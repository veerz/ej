<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Electronic Journal</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminltefull.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  	<!-- Select2 -->
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2-bootstrap4.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="<?php echo base_url(); ?>assets/css/font.googleapis.css" rel="stylesheet">
</head>

<style>
.vl {
  border-left: 1px solid white;
  height: 80%;
  position: absolute;
  left: 0%;
  margin-left: -3px;
  top: 5px;
  bottom: 5px;
}
</style>


		<!-- Navbar -->
		<nav class="main-header navbar border-bottom navbar-dark bg-navbar-login" style="margin-left:0px!important;">
			<!-- Left navbar links -->
			<div class="row">
				<div class="col-sm-2" style="padding-left:2.5%!important">
					<img src="<?php echo base_url() ?>/../assets/img/logo_login.png" height='50'/>				
				</div>	
				<div class="col-sm-9">
					<div class="vl"></div>
					<h4 style="color:#FFFFFF;margin:1px!important;font-family: Segoe UI;font-weight: lighter">EJ MONITORING</h4>
					<h9 style="color:#FFFFFF;margin:2px!important;font-family: Segoe UI;font-weight: lighter">Electronic Channel Operations Group</h9>	
				</div>
				<div class="col-sm-1">
					<div style="padding-left:0%" >						
						<span style="color:#FFFFFF;margin:1px!important;font-family: Segoe UI;font-weight: lighter"><?php echo "Hi ". $this->session->userdata("username"); ?> </span>
						<div class="row">
							<div class="col-sm-2">
								<a style="padding-left:0%" class="nav-link" href="<?php echo base_url("Chart"); ?>">
								<i class="fa fa-home" style="height:50px;width:50px;color:#fff"></i></a>
							</div>
							<div class="col-sm-2">
								<a class="nav-link" href="<?php echo base_url("Logout"); ?>">
								<i class="fa fa-sign-out" style="color:#fff"></i></a>
							</div>
						</div>				
					</div>
				</div>
			</div>
		</nav>


	

			<!-- <ul class="navbar-nav-back">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url("Chart"); ?>">
					<i class="fa fa-home"></i></a>
				</li>
			</ul> -->

			<!-- Right navbar links -->
			<!-- <ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url("Logout"); ?>">
					<i class="fa fa-sign-out"></i></a>
				</li>
			</ul> -->

		<!-- /.navbar -->
