<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Electronic Journal</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/foundation-datepicker-master/css/foundation-datepicker.css">

	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css"-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.dataTables.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" type="text/css" />
  	<!-- Select2 -->
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2-bootstrap4.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="<?php echo base_url(); ?>assets/css/font.googleapis.css" rel="stylesheet">

	<!-- select2 & reset -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2.css">

	<!-- datetimepicker -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.js"></script>

	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
	<!-- datepicker -->
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" type="text/css"> -->
</head>

<style>
.active1, .btns:hover {
  background-color: #27416C;
  color: #ffffff;
}

.vl {
  border-left: 1px solid white;
  height: 80%;
  position: absolute;
  left: 0%;
  margin-left: -3px;
  top: 5px;
  bottom: 5px;
}


/*
 *  STYLE 5
 */

.sidebar::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

.sidebar::-webkit-scrollbar
{
	width: 10px;
	background-color: #F5F5F5;
}

.sidebar::-webkit-scrollbar-thumb
{
	background-color: #007bff;
	
	background-image: -webkit-gradient(linear, 0 0, 0 100%,
	                   color-stop(.5, rgba(255, 255, 255, .075)),
					   color-stop(.5, transparent), to(transparent));
}
</style>

<!--<body class="hold-transition sidebar-mini">
	<div class="wrapper">-->
<body style="background-color:#F4F6F9;">

		<!-- Navbar -->
		<nav class="main-header navbar border-bottom navbar-dark bg-navbar-login" style="margin-left:0px!important;">
			<!-- Left navbar links -->
			<div class="row">
				<div class="col-sm-2" style="padding-left:4%!important;padding-top:0.5%;">
					<img src="<?php echo base_url() ?>/../assets/img/logo_login.png" height='50'/>				
				</div>	
				<div class="col-sm-9">
					<div class="vl" style=" margin-left:0.8%;">
					</div>
					<div style=" margin-left:2%;">
					<h4 style="color:#FFFFFF;margin:1px!important;font-family: Segoe UI;font-weight: lighter">EJ MONITORING</h4>
					<h9 style="color:#FFFFFF;margin:2px!important;font-family: Segoe UI;font-weight: lighter">Electronic Channel Operations Group</h9>	
					</div>
				</div>
				<div class="col-sm-1">
					<div style="padding-left:0%" >						
						<span style="color:#FFFFFF;margin:1px!important;font-family: Segoe UI;font-weight: lighter"><?php echo "Hi ". $this->session->userdata("username"); ?> </span>
						<div class="row">
							<div class="col-sm-2">
								<a style="padding-left:0%" class="nav-link" href="<?php echo base_url("Chart"); ?>">
								<i class="fa fa-home" style="height:50px;width:50px;color:#fff"></i></a>
							</div>
							<div class="col-sm-2">
								<a class="nav-link" href="<?php echo base_url("Logout"); ?>">
								<i class="fa fa-sign-out" style="color:#fff"></i></a>
							</div>
						</div>				
					</div>
				</div>
			</div>
		</nav>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4" style="margin-top:4.8%!important;width:17%;">
		
			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar Menu -->
				<nav class="mt-2" >
					<ul class="nav nav-pills nav-sidebar flex-column nav" data-widget="treeview" role="menu" data-accordion="false" id="menu">
						<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
							<?php
								$ij = 0;
								foreach($datalistmenu as $dlm){
										if($dlm['module'] == "parent" ){
										?>
										<li class="nav-item nav-parent-uri<?php if ($this->uri->segment('1') == $dlm['name']){echo "active1";} ?>"  id="menuid-<?php echo  $dlm['id_menu'] ?>" data-uri="<?php echo $dlm['route']; ?>">
											<a href="#" class="nav-link" >
												<i class="nav-icon <?php echo  $dlm['fa']?>"></i>
												<p>
													<?php echo  $dlm['name']?>
												</p>
											</a>
												<ul class="nav-treeview">
													<div style="margin-left:20px;">
													<?php
													foreach($datalistmenusub as $dlms){
														if( $dlms['module'] ==  $dlm['id_menu']){
													?>
														<li class="nav-item nav-child-uri"  id="menuid-<?php echo  $dlms['id_menu'] ?>" data-uri="<?php echo $dlms['route']; ?>">
															<a  href="<?php echo base_url($dlms['route'])?>" class="nav-link">
																<?php echo  $dlms['name']?>
															</a>
														</li>
													<?php }} ?>
													</div>
												</ul>
											</li>
										<?php
										}else{
										?>
											<li class="nav-item nav-child-uri" data-uri="<?php echo $dlm['route']; ?>">
											<a href="<?php echo base_url($dlm['route'])?>" class="nav-link" id="menuid-<?php echo  $dlm['id_menu'] ?>">
												<i class="nav-icon <?php echo  $dlm['fa']?>"></i>
												<p>
													<?php echo  $dlm['name']?>
												</p>
											</a>
											</li>
										<?php
										}
								}
							//}
							?>

						<li class="nav-item">
							<a href="<?php echo base_url("Logout"); ?>" class="nav-link">
								<i class="nav-icon fa fa-sign-out"></i>
								<p>
									Logout
								</p>
							</a>
						</li>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>
