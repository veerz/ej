<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title> EJ Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo base_url() ?>/../assets/img/ej.png">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugins/sweetalert/sweetalert.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="<?php echo base_url(); ?>assets/css/font.googleapis.css" rel="stylesheet">

<style>
.vl {
  border-left: 1px solid white;
  height: 100%;
  position: absolute;
  left: 0%;
  margin-left: -3px;
  top: 0;
}
</style>
	
</head>

	<div class="wrapper">
		<!-- Navbar -->
		<nav class="main-header navbar border-bottom navbar-dark bg-navbar-login" style="margin-left:0px!important">
			<!-- Left navbar links -->
			<div class="col-sm-1" style="padding-left:2.5%!important">
				<img src="<?php echo base_url() ?>/../assets/img/logo_login.png" height='50'/>				
			</div>
				
				<div class="col-sm-10">
				<div class="vl"></div>
						<h4 style="color:#FFFFFF;margin:1px!important;font-family: Segoe UI;font-weight: lighter">EJ MONITORING</h4>
						<h9 style="color:#FFFFFF;margin:2px!important;font-family: Segoe UI;font-weight: lighter">Electronic Channel Operations Group</h9>	
				</div>
		</nav>
	</div>