<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Electronic Journal</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  	<!-- Select2 -->
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2-bootstrap4.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="<?php echo base_url(); ?>assets/css/font.googleapis.css" rel="stylesheet">
</head>

<style>


.active1, .btns:hover {
  background-color: #27416C;
  color: #ffffff;
}
</style>

<body class="hold-transition sidebar-mini">
	<div class="wrapper">

		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-primary">
			<!-- Left navbar links -->
			<ul class="navbar-nav-title">
				<h3 style="color:#FFFFFF;display:flex;justify-content: center;">EJ Monitoring </h3>
			</ul>

			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url("login"); ?>"><i
            class="fa fa-sign-out"></i></a>
      </li>
			</ul>
		</nav>
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="index3.html" class="brand-link bg-primary">
				<img src="<?php echo base_url(); ?>assets/img/mandiri_logo.png" alt="EJ Logo" class="brand-image elevation-3">
				<span class="brand-text font-weight-light">&nbsp;</span>
			</a>

			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar user panel (optional) -->
				<!--div class="user-panel mt-3 pb-3 mb-3 d-flex">
					<div class="image">
						<img src="<?php echo base_url(); ?>assets/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
					</div>
					<div class="info">
						<a href="<?php echo base_url("Admin/AdminChange"); ?>" class="d-block">Admin
						<br>Change Password</br></a>

					</div>
				</div-->

				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column nav" data-widget="treeview" role="menu" data-accordion="false">
						<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

			   			<li class="nav-item <?php if ($this->uri->segment('1') == 'Chart'){echo "active1";} ?>">
							<a href="<?php echo base_url("Chart"); ?>" class="nav-link">
								<i class="nav-icon fa fa-home"></i>
								<p>
									Dashboard
								</p>
							</a>

			   			<li class="nav-item">
									<a href="#" class="nav-link">
										<i class="nav-icon fa fa-user"></i>
										<p>
											Admin
										</p>
									</a>


							<ul class="nav-treeview">
							<div class="nav-item">
								<a href="<?php echo base_url("Admin/Mappingdefinisi"); ?>" class="nav-link">Mapping Definisi</a>
								<a href="<?php echo base_url("Admin/Mappingcode"); ?>" class="nav-link">Mapping Code</a>
								<!--<a href="<?php echo base_url("Admin/MappingErrorProblem"); ?>" class="nav-link">Mapping Error Problem</a>-->
								<a href="<?php echo base_url("Admin/Mappingkey"); ?>" class="nav-link">Mapping Keyword List EJ</a>
								<a href="<?php echo base_url("Admin/UserManagement"); ?>" class="nav-link">User Management</a>
								<a href="<?php echo base_url("Admin/Role"); ?>" class="nav-link">Access Matrix</a>
								<a href="<?php echo base_url("Admin/BatchTimeUpdate"); ?>" class="nav-link">Batch Time Update</a>
								<a href="<?php echo base_url("Admin/Mappingcard"); ?>" class="nav-link">Mapping Card Bin Number</a>
								<a href="<?php echo base_url("Admin/ListOpCode"); ?>" class="nav-link">List Op Code</a>
								<a href="<?php echo base_url("Admin/Audittrail"); ?>" class="nav-link">Audit Trail</a>
							</div>
							</ul>
						</li>

						<li class="nav-item">
							<a href="#" class="nav-link">
							<i class="nav-icon fa fa-bar-chart"></i>
								<p>
									Status Mesin
								</p>
							</a>

							<ul class="nav-treeview">
							<div class="nav-item <?php if ($this->uri->segment('1') == 'StatMach0'){echo "active1";} ?>">
								<a href="<?php echo base_url("StatMach0"); ?>" class="nav-link">H+0</a>
							</div>
							<div class="nav-item <?php if ($this->uri->segment('1') == 'StatMach1'){echo "active1";} ?>">
								<a href="<?php echo base_url("StatMach1"); ?>" class="nav-link">H+1</a>
							</div>
							</ul>

						</li>

						<li class="nav-item <?php if ($this->uri->segment('1') == 'StatTrans'){echo "active1";} ?>">
							<a href="<?php echo base_url("StatTrans"); ?>" class="nav-link">
								<i class="nav-icon fa fa-bar-chart"></i>
								<p>
									Status Transaksi
								</p>
							</a>

						<li class="nav-item <?php if ($this->uri->segment('1') == 'Browser'){echo "active1";} ?>">
							<a href="<?php echo base_url("Browser"); ?>" class="nav-link">
								<i class="nav-icon fa fa-search"></i>
								<p>
									EJ Browser
								</p>
							</a>

							<li class="nav-item <?php if ($this->uri->segment('1') == 'Cardretained'){echo "active1";} ?>">
							<a href="<?php echo base_url("Cardretained"); ?>" class="nav-link">
								<i class="nav-icon fa fa-bar-chart"></i>
								<p>
									Kartu Tertelan
								</p>
							</a>

							<li class="nav-item <?php if ($this->uri->segment('1') == 'Detailsearch'){echo "active1";} ?>">
							<a href="<?php echo base_url("Detailsearch"); ?>" class="nav-link">
								<i class="nav-icon fa fa-bar-chart"></i>
								<p>
									Detail Search
								</p>
							</a>

						<li class="nav-item">
							<a href="<?php echo base_url("Logout"); ?>" class="nav-link">
								<i class="nav-icon fa fa-sign-out"></i>
								<p>
									Logout

								</p>
							</a>
						</li>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>