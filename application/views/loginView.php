<style>
h1, h5 {
  color: #fff;
  font-family: Segoe UI;font-weight: lighter;
}
</style>

<body class="bg-login">
<br><br><br><br><br><br><br>
	<div class="row">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-4">
		<br><br><br>
		<h1>WELCOME</h1>
		<h5>Dear Colleague,<br>For further information please contact us</h5>
		<h5><i class="nav-icon fa fa-envelope fa-2x"></i> nanang.suhada@bankmandiri.co.id</h5>
		</div>
		<div class="col-sm-2">
		</div>
		<div class="col-sm-4">
			<div class="login-box">
				<!--<div class="login-logo">

					<img src="<?php echo base_url() ?>/../assets/img/mandiri2.png" height='70'/>

				</div>-->
				<!-- /.login-logo -->
				<div class="card">
					<div class="card-body login-card-body" style="background-color:rgba(175, 170, 163, 0.5)">
					<h3 style="color:#000;font-family: Segoe UI;font-weight: lighter;">Login</h3>
					<h5 style="color:#000;font-family: Segoe UI;font-weight: lighter;">Sign in your account</h5>
					<p class="login-box-msg" style="color:#fff"></p>

						<?php if(isset($error)) { echo $error; }; ?>
						 <form id="zlog" method="post">
								<div class="form-group has-feedback">
									<input type="text" class="form-control" placeholder="Username" id="username" name="username" style="color:#000;background-color:rgba(175,170,163,0.5);border-color:#000;font-family: Segoe UI;font-weight: lighter;;" pattern="^[a-zA-Z-0-9- -]+$" title="Input harus berupa Alphabet/numerik" onblur="this.value=removeSpaces(this.value);"> </div>
								<div class="form-group has-feedback">
									<input type="password" class="form-control" placeholder="Password" id="password" name="password" style="color:#000;background-color:rgba(175,170,163,0.5);border-color:#000;font-family: Segoe UI;font-weight: lighter;;"> </div>
								<!-- /.col -->
								<div class="col-12">
									<button type="submit" class="btn btn-submit btn-block" style="background-color:rgba(15,43,91,2);color:#fff;font-weight:400!important;font-size:20px;font-family: Segoe UI;font-weight: lighter; width: 30%;;">Login</button>
								</div>
							<!-- /.col -->
							</div>
						</form>
				</div>
				
				<!-- /.login-card-body -->
			</div>
		</div>
		<div class="col-sm-1">
		</div>
		

	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery/jquery2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/jquery/jquery.form.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script>
	$(function() {
		var base = "<?php echo base_url(); ?>" ;
	  $("#zlog").ajaxForm({
	    url :		base + "Login/LoginProcess/",
	    dataType : "JSON",
			type:"POST",
	    beforeSubmit : function(){
			//	$(".login-box-msg").html ( "<br />Checkin username and password..." ).addClass('bg-danger').removeClass('bg-red');
	     // $(".btn").html ( "Please wait..." ).removeClass("btn-primary").addClass("btn-warning").prop("disabled", true);
	    },
		 	success : function(data){
				if ( data.type == "failed" ){
				//	$(".login-box-msg").html( data.msg ).addClass('bg-red');
					swal({
	          title : "",
	          text : data.msg,
	          type : "error"
	        });
					//$(".btn").prop('disabled', false);
				}
				else if ( data.type == "done" ){
			//	 $(".login-box-msg").html( data.msg ).removeClass('bg-danger bg-red').addClass('bg-success');
					swal({
	          title : "",
	          text : data.msg,
	          type : "success"
	        });
					setTimeout(function(){ window.location.replace("<?php echo base_url()?>Chart"); }, 1000);
				}
	      else{
	        //$("#btn-submit").html ( "Sign In" ).removeClass("btn-warning").addClass("btn-primary").prop("disabled", false);
	        swal({
	          title : "",
	          text : data.msg,
	          type : "error"
	        });
	      }
	    },
	    error : function(){
	      $("#btn-submit").html ( "Sign In" ).removeClass("btn-warning").addClass("btn-primary").prop("disabled", false);
	      swal ( "", "Error Occurred, Please refresh your browser.", "error" );
	    }
	  });
	});
	</script>
	</body>

	</html>
