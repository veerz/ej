<?php
$updatePie = $piedataupdate[0]['Update'];
$nonupdatePie = $piedataupdate[0]['notUpdate'];
$totalupdatePie = $updatePie + $nonupdatePie;
$nonupdatePiepercentage = $nonupdatePie / $totalupdatePie * 100;
$updatePiepercentage = $updatePie / $totalupdatePie * 100;
$nilai = $totalupdatePie.",".$updatePie.",".$nonupdatePie;
/*========================================================*/
$bardataMerks = $bardataMerk;
$bardataMerk=json_encode($bardataMerk);
/*========================================================*/
$bardataregions = $bardataregion;
$bardataregion =json_encode($bardataregion);
/*========================================================*/
$cardRetainedRegions = $cardRetainedRegion;
$cardRetainedRegion =json_encode($cardRetainedRegion);
/*========================================================*/
$cardRetainedMerks = $cardRetainedMerk;
$cardRetainedMerk =json_encode($cardRetainedMerk);
/*========================================================*/
$CardRetainedNamaBanks = $CardRetainedNamaBank;
$CardRetainedNamaBank =json_encode($CardRetainedNamaBank);
/*========================================================*/
$CardRetainedNamaBanktop15s = $CardRetainedNamaBanktop15;
$CardRetainedNamaBanktop15 =json_encode($CardRetainedNamaBanktop15);
/*========================================================*/


/*========================================================*/

$StatusTrxMapping =json_encode($StatusTrxMapping);
/*========================================================*/

$StatusTrxBad =json_encode($StatusTrxBad);
/*========================================================*/

$StatusTrxGood =json_encode($StatusTrxGood);

/*========================================================*/
$StatusTrxByJeniss = $StatusTrxByJenis;
$StatusTrxByJenis =json_encode($StatusTrxByJenis);


/* for($i=0;$i<count($regiontrx);$i++){
	if($regiontrx[$i]['Status'] == "Gagal")
	$totalTrxGagal  = $regiontrx[$i]['Jumlah'];
	else
	$totalTrxSukses = $regiontrx[$i]['Jumlah'];
} */
$Replenishments = $Replenishment;
$Replenishment =json_encode($Replenishment);
$totalpengisian = 0;
$totalpengosongan = 0;
for($i=0;$i<count($Replenishments);$i++){
	if($Replenishments[$i]['JenisTrx'] == "Pengisian")
	$totalpengisian  += $Replenishments[$i]['Total'];
	else
	$totalpengosongan += $Replenishments[$i]['Total'];
}

$regiontrxs = $regiontrx;
$regiontrx =json_encode($regiontrx);
$totalTrxGagal = 0;
$totalTrxSukses = 0;
for($i=0;$i<count($regiontrxs);$i++){
	if($regiontrxs[$i]['Status'] == "Gagal")
	$totalTrxGagal  += $regiontrxs[$i]['Jumlah'];
	else
	$totalTrxSukses += $regiontrxs[$i]['Jumlah'];
}


/*========================================================*/


?>

<script type="text/javascript">
var bardataMerk = <?php echo $bardataMerk; ?>;
var bardataregion = <?php echo $bardataregion; ?>;
var cardRetainedRegion = <?php echo $cardRetainedRegion; ?>;
var cardRetainedMerk = <?php echo $cardRetainedMerk; ?>;
var CardRetainedNamaBank = <?php echo $CardRetainedNamaBank; ?>;
var CardRetainedNamaBanktop15 = <?php echo $CardRetainedNamaBanktop15; ?>;
var Replenishment = <?php echo $Replenishment; ?>;
var StatusTrxMapping = <?php echo $StatusTrxMapping; ?>;
var StatusTrxBad = <?php echo $StatusTrxBad; ?>;
var StatusTrxGood = <?php echo $StatusTrxGood; ?>;
var StatusTrxByJenis = <?php echo $StatusTrxByJenis; ?>;
var regiontrx = <?php echo $regiontrx; ?>;
</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slide.css">
<style>
	* {box-sizing: border-box}
	body {font-family: Verdana, sans-serif; margin:0}
	.mySlides {display: none}
	img {vertical-align: middle;}

	/* Slideshow container */
	.slideshow-container {
		max-width: 1000px;
		position: relative;
		margin: auto;
	}

	/* Next & previous buttons */
	.prev, .next {
		cursor: pointer;
		position: absolute;
		top: 50%;
		width: absolute;
		padding: 16px;
		margin-top: -22px;
		color: white;
		font-weight: bold;
		font-size: 18px;
		transition: 0.6s ease;
		border-radius: 0 3px 3px 0;
		user-select: none;
	}

	/* Position the "next button" to the right */
	.next {
		left: 99%;
		border-radius: 3px 0 0 3px;
	}

	.prev {
		right: 99%;
		border-radius: 3px 0 0 3px;
	}

	/* On hover, add a black background color with a little bit see-through */
	.prev:hover, .next:hover {
		background-color: #FACC00;
	}

	/* Caption text */
	.text {
		color: #f2f2f2;
		font-size: 15px;
		padding: 8px 12px;
		position: absolute;
		bottom: 8px;
		width: 100%;
		text-align: center;
	}

	/* Number text (1/3 etc) */
	.numbertext {
		color: #f2f2f2;
		font-size: 12px;
		padding: 8px 12px;
		position: absolute;
		top: 0;
	}

	/* The dots/bullets/indicators */
	.dot {
		cursor: pointer;
		height: 15px;
		width: 15px;
		margin: 0 2px;
		background-color: #0F2B5B;
		border-radius: 50%;
		display: inline-block;
		transition: background-color 0.6s ease;
	}

	.active, .dot:hover {
		background-color: #FCD116;
	}

	/* Fading animation */
	.fade {
		-webkit-animation-name: fade;
		-webkit-animation-duration: 1.5s;
		animation-name: fade;
		animation-duration: 1.5s;
	}

	@-webkit-keyframes fade {
		from {opacity: .4}
		to {opacity: 1}
	}

	@keyframes fade {
		from {opacity: .4}
		to {opacity: 1}
	}

	/* On smaller screens, decrease text size */
	@media only screen and (max-width: 300px) {
		.prev, .next,.text {font-size: 11px}
	}

</style>
<body>

	<div class="slideshow-container" style="max-width:95%">

		<div class="mySlides" style="width:100%">
			<section class="content">
				<div class="container-fluid">
					<div class="card">
						<div class="card-header">
							<div class="d-flex justify-content-between">
								<h2 class="card-title"><b>Status Mesin<b></h2>

							</div>
						</div>
						<div class="row">
							<div class="col-sm-5">
								<h9 style="font-family:calibri;"> <center><label>Status RMM Agent</label></center> </h9>
								<canvas id="pi" dataval="<?php echo $nilai?>" width="30" height="13"></canvas>
								<div class="card-body table-responsive">
									<table id="mydata" class="table table-bordered table-striped" style="font-size:10px">
										<tr>
											<td><label>Status EJ</label></td>
											<td><label>Jumlah</label></td>
											<td><label>Persentase</label></td>
										</tr>
										<tr>
											<td>Update</td>
											<td><?php echo number_format($updatePie,0,',','.') ?></td>
											<td><?php echo round($updatePiepercentage,2)?> %</td>
										</tr>
										<tr>
											<td>Tidak Update</td>
											<td><?php echo number_format($nonupdatePie,0,',','.')?></td>
											<td><?php echo round($nonupdatePiepercentage,2)?> %</td>
										</tr>
										<tr>
											<td>Total Mesin</td>
											<td><?php echo number_format($totalupdatePie,0,',','.') ?></td>
											<td>100%</td>
										</tr>
									</table>
								</div>
							</div>
							<div class="col-sm-7">
								<h9 style="font-family:calibri;"> <center><label>Status Mesin Berdasarkan Merk</label> <a data-toggle="modal" data-target="#Modal_Merek_Mesin" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a> </center></h9>
								<canvas  id="bar2" width="12" height="3"></canvas>

								<div class="modal fade" id="Modal_Merek_Mesin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header bg-primary">
												<h9 class="modal-title" id="exampleModalLabel">Status Mesin Berdasarkan Merk</h9>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="form-group row">
													<canvas  id="bar2full" width="20" height="7"></canvas>
													<div class="card-body table-responsive">
														<input type="hidden"  id="tarikdatabarmerk"/>
														<table id="mydata" class="table table-bordered table-striped" style="font-size:10px">
															<tr>
																<td><label>Status EJ</label></td>
																<?php
																for($i=0;$i<count($bardataMerks);$i++){
																?>
																<td><?php echo $bardataMerks[$i]['merk']; }?></td>
															</tr>
															<tr>
																<td>Update</td>
																<?php for($i=0;$i<count($bardataMerks);$i++){?>
																<td><?php echo number_format($bardataMerks[$i]['Update'],0,',','.'); } ?></td>
															</tr>
															<tr>
																<td>Tidak Update</td>
																<?php for($i=0;$i<count($bardataMerks);$i++){?>
																<td><?php echo number_format($bardataMerks[$i]['notUpdate'],0,',','.');}?></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<br>
								<h9 style="font-family:calibri;"> <center><label>Status Mesin Berdasarkan Wilayah</label> <a data-toggle="modal" data-target="#Modal_Wilayah_Mesin" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center> </h9>
								<canvas  id="bar" width="10" height="3.5"></canvas>

								<div class="modal fade" id="Modal_Wilayah_Mesin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header bg-primary">
													<h9 class="modal-title" id="exampleModalLabel">Status Mesin Berdasarkan Wilayah</h9>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
													</button>
											</div>
											<div class="modal-body">
												<div class="form-group row">
													<canvas  id="barfull" width="20" height="7"></canvas>
													<div class="card-body table-responsive">
														<table id="mydata" class="table table-bordered table-striped"  style="font-size:10px">
															<tr>
																<td><label>Status EJ</label></td>
																	<?php
																	for($i=0;$i<count($bardataregions);$i++){
																	?>
																	<td><?php echo $bardataregions[$i]['regionAlias']; }?></td>
															</tr>
															<tr>
																<td><label>Update</label></td>
																	<?php for($i=0;$i<count($bardataregions);$i++){?>
																	<td><?php echo number_format($bardataregions[$i]['Update'],0,',','.'); } ?></td>
															</tr>
															<tr>
																<td><label>Tidak Update</label></td>
																	<?php for($i=0;$i<count($bardataregions);$i++){?>
																	<td><?php echo number_format($bardataregions[$i]['notUpdate'],0,',','.');}?></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="mySlides" style="width:100%">
				<section class="content">
					<div class="container-fluid">
						<div class="card">
							<div class="card-header">
								<div class="d-flex justify-content-between">
								<h3 class="card-title"><b>Kartu Tertelan</b></h3>

							</div>
						</div>
					<div>
							<div class="row">
								<div class="col-sm-12">
									<h9 style="font-family:calibri;"> <center><label>Kartu Tertelan Semua wilayah</label> <a data-toggle="modal" data-target="#Modal_kartu_Wilayah_Tertelan" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center> </h9>
										<canvas  id="bar5" width="100" height="15"></canvas>
										<div class="modal fade" id="Modal_kartu_Wilayah_Tertelan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
															<div class="modal-header bg-primary">
																	<h9 class="modal-title" id="exampleModalLabel">Kartu Tertelan Berdasarkan Wilayah</h9>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
																	</button>
															</div>
															<div class="modal-body">
																	<div class="form-group row">
																		<canvas  id="bar5full" width="20" height="7"></canvas>
																			<div class="card-body table-responsive">
																				<table id="mydata" class="table table-bordered table-striped"  style="font-size:10px">
																					<tr>
						                            <td><label>Kartu Tertelan</label></td>
						                              <?php
						                              for($i=0;$i<count($cardRetainedRegions);$i++){
						                              ?>
						                              <td><?php echo $cardRetainedRegions[$i]['regionAlias']; }?></td>
						                          </tr>

						                          <tr>
						                            <td><label>Jumlah</label></td>
						                              <?php for($i=0;$i<count($cardRetainedRegions);$i++){?>
						                              <td><?php echo $cardRetainedRegions[$i]['CardRetained']; } ?></td>
						                          </tr>
																				</table>
																			</div>
																		</div>
																</div>
														</div>
												</div>
											</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6">
									<h9 style="font-family:calibri;"> <center><label>Kartu Tertelan Berdasarkan Merek Mesin</label></center> </h9>
									<canvas id="horbar2" width="90" height="30"></canvas>
								</div>

								<div class="col-sm-6">
									<h9 style="font-family:calibri;"> <center><label>Kartu Tertelan Berdasarkan Bank </label> <a data-toggle="modal" data-target="#Modal_kartu_tertelan_bank" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center> </h9>
									<?php
									$cardretain = count($CardRetainedNamaBanktop15s);
									if ($cardretain > 0){
									?>
									<div class="card-body table-responsive">
												<table id="mydata" class=" table-bordered table-striped" style="font-size:10px" align="center">
														<tr>
															<td style="width:120px"><label>Nama Bank</label></td>
															<td style="width:80px"><label>Jumlah</label></td>
															<td style="width:70px"><label>Persentase</label></td>
														</tr>
															<?php
															$cardretain15s = count($CardRetainedNamaBanktop15s);
															//echo $cardretain;
															for($i=0;$i<$cardretain15s;$i++){
															?>
															<tr height="8">
															<td><?php
															echo $CardRetainedNamaBanktop15s[$i]['bankName']; ?></td>
															<td><?php
															echo $CardRetainedNamaBanktop15s[$i]['Card Retained'];  ?></td>
															<td><?php

															if($i == $cardretain15s){
															echo "100 %";
															}
															else{
															echo  round($CardRetainedNamaBanktop15s[$i]['Card Retained']/$CardRetainedNamaBanktop15s[$cardretain15s-1]['Card Retained']*100,2)." %";}
															}
														?> </td>
															</tr>
													</table>
									</div>
									<?php
								}else{
									?>
									<div class="card-body table-responsive">
										<table id="mydata" class="table table-bordered table-striped"  style="font-size:10px">
											<tr>
												<td><label>Nama Bank</label></td>
												<td><label>Jumlah</label></td>
												<td><label>Persentase</label></td>
											</tr>
											<tr>
												<td>Data tidak ditemukan</td>
												<td>Data tidak ditemukan</td>
												<td>Data tidak ditemukan</td>
											</tr>
										</table>
									</div>
								<?php }; ?>
								</div>
								<div class="modal fade" id="Modal_kartu_tertelan_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
													<div class="modal-header bg-primary">
															<h9 style="font-family:calibri;" class="modal-title" id="exampleModalLabel">Total Kartu Tertelan Berdasarkan Bank</h9>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
															</button>
													</div>
													<div class="modal-body">
															<div class="form-group row">
																<canvas  id="barfull" width="20" height="7"></canvas>
																	<?php
																	 $cardretains = count($CardRetainedNamaBanks);
																	 if ($cardretains >= 0 && $cardretains['bankName'] == "Total"){
																	 ?>

																		<div class="card-body table-responsive">
																			<table id="mydata" class="table table-bordered table-striped"  style="font-size:10px">
																				<tr>
																					<td style="width:120px"><label>Nama Bank</label></td>
																					<td style="width:80px"><label>Jumlah</label></td>
																					<td style="width:70px"><label>Persentase</label></td>
																				</tr>
																					<?php
																					//echo $cardretain;
																					for($i=0;$i<$cardretains;$i++){
																					?>
																				<tr height="8">
																					<td><?php
																					echo $CardRetainedNamaBanks[$i]['bankName']; ?></td>
																					<td><?php
																					echo $CardRetainedNamaBanks[$i]['Card Retained'];  ?></td>
																					<td><?php

																					if($i == $cardretains){
																					echo "100 %";
																					}
																					else{
																					echo  round($CardRetainedNamaBanks[$i]['Card Retained']/$CardRetainedNamaBanks[$cardretains-1]['Card Retained']*100,2)." %";}
																					}
																				?> </td>
																				</tr>
																			</table>
																		</div>

																		 <?php
																			}else{
																			  ?>
																				<div class="card-body table-responsive">
																					<table id="mydata" class="table table-bordered table-striped"  style="font-size:10px">
																						<tr>
																							<td style="width:120px"><label>Nama Bank</label></td>
																							<td style="width:80px"><label>Jumlah</label></td>
																							<td style="width:70px"><label>Persentase</label></td>
																						</tr>
																						<tr>
																							<td style="width:120px"><label>Data Tidak Ada</label></td>
																							<td style="width:80px"><label>Data Tidak Ada</label></td>
																							<td style="width:70px"><label>Data Tidak Ada</label></td>
																						</tr>
																					</table>
																				</div>

																		<?php }; ?>
															</div>

													</div>
											</div>
									</div>
							</div>
							</div>
				</div>

				</div>
			</div>
		</section>
	</div>

		<div class="mySlides" style="width:100%">
			<section class="content">
				<div class="container-fluid">
					<div class="card">
						<div class="card-header">
							<div class="d-flex justify-content-between">
								<h3 class="card-title"><b>Status Transaksi</b></h3>
							</div>
						</div>
						<div>
							<!-- Main row -->
							<div class="row">
								<div class="col-sm-8">
									<div class="row">
										<div class="col-sm-12">
											<h9 style="font-family:calibri;"> <center><label>Transaksi Sukses/Gagal Berdasarkan Wilayah</label> <a data-toggle="modal" data-target="#Modal_Sukses_Gagal" class="btn btn-sm btn-light btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a> </center></h9>
											<canvas  id="bar3" width="10" height="3"></canvas>

											<div class="modal fade" id="Modal_Sukses_Gagal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header bg-primary">
																<h9 class="modal-title" id="exampleModalLabel">Transaksi Sukses/Gagal Berdasarkan Wilayah</h9>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
																</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<canvas  id="bar3full" width="20" height="7"></canvas>
																<div class="card-body table-responsive">
																	<table id="mydata" class="table table-bordered table-striped" style="font-size:10px">
																		<tr>
																			<td><label>Status EJ</label></td>
																			<?php
																			for($i=0;$i<count($regiontrxs);$i++){
																				if($i%2==0){
																			?>
																			<td><?php echo $regiontrxs[$i]['Region']; }}?></td>
																		</tr>
																		<tr>
																			<td>Gagal</td>
																			<?php
																			for($i=0;$i<count($regiontrxs);$i++){
																				if($regiontrxs[$i]['Status'] == "Gagal"){
																			?>
																			<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																		</tr>
																		<tr>
																			<td>Sukses</td>
																			<?php
																			for($i=0;$i<count($regiontrxs);$i++){
																				if($regiontrxs[$i]['Status'] == "Sukes"){
																			?>
																			<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-4">
											<center><div class="card-body table-responsive">
												<table id="mydata" class=" table-bordered table-striped" style="font-size:10px">
													<tr>
														<td style="width:130px">Total Transaksi Gagal</td>
														<td style="width:80px"><?php echo $totalTrxGagal ?></td>
													</tr>
													<tr>
														<td>Total Transaksi Sukses</td>
														<td><?php echo number_format( $totalTrxSukses,0,',','.'); ?></td>

													</tr>
													<tr>
														<td>Total EJ Salah</td>
														<td>-</td>
													</tr>
													<tr>
														<td>Total Pengisian ATM</td>
														<td><?php echo number_format( $totalpengisian,0,',','.'); ?></td>
													</tr>
													<tr>
														<td>Total Pengosongan CDM</td>
														<td><?php echo number_format( $totalpengosongan,0,',','.'); ?></td>
													</tr>
													<tr>
														<td>Total Card Ratained</td>
														<td>2500</td>
													</tr>
												</table>
											</div></center>
										</div>
										<div class="col-sm-4">
												<center><h9 style="font-family:calibri;"><label>Status Transaksi Gagal</label></h9></center>
												<!-- <center><h9 style="font-family:calibri;"> <label>Mapping Status Transaksi</label> </h9></center> -->
												<canvas  id="horizontalBar" width="40" height="25"></canvas>
										</div>
										<div class="col-sm-4">
											<center><div class="card-body table-responsive">
												<table id="mydata" class=" table-bordered table-striped" style="font-size:10px">
													<thead>
														<tr>
															<td style="width:150px">Jenis Transaksi</td>
															<td style="width:80px">Total</td>
														</tr>
													</thead>
													<?php
													for($i=0;$i<count($StatusTrxByJeniss);$i++){
													?>
													<tr>
														<td><?php echo $StatusTrxByJeniss[$i]['transactiongroup'];?></td>
														<td><?php echo number_format($StatusTrxByJeniss[$i]['Jumlah'],0,',','.');}?></td>
													</tr>
												</table>
											</div></center>
										</div>
									</div>
								</div>

								<div class="col-sm-4">

									<div class="row">
										<div class="col-sm-6">
										<h9 style="font-family:calibri;"><center><label>Total Pengisian ATM</label></center></h9>
											<canvas  id="bar71" width="15" height="5"></canvas>
										</div>
										<div class="col-sm-6">
											<h9 style="font-family:calibri;"><center><label>Total Pengosongan CDM</label></center></h9>
											<canvas  id="bar72" width="15" height="5"></canvas>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
										<h9 style="font-family:calibri;"><center><label>Total Pengisian CRM</label></center></h9>
											<canvas  id="bar73" width="15" height="5"></canvas>
										</div>
										<div class="col-sm-6">
											<!-- <h9 style="font-family:calibri;"><center><label>Total Pengosongan CRM</label></center></h9>
											<canvas  id="bar74" width="15" height="5"></canvas> -->
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<h9 style="font-family:calibri;"><center><label>Top 10 Machine Bad Performance</label></center></h9>
										</div>
									<canvas  id="horbar4" width="40" height="14.5"></canvas>
									</div>
									<div class="row">
										<div class="col-sm-12">
										<h9 style="font-family:calibri;"><center><label>Top 10 Machine Good Performance</label></center></h9>
										</div>
									<canvas  id="horbar5" width="40" height="14.5"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>


<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span>
  <span class="dot" onclick="currentSlide(2)"></span>
  <span class="dot" onclick="currentSlide(3)"></span>
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>

</body>
`
