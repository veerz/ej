<!-- Content Wrapper. Contains page content -->
<style>
select:required:invalid {
  color: gray;
}
option[value=""][disabled] {
  display: none;
	color: black;
}
option {
  color: black;
}
#adadata{
	display:none;
}
</style>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
								<h1 class="m-0 text-dark">Status Transaksi</h1>
        				</div>
								<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Status Transaksi</li>
                    </ol>
                </div>
        <!-- /.container-fluid -->
						</div>
				</div>
		</div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
							<form method="post">
							<!-- <form action="<?php echo base_url('StatTrans/getData') ?>" method="post"> -->
                <div class="card-header">
									<div>
                      <h6><b>Cari berdasarkan : </b></h6>
                  </div>
                    <div class="d-flex justify-content-between">
                      <table class="card-body table-responsive">
												<tr>
                        <td>
                          <select id="definisi" name="definisi" class="form-control select2" style="width:240px">
                            <option selected disabled>Definisi Transaksi</option>
														<option value="2">Gagal</option>
														<option value="1">Sukses</option>

													</select>
                        </td>
                        <td>
                          <select id="region" name="region" class="form-control select2" style="width:240px">
                           <option selected disabled>Wilayah</option>
																<?php
								    							foreach($region as $region){
								    								?>
								    							<option  value="<?php echo $region['kanwil'] ?>">
								    							<?php echo $region['kanwil']; ?>
								    							</option>
								    							<?php
								    							}
								    							?>
								    						</select>
												</td>
                        <td>
                          <select id="area" name="area" class="form-control select2" style="width:240px">
															<option selected disabled>Area</option>
																<?php
								    							foreach($area as $area){
								    								?>
								    							<option  value="<?php echo $area['area'] ?>">
								    							<?php echo $area['area']; ?>
								    							</option>
								    							<?php
								    							}
								    							?>
								    						</select>
															</td>
													<td>
													  <select id="managerType" name="managerType" class="form-control select2" style="width:240px">
														  <option selected disabled>Jenis Pengelola</option>
															<option value="Vendor-">Vendor</option>
															<option value="Branch-">Branch</option>
														</select>
													</td>
												</tr>
											<tr>
                        <td>
                          <select id="managedBy" name="managedBy" class="form-control select2" style="width:240px">
	                        <option selected disabled>Nama Pengelola</option>
														<?php
															foreach($managedBy as $managedBy){
																?>
															<option  value="<?php echo $managedBy['managedBy'] ?>">
															<?php echo $managedBy['managedBy']; ?>
															</option>
															<?php
															}
															?>
													  </select>
												</td>
                        <td>
                          <select id="terminalType" name="terminalType" class="form-control select2" style="width:240px">
                            <option selected disabled>Jenis Mesin</option>
														<?php
						    							foreach($terminalType as $terminalType){
						    								?>
						    							<option  value="<?php echo $terminalType['terminalType'] ?>">
						    							<?php echo $terminalType['terminalType']; ?>
						    							</option>
						    							<?php
						    							}
						    							?>
						    						</select>
													</td>
                        <td>
                          <select id="merk" name="merk" class="form-control select2" style="width:240px">
													<option selected disabled>Merk</option>
													<?php
					    							foreach($merk as $merk){
					    								?>
					    							<option  value="<?php echo $merk['merk'] ?>">
					    							<?php echo $merk['merk']; ?>
					    							</option>
					    							<?php
					    							}
					    							?>
					    						</select>
												</td>
											</tr>
											<tr>
											</tr>
                      </table>
                    </div>
                    <div class="row">
                      <button type="button" class="btn btn-primary" id="filter" style="margin-left:2%">Submit</button>
										  <span style="font-size:12px;color:green"><?php echo $params; ?></span>
                      <button type="button" id="submitreset" class="btn btn-danger"style="margin-left:0.2%">Reset</button>
                      <div id="tempsearch" style="margin-left:1%"></div>
                    </div>
                	</div>
								</form>

									<div id="nodata" style="margin-left:1%">
										
                 </div> 
<div class="card-body table-responsive"  id="adadata">
<!-- Main row -->
	<!-- <div id="table"> -->
		<div class="row">
    		<div class="col-sm-12">
            <h5> <center><label>Transaksi Sukses/Gagal Berdasarkan Wilayah</label></center> </h5>
						<div id="chartReport1">
								<canvas  id="bar3" width="60" height="30"></canvas>
						</div>
						<div id="regiondata" class="card-body table-responsive">
												<!-- ini isinya table di controller -->
						</div>
			</div>
		</div>

        	<div class="row">
							<!-- <div id="regiondata" class="card-body table-responsive">
										ini isinya table di controller
							</div> -->
							<div class="col-sm-3" id="tablegagalsukses"></div>
							<div class="col-sm-4">
								<h5> <center><label>Status Transaksi Gagal</label></center> </h5>
								<div id="chartReport2">
								<canvas  id="horizontalBar" width="60" height="50"></canvas>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<div class="col-sm-6">
										<h9 style="font-family:calibri;"><center><label>Total Pengisian ATM</label></center></h9>
										<div id="chartReport71">
											<canvas  id="bar71" width="15" height="5"></canvas>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<h9 style="font-family:calibri;"><center><label>Total Pengisian CRM</label></center></h9>
												<div id="chartReport73">
													<canvas  id="bar73" width="15" height="5"></canvas>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<h9 style="font-family:calibri;"><center><label>Total Pengosongan CDM</label></center></h9>
										<div id="chartReport72">
											<canvas  id="bar72" width="15" height="5"></canvas>
										</div>
									</div>
									</div>
							</div>
					</div>
					
					<br><br>
						<div class="row">
							<div class="col-sm-4">
							<h5><center><label>Top 10 Machine Bad Performance</label></center> </h5>
								<div id="chartReport4">
										<canvas  id="horbar4" width="20" height="30"></canvas>
								</div>
							</div>

							<div class="col-sm-4">
							<h5><center><label>Top 10 Machine Good Performance</label></center> </h5>
								<div id="chartReport5">
										<canvas  id="horbar5" width="20" height="30"></canvas>
								</div>
							</div>

							<div class="col-sm-4">
								<h5><center><label>Jenis Transaksi</label></center> </h5>
									<div id="datajenistrans" class="card-body table-responsive">
									</div>
									</div>
							</div>
						</div>
    			</div>
    		</div>
			</div>
		</div>