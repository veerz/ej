<!-- Content Wrapper. Contains page content -->
<style>
select:required:invalid {
  color: gray;
}
option[value=""][disabled] {
  display: none;
}
option {
  color: black;
}
#adadata{
	display:none;
}
</style>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
								<h1 class="m-0 text-dark">Status Mesin H+0</h1>
        				</div>
								<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Status Mesin H+0</li>
                    </ol>
                </div>
        <!-- /.container-fluid -->
						</div>
				</div>
		</div>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
							<form method="post">
								<div class="card-header"> <!--Filter start-->

								  <div>
									  <h6><b>Cari berdasarkan : </b></h6>
								  </div>
									<div class="d-flex justify-content-between">
									  <table class="card-body table-responsive">
										  <tr>
											<td>
											  <select id="status" name="status" class="form-control select2" style="width:240px">
												<option selected disabled>Status</option>
												<option value="Update">Update</option>
												<option value="notUpdate">Tidak Update</option>
												<!-- harus di cek lagi ini, soalnya antara notUpdate atau NotUpdate -->
											  </select>
											</td>
											<td>
												<select id="region" name="region" class="form-control select2" style="width:240px">
													  <option selected disabled>Wilayah</option>
													  <?php
																	foreach($region as $region){
																		?>
																	<option  value="<?php echo $region['kanwil'] ?>">
																	<?php echo $region['kanwil']; ?>
																	</option>
																	<?php
																	}
																	?>
												</select>
										  </td>
											<td>
												<select id="area" name="area" class="form-control select2" style="width:240px">
													  <option selected disabled>Area</option>
													  <?php
																	foreach($area as $area){
																		?>
																	<option  value="<?php echo $area['area'] ?>">
																	<?php echo $area['area']; ?>
																	</option>
																	<?php
																	}
																	?>
												</select>
											</td>
											<td>
												<select id="managerType" name="managerType" class="form-control select2" style="width:240px">
														<option selected disabled>Jenis Pengelola</option>
														<option value="Vendor">Vendor</option>
														<option value="Branch">Branch</option>
												</select>
											</td>
										</tr>
										<td>
											<select id="managedBy" name="managedBy" class="form-control select2" style="width:240px">
													<option selected disabled>Nama Pengelola</option>
													<?php
																foreach($managedBy as $managedBy){
																	?>
																<option  value="<?php echo $managedBy['managedBy'] ?>">
																<?php echo $managedBy['managedBy']; ?>
																</option>
																<?php
															}
																?>
											</select>
										</td>
												<td>
													<select id="terminalType" name="terminalType" class="form-control select2" style="width:240px">
													  <option selected disabled>Jenis Mesin</option>
													  <?php
																	foreach($terminalType as $terminalType){
																		?>
																	<option  value="<?php echo $terminalType['terminalType'] ?>">
																	<?php echo $terminalType['terminalType']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
																</td>
																<td>
													<select id="merk" name="merk" class="form-control select2" style="width:240px">
													  <option selected disabled>Merk</option>
													  <?php
																	foreach($merk as $merk){
																		?>
																	<option  value="<?php echo $merk['merk'] ?>">
																	<?php echo $merk['merk']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
																</td>
																<td>
													<select id="connectivityType" name="connectivityType" class="form-control select2" style="width:240px">
													  <option selected disabled>Jarkom</option>
													  <?php
																	foreach($connectivityType as $connectivityType){
																		?>
																	<option  value="<?php echo $connectivityType['connectivityType'] ?>">
																	<?php echo $connectivityType['connectivityType']; ?>
																	</option>
																	<?php
																	}
																	?>
																</select>
																</td>
															</tr>
									  </table>
									</div>
                  <div class="row">
									  <button type="button" class="btn btn-primary" id="filter" style="margin-left:2%">Submit</button>
									 <span style="font-size:12px;color:green"><?php echo $params; ?></span>
                   <button type="button" id="submitreset" class="btn btn-danger"style="margin-left:0.2%">Reset</button>
                   <div id="tempsearch" style="margin-left:1%"></div>
                 </div>
									 </div>
				 			 </form>

								<div id="nodata" style="margin-left:1%">
										
                 </div> 
			<div class="card-body table-responsive" id="adadata">
                <div class="row">
                    <div class="col-sm-4">
                      <h5> <center><label>Status RMM Agent</label></center> </h5>
											<div id="chartReport1">
                      		<canvas id="pi" width="90" height="65"></canvas>
											</div>
                      <div id="piedata" class="card-body table-responsive">

                      </div>
                      <br>
                </div>

    <div class="col-sm-8" >
                <h5> <center><label>Status Mesin Berdasarkan Merk</label></center> </h5>
								<div id="chartReport2">
                  <canvas  id="bar2" width="59" height="23"></canvas>
									</div>
                <div id="merkdata" class="card-body table-responsive">

                </div>
                 
                </div>

                <div class="col-sm-12" style="margin-top:30px;">
								<h5> <center><label>Status Mesin Berdasarkan Wilayah</label></center> </h5>
													<div id="chartReport3">
								  <canvas  id="bar" width="60" height="30" ></canvas>
												</div>
								<div id="regiondata" class="card-body table-responsive">
									  <!-- ini isinya table di controller -->
								</div>
                </div>
            </div>
			<div class="row">
									  <button type="button" class="btn btn-primary" id="ecsv" style="margin-left:2%">Export Excel</button>
									  <button type="button" class="btn btn-primary" id="epdf" style="margin-left:2%">Export PDF</button>
                   <div id="tempsearch" style="margin-left:1%"></div>
                 </div>
		</div>
          </div>
          </section>
    </div>
