<style type="text/css">
	canvas{
		position: outside;
	}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Home</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex justify-content-between">
										<h3 class="card-title"><label>Status Mesin</label></h3>
									</div>
								</div>
								<!-- /.card-header -->
								<!-- Main content -->
								<section class="content">
									<div class="container-fluid">
											<!-- Main row -->
											<div class="row">
													<div class="col-sm-4">
														<h5> <center><label>Status RMM Agent</label></center> </h5>
														<canvas id="pi" width="90" height="65"></canvas>
														<br/>
														<div id="piedata" class="card-body table-responsive">
														</div>
													</div>

			 							<div class="col-sm-8">
											<h5> <center><label>Status Mesin Berdasarkan Merek</label></center> </h5>
												<canvas  id="bar2" width="59" height="23"></canvas>
											<div id="merkdata" class="card-body table-responsive">
                			</div>
											</div>
											<div class="col-sm-12" style="margin-top:30px;">
											<h5> <center><label>Status Mesin Berdasarkan Wilayah</label></center> </h5>
												<canvas  id="bar" width="60" height="30" ></canvas>
											<div id="regiondata" class="card-body table-responsive">
									  <!-- ini isinya table di controller -->
											</div>
									</div>
                </div>
								</section>
					<!-- /.container-fluid -->
				</div>
      </div>
    </div>
	</div>
</section>
<!--
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<div class="d-flex justify-content-between">
									<h3 class="card-title"><label>Kartu Tertelan</label></h3>
								</div>
						</div>

							<div class="row">
								<div class="col-sm-12">
									<h5> <center><label>Kartu Tertelan Semua Wilayah</label></center> </h5>
									<canvas  id="bar5" width="60" height="30"></canvas>
									<div class="card-body table-responsive">
															<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
																<tr>
																	<td><label>Kartu Tertelan</label></td>
																		<?php
																		for($i=0;$i<count($cardRetainedRegions);$i++){
																		?>
																		<td><?php echo $cardRetainedRegions[$i]['regionAlias']; }?></td>
																</tr>

														<tr>
															<td><label>Jumlah</label></td>
																<?php for($i=0;$i<count($cardRetainedRegions);$i++){?>
																<td><?php echo number_format($cardRetainedRegions[$i]['CardRetained'],0,',','.'); } ?></td>
														</tr>
															</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6">
										<h5> <center><label>Total Kartu Tertelan Berdasarkan Merek Mesin</label></center> </h5>
										<canvas  id="horbar2" width="90" height="55"></canvas>
									</div>
									<div class="col-sm-6">
						           <h5> <center><label>Total Kartu Tertelan Berdasarkan Bank  </label> <a data-toggle="modal" data-target="#Modal_kartu_tertelan_bank" class="btn btn-sm btn-info btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center> </h5>

	                     <?php
	                     $cardretain = count($CardRetainedNamaBanktop15s);
	                     if ($cardretain > 0){
	                     ?>
						                <div class="card-body table-responsive">
	                  						<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
	                  						  <tr>
	                  							<td><label>Nama Bank</label></td>
	                  							<td><label>Jumlah</label></td>
	                  							<td><label>Persentase</label></td>
	                  						  </tr>
	                  							<?php
	                  							//echo $cardretain;
	                  							for($i=0;$i<$cardretain;$i++){
	                  							?>
	                  							<tr>
	                  							<td><?php
	                  							echo $CardRetainedNamaBanktop15s[$i]['bankName']; ?></td>
	                  							<td><?php
	                  							echo number_format($CardRetainedNamaBanktop15s[$i]['Card Retained'],0,',','.');  ?></td>
	                  							<td><?php

	                  							if($i == $cardretain){
	                  							echo "100 %";
	                  							}
	                  							else{
	                  							echo  round($CardRetainedNamaBanktop15s[$i]['Card Retained']/$CardRetainedNamaBanktop15s[$cardretain-1]['Card Retained']*100,2)." %";}
	                  							}
	                  						  ?> </td>
	                  							</tr>
	                  						</table>
	              					  </div>
	                          <?php
	                        }else{
	                          ?>
								<div class="card-body table-responsive">
									<table id="mydata" class="table table-bordered table-striped"  style="font-size:12px">
										<tr>
											<td><label>Nama Bank</label></td>
											<td><label>Jumlah</label></td>
											<td><label>Persentase</label></td>
										</tr>
										<tr>
											<td>Data tidak ditemukan</td>
											<td>Data tidak ditemukan</td>
											<td>Data tidak ditemukan</td>
										</tr>
									</table>
								</div>
	                        <?php }; ?>
	                      </div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</section>


		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<div class="d-flex justify-content-between">
									<h3 class="card-title"><label>Status Transaksi</label></h3>
								</div>
							</div>


				<div class="row">
					<div class="col-sm-12">
            <h5> <center><label>Transaksi Sukses/Gagal Berdasarkan Wilayah</label></center> </h5>
						<canvas  id="bar3" width="60" height="30"></canvas>
						<div class="card-body table-responsive">
															<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
																<tr>
																	<td><label>Status EJ</label></td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($i%2==0){
																	?>
																	<td><?php echo $regiontrxs[$i]['Region']; }}?></td>
																</tr>
																<tr>
																	<td>Gagal</td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($regiontrxs[$i]['Status'] == "Gagal"){
																	?>
																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																</tr>
																<tr>
																	<td>Sukses</td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($regiontrxs[$i]['Status'] == "Sukes"){
																	?>
																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																</tr>
															</table>
													</div>
							</div>
						</div>

              <div class="row">
      					   <div class="col-sm-4">
									 <div class="card-body table-responsive">
											<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
												<tr>
													<td><label>Total Transaksi Gagal</label></td>

													<td><?php echo number_format($totalTrxGagal,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Transaksi Sukses</label></td>
													<td><?php echo number_format($totalTrxSukses,0,',','.') ?></td>

												</tr>
												<tr>
													<td><label>Total EJ Salah</label></td>
													<td>-</td>
												</tr>
												<tr>
													<td><label>Total Pengisian ATM</label></td>
													<td><?php echo number_format($totalpengisian,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Pengosongan CDM</label></td>
													<td><?php echo number_format($totalpengosongan,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Kartu Tertelan</label></td>
													<td>2.500</td>
												</tr>
											</table>
										</div>
								</div>
								<div class="col-sm-4">
									<h5> <center><label>Mapping Status Transaksi</label></center> </h5>
									<canvas  id="horizontalBar" width="60" height="50"></canvas>
								</div>
								<div class="col-sm-4">
									<h5> <center><label>Pengisian / Pengosongan Mesin</label></center> </h5>
									<canvas  id="bar7" width="60" height="50"></canvas>
								</div>
						</div>


						<div class="row">
							<div class="col-sm-4">
							<h5> <center><label>Top 10 Machine Bad Perfomance</label></center> </h5>
								<canvas  id="horbar4" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
							<h5> <center><label>Top 10 Machine Good Perfomance</label></center> </h5>
								<canvas  id="horbar5" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
								<h5><center><label>Jenis Transaksi</label></center> </h5>
								<canvas  id="pie5" width="15" height="15"></canvas>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
</section>-->
</div>
