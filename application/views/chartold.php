<?php
/*$updatePie = $piedataupdate[0]['Update'];
$nonupdatePie = $piedataupdate[0]['notUpdate'];
$totalupdatePie = $updatePie + $nonupdatePie;

//ini yg lama
// $totalupdatePie = $piedataupdate[0]['Total'];
// $updatePie = $piedataupdate[0]['Update'];
// $nonupdatePie = $totalupdatePie - $updatePie;
$nonupdatePiepercentage = $nonupdatePie / $totalupdatePie * 100;
$updatePiepercentage = $updatePie / $totalupdatePie * 100;
$nilai = $totalupdatePie.",".$updatePie.",".$nonupdatePie;
/*========================================================
$bardataMerks = $bardataMerk;
$bardataMerk=json_encode($bardataMerk);
/*========================================================
$bardataregions = $bardataregion;
$bardataregion =json_encode($bardataregion);
========================================================
$cardRetainedRegions = $cardRetainedRegion;
$cardRetainedRegion =json_encode($cardRetainedRegion);
/*========================================================
$cardRetainedMerks = $cardRetainedMerk;
$cardRetainedMerk =json_encode($cardRetainedMerk);
/*========================================================
$CardRetainedNamaBanks = $CardRetainedNamaBank;
$CardRetainedNamaBank =json_encode($CardRetainedNamaBank);
/*========================================================
$CardRetainedNamaBanktop15s = $CardRetainedNamaBanktop15;
$CardRetainedNamaBanktop15 =json_encode($CardRetainedNamaBanktop15);
/*========================================================
$Replenishments = $Replenishment;
$Replenishment =json_encode($Replenishment);
$totalpengisian = 0;
$totalpengosongan = 0;
for($i=0;$i<count($Replenishments);$i++){
	if($Replenishments[$i]['JenisTrx'] == "Pengisian")
	$totalpengisian  += $Replenishments[$i]['Total'];
	else
	$totalpengosongan += $Replenishments[$i]['Total'];
}

$StatusTrxMapping =json_encode($StatusTrxMapping);
/*========================================================*/
/*
$StatusTrxBad =json_encode($StatusTrxBad);
========================================================

$StatusTrxGood =json_encode($StatusTrxGood);

/*========================================================
$StatusTrxByJenis =json_encode($StatusTrxByJenis);


/* for($i=0;$i<count($regiontrx);$i++){
	if($regiontrx[$i]['Status'] == "Gagal")
	$totalTrxGagal  = $regiontrx[$i]['Jumlah'];
	else
	$totalTrxSukses = $regiontrx[$i]['Jumlah'];
} */

/* 
$regiontrxs = $regiontrx;
$regiontrx =json_encode($regiontrx);
$totalTrxGagal = 0;
$totalTrxSukses = 0;
for($i=0;$i<count($regiontrxs);$i++){
	if($regiontrxs[$i]['Status'] == "Gagal")
	$totalTrxGagal  += $regiontrxs[$i]['Jumlah'];
	else
	$totalTrxSukses += $regiontrxs[$i]['Jumlah'];
}
 */

/*========================================================*/


?>

<script type="text/javascript">
/* var bardataMerk = <?php echo $bardataMerk; ?>;
var bardataregion = <?php echo $bardataregion; ?>;
var cardRetainedRegion = <?php echo $cardRetainedRegion; ?>;
var cardRetainedMerk = <?php echo $cardRetainedMerk; ?>;
var CardRetainedNamaBank = <?php echo $CardRetainedNamaBank; ?>;
var CardRetainedNamaBanktop15 = <?php echo $CardRetainedNamaBanktop15; ?>;
var Replenishment = <?php echo $Replenishment; ?>;
var StatusTrxMapping = <?php echo $StatusTrxMapping; ?>;
var StatusTrxBad = <?php echo $StatusTrxBad; ?>;
var StatusTrxGood = <?php echo $StatusTrxGood; ?>;
var StatusTrxByJenis = <?php echo $StatusTrxByJenis; ?>; 
var regiontrx = <?php echo $regiontrx; ?>;*/
</script>

<style type="text/css">
	canvas{
		position: outside;
	}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">DASHBOARD</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"> <a href="#">Home</a> </li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex justify-content-between">
										<h3 class="card-title"><label>Status Mesin</label></h3>
									</div>
								</div>
								<!-- /.card-header -->
								<!-- Main content -->
								<section class="content">
									<div class="container-fluid">
											<!-- Main row -->
											<div class="row">
													<div class="col-sm-4">
														<h5> <center><label>Status RMM Agent</label></center> </h5>
														<canvas id="pi" dataval="<?php echo $nilai?>" width="90" height="65"></canvas>
														<br/>
														<div class="card-body">
															<table id="mydata" class="table table-striped table-bordered"  style="font-size:12px">
																<tr>
																	<td><label>Status EJ</label></td>
																	<td><label>Jumlah</label></td>
																	<td><label>Persentase</label></td>
																</tr>
																<tr>
																	<td>Update</td>
																	<td><?php echo number_format($updatePie,0,',','.') ?></td>
																	<td><?php echo round($updatePiepercentage,2)?> %</td>
																</tr>
																<tr>
																	<td>Tidak Update</td>
																	<td><?php echo number_format($nonupdatePie,0,',','.')?></td>
																	<td><?php echo round($nonupdatePiepercentage,2)?> %</td>
																</tr>
																<tr>
																	<td>Total Mesin</td>
																	<td><?php echo number_format($totalupdatePie,0,',','.') ?></td>
																	<td>100%</td>
																</tr>
															</table>
														</div>
													</div>

			 							<div class="col-sm-8">
											<h5> <center><label>Status Mesin Berdasarkan Merek</label></center> </h5>
												<canvas  id="bar2" width="59" height="23"></canvas>
												<div class="card-body table-responsive">
												<input type="hidden"  id="tarikdatabarmerk"/>
													<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
													<tr>
															<td><label>Status EJ</label></td>
																<?php
																for($i=0;$i<count($bardataMerks);$i++){
																?>
																<td><?php echo $bardataMerks[$i]['merk']; }?></td>
														</tr>

														<tr>
															<td>Update</td>
																<?php for($i=0;$i<count($bardataMerks);$i++){?>
																<td><?php echo number_format($bardataMerks[$i]['Update'],0,',','.'); } ?></td>
														</tr>
														<tr>
															<td>Tidak Update</td>
																<?php for($i=0;$i<count($bardataMerks);$i++){?>
																<td><?php echo number_format($bardataMerks[$i]['notUpdate'],0,',','.');}?></td>
														</tr>
													</table>
												</div>
                      </div>
											<div class="col-sm-12" style="margin-top:30px;">
											<h5> <center><label>Status Mesin Berdasarkan Wilayah</label></center> </h5>
												<canvas  id="bar" width="60" height="30" ></canvas>

												<div class="card-body table-responsive">
															<table id="mydata" class="table table-bordered table-striped"  style="font-size:12px">
																<tr>
																	<td><label>Status EJ</label></td>
																		<?php
																		for($i=0;$i<count($bardataregions);$i++){
																		?>
																		<td><?php echo $bardataregions[$i]['regionAlias']; }?></td>
																</tr>

																<tr>
																	<td>Update</td>
																		<?php for($i=0;$i<count($bardataregions);$i++){?>
																		<td><?php echo number_format($bardataregions[$i]['Update'],0,',','.'); } ?></td>
																</tr>
																<tr>
																	<td>Tidak Update</td>
																		<?php for($i=0;$i<count($bardataregions);$i++){?>
																		<td><?php echo number_format($bardataregions[$i]['notUpdate'],0,',','.');}?></td>
																</tr>
															</table>
													</div>

											</div>
									</div>
                </div>
								</section>
					<!-- /.container-fluid -->
				</div>
      </div>
    </div>
	</div>
</section>
<!--
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<div class="d-flex justify-content-between">
									<h3 class="card-title"><label>Kartu Tertelan</label></h3>
								</div>
						</div>

							<div class="row">
								<div class="col-sm-12">
									<h5> <center><label>Kartu Tertelan Semua Wilayah</label></center> </h5>
									<canvas  id="bar5" width="60" height="30"></canvas>
									<div class="card-body table-responsive">
															<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
																<tr>
																	<td><label>Kartu Tertelan</label></td>
																		<?php
																		for($i=0;$i<count($cardRetainedRegions);$i++){
																		?>
																		<td><?php echo $cardRetainedRegions[$i]['regionAlias']; }?></td>
																</tr>

														<tr>
															<td><label>Jumlah</label></td>
																<?php for($i=0;$i<count($cardRetainedRegions);$i++){?>
																<td><?php echo number_format($cardRetainedRegions[$i]['CardRetained'],0,',','.'); } ?></td>
														</tr>
															</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6">
										<h5> <center><label>Total Kartu Tertelan Berdasarkan Merek Mesin</label></center> </h5>
										<canvas  id="horbar2" width="90" height="55"></canvas>
									</div>
									<div class="col-sm-6">
						           <h5> <center><label>Total Kartu Tertelan Berdasarkan Bank  </label> <a data-toggle="modal" data-target="#Modal_kartu_tertelan_bank" class="btn btn-sm btn-info btn-circle" data-popup="tooltip" data-placement="top" title="Full Chart"><i class="fa fa-window-maximize"></i></a></center> </h5>

	                     <?php
	                     $cardretain = count($CardRetainedNamaBanktop15s);
	                     if ($cardretain > 0){
	                     ?>
						                <div class="card-body table-responsive">
	                  						<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
	                  						  <tr>
	                  							<td><label>Nama Bank</label></td>
	                  							<td><label>Jumlah</label></td>
	                  							<td><label>Persentase</label></td>
	                  						  </tr>
	                  							<?php
	                  							//echo $cardretain;
	                  							for($i=0;$i<$cardretain;$i++){
	                  							?>
	                  							<tr>
	                  							<td><?php
	                  							echo $CardRetainedNamaBanktop15s[$i]['bankName']; ?></td>
	                  							<td><?php
	                  							echo number_format($CardRetainedNamaBanktop15s[$i]['Card Retained'],0,',','.');  ?></td>
	                  							<td><?php

	                  							if($i == $cardretain){
	                  							echo "100 %";
	                  							}
	                  							else{
	                  							echo  round($CardRetainedNamaBanktop15s[$i]['Card Retained']/$CardRetainedNamaBanktop15s[$cardretain-1]['Card Retained']*100,2)." %";}
	                  							}
	                  						  ?> </td>
	                  							</tr>
	                  						</table>
	              					  </div>
	                          <?php
	                        }else{
	                          ?>
								<div class="card-body table-responsive">
									<table id="mydata" class="table table-bordered table-striped"  style="font-size:12px">
										<tr>
											<td><label>Nama Bank</label></td>
											<td><label>Jumlah</label></td>
											<td><label>Persentase</label></td>
										</tr>
										<tr>
											<td>Data tidak ditemukan</td>
											<td>Data tidak ditemukan</td>
											<td>Data tidak ditemukan</td>
										</tr>
									</table>
								</div>
	                        <?php }; ?>
	                      </div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</section>


		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<div class="d-flex justify-content-between">
									<h3 class="card-title"><label>Status Transaksi</label></h3>
								</div>
							</div>
							

				<div class="row">
					<div class="col-sm-12">
            <h5> <center><label>Transaksi Sukses/Gagal Berdasarkan Wilayah</label></center> </h5>
						<canvas  id="bar3" width="60" height="30"></canvas>
						<div class="card-body table-responsive">
															<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
																<tr>
																	<td><label>Status EJ</label></td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($i%2==0){
																	?>
																	<td><?php echo $regiontrxs[$i]['Region']; }}?></td>
																</tr>
																<tr>
																	<td>Gagal</td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($regiontrxs[$i]['Status'] == "Gagal"){
																	?>
																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																</tr>
																<tr>
																	<td>Sukses</td>
																	<?php
																	for($i=0;$i<count($regiontrxs);$i++){
																		if($regiontrxs[$i]['Status'] == "Sukes"){
																	?>
																	<td><?php echo number_format($regiontrxs[$i]['Jumlah'],0,',','.'); }}?></td>
																</tr>
															</table>
													</div>
							</div>
						</div>

              <div class="row">
      					   <div class="col-sm-4">
									 <div class="card-body table-responsive">
											<table id="mydata" class="table table-bordered table-striped" style="font-size:12px">
												<tr>
													<td><label>Total Transaksi Gagal</label></td>

													<td><?php echo number_format($totalTrxGagal,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Transaksi Sukses</label></td>
													<td><?php echo number_format($totalTrxSukses,0,',','.') ?></td>

												</tr>
												<tr>
													<td><label>Total EJ Salah</label></td>
													<td>-</td>
												</tr>
												<tr>
													<td><label>Total Pengisian ATM</label></td>
													<td><?php echo number_format($totalpengisian,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Pengosongan CDM</label></td>
													<td><?php echo number_format($totalpengosongan,0,',','.') ?></td>
												</tr>
												<tr>
													<td><label>Total Kartu Tertelan</label></td>
													<td>2.500</td>
												</tr>
											</table>
										</div>
								</div>
								<div class="col-sm-4">
									<h5> <center><label>Mapping Status Transaksi</label></center> </h5>
									<canvas  id="horizontalBar" width="60" height="50"></canvas>
								</div>
								<div class="col-sm-4">
									<h5> <center><label>Pengisian / Pengosongan Mesin</label></center> </h5>
									<canvas  id="bar7" width="60" height="50"></canvas>
								</div>
						</div>


						<div class="row">
							<div class="col-sm-4">
							<h5> <center><label>Top 10 Machine Bad Perfomance</label></center> </h5>
								<canvas  id="horbar4" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
							<h5> <center><label>Top 10 Machine Good Perfomance</label></center> </h5>
								<canvas  id="horbar5" width="20" height="15"></canvas>
							</div>

							<div class="col-sm-4">
								<h5><center><label>Jenis Transaksi</label></center> </h5>
								<canvas  id="pie5" width="15" height="15"></canvas>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
</section>-->
</div>
