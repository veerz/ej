<?php
/**
 * MsMenu Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class menu_model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data MsMenu dari Tabel MsMenu
     *
     * @return Sql_Result ?
     */
    function getData()
    {
		//$this->db->where("module !=", 'parent');
		/* $this
            ->db
            ->order_by("name","ASC"); */
		$this
            ->db
            ->order_by("priority","ASC");
        $hasil = $this
            ->db
            ->get('settings_Menu');
        return $hasil->result();
    }
    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data MsMenu dari Tabel MsMenu
     *
     * @param string $menuID ID MsMenu
     *
     * @return Sql_Result ?
     */
    function getSingleData($id_menu)
    {
        $this
            ->db
            ->where("id_menu", $id_menu);
        $hasil = $this
            ->db
            ->get('settings_Menu');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel MsMenu
     *
     * @return Sql_Result ?
     */
    function saveData()
    {
        $data = array(
            'name'       => $this
                          ->input
                          ->post('name') ,
            'route'      => $this
                          ->input
                          ->post('route') ,
            'type'       => $this
                          ->input
                          ->post('type') ,
            'created_at' => $this
                          ->input
                          ->post('created_at', date('Y-m-d H:i:s')),
            'updated_at' => $this
                          ->input
                          ->post('updated_at', date('Y-m-d H:i:s')),
            'created_by' => 0,  //development only
            'updated_by' => 0   //development only

        );
        $result = $this
            ->db
            ->insert('settings_Menu', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data MsMenu
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $id_menu    = $this
                      ->input
                      ->post('id_menu');
        $name       = $this
                      ->input
                      ->post('name');
        $route      = $this
                      ->input
                      ->post('route');
        $type       = $this
                      ->input
                      ->post('type');
        $module     = $this
                      ->input
                      ->post('module');
        $updated_at = $this
                      ->input
                      ->post('updated_at', date('Y-m-d H:i:s'));
        $updated_by = 0;
        $this
            ->db
            ->set('name', $name);
        $this
            ->db
            ->set('route', $route);
        $this
            ->db
            ->set('type', $type);
        $this
            ->db
            ->set('module', $module);
        $this
            ->db
            ->set('updated_at', date('Y-m-d H:i:s'));
        $this
            ->db
            ->where('id_menu', $id_menu);
        $result = $this
            ->db
            ->update('settings_Menu');
        return $result;
    }
    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data MsMenu dari Database
     *
     * @param string $menuID ID MsMenu
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $id_menu = $this
                  ->input
                  ->post('id_menu');
        $this
                  ->db
                  ->where('id_menu', $id_menu);
        $result = $this
                  ->db
                  ->delete('settings_Menu');
        return $result;
    }
}
