<?php


class Listopcode_Model extends CI_Model {

	function getData()
    {
      $hasil = $this
          ->db
          ->select('OPCodes.*, transactionTypeDefinition.transactionType as transaction_type, transactionTypeDefinition.transactionGroup as transaction_group')
          ->from('OPCodes')
          ->join('transactionTypeDefinition', 'transactionTypeDefinition.transactionTypeID = OPCodes.transactionTypeID')
					->limit(200)
					->get();
      return $hasil;
        //return $this->db->get('OPCodes');
    }

	// function getDatabyTransaksi($transactionTypeID)
  //       {
  //           $this
  //               ->db
  //               ->select("id");
  //           $this
  //               ->db
  //               ->from('OPCodes');
  //           $this
  //               ->db
  //               ->where('transactionTypeID', $transactionTypeID);
  //           $result = $this
  //               ->db
  //               ->get();
  //           return $result->result_array();
  //       }

	// function getSingleData($id)
  //   {
  //       $this
  //           ->db
  //           ->where("id", $id);
  //       $hasil = $this
  //           ->db
  //           ->get('OPCodes');
  //       return $hasil->row();
  //   }

	function saveData()
  {
		$data = array(
			'transactionTypeID'	=>$this
								->input
								->post('transactionTypeID') ,
			'objectID'    			=>$this
								->input
								->post('objectID') ,

			'OPCode'     		=> $this
								->input
								->post('OPCode') ,
			       );
		$result = $this
			->db
			->insert('OPCodes', $data);
		return $result;
    }

	function updateData()
    {
		$id    			= $this
						->input
						->post('id');

    $transactionTypeID  = $this
						->input
						->post('transactionTypeID');

	$objectID 			= $this
						->input
						->post('objectID');
	$OPCode				= $this
						->input
						->post('OPCode');
		$this
					->db
					->set('transactionTypeID', $transactionTypeID);
		$this
					->db
					->set('objectID', $objectID);
		$this
					->db
					->set('OPCode', $OPCode);
		$this
					->db
					->where('id', $id);
    $result = $this
	        ->db
	        ->update('OPCodes');
    return $result;
		}



    function deleteData()
    {
        $id = $this
            ->input
            ->post('id');
        $this
            ->db
            ->where('id', $id);
        $result = $this
            ->db
            ->delete('OPCodes');
        return $result;
    }
}
