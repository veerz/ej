<?php
/**
 * previledgeGroup Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class transactionType_Model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data previledgeGroup dari Tabel previledgeGroup
     *
     * @return Sql_Result ?
     */
    function getData()
    {
        return $this->db->get('transactionTypeDefinition');
    }
}
