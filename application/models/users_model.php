<?php
/**
 * previledgeUser Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class users_model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data previledgeUser dari Tabel previledgeUser
     *
     * @return Sql_Result ?
     */
    function getData()
    {
      $hasil = $this
          ->db
          ->select('users.*, roles.name as role_name')
          ->from('users')
          ->join('roles', 'roles.id_role = users.id_role')
          ->get();
      return $hasil;
        //return $this->db->get('users');
    }

    /**
         * Function getDatabyGroupID
         *
         * Fungsi ini bertugas untuk mengambil userID berdasarkan groupID
         *
         * @return Sql_Result ?
         */
        function getDatabyRoleID($id_role)
        {
            $this
                ->db
                ->select("id_user");
            $this
                ->db
                ->from('users');
            $this
                ->db
                ->where('id_role', $id_role);
            $result = $this
                ->db
                ->get();
            return $result->result_array();
        }

    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data previledgeUser dari Tabel previledgeUser
     *
     * @param string $prvUserID ID previledgeUser
     *
     * @return Sql_Result ?
     */
    function getSingleData($id_user)
    {
        $this
            ->db
            ->where("id_user", $id_user);
        $hasil = $this
            ->db
            ->get('users');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel previledgeUser
     *
     * @return Sql_Result ?
     */
    function saveData()
    {
        $data = array(
            'id_role'     => $this
                          ->input
                          ->post('id_role') ,
            'name'      => $this
                          ->input
                          ->post('name') ,
            'username'  => $this
                          ->input
                          ->post('username') ,
            'password'   => hash('sha384', $this
                          ->input
                          ->post('password')),
            'status'     => $this
                          ->input
                          ->post('status') ,
            'created_by' => 0, //development only
            'updated_by' => 0, //development only
            'created_date'  => date('Y-m-d H:i:s'),
            'updated_date'  => date('Y-m-d H:i:s')
        );
        $result = $this
            ->db
            ->insert('users', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data previledgeUser
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $id_user    = $this
              ->input
              ->post('eid_user');
        $id_role     = $this
              ->input
              ->post('eid_role');
        $name = $this
            ->input
            ->post('ename');
        $changepass = $this
            ->input
            ->post('passcek');
        $username = $this
            ->input
            ->post('eusername');
        $password =  hash('sha384', $this
                      ->input
                      ->post('epassword'));
        $status = $this
            ->input
            ->post('estatus');
        $updated_by  = 0;
        $this
            ->db
            ->set('id_role', $id_role);
        $this
            ->db
            ->set('name', $name);
        $this
            ->db
            ->set('username', $username);
        
        if($changepass == 'checked1'){
            if (isset($password) || $password != "")
            {
            $this
                ->db
                ->set('password', $password);
            }
        }
        $this
            ->db
            ->set('status', $status);
        $this
            ->db
            ->set('updated_by', $updated_by);
        $this
            ->db
            ->set('updated_date', date('Y-m-d H:i:s'));
        $this
            ->db
            ->where('id_user', $id_user);
        $result = $this
            ->db
            ->update('users');
        return $result;
    }


    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data previledgeUser dari Database
     *
     * @param string $prvUserID ID previledgeUser
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $id_user = $this
            ->input
            ->post('id_user');
        $this
            ->db
            ->where('id_user', $id_user);
        $result = $this
            ->db
            ->delete('users');
        return $result;
    }
}
