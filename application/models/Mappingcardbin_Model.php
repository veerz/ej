<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingcardbin_Model extends CI_Model {

	// db2 digunakan untuk mengakses database ke-2
	 private $db2;

	 public function __construct()
	 {
	  parent::__construct();
			 // $this->db2 = $this->load->database('CNEnterprise1', TRUE);
	 }

	public function getData(){
		$this->db->from('MsBin');
		$this->db->order_by("bankName", "asc");
		$query = $this->db->get();
		return $query;
	}	
	// public function getData2(){
		// $this->db2->from('MSI_PREFIXNO');
		// $this->db2->order_by("BANKNAME", "asc");
		// $query = $this->db2->get();
		// return $query;

		// $this->db->order_by('bankName', 'asc');
		// $query = $this->db->get('MsBin');
		// return $query->result();


		// return $this->db->get('MsBIN');
	// }
}

/* End of file MsMachine_Model.php */
/* Location: ./application/models/MsMachine_Model.php */
