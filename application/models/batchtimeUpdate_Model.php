<?php
/**
 * previledgeGroup Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class batchtimeupdate_Model extends CI_Model
{

    	public function getData(){
		return $this->db->get('batchtimeUpdate');
	}
}

