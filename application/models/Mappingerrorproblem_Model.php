<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingerrorproblem_Model extends CI_Model {

	public function getData(){
		return $this->db->get('MappingErrorProblem');
	}
}

/* End of file MsMachine_Model.php */
/* Location: ./application/models/MsMachine_Model.php */
