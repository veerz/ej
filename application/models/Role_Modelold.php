<?php
/**
 * previledgeGroup Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class Role_model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data previledgeGroup dari Tabel previledgeGroup
     *
     * @return Sql_Result ?
     */
    function getData()
    {
        return $this->db->get('roles');
    }
    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data previledgeGroup dari Tabel previledgeGroup
     *
     * @param string $prvGroupID ID previledgeGroup
     *
     * @return Sql_Result ?
     */
    function getSingleData($id_role)
    {
        $this
            ->db
            ->where("id_role", $id_role);
        $hasil = $this
            ->db
            ->get('roles');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel previledgeGroup
     *
     * @return Sql_Result ?
     */
    function saveData()
    {
        $data = array(
            'name'        => $this
                            ->input
                            ->post('name') ,
            'description' => $this
                            ->input
                            ->post('description') ,
            'is_display'      => $this
                            ->input
                            ->post('is_display') ,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => date('Y-m-d H:i:s'),
            'created_by'    => 0, //development only
            'updated_by'    => 0
            //development only

        );
        $result = $this
            ->db
            ->insert('roles', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data previledgeGroup
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $id_role     = $this
                      ->input
                      ->post('id_role');
        $name        = $this
                      ->input
                      ->post('name');
        $description = $this
                      ->input
                      ->post('description');
        $is_display  = 0;
        $updated_by  = 0;
        $this
            ->db
            ->set('name', $name);
        $this
            ->db
            ->set('description', $description);
        $this
            ->db
            ->set('is_display', $is_display);
        $this
            ->db
            ->set('updated_at', date('Y-m-d H:i:s'));
        $this
            ->db
            ->where('id_role', $id_role);
        $result = $this
            ->db
            ->update('roles');
        return $result;
    }
    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data previledgeGroup dari Database
     *
     * @param string $prvGroupID ID previledgeGroup
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $id_role = $this
            ->input
            ->post('id_role');
        $this
            ->db
            ->where('id_role', $id_role);
        $result = $this
            ->db
            ->delete('roles');
        return $result;
    }
}
