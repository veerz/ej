<?php

class Entity_Model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data userDetail dari Tabel userDetail
     *
     * @return Sql_Result ?
     */
    function getData()
    {
		
        $hasil = $this
            ->db
            ->select('MsEntity.*, MaEntitiyType.nameEntityType')
            ->from('MsEntity')
			->join('MsEntitiyType', 'MsEntitiyType.idEntityType = MsEntity.idEntityType')
            ->get();
			
        return $hasil->result();
    }
    /**
     * Function getDatabyGroupID
     *
     * Fungsi ini bertugas untuk mengambil userID berdasarkan groupID
     *
     * @return Sql_Result ?
     */
    function getDatabyidEntityType($idEntityType)
    {
        $this
            ->db
            ->select("idEntitiy");
        $this
            ->db
            ->from('MsEntity');
        $this
            ->db
            ->where('idEntityType', $idEntityType);
        $result = $this
            ->db
            ->get();
        return $result->result_array();
    }
    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data userDetail dari Tabel userDetail
     *
     * @param string $userID ID userDetail
     *
     * @return Sql_Result ?
     */
    function getSingleData($idEntitiy)
    {
        $this
            ->db
            ->where("idEntitiy", $idEntitiy);
        $hasil = $this
            ->db
            ->get('MsEntity');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel userDetail
     *
     * @return Sql_Result ?
     */
    function saveData()
    {	
	
        $data = array(
            'idEntitiy' => $this
                ->input
                ->post('idEntitiy') ,
			'idEntityType' => $this
                ->input
                ->post('idEntityType') ,
            'nameEntity' => $this
                ->input
                ->post('nameEntity') ,
            'detailEntity' => $this
                ->input
                ->post('detailEntity') ,
            'statusEntity' => $this
                ->input
                ->post('statusEntity') ,
            /*'updateBy' => $this
                ->input
                ->post('updateBy') ,*/
			'updateby' => 1,
            'updateDate' => $this
                ->input
                ->post('updateDate') 
        );
        $result = $this
            ->db
            ->insert('MsEntity', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data userDetail
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $idEntitiy = $this
            ->input
            ->post('idEntitiy');
        $idEntityType = $this
            ->input
            ->post('idEntityType');
        $nameEntity = $this
            ->input
            ->post('nameEntity');
        $detailEntity = $this
            ->input
            ->post('detailEntity');
        $statusEntity = $this
            ->input
            ->post('statusEntity');
        $updateBy = $this
            ->input
            ->post('updateBy');
        $updateDate = $this
            ->input
            ->post('updateDate');
			
        $this
            ->db
            ->set('idEntitiy', $idEntitiy);
        $this
            ->db
            ->set('idEntityType', $idEntityType);
        $this
            ->db
            ->set('nameEntity', $nameEntity);
			$this
            ->db
            ->set('detailEntity', $detailEntity);
        $this
            ->db
            ->set('statusEntity', $statusEntity);
        $this
            ->db
            ->set('updateby', $updateby);
        $this
            ->db
            ->set('updateDate', date('Y-m-d H:i:s'));
        $result = $this
            ->db
            ->update('MsEntity');
        return $result;
    }
    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data userDetail dari Database
     *
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $idEntitiy = $this
            ->input
            ->post('idEntitiy');
        $this
            ->db
            ->where('idEntitiy', $idEntitiy);
        $result = $this
            ->db
            ->delete('MsEntity');
        return $result;
    }
}

