<?php
/**
 * previledgeUser Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class audit_model extends CI_Model
{

  function getData()
  {
      return $this->db->get('AuditTrail');
  }

  function saveData($action)
    {
        // $sql        = $this->db->insert_string('AuditTrail',$param);
        // $ex         = $this->db->query($sql);
        // return $this->db->affected_rows($sql);
        if (strtolower($action) == "login"){
            $action   = 'login';
        }
        elseif(strtolower($action) == "logout")
        {
            $action   = 'logout';
        }
        elseif(strtolower($action) == "save"){
            $action   = 'save';
        }
        elseif(strtolower($action) == "update"){
            $action  = 'update';
        }
        else{
            $action  = 'delete';
        }

        $data = array(
            'id_user'       => $CI->session->userdata('id_user'),
            'username'      => $CI->session->userdata('username'),
            'page'          => $_SERVER['PHP_SELF'],
            'action'        => $action,
            'detail'        => '',
            'created_date'  => date('Y-m-d H:i:s')
        );
        $result = $this
            ->db
            ->insert('AuditTrail', $data);
        return $result;
    }

}
