<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingcode_Model extends CI_Model {

	public function getData(){
		$hasil = $this
				->db
				->select('errorSuccessCodeDefs.*, MappingDefinisi.Definisi as MappingDefinisi_Definisi')
				->from('errorSuccessCodeDefs')
				->join('MappingDefinisi', 'MappingDefinisi.status = errorSuccessCodeDefs.status')
				->get();
		return $hasil;
		// return $this->db->get('errorSuccessCodeDefs');
	}

	function saveData()
	{
			$data = array(
					'status'     => $this
												->input
												->post('status') ,
					'code'      => $this
												->input
												->post('code') ,
					'Definition'  => $this
												->input
												->post('Definition') ,
					'isActive'     => $this
												->input
												->post('isActive')
			);
			$result = $this
					->db
					->insert('errorSuccessCodeDefs', $data);
			return $result;
	}
	/**
	 * Function updateData
	 *
	 * Fungsi ini bertugas untuk mengupdate data previledgeUser
	 *
	 * @return Sql_Result ?
	 */
	function updateData()
	{
			$errorSuccessCodeIDefsD    = $this
						->input
						->post('errorSuccessCodeIDefsD');
			$status     = $this
						->input
						->post('status');
			$code = $this
					->input
					->post('code');
			$Definition = $this
					->input
					->post('Definition');
			$isActive = $this
					->input
					->post('isActive');
			$this
          ->db
          ->set('status', $status);
			$this
					->db
					->set('code', $code);
			$this
          ->db
          ->set('Definition', $Definition);
			$this
          ->db
          ->set('isActive', $isActive);
			$this
					->db
					->where('errorSuccessCodeIDefsD', $errorSuccessCodeIDefsD);
			$result = $this
          ->db
          ->update('errorSuccessCodeDefs');
      return $result;
	}


	/**
	 * Function deleteData
	 *
	 * Fungsi ini bertugas untuk menghapus data previledgeUser dari Database
	 *
	 * @param string $prvUserID ID previledgeUser
	 *
	 * @return Sql_Result ?
	 */
	function deleteData()
	{
			$errorSuccessCodeIDefsD = $this
					->input
					->post('errorSuccessCodeIDefsD');
			$this
					->db
					->where('errorSuccessCodeIDefsD', $errorSuccessCodeIDefsD);
			$result = $this
					->db
					->delete('errorSuccessCodeDefs');
			return $result;
	}
}

/* End of file MsMachine_Model.php */
/* Location: ./application/models/MsMachine_Model.php */
