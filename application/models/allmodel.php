<?php
class Allmodel extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	function rp($angka){
	$angka = number_format($angka);
	$angka = str_replace(',', '.', $angka);
	$angka ="$angka";
	return "Rp.".$angka.",00";
	}

	function get_data($field, $table, $join = array(), $where= array(), $like = array(), $single = false,$perPage='', $uri='', $object = false, $index_field='', $order_by='DESC',$group_by = NULL)
	{
		$this->db->select($field);
		if(count($like) > 0)
			$this->db->or_like($like);
		$this->db->from($table);
		if(count($join) > 0){
			foreach($join as $key => $value){
				$this->db->join($join[$key]['table'], $join[$key]['field'],$join[$key]['method']);
			}
		}
		if(count($where) > 0)
			$this->db->where($where);
		if($group_by != NULL)
			$this->db->group_by($group_by);
		if($index_field !='')
			$this->db->order_by($index_field, $order_by);
		if($perPage!='' )
			$query =  $this->db->get('', $perPage, $uri);
		else
			$query = $this->db->get('');
		if($query->num_rows()>0){
			if($single){
				if($object)
					return $query->row();
				else
					return $query->row_array();
			}else{
				if($object)
					return $query->result();
				else
					return $query->result_array();
			}
		}else
			return false;
	}
	function query_data($query_code, $single = false)
	{
		$query = $this->db->query($query_code);
		if($query->num_rows()>0){
			if ($single) return $query->row_array();
			else return $query->result_array();
		}else
			return false;
	}
	function delete_data($table, $where)//where is an array
	{
		return $this->db->delete($table, $where);
	}

	function insert_data($table, $data){
		return $this->db->insert($table,$data);
	}
	function update_data($table, $data, $where){
		$this->db->where($where);
		return $this->db->update($table,$data);
	}

}
?>
