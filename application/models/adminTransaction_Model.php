<?php
/**
 * userGroup Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class adminTransaction_model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data userGroup dari Tabel userGroup
     *
     * @return Mysqli_result ?
     */
    function getData()
    {
        $hasil = $this
            ->db
            ->get('adminTransaction');
        return $hasil->result();
    }
    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data userGroup dari Tabel userGroup
     *
     * @param string $id ID userGroup
     *
     * @return Mysqli_result ?
     */
    function getSingleData($groupID)
    {
        $this
            ->db
            ->where("groupID", $groupID);
        $hasil = $this
            ->db
            ->get('userGroup');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel userGroup
     *
     * @return Sql_Result ?
     */
    function saveData()
    {
        $data = array(
            'groupName' => $this
                ->input
                ->post('groupName') ,
            'createby' => 1, //development only
            'updateby' => 1
            //development only
            
        );
        $result = $this
            ->db
            ->insert('userGroup', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data userGroup
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $groupID = $this
            ->input
            ->post('groupID');
        $groupName = $this
            ->input
            ->post('groupName');
        $updateby = 1; //development only
        $this
            ->db
            ->set('groupName', $groupName);
        $this
            ->db
            ->set('updateby', $updateby);
        $this
            ->db
            ->where('groupID', $groupID);
        $result = $this
            ->db
            ->update('userGroup');
        return $result;
    }
    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data userGroup dari Database
     *
     * @param string $id ID userGroup
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $groupID = $this
            ->input
            ->post('groupID');
        $this
            ->db
            ->where('groupID', $groupID);
        $result = $this
            ->db
            ->delete('userGroup');
        return $result;
    }
}

