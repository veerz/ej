<?php
/**
 * previledgeGroup Model Class Doc Comment
 *
 * @category Model
 * @package  EJBrowser
 * @author   Naufal Hakim Syahputra <naufalhsyahputra@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://ej.test
 */
class PriviledgeRole_model extends CI_Model
{

    /**
     * Function getData
     *
     * Fungsi ini bertugas untuk mengambil data previledgeGroup dari Tabel previledgeGroup
     *
     * @return Sql_Result ?
     */
    function getData()
    {
        $hasil = $this
            ->db
            ->select('priviledgeRole.*, roles.name, settings_Menu.name')
            ->from('priviledgeRole')
            ->join('roles', 'roles.id_role = priviledgeRole.id_role')
            ->join('settings_Menu', 'settings_Menu.id_menu = priviledgeRole.id_menu')
            ->get();
        return $hasil->result();
    }

    function getByRole()
    {
        $hasil = $this->db->get('roles');
        return $hasil->result();
    }
    /**
     * Function getSingleData
     *
     * Fungsi ini bertugas untuk mengambil Single data previledgeGroup dari Tabel previledgeGroup
     *
     * @param string $prvGroupID ID previledgeGroup
     *
     * @return Sql_Result ?
     */
    function getSingleData($id_prvRole)
    {
        $this
            ->db
            ->where("id_prvRole", $id_prvRole);
        $hasil = $this
            ->db
            ->get('priviledgeRole');
        return $hasil->row();
    }
    /**
     * Function saveData
     *
     * Fungsi ini bertugas untuk insert data ke Tabel previledgeGroup
     *
     * @return Sql_Result ?
     */
    function saveData()
    {
        $data = array(
            'id_role' => $this
                ->input
                ->post('id_role') ,
            'id_menu' => $this
                ->input
                ->post('id_menu') ,
            'value' => $this
                ->input
                ->post('value') ,
            'created_by' => 0, //development only
            'updated_by' => 0,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => date('Y-m-d H:i:s'),

        );
        $result = $this
            ->db
            ->insert('priviledgeRole', $data);
        return $result;
    }
    /**
     * Function updateData
     *
     * Fungsi ini bertugas untuk mengupdate data previledgeGroup
     *
     * @return Sql_Result ?
     */
    function updateData()
    {
        $id_prvRole = $this
            ->input
            ->post('id_prvRole');
        $id_role = $this
            ->input
            ->post('id_role');
        $id_menu = $this
            ->input
            ->post('id_menu');
        $value = $this
            ->input
            ->post('value');
        $updateby = 0; //development only
        $this
            ->db
            ->set('id_role', $id_role);
        $this
            ->db
            ->set('id_menu', $id_menu);
        $this
            ->db
            ->set('value', $value);
        $this
            ->db
            ->set('updated_at', date('Y-m-d H:i:s'));
        $this
            ->db
            ->where('id_prvRole', $id_prvRole);
        $result = $this
            ->db
            ->update('priviledgeRole');
        return $result;
    }
    /**
     * Function deleteData
     *
     * Fungsi ini bertugas untuk menghapus data previledgeGroup dari Database
     *
     * @param string $prvGroupID ID previledgeGroup
     *
     * @return Sql_Result ?
     */
    function deleteData()
    {
        $id_prvRole = $this
            ->input
            ->post('id_prvRole');
        $this
            ->db
            ->where('id_prvRole', $id_prvRole);
        $result = $this
            ->db
            ->delete('priviledgeRole');
        return $result;
    }
}
