<?php
class browse_Model extends CI_Model
{

    function getData()
    {
        $hasil = $this
            ->db
            /*->select('terminalID,insertedTimestamp,transactionTime,cardNumber,transactionType,amount,responseCode,ejChunk')
            ->from('transactions')*/
            ->get("transactions");
        return $hasil->result();
    }
    
	function GetBrowse($bln){
	  $query = $this->db->query("Select * from transactions");

      if ($query->num_rows() > 0) {
          return $query->result();
      }
	}
	
	function getDatabyGroupID($groupID)
    {
        $this
            ->db
            ->select("userID");
        $this
            ->db
            ->from('userDetail');
        $this
            ->db
            ->where('GroupID', $groupID);
        $result = $this
            ->db
            ->get();
        return $result->result_array();
    }
    
	function getSingleData($terminalID)
    {
        $this
            ->db
            ->where("terminalID", $terminalID);
        $hasil = $this
            ->db
            ->get('transaction');
        return $hasil->row();
    }
    
	function saveData()
    {
        $data = array(
            'groupID' => $this
                ->input
                ->post('groupID') ,
            'userName' => $this
                ->input
                ->post('userName') ,
            'password' => md5($this
                ->input
                ->post('password')) ,
            'Status' => $this
                ->input
                ->post('Status') ,
            'userEmail' => $this
                ->input
                ->post('userEmail') ,
            'lastLogin' => '',
            'createby' => 1, //development only
            'updateby' => 1, //development only
            'firstname' => $this
                ->input
                ->post('firstname') ,
            'lastname' => $this
                ->input
                ->post('lastname') ,
            'phonenumber' => $this
                ->input
                ->post('phonenumber')
        );
        $result = $this
            ->db
            ->insert('userDetail', $data);
        return $result;
    }
    
	function updateData()
    {
        $userID = $this
            ->input
            ->post('userID');
        $groupID = $this
            ->input
            ->post('groupID');
        $userName = $this
            ->input
            ->post('userName');
        $password = $this
            ->input
            ->post('password');
        $Status = $this
            ->input
            ->post('Status');
        $userEmail = $this
            ->input
            ->post('userEmail');
        $updateby = 1; //development only
        $firstname = $this
            ->input
            ->post('firstname');
        $lastname = $this
            ->input
            ->post('lastname');
        $phonenumber = $this
            ->input
            ->post('phonenumber');
        $this
            ->db
            ->set('groupID', $groupID);
        $this
            ->db
            ->set('userName', $userName);
        if (isset($password) || $password != "")
        {
            $this
                ->db
                ->set('password', $password);
        }
        $this
            ->db
            ->set('Status', $Status);
        $this
            ->db
            ->set('userEmail', $userEmail);
        $this
            ->db
            ->set('updateby', $updateby);
        $this
            ->db
            ->set('firstname', $firstname);
        $this
            ->db
            ->set('lastname', $lastname);
        $this
            ->db
            ->set('phonenumber', $phonenumber);
        $this
            ->db
            ->set('updateTime', date('Y-m-d H:i:s'));
        $this
            ->db
            ->where('userID', $userID);
        $result = $this
            ->db
            ->update('userDetail');
        return $result;
    }
    
	function deleteData()
    {
        $userID = $this
            ->input
            ->post('userID');
        $this
            ->db
            ->where('userID', $userID);
        $result = $this
            ->db
            ->delete('userDetail');
        return $result;
    }
}
?>