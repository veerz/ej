<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transtypedef_Model extends CI_Model {

	public function getData(){
		return $this->db->get('transactionTypeDefinitionTemp');
	}
}

/* End of file MsMachine_Model.php */
/* Location: ./application/models/MsMachine_Model.php */
