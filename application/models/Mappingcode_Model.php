<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappingcode_Model extends CI_Model {

	public function getData(){
		$hasil = $this
				->db
				->select('errorSuccessCodeDefs.*, MappingDefinisi.Definisi as MappingDefinisi_Definisi')
				->from('errorSuccessCodeDefs')
				->join('MappingDefinisi', 'MappingDefinisi.status = errorSuccessCodeDefs.status')
				->get();
		return $hasil;
		// return $this->db->get('errorSuccessCodeDefs');
	}

	function saveData()
	{
			$data = array(
					'status'     	=> $this->input->post('status') ,
					'code'      	=> $this->input->post('Definition') ,
					'Definition'  	=> $this->input->post('code') ,
					'isActive'     	=> $this->input->post('isActive'),
					'priority'		=>$this->input->post('priority')
			);
			$result = $this->db->insert('errorSuccessCodeDefs', $data);
			return $result;
	}
	
	function updateData()
	{
			$errorSuccessCodeIDefsD    = $this->input->post('eerrorSuccessCodeIDefsD');
			$status     = $this->input->post('estatus');
			$code = $this->input->post('eDefinition');
			$Definition = $this->input->post('ecode');
			$isActive = $this->input->post('eisActive');
			//ini priority(new)
			$priority = $this->input->post('epriority');
			$this->db->set('status', $status);
			$this->db->set('code', $code);
			$this->db->set('Definition', $Definition);
			$this->db->set('isActive', $isActive);
			//ini priority(new)
			$this->db->set('priority', $priority);
			$this->db->where('errorSuccessCodeIDefsD', $errorSuccessCodeIDefsD);
			$result = $this->db->update('errorSuccessCodeDefs');
      return $result;
	}

	function deleteData()
	{
		$errorSuccessCodeIDefsD = $this->input->post('errorSuccessCodeIDefsD');
		$this->db->where('errorSuccessCodeIDefsD', $errorSuccessCodeIDefsD);
		$result = $this->db->delete('errorSuccessCodeDefs');
		return $result;
	}
}

/* End of file MsMachine_Model.php */
/* Location: ./application/models/MsMachine_Model.php */
