File Javascript setiap view disimpan di asset/js/pages
Nama Javascript harus sama persis dengan nama Controller
File Javascript berguna untuk meload data ke datatable, dan AJAX CRUD.
Format ShowData di Controller sudah sesuai dengan format yang diminta oleh DataTables.
File Header ada di application/views/layouts/HeaderView.php
File Footer ada di application/views/layouts/FooterView.php
File HeaderLogin ada di application/views/layouts/HeaderLoginView.php
File FooterLogin ada di application/views/layouts/FooterLoginView.php
fungsi $this->load->template dapat dilihat di application/core/MY_LOADER.php